'use strict';

class ComponentHelper {
    constructor() {

    }

    serializeLanguageToTbone(cnetLang){
        if (cnetLang === 'ee'){
            return 'en-GB'
        }
        return 'x-default'
    }
}

module.exports = ComponentHelper;