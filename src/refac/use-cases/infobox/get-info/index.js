'use strict';

const RepoInfobox = require('../../../../repository/repo.infobox');
class Index {
    async getInfo(type, nr) {
        let repoInfobox = new RepoInfobox();
        return repoInfobox.getInfo(type, nr);
    }
}

module.exports = Index;
