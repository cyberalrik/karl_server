'use strict';

const RepoInfobox = require('../../../../repository/repo.infobox');
class Index {
    async getAllInfo(data) {
        let article = [];
        let result = {};
        let productData = {infoText: ''};

        let repoInfobox = new RepoInfobox();
        let resultInfobox = await repoInfobox.getInfo(0, data.product['p_nr']);

        if (resultInfobox.successful && resultInfobox.infoText !== '') {
            productData.infoboxHasData = true;
            productData.infoText = resultInfobox.infoText;
        } else {
            productData.infoboxHasData = false;
        }
        result.product = productData;
        for (let i = 0; i < data.article.length; i++) {
            let articleNumber = data.article[i]['a_nr'];
            resultInfobox = await repoInfobox.getInfo(1, articleNumber);

            if (resultInfobox.successful && resultInfobox.infoText !== '') {
                article[articleNumber] = [];
                article[articleNumber]['infoboxHasData'] = true;
                article[articleNumber]['infoText'] = resultInfobox.infoText;
            } else {
                article[articleNumber] = false;
            }
        }

        result.article = article;

        return result;

    }
}

module.exports = Index;
