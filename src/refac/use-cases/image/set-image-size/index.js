'use strict'

const imgRepo = require('../../../../repository/repo.images');

class DeactivateImage{
    constructor(_db) {
        this.db = _db
    }

    async setImageSize(id, height, width) {
        await imgRepo.setImageSize(id, height, width);
    }
}

module.exports = DeactivateImage