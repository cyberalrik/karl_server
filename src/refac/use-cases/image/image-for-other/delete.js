'use strict'

const RepoProduct2productImages = require('../../../../repository/repo.product2productImages');
const RepoImages = require('../../../../repository/repo.images');

class Index {
    /** Löscht die Bildverlinkung zu einen anderen Produkt  */
    async deleteImageLink(p_nr, p_nr_image) {
        let result = {successful: true}
        let repoProduct2productImages = new RepoProduct2productImages();
        await repoProduct2productImages.deleteImagesConnectionByPNrAndPNrImage(p_nr, p_nr_image);

        /**
         * Besitzt das Produkt aber eigene Bilder?
         * Der Verweis dazu wurden von der Verlinkung zum anderen Produkt überschrieben und muss jetzt wieder gesetzt werden.
         * */

        if ((await RepoImages.hasProductImage(p_nr))) {
            await repoProduct2productImages.addImagesConnection(p_nr, p_nr);
        }

        return result;
    }
}

module.exports = Index