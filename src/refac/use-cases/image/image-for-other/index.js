'use strict'

const RepoProduct2productImages = require('../../../../repository/repo.product2productImages');
const logger = require('../../../../helper/util/logger.util');
let TAG = '';
let repoProduct2productImages = {};

class Index {
    constructor(tag, pNrLink) {
        this.pNrLink = pNrLink;
        TAG = tag;
        repoProduct2productImages = new RepoProduct2productImages();
    }

    /** Prüft, ob die übergebenen Produkte schon verlinkt sind */
    async checkImagesShare(currentLinkingPair) {
        /**
         * Gibt es die Verlinkung schön?
         *
         * Da das Array so aufgebaut ist, das es nur eine Richtung der Verlinkung aufnimmt
         * (um es nicht so groß werden zu lassen)
         * muss hier in beiden Richtungen geprüft werden.
         * */
        let newImageLinksCreated = await this.checkTheLinkIndividually(currentLinkingPair.parent, currentLinkingPair.child);
        newImageLinksCreated += await this.checkTheLinkIndividually(currentLinkingPair.child, currentLinkingPair.parent);

        return newImageLinksCreated;
    }

    async checkTheLinkIndividually(fromLink, toLink) {
        let newImageLinksCreated = 0
        if (this.pNrLink['p_nr-' + fromLink + '-p_nr_image-' + toLink] !== undefined) {
            /** Darf nicht gelöscht werden */
            this.pNrLink['p_nr-' + fromLink + '-p_nr_image-' + toLink].delete = false;

        } else {
            /** In pNrLink aufnehmen */
            this.pNrLink['p_nr-' + fromLink + '-p_nr_image-' + toLink] = {
                delete: false,
                pNr: fromLink,
                pNrImage: toLink
            };

            /** Verlinkung aufnehmen */
            await repoProduct2productImages.addImagesConnection(fromLink, toLink);

            newImageLinksCreated++;
        }

        return newImageLinksCreated;
    }

    async deleteNotUsedImageLink() {
        let number = 0;
        for (let index in this.pNrLink) {
            let currentLink =  this.pNrLink[index]
            let deleteLink = currentLink.delete;
            if (deleteLink) {
                number++;
                let p_nr = currentLink.pNr;
                let p_nr_image = currentLink.pNrImage;
                logger.log(TAG, `Verknüpfung gelöscht`, `Die Fotoverknüpfung von PNr ${p_nr} zu ${p_nr_image} wurde gelöscht.`)
                await repoProduct2productImages.deleteImagesConnection(p_nr, p_nr_image);
            }
        }

        return number;
    }
}

module.exports = Index