'use strict'

const constants = require('../../../../constants/images.constant');

class AddImage{
    async getImageVersion(url, lightVersion = false) {
        /** Festlegen der übergebenen Bilderversion -1 = unbekannt, 0 = cNet, 1 = Canto */
        let version = constants.IMAGES_UNKNOWN_TYPE_VALUE;
        switch (true) {
            case lightVersion:
            case url.indexOf('cnetcontent') !== -1:
            case url.indexOf('cdn.cs.1worldsync.com') !== -1:
                version = constants.IMAGES_CNET_TYPE_VALUE;
                break;
            case url.indexOf('cybertrading') !== -1:
                version = constants.IMAGES_CANTO_TYPE_VALUE;
                break;
        }

        return version;
    }
}

module.exports = AddImage