'use strict'

const imgRepo = require('../../../../repository/repo.images')
// const _ = require('underscore');

class GetLinks {
    // constructor(_db) {
    //     this.db = _db;
    // }

    async getAll() {
        return await imgRepo.getAll();
    }

    async getAllWhitSize() {
        return await imgRepo.getAllWhitSize();
    }

    async getImageMaxPosForPNr(p_nr) {
        return await imgRepo.getImageMaxPosForPNr(p_nr);
    }
}

module.exports = GetLinks