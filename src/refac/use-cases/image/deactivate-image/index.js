'use strict'

const imgRepo = require('../../../../repository/repo.images');

class DeactivateImage{
    constructor(_db) {
        this.db = _db
    }

    async deactivateByUrl(url){
        await imgRepo.deactivateImageByUrl(url);
    }

    async deactivateById(id){
        await imgRepo.deactivateImageById(id);
    }

}

module.exports = DeactivateImage