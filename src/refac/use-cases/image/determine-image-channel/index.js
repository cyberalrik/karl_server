'use strict'

const channelRepo = require('../../../../repository/repo.channel');
const constants = require('../../../../constants/images.constant');

class Index{
    async determineChannel(imageSize) {
        let channelList = await channelRepo.getChannels();
        let imageChannel = 0;
        for (let index in channelList) {
            let currentChannel = channelList[index];
            if (imageSize.height >= currentChannel.image_height
                && imageSize.width >= currentChannel.image_width
            ) {
                let imageValue = await constants.getChannelTypeValue(currentChannel.channel_key);
                imageChannel += imageValue;
            }
        }
        return imageChannel;
    }
}

module.exports = Index