'use strict'

const imgRepo = require('../../../../repository/repo.images')
const _ = require('underscore');

class GetLinks {
    constructor(_db) {
        this.db = _db;
    }

    async getAllActive() {
        let result = []
        let list = await imgRepo.getAllActive();
        for (let i = 0; i < list.length; i++){
            result.push({id: list[i].id, url: list[i].image_url})
        }
        return list
    }

    async getImagesPerMPN(mpn) {
        let result = [];
        let list  = await imgRepo.getImagesPerProductNumber(mpn);
        for (let i = 0; i < list.length; i++){
            result.push(list[i].image_url)
        }
        return result
    }
}

module.exports = GetLinks