'use strict'

const { Image } = require('image-js');
const fs = require('fs');
const axios = require('axios');

/**
 * Läd über eine URL das Bild und legt es lokal ab.
 * */
class Index {
    /**
     * Ist unter der URL kein Bild aufrufbar, wird successful auf false gesetzt und message enthält den Fehlercode.
     * Konnte das Bild geladen werden, wird die Höhe und Breite ermittelt und zurück gegeben.
     *
     * @param url
     * @return Promise {<{}>}
     * */
    async getImageSize(url) {
        let result = {
            successful: true,
            message: '',
            height: 0,
            width: 0
        };
        try {
            await download_image(url, __filename + 'todo.jpg');
            let image = await Image.load(__filename + 'todo.jpg');

            result.height = image.height;
            result.width = image.width;
        } catch (e) {
            result.message = `Es ist ein Fehler aufgetreten. \nBitte überprüfen sie den eingetragenen Bilder-Link. \nFehler: "${e.message}"`;
            result.successful = false;
        }

        return result;
    }
}

const download_image = async (url, image_path) =>
    axios({
        url,
        responseType: 'stream',
    }).then(
        response =>
            new Promise((resolve, reject) => {
                response.data
                    .pipe(fs.createWriteStream(image_path))
                    .on('finish', () => resolve())
                    .on('error', e => reject(e));
            }),
    );

module.exports = Index;