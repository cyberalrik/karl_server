'use strict'

const imgRepo = require('../../../../repository/repo.images');
const prodRepo = require('../../../../repository/repo.product');
const UseCaseGetImageSize = require('../get-image-size');
const UseCaseDetermineImageChannel = require('../determine-image-channel');
const UseCaseGetImageVersion = require('../get-image-version');
const logger = require('../../../../helper/util/logger.util');
const constants = require('../../../../constants/images.constant');

const TAG = 'AddImage'


class AddImage{
    constructor(_db) {
        this.db = _db
    }

    async addOwnImageToProduct(mpn, manufacturer, url, pos, isAktive = false, lightVersion = false) {
        let result = {
            successful: true,
            error: {}
        }
        if (mpn === undefined || mpn === null ||
            manufacturer === undefined || manufacturer === null ||
            url === undefined || url === null || url === '' ||
            pos === undefined || pos === null
        ){
            logger.err(TAG, 'API', `missing params`);
            result.successful = false;
            result.error.message = `API - missing params`;
            result.message = `Es wurden nicht alle Parameter übergeben.`;
            return result;
        }
        let deactivated = [];
        let product = await prodRepo.getProductsPerMpnAndManufacturer(mpn, manufacturer);
        let mailBodyXXL = `Das von Otto empfangende Bild konnte keinem Produkt zugewiesen werden.\n\nHier der Link zum Bild:\n${url}`;

        if (product === undefined || product === null) {
            logger.err(
                TAG
                , 'API'
                , `Aus ${mpn} und ${manufacturer} wurde keine Kombination gefunden`
                , mailBodyXXL
                , 's.wiehe@cybertrading.de'
            );

            /**  3 Sekunden warten, um den Mailserver nicht zu überlasten. */
            await new Promise(resolve => setTimeout(resolve, 3000));
            result.error.message = `Karl hat die Kombination ${mpn} und ${manufacturer} nicht gefunden`;
            result.successful = false;

            return result;
        }

        /** Festlegen der übergebenen Bilderversion 0 = cNet, 1 = Canto */
        let useCaseGetImageVersion = new UseCaseGetImageVersion();
        let version = await useCaseGetImageVersion.getImageVersion(url, lightVersion);
        let images = await imgRepo.getImagesPerProductNumber(product.p_nr);
        let isExistingVersion1 = false;
        let maxPos = 0;

        for (let i = 0; i < images.length; i++){
            if (images[i].image_url === url) {
                logger.log(TAG, 'API', `URL: ${url} für ${product.p_nr} schon vorhanden.`)
                return result;
            }
            /**
             * Wenn ein Canto Bild übergeben wird, sollen alle cNEt Bilder deaktiviert werden.
             * Images Version 0 = cNet
             * */
            if (
                version === constants.IMAGES_CANTO_TYPE_VALUE
                && images[i].is_active
                && images[i].version === constants.IMAGES_CNET_TYPE_VALUE
            ){
                deactivated.push(images[i])
            }

            if (images[i].version === constants.IMAGES_CANTO_TYPE_VALUE) {
                isExistingVersion1 = true;
            }
        }

        /**
         * Gibt es ein Version 1 Bild und das neue ist ein Version 0 Bild,
         * dann darf es nicht aktiviert werden.
         * */
        if (
            isExistingVersion1
            && version === constants.IMAGES_CNET_TYPE_VALUE
        ) {
            isAktive = false;
        }

        /**
         * Ein Bild der Version 0 soll immer hinten angefügt werden.
         * */
        if (version === constants.IMAGES_CNET_TYPE_VALUE ) {
            for (let i = 0; i < images.length; i++){
                if (maxPos < images[i].getDataValue('image_pos')) {
                    maxPos = images[i].getDataValue('image_pos');
                }
            }

            pos = maxPos + 1;
        }

        let useCaseGetImageSize = new UseCaseGetImageSize();
        let imageSize = await useCaseGetImageSize.getImageSize(url);

        /** Ist der Bilderlink nicht aktiv oder wurde keiner übergeben, wird hier abgebrochen. */
        if (!imageSize.successful) {
            result.successful = false;
            result.message = imageSize.message

            return result
        }

        let useCaseDetermineImageChannel = new UseCaseDetermineImageChannel();
        let channel = await useCaseDetermineImageChannel.determineChannel(imageSize);

        await imgRepo.addOrUpdateImage(
            product.p_nr,
            url,
            pos,
            await this.shouldItBeActive(pos, version, isAktive),
            version,
            channel,
            imageSize
        );

        logger.log(TAG, 'API', `URL: ${url} für ${product.p_nr} angelegt`)

        for (let i = 0; i < deactivated.length; i++){
            await imgRepo.deactivateImagePerPNR(deactivated[i].image_url, product.p_nr);
            logger.log(TAG, 'API-URL deaktiviert', `URL: ${deactivated[i].image_url} für ${product.p_nr} deaktiviert`)
        }

        return result;
    }

    async shouldItBeActive(pos, version, isAktive) {
        let result = false;
        if (isAktive) {
            return isAktive;
        }

        if (
            version === constants.IMAGES_CANTO_TYPE_VALUE
            && constants.IMAGES_CANTO_ACTIVE_POS.includes(pos)
        ) {
            result = true;
        }

        return result;
    }
}

module.exports = AddImage