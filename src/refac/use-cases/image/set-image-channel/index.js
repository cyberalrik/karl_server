'use strict'

const imgRepo = require('../../../../repository/repo.images');
const constants = require('../../../../constants/images.constant');
// const _ = require('underscore');

class Index {
    // constructor(_db) {
    //     this.db = _db
    // }

    /**
     * Setzt die Freigabe des übergebenen Kanals neu.
     * ID ist die Image.id
     *
     * @param id
     * @param channel
     * @param active
     * @return Promise <{object}
     * */
    async changesImageChannel(id, channel, active) {
        let result = {
            successful: true
        }
        let image = await imgRepo.getImagesById(id);

        if (image.length === 0) {
            result.successful = false;
            return result
        }

        let channelTypeValue = await constants.getChannelTypeValue(channel);
        let imageChannel = image.channel;
        let newImageChannelValue = 0;

        if (active) {
            if ((imageChannel & channelTypeValue) !== channelTypeValue) {
                newImageChannelValue = imageChannel + channelTypeValue;
            }
        } else {
            if ((imageChannel & channelTypeValue) === channelTypeValue) {
                newImageChannelValue = imageChannel - channelTypeValue;
            }
        }

        await imgRepo.setImageChannel(id, newImageChannelValue);

        return result;
    }

    async setChannelById(id, channel) {
        return await imgRepo.setChannelById(id, channel);
    }

}

module.exports = Index
