'use strict'

const DB = require('../../../../refac/data-access');
const dbConstants = require('../../../../refac/constants');
const UseCaseGetImageSize = require('../get-image-size');
const UseCaseDeactivateImage = require('../../../../refac/use-cases/image/deactivate-image');
const UseCaseSetImageSize = require('../../../../refac/use-cases/image/set-image-size');
const UseCaseGetAllImages = require('../../../../refac/use-cases/image/get-images');
const UseCaseSetImageChannel = require('../../../../refac/use-cases/image/set-image-channel');
const UseCaseDetermineImageChannel = require('../determine-image-channel');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'set-all-image-size';

class Index {
    /**
     * Alle in der Tabelle 'images' befindlichen Bilderlinks werden überprüft.
     * Ist ein Link "tot", wird das Bild deaktiviert.
     * Kann das Bild geladen werden, werden seine Höhe und Breite in die DB eingetragen.
     * */
    async setAllImageSize() {
        let db_karl = new DB(dbConstants.KARL);
        let useCaseDeactivateImage = new UseCaseDeactivateImage(db_karl);
        let useCaseGetImageSize = new UseCaseGetImageSize();
        let useCaseSetImageSize = new UseCaseSetImageSize(db_karl);
        let useCaseGetAllImages = new UseCaseGetAllImages(db_karl);

        logger.log(TAG, `Bilder sammeln`)
        let allImages = await useCaseGetAllImages.getAll();
        logger.log(TAG, `Bilder gesammelt`, `Insgesamt ${allImages.length} Bilder vorhanden.`)
        let index = 0;
        for (index in allImages) {
            let currentImage = allImages[index];
            let result = await useCaseGetImageSize.getImageSize(currentImage.image_url);
            if (result.successful) {
                await useCaseSetImageSize.setImageSize(currentImage.id, result.height, result.width);
            } else {
                await useCaseDeactivateImage.deactivateById(currentImage.id);
                logger.err(TAG, 'Bild wird deaktiviert', `${result.message} bei ${currentImage.image_url}`);
            }

            if (index % 500 === 0 && index > 0) {
                logger.log(TAG, '500 Bilder', `Schon ${index} Bilder abgeglichen. (${allImages.length})`);
            }
        }

        logger.log(TAG, 'Ende Bilder', `Insgesamt ${index} Bilder abgeglichen. (${allImages.length})`);
    }

    /**
     * Wenn alle Bilder geprüft wurden, kann der Channel bestimmt werden,
     * das wird hier gemacht.
     * */
    async setImageChannel() {
        // let db_karl = new DB(dbConstants.KARL);
        let useCaseSetImageChannel = new UseCaseSetImageChannel();
        let useCaseGetAllImages = new UseCaseGetAllImages();
        let useCaseDetermineImageChannel = new UseCaseDetermineImageChannel();

        logger.log(TAG, `Bilder sammeln`, 'Alle Bilder, bei denen eine Größe gespeichert wurde.')
        let allImagesWhitSize = await useCaseGetAllImages.getAllWhitSize();
        logger.log(TAG, `Bilder gesammelt`, `Insgesamt ${allImagesWhitSize.length} Bilder mit Größenangabe vorhanden.`)
        let index = 0;

        for (index in allImagesWhitSize) {
            let currentImage = allImagesWhitSize[index];
            let imageSize = {
                height: currentImage.height,
                width: currentImage.width
            }

            let channel = await useCaseDetermineImageChannel.determineChannel(imageSize);
            await useCaseSetImageChannel.setChannelById(currentImage.id, channel);

            if (index % 500 === 0 && index > 0) {
                logger.log(TAG, '500 Bilder', `Schon ${index} Bilder abgeglichen. (${allImagesWhitSize.length})`);
            }
        }

        logger.log(TAG, 'Ende Bilder', `Insgesamt ${index} Bilder abgeglichen. (${allImagesWhitSize.length})`);
    }
}

module.exports = Index;