'use strict'

const DB = require('../../../../refac/data-access');
const dbConstants = require('../../../../refac/constants');
const UseCaseDeactivateImage = require('../../../../refac/use-cases/image/deactivate-image');
const UseCaseGetImageSize = require('../get-image-size');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'isValid';

class IsValid {
    async imageCheck(url) {
        let db_karl = new DB(dbConstants.KARL);
        let useCaseDeactivateImage = new UseCaseDeactivateImage(db_karl);
        let useCaseGetImageSize = new UseCaseGetImageSize();

        let result = await useCaseGetImageSize.getImageSize(url);

        if (!result.successful) {
            await useCaseDeactivateImage.deactivateByUrl(url)
            logger.err(TAG, 'Bild wird deaktiviert', `${result.message} bei ${url}`);
        }

        return result.height >= 500 || result.width >= 500;
    }
}

module.exports = IsValid;