'use strict'


const Product2productImagesRepo = require("../../../../repository/repo.product2productImages");
const imageRepo = require("../../../../repository/repo.images");

/**
 * In Karl sind folgende Bilderregel eingestellt:
 *
 * - Es werden alle Bilder aller verlinkten Produkte kontrolliert.
 * - Besitzt das Originalprodukt CT (Canto) Bilder, so werden diese benutzt, ohne weiter zu kontrollieren.
 * - Besitzt ein anderes oder mehrere Produkte CT Bilder, so werden die Bilder des Produkts genutzt,
 *   dass das jüngste Bild besitzt.
 * - Sind keine CT Bilder vorhanden, werden cNet Bilder zur Kontrolle genommen.
 * - Es werden alle cNet Bilder der Größe nach sortiert und die Bilder des Produkts
 *   mit dem größten Bild werden verwendet.
 * - Gibt es mehrere Produkte, mit der gleich großen Bildgröße, so wird wieder das Bild mit dem jüngsten Datum
 *   zur Entscheidung herangezogen.
 * - Die Bilder, des ausgewählten Produkts, werden der Größe nach absteigend sortiert
 *   und in einer neuen Reihenfolge (Bildposition) an TB übermittelt.
 * - In Karl kann die Mindestgröße und die Anzahl der an TB übermittelten Bilder pro Channel eingestellt werden.
 * */


class Index {
    /**
     * Gibt alle relevanten Bilder zum Produkt zurück
     * Regel:
     * Hat die aktuelle p_nr genügend gute Bilder z.B.: 4 CTBilder, sollen diese und nur diese benutzt werden.
     * Hat die p_nr "nur" CNetBilder aber eine andere p_nr hat CT-Bilder dann sollen diese benutzt werden.
     * Haben verschiedene p_nr CT-Bilder, sollen die Bilder benutzt werden von der p_nr
     * die die jüngsten Bilder besitzt.
     *
     * @param p_nr
     * @return Promise <{[]}>
     * */
    async go(p_nr) {
        let product2productImagesRepo = new Product2productImagesRepo();
        let imagesResult = await product2productImagesRepo.getImagesConnection(p_nr);
        let images = [];
        if (imagesResult.successful) {
            let collectImages = await this.collectImages(p_nr, imagesResult.p_nr_imagesArray);
            images = await this.getCleanImages(collectImages);
        }

        return images
    }

    /**
     * Sammelt alle Bilder der übergebenen p_nr zusammen
     *
     * Die Daten müssen besser aufbereitet werden.
     * Es soll zu sehen sein, welches Erstellungsdatum die Bilder haben.
     * Welcher p_nr das Bild gehört.
     *
     * Danach soll entschieden werden, von welcher p_nr die Bilder benutzt werden sollen.
     * */
    async collectImages(masterP_nr, p_nr_imagesArray) {
        let images = [];
        let resultActiveImagesPerProductNumber = await imageRepo.getActiveImagesPerProductNumber(masterP_nr);

        let cleanVersionImages = await this.getVersionClean(resultActiveImagesPerProductNumber);

        /** Besitzt das Produkt schon selber die besten Bilder */
        if (
            cleanVersionImages.length > 0
            && await this.getVersion(cleanVersionImages) === 1
        ) {
            return cleanVersionImages;
        }

        let productImageCollection = [];

        /** Gibt es schon Bilder, werden die Daten besser aufbereitet */
        if (cleanVersionImages.length > 0) {
            productImageCollection.push(await this.evaluateImages(cleanVersionImages));
        }

        for (let index in p_nr_imagesArray) {
            if (masterP_nr === p_nr_imagesArray[index]) {
                continue;
            }

            resultActiveImagesPerProductNumber = await imageRepo.getActiveImagesPerProductNumber(p_nr_imagesArray[index]);
            cleanVersionImages = await this.getVersionClean(resultActiveImagesPerProductNumber);

            if (cleanVersionImages.length === 0) {
                continue;
            }

            productImageCollection.push(await this.evaluateImages(cleanVersionImages));

        }

        let resultEvaluateCollection = await this.evaluateCollection(productImageCollection);
        let bestImageCollection = await this.getBestImageCollection(resultEvaluateCollection);
        images = await this.sortImageBySize(bestImageCollection);

        return images;
    }

    /**
     * Hier werden die Bilder in einzelne Arrays aufgeteilt, bei denen der Key die Größe des Bildes ist.
     * Hinter jedem Größen-Key verbirgt sich ein Array in dem all Bilder der gleichen größe gesammelt werden.
     * Dadurch wird eine sortierung erreicht.
     * Danach wird das Array von hinten an iteriert und die Bilder in einem neuen Array gesammelt und deren PosNr neu vergeben.
     * */
    async sortImageBySize(images) {
        let sort = [];
        for (let index in images) {
            let currentImage = images[index];
            let height_X_Width = currentImage.getDataValue('height') * currentImage.getDataValue('width');

            if (sort[height_X_Width] === undefined) {
                sort[height_X_Width] = []
            }

            sort[height_X_Width].push(currentImage);
        }

        let result = [];
        let sortImageArray;
        let imagePos = 0;
        let indexArray = Object.keys(sort);

        for (let index = indexArray.length - 1; index >= 0; index--) {
            sortImageArray = sort[indexArray[index]];

            if (sortImageArray === undefined) {
                continue;
            }

            for (let sortImageArrayKey in sortImageArray) {
                let currentImage = sortImageArray[sortImageArrayKey];
                currentImage.setDataValue('image_pos', imagePos++);
                result.push(currentImage);
            }
        }

        return result
    }

    /**
     * Wenn es Bilder der Version 1 gibt, werden sie zurückgegeben
     *
     * @return Promise {<[]>}
     * */
    async getBestImageCollection(productImageCollection) {

        if (productImageCollection[1] !== undefined) {

            return productImageCollection[1].images
        } else if (productImageCollection[0] !== undefined) {

            return productImageCollection[0].images
        } else {
            return []
        }
    }

    /**
     * Alle Bilderserien kontrollieren und jeweils für jeden Typ nach den Regeln heraussuchen.
     * Version 0 = CNet
     *   wenn nicht eigene vorhanden, dann die vom Produkt mit den größten Bildern (höhe x breite)
     *   wenn mehrere Produkte mit den größten Bildern, dann das mit den jüngsten Bildern
     *
     * Version 1 = CT
     *   wenn nicht eigene vorhanden, dann die vom Produkt mit den jüngsten Bildern
     *
     * @param productImageCollection
     * @return Promise {<Object>}
     * */
    async evaluateCollection(productImageCollection) {

        let result = [];

        for (let index in productImageCollection) {
            let currentImagesCollection = productImageCollection[index];
            let imageVersion = currentImagesCollection.version;

            switch (imageVersion) {
                case 0 :
                    result[0] = await this.evaluateVersion0(currentImagesCollection, result[0]);
                    break;
                case 1 :
                    result[1] = await this.evaluateVersion1(currentImagesCollection, result[1]);
                    break;
            }
        }

        return result;
    }

    /**
     * Für diese Version werden die Bilderserien einer p_nr mit
     * 1. dem größten Bild und
     * 2. dem jüngsten 1. Bild
     * übernommen.
     * (2. nur, wenn es mehrere Produkte mit gleicher Bildgröße gibt.)
     * */
    async evaluateVersion0(currentImagesCollection, result) {
        if (result === undefined) {
            result = {
                date: currentImagesCollection.createDate,
                height_X_Width: currentImagesCollection.height_X_Width,
                successful: true,
                images: currentImagesCollection.images,
            }
        }

        if (
            result.height_X_Width < currentImagesCollection.height_X_Width
        ) {
            result.height_X_Width = currentImagesCollection.height_X_Width;
            result.images = currentImagesCollection.images;
        } else if (
            result.height_X_Width === currentImagesCollection.height_X_Width
            && result.date < currentImagesCollection.createDate
        ) {
            result.date = currentImagesCollection.createDate;
            result.images = currentImagesCollection.images;
        }

        return result;
    }

    /**
     * Für jede Version werden die Bilderserien einer p_nr mit dem jüngsten 1. Bild übernommen.
     * */
    async evaluateVersion1(currentImagesCollection, result) {
        if (
            (result === undefined || result.date === undefined)
            || result.date < currentImagesCollection.createDate
        ) {
            if (result === undefined) {
                result = {};
            }
            result.date = currentImagesCollection.createDate;
            result.successful = true;
            result.images = currentImagesCollection.images;
        }

        return result;
    }

    /**
     * Versionserkennung.
     * Erst wenn es 4 oder mehr sind, muss man davon ausgeben, dass es CT-Bilder (Version = 1) sind.
     * Dass kommt daher, dass Canto auch für einzelne Bilder benutzt wurde und hier kein CT-Foto benutzt wurde.
     * Um Bilder als CT-Fotos identifizieren zu können, sollten sie auch von Version 1 sein.
     *
     * Alles anderen sind von Version 0
     * */
    async getVersion(currentImagesCollection) {
        let result = 0;
        let firstImages = currentImagesCollection[0];
        if (
            currentImagesCollection.length >= 4
            && firstImages !== undefined
            && firstImages.version === 1
        ) {
            result = 1
        }

        return result;
    }

    /**
     * Es dürfte zwar nicht vorkommen, aber um Fehler vorzubeugen.
     * Hier werden die Bilder nach ihrer Version sortiert, sodass sie Sortenrein zurückgegeben werden.
     * Bevorzugt wird die Version 1 zurückgegeben.
     *
     * @return Promise {<array of Images>}
     * @param currentImagesCollection
     * */
    async getVersionClean(currentImagesCollection) {
        let result = [];
        let workResult = [];

        /** Hier wird sortiert. Bilder werden nach ihrem Typ aufgeteilt. */
        for (let index in currentImagesCollection) {
            let currentImage = currentImagesCollection[index];
            let currentVersion = currentImage.getDataValue('version');

            if (workResult[currentVersion] === undefined) {
                workResult[currentVersion] = [];
            }

            workResult[currentVersion].push(currentImage);
        }

        /** Version 1 wird bevorzugt zurückgegeben. */
        if (workResult[1] !== undefined) {
            result = workResult[1];
        } else if (workResult[0] !== undefined) {
            result = workResult[0];
        }

        return result;
    }

    /**
     * Hier werden alle Bilder eines Produkts übergeben.
     * Die Daten der Bilder sollen hier ausgewertet werden und das beste Bild ermittelt
     * und seine Daten zurückgegeben werden.
     *
     * - Bild mit der Version 1 werden bevorzugt.
     * - ermitteln des jüngsten Datums.
     *
     * - Gibt es nur Bilder der Version 0 dann
     * -- größtes Bild ermitteln
     *
     * @param images
     * @return Promise {<object>}
     * */
    async evaluateImages(images) {
        let currentImage = images[0];
        let result = {
            createDate: currentImage.getDataValue('createdAt'),
            height_X_Width: (currentImage.getDataValue('height') * currentImage.getDataValue('width')),
            version: await this.getVersion(images),
            images: images,
            number: images.length,
            p_nr: images[0].getDataValue('p_nr')
        }

        /** Das jüngste Datum und die maximale Größe soll enthalten sein */
        for (let index in images) {
            currentImage = images[index];

            if (result.createDate < currentImage.getDataValue('createdAt')) {
                result.createDate = currentImage.getDataValue('createdAt');
            }

            if (result.height_X_Width < (currentImage.getDataValue('height') * currentImage.getDataValue('width'))) {
                result.height_X_Width = (currentImage.getDataValue('height') * currentImage.getDataValue('width'));
            }
        }

        return result
    }

    /**
     * Hier werden Image mit gleicher URL ausgefiltert
     * */
    async getCleanImages(images) {
        let controlData = [];
        let resultImages = [];
        for (let index in images) {
            let currentImage = images[index];

            let controlUrl = currentImage.getDataValue('image_url');
            if (controlData[controlUrl] === undefined) {
                controlData[controlUrl] = true;
                resultImages.push(currentImage);
            }
        }

        return resultImages;
    }

}

module.exports = Index