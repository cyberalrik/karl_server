'use strict';

const DataAccessSys = require('../../../../repository/repo.sys');

class Index {
    async getAllProcesses(){
        let dataAccessSys = new DataAccessSys();
        let processes = await dataAccessSys.getAll();

        if (processes === undefined) {
            return [];
        }

        processes.forEach(process => {
            process.displayStatus = process.status ? 'wird ausgeführt' : 'ist beendet'
        })

        return processes;
    }
}

module.exports = Index;