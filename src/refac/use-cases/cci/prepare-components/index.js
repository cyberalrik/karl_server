'use strict';

const logger = require('../../../../helper/util/logger.util');

class Index{
    constructor(db) {
        this.db = db;
    }

    async prepareComponents(TAG){
        let result = {
            successful: true,
        };

        try {
            let access = this.db.getComponentsDataAccess();
            await access.prepareComponents(TAG);
            result.message = 'Komponenten Tabelle neu befüllt.';
        } catch (e) {
            logger.err(TAG, 'Datenbank-Fehler', e.message)
            result.successful = false;
            result.message = e.message;
        }

        return result;
    }
}

module.exports = Index;

