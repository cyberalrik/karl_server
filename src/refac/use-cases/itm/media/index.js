'use strict';

const logger = require('../../../../helper/util/logger.util');
const DB = require('../../../data-access');
const dbConstants = require('../../../constants');
const UseCaseItmMediaId = require('./getItmData');
const TAG = 'UseCaseItmMedia';

class Index{
    async getProductsFromMediaId(mediaData){
        let result = {
            successful: false,
        };

        let db = new DB(dbConstants.ITM);
        let useCaseItmMediaId = new UseCaseItmMediaId(TAG, db);

        try {
            result.itm = await useCaseItmMediaId.getProductsFromMediaId(mediaData);
            result.successful = true
        } catch (e) {
            logger.err(TAG, 'Datenbank-Fehler', e.message)
            result.successful = false;
            result.message = e.message;
        }

        return result;
    }
}

module.exports = Index;

