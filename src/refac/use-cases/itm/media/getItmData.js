'use strict';

const logger = require('../../../../helper/util/logger.util');

class Index{
    constructor(tag, db) {
        this.tag = tag;
        this.db = db;
    }

    async getProductsFromMediaId(mediaData){
        let result = {
            successful: true,
        };

        try {
            let access = this.db.getProductsDataAccess();
            result.data = await access.getProductsFromMediaId(mediaData);
            result.isData = result.data.length > 0;
        } catch (e) {
            logger.err(this.tag, 'Datenbank-Fehler', e.message)
            result.successful = false;
            result.message = e.message;
        }

        return result;
    }
}

module.exports = Index;

