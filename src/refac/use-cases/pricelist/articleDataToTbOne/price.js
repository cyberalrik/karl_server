'use strict';

const priceListRepo = require('../../../../repository/repo.pricelist');
const channelRepo = require('../../../../repository/repo.channel');
const ServiceTbOnePriceTemplate = require('../../../../services/service.tbone.price.template');
const ServiceIsPriceUpdateAllowed = require ('../../../../services/service.is.price.update.allowed');
const tbOneHelper  = require('../../../../helper/tbone.helper');
const upload = require('../../../../services/service.sft.tbone');
const config = require('../../../../configs/tbone.config');
const logger = require('../../../../helper/util/logger.util');
let TAG = 'articlePriceToTbOne';

class Index {
    constructor(tag) {
        TAG = tag;
    }

    /**
     * Übergibt eine XML-Datei mit den Preisen der übergebenen Artikel mit den übergebenen Channels an TbOne.
     * */
    async sendArticlePriceToTbOne(a_nr, channel){
        let channels = [];

        if (channel === '') {
            let channelList = await channelRepo.getChannels();
            channelList.forEach(channel => {
                channels.push(channel.channel_key);
            })
        } else {
            channels.push(channel);
        }

        let priceUpdate = await priceListRepo.getPriceListPerANrAndChannel(a_nr, channels);

        let serviceIsPriceUpdateAllowed = new ServiceIsPriceUpdateAllowed();
        priceUpdate = await serviceIsPriceUpdateAllowed.getAllApproved(priceUpdate);

        if (priceUpdate.length === 0) {
            return {successful: true};
        }

        let serviceTbOnePriceTemplate = new ServiceTbOnePriceTemplate();

        let list = await serviceTbOnePriceTemplate.createExportTemplate(
            priceUpdate,
            tbOneHelper.calcExportType(config.EXPORT_DELTA_ID)
        );

        logger.log(TAG,
            `TbOne Preisupdate`,
            `Lade für den Artikel ${a_nr} die Exportdatei mit ${channels.join()} Preisen zu TbOne hoch.`
        );

        let result = await upload.sendToTbOne(list, config.EXPORT_PRICE_ID, TAG);

        if (result.successful) {
            logger.log(TAG,
                `TbOne Preisupdate versandt`,
                `Exportdatei ${result.xmlFile} an TbOne übertragen.`
            );
        } else {
            logger.log(TAG,
                `TbOne Preisupdate`,
                result.message
            );
        }

        return result;
    }
}

module.exports = Index
