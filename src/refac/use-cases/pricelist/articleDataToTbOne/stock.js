'use strict';

const priceListRepo = require('../../../../repository/repo.pricelist');
const channelRepo = require('../../../../repository/repo.channel');
const ServiceTbOneStockTemplate = require('../../../../services/service.tbone.stock.template');
const upload = require('../../../../services/service.sft.tbone');
const config = require('../../../../configs/tbone.config');

const logger = require('../../../../helper/util/logger.util');
let TAG = 'articleStockToTbOne';

class Index {

    constructor(tag) {
        TAG = tag;
    }

    /**
     * Übergibt eine XML-Datei mit den Mengen des übergebenen Artikel mit den übergebenen Channels an TbOne.
     * */
    async sendArticleStockToTbOne(a_nr, channel){
        let channels = [];

        if (channel === '') {
            let channelList = await channelRepo.getChannels();
            channelList.forEach(channel => {
                channels.push(channel.channel_key);
            })
        } else {
            channels.push(channel);
        }

        let stockUpdate = await priceListRepo.getPriceListPerANrAndChannel(a_nr, channels);
        let serviceTbOneStockTemplate = new ServiceTbOneStockTemplate();
        let list = await serviceTbOneStockTemplate.calcExportTemplate(stockUpdate);

        logger.log(TAG,
            `TbOne Mengenupdate`,
            `Lade für den Artikel ${a_nr} die Exportdatei mit ${channels.join()} Mengen zu TbOne hoch.`
        );

        let result = await upload.sendToTbOne(list, config.EXPORT_STOCK_ID, TAG);

        if (result.successful) {
            logger.log(TAG,
                `TbOne Mengenupdate versandt`,
                `Exportdatei ${result.xmlFile} an TbOne übertragen.`
            );
        } else {
            logger.log(TAG,
                `TbOne Mengenupdate`,
                result.message
            );
        }

        return result;
    }
}

module.exports = Index
