'use strict';

const artRepo = require('../../../../repository/repo.pricelist');
const ServiceDateFormat = require('../../../../services/service.date.format');
const path = require('path');
const config = require('../../../../configs/app.config').getConfig();
const logger = require('../../../../helper/util/logger.util');
const fs = require("fs");

class AddPriceListItem {
    constructor(db) {
        this.db = db;
    }

    async addPriceListItems(TAG, itemsKarlPrepare, items) {
        let index = 0;
        let adjustedNumber = 0;
        // let changePriseList = [];
        for(index; index < items.length; index++) {

            if (index % 10000 === 0 && index > 0) {
                logger.log(TAG, 'Preis/ Mengen angepasst', `${index} kontrolliert und ${adjustedNumber} Preis/ Mengen korrigiert. (${items.length})`);
            }

            let currentLynnItem = items[index];

            let karlPrice = '';
            let karlQuant = '';
            let lynnPrice = '';
            let lynnQuant = '';
            let isOldArticle = false;

            /** Ist es ein Bestandsartikel? */
            if (itemsKarlPrepare[`${currentLynnItem.a_nr}_${currentLynnItem.channel}`] !== undefined) {
                if (isNaN(itemsKarlPrepare[`${currentLynnItem.a_nr}_${currentLynnItem.channel}`].price)) {
                    karlPrice = itemsKarlPrepare[`${currentLynnItem.a_nr}_${currentLynnItem.channel}`].price;
                } else {
                    karlPrice = itemsKarlPrepare[`${currentLynnItem.a_nr}_${currentLynnItem.channel}`].price.toString();
                }

                if (isNaN(itemsKarlPrepare[`${currentLynnItem.a_nr}_${currentLynnItem.channel}`].quant)) {
                    karlQuant = itemsKarlPrepare[`${currentLynnItem.a_nr}_${currentLynnItem.channel}`].quant;
                } else {
                    karlQuant = itemsKarlPrepare[`${currentLynnItem.a_nr}_${currentLynnItem.channel}`].quant.toString();
                }

                isOldArticle = true;
            }

            /** Vor dem Vergleich auf einen Variablen Typ  */
            if (isNaN(currentLynnItem.price)) {
                lynnPrice = currentLynnItem.price;
            } else {
                lynnPrice = currentLynnItem.price.toString();
            }

            if (isNaN(currentLynnItem.quant)) {
                lynnQuant = currentLynnItem.quant;
            } else {
                lynnQuant = currentLynnItem.quant.toString();
            }

            /**
             * Prüfen nur bei Bestandsartikel.
             * Die Preise dürfen nur mit "==" verglichen werden, da es einmal ein String (Karl) und einmal ein Float (Lynn) ist.
             * */
            if (
                isOldArticle
                && karlPrice === lynnPrice
                && karlQuant === lynnQuant
            ) {
                continue;
            }

            adjustedNumber++;
            await artRepo.addPriceListItem(items[index])
            // changePriseList.push({
            //     a_nr: currentLynnItem.a_nr,
            //     channel: currentLynnItem.channel,
            //     karl: {
            //         price: karlPrice,
            //         quant: karlQuant
            //     },
            //     lynn:{
            //         price: currentLynnItem.price,
            //         quant: currentLynnItem.quant
            //     }
            // });
        }

        logger.log(TAG, 'Preis/ Mengen angepasst', `${index} kontrolliert und ${adjustedNumber} Preis/ Mengen korrigiert - abgeschlossen. (${items.length})`);

        // if (changePriseList.length > 0) {
        //     await this.writeChanges(changePriseList);
        //     logger.log(TAG, 'Preis/ Mengen angepasst', `Datei mit ${changePriseList.length} Preisänderungen geschrieben.`);
        // }

        return {
            successful: true,
            messages: `${index} kontrolliert und ${adjustedNumber} Preis/ Mengen korrigiert - abgeschlossen. (${items.length})`
        };
    }

    /** Hier werden die Preisänderungen in einer csv-Datei dokumentiert. */
    async writeChanges(changePriseList) {
        let filePath = config.REPORT_PATH;
        // let fileName = 'D:\\Daten\\karl\\changePriseList.csv';
        // let fileName = '/services/report/changePriseList.csv';
        // let fileName = '/Users/Andy/Cybertrading/changePriseList.csv';
        // filePath = '/Users/Andy/Cybertrading';

        let serviceDateFormat = new ServiceDateFormat();
        let datePath = await serviceDateFormat.getStringDateWithTime();
        let fileName = path.join(filePath, datePath + '_changePriseList.csv');

        try {
            if (fs.existsSync(fileName)) {
                fs.unlinkSync(fileName);
            }
            let writeStream = fs.createWriteStream(fileName, {
                flags: 'a' // 'a' means appending (old data will be preserved)
            })

            writeStream.write('Artikel Nr.;Kanal;Karl Preis;Lynn Preis;Karl Menge;Lynn Menge;' + "\n");

            for (let index in changePriseList) {
                let currentElementValue = changePriseList[index];
                writeStream.write( `"${
                    currentElementValue.a_nr}";"${
                    currentElementValue.channel}";"${
                    currentElementValue.karl.price}";"${
                    currentElementValue.lynn.price}";"${
                    currentElementValue.karl.quant}";"${
                    currentElementValue.karl.quant}";\n`);
            }
            writeStream.end();
        } catch (e) {
            // let message = e.message;
        }
    }
}

module.exports = AddPriceListItem
