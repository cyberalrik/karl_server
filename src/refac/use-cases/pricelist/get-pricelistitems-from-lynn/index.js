'use strict';

const logger = require('../../../../helper/util/logger.util');
class GetPriceListItemsFromLynn {
    constructor(db) {
        this.db = db;
    }

    async getAllPriceListItems(TAG, delta, p_nr){
        let result = []
        let access = this.db.getPriceListDataAccess();

        let ebay =  await access.getAllPriceListItems_old('ebde', delta, p_nr);
        result = result.concat(ebay);
        logger.log(TAG, 'eBay.de Preis/ Mengen', `Es wurden ${ebay.length} Preis/ Mengen für eBay.de ermittelt.` )

        // let ebuk =  await access.getAllPriceListItems_old('ebuk', delta, p_nr);
        // result = result.concat(ebuk);
        // logger.log(TAG, 'eBay.uk Preis/ Mengen', `Es wurden ${ebuk.length} Preis/ Mengen für eBay.uk ermittelt.` )

        let cucybertr1 =  await access.getAllPriceListItems_old('cucybertr1', delta, p_nr);
        result = result.concat(cucybertr1);
        logger.log(TAG, 'ITM Preis/ Mengen', `Es wurden ${cucybertr1.length} Preis/ Mengen für ITM ermittelt.` )

        // let amde =  await access.getAllPriceListItems_old('amde', delta, p_nr);
        // result = result.concat(amde);
        // logger.log(TAG, 'AMZ Preis/ Mengen', `Es wurden ${amde.length} Preis/ Mengen für AMZ ermittelt.` )

        return result;
    }

}

module.exports = GetPriceListItemsFromLynn
