'use strict';

const config = require("../../../configs/tbone.config");
const upload = require("../../../services/service.sft.tbone");
const logger = require("../../../helper/util/logger.util");
const ServiceIsPriceUpdateAllowed = require("../../../services/service.is.price.update.allowed");
const ServiceTbOnePriceTemplate = require("../../../services/service.tbone.price.template");
const tbOneHelper = require("../../../helper/tbone.helper");


class Index {

    /**
     * @param TAG
     * @param result
     * @param priceUpdate
     * @return Promise {<>}
     * */
    async workUpload(TAG, result, priceUpdate){
        let serviceIsPriceUpdateAllowed = new ServiceIsPriceUpdateAllowed();
        priceUpdate = await serviceIsPriceUpdateAllowed.getAllApproved(priceUpdate);

        let message= '';
        if (priceUpdate.length > 0) {
            message = `Es werden ${priceUpdate.length} Preisänderungen an TB geschickt.`;
            await logger.log(TAG, `TbOne Preisupdate`, message);
            result.messages.push(message);

            await logger.log(TAG, `TbOne Preisupdate`, `Erstelle Template`);

            let serviceTbOnePriceTemplate = new ServiceTbOnePriceTemplate();

            let list = await serviceTbOnePriceTemplate.createExportTemplate(priceUpdate, tbOneHelper.calcExportType(config.EXPORT_DELTA_ID));
            await logger.log(TAG, `TbOne Preisupdate`, `Lade Datei zu TB.One hoch.`);
            let resultOfSendToTbOne = await upload.sendToTbOne(list, config.EXPORT_PRODUCT_ID, TAG);

            if (resultOfSendToTbOne.successful) {
                message = `Exportdatei ${resultOfSendToTbOne.xmlFile} an TB.One übertragen.`;
                await logger.log(TAG, `TB.One Preisupdate`, message );
            } else {
                message =  resultOfSendToTbOne.message;
                await logger.log(TAG, `TB.One Preisupdate`, message);
            }
            result.messages.push(message);
        }

        return result;
    }
}

module.exports = Index;