'use strict';

const config = require("../../../configs/tbone.config");
const tboneHelper = require("../../../helper/tbone.helper");
const upload = require("../../../services/service.sft.tbone");
const logger = require("../../../helper/util/logger.util");
const ServiceBuildProductTemplate = require('../../../services/service.build.product.template');


class Index {

    /**
     * @param TAG
     * @param items
     * @param params
     * @return Promise {<>}
     * */
    async DoUpload(TAG, items, params){
        if (items.length < 1){
            return;
        }

        let begin = 0;
        let steps = config.EXPORT_ARTICLE_STEPS;
        let exportDone = false;
        let serviceBuildProductTemplate = new ServiceBuildProductTemplate(params);

        while (!exportDone){
            if (begin + steps > items.length){
                exportDone = true
            }

            let workingList =  await items.slice(begin, begin + steps);
            let list = await serviceBuildProductTemplate.calcExportTemplate(
                TAG
                , workingList
                , tboneHelper.calcExportType(config.EXPORT_FULL_ID)
            );

            let result = await upload.sendToTbOne(list, config.EXPORT_PRODUCT_ID, TAG);

            if (result.successful) {
                logger.log(TAG,
                    `Tb.One upload`,
                    `Exportdatei ${result.xmlFile} mit ${workingList.length} Produkte an TbOne übertragen.`
                );
            } else {
                logger.log(TAG,
                    `TbOne Preisupdate`,
                    result.message
                );
            }

            begin = begin + steps;
        }
    }
}

module.exports = Index;