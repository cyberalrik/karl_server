'use strict';

const config = require("../../../configs/tbone.config");
const upload = require("../../../services/service.sft.tbone");
const logger = require("../../../helper/util/logger.util");
const ServicePrepareArticleNoUpdate = require("../../../services/service.prepare.article.no.update");
const ServiceTbOneStockTemplate = require("../../../services/service.tbone.stock.template");


class Index {

    /**
     * @param TAG
     * @param result
     * @param stockUpdate
     * @return Promise {<>}
     * */
    async workUpload(TAG, result, stockUpdate){
        let servicePrepareArticleNoUpdate = new ServicePrepareArticleNoUpdate();
        let articleNoUpdateData = await servicePrepareArticleNoUpdate.getPrepareArticleNoUpdate();
        let message= '';
        let stockUpdateData = [];

        for (let i = 0; i < stockUpdate.length;i++){
            if (articleNoUpdateData['a_nr-' + stockUpdate[i]['a_nr']] === undefined) {
                stockUpdateData.push(stockUpdate[i]);
            }
        }

        if (stockUpdateData.length > 0) {
            message = `Es werden von ${stockUpdateData.length} Artikel die Stückzahlen geändert.`;
            await logger.log(TAG, `TbOne Mengenupdate`, message);
            result.messages.push(message);

            await logger.log(TAG, `TbOne Mengenupdate`, `Erstelle Template`);

            let serviceTbOneStockTemplate = new ServiceTbOneStockTemplate();
            let list = await serviceTbOneStockTemplate.calcExportTemplate(stockUpdateData);
            let resultOfSendToTbOne = await upload.sendToTbOne(list, config.EXPORT_STOCK_ID, TAG);
            if (resultOfSendToTbOne.successful) {
                message = `Exportdatei ${resultOfSendToTbOne.xmlFile} an TbOne übertragen.`;
                await logger.log(TAG, `TbOne Mengenupdate`, message);
            } else {
                message = resultOfSendToTbOne.message;
                await logger.log(TAG,
                    `TbOne Mengenupdate`,
                    message
                );
            }
            result.messages.push(message);
        }

        return result
    }
}

module.exports = Index;