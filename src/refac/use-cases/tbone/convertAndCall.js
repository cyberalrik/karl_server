'use strict';

const UseCaseBuildDataForXML = require("./buildDataForXml");
const DataAccessForJoin = require("../../data-access/karl/articleProduct");

class Index {

    /**
     * Hier werden die übergebenen Daten (assoziatives Array) in ein String umgewandelt
     * und an die ausführende Funktion übergeben.
     *
     * @param TAG
     * @param articlesChange
     * @return Promise {<>}
     * */
    async prepareAndPassOn(TAG, articlesChange = []){
        /***/
        if (articlesChange !== undefined) {
            let useCaseBuildDataForXML = new UseCaseBuildDataForXML();
            let dataAccessForJoin = new DataAccessForJoin();
            //let articleNumbers = articleNumbers;
            let numbers = 0;
            let collection = [];
            for (let index in articlesChange) {
                let articleChange = articlesChange[index]
                collection.push(articleChange);
                numbers++;
                if (numbers >= 50) {
                    let resultCollection = '\'' + collection.join('\', \'') + '\'';
                    let products = await dataAccessForJoin.getAllProductOfArticleList(resultCollection);

                    await useCaseBuildDataForXML.buildDataForXml(TAG, products);
                    numbers = 0;
                    collection = [];
                }
            }

            if (numbers > 0) {
                let resultCollection = '\'' + collection.join('\', \'') + '\'';
                let products = await dataAccessForJoin.getAllProductOfArticleList(resultCollection);
                await useCaseBuildDataForXML.buildDataForXml(TAG, products);
            }
        }
    }
}

module.exports = Index;