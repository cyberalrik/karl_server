'use strict';

const logger = require("../../../helper/util/logger.util");
const UseCaseTbOneSendXML = require("./sendXML");
const ServicePrepareArticleNoUpdate = require("../../../services/service.prepare.article.no.update");
const componentRepo = require("../../../repository/repo.components");
const articleRepo = require("../../../repository/article/repo.article");
const RepoCNetCategory2LynnAndShopCategory = require("../../../repository/repo.cNetCategory2LynnAndShopCategory");
const UseCaseGetImagesForProduct = require("../image/get-Images-for-Product");

class Index {

    /**
     * @param TAG
     * @param products
     * @return Promise {<>}
     * */
    async buildDataForXml(TAG, products){
        let result = {
            successful:true,
            messages: []
        }

        let productsNumber = products.length;

        if (productsNumber < 1){
            let message = `Es gab keine Produkte zum Verarbeiten.`;
            logger.log(TAG, 'Job closed', message);
            result.messages.push(message);

            return result;
        }

        let message = `Es werden ${productsNumber} Produkte verarbeitet.`;
        logger.log(TAG, 'Start Verarbeitung', message);
        result.messages.push(message);

        let numberOfProductsProcessed = 0;
        let useCaseTbOneSendXML = new UseCaseTbOneSendXML();
        let servicePrepareArticleNoUpdate = new ServicePrepareArticleNoUpdate();
        let articleNoUpdateData = await servicePrepareArticleNoUpdate.getPrepareArticleNoUpdate();

        let repoCNetCategory2LynnAndShopCategory = new RepoCNetCategory2LynnAndShopCategory();
        logger.log(TAG, 'p_cat_id ermitteln', 'Zum erkennen von Lizenzen müssen die Lizenz-p_cat_id ermittelt werden.');
        let eDelivery = await repoCNetCategory2LynnAndShopCategory.getAllShopCategoriesFromLynnCategory(
            ['Software/ Lizenz', 'Service/ Support']
        );
        logger.log(TAG, '', `Es wurden ${eDelivery.length} Lizenz-p_cat_id ermittelt.`);
        let prepareEDelivery = await this.dataPrepare(eDelivery);
        let params = {p_cat_id : prepareEDelivery};

        let i = 0;
        try {
            let items = [];
            let messages = [];

            for (; i < productsNumber; i++) {
                if (products[i].p_export_block)
                    continue;

                let article = await this.getArticlePerID(TAG, products[i].p_nr, articleNoUpdateData);

                if (article.length < 1) {
                    logger.err(TAG, 'keine Artikeldaten', `Zu dem Produkt gibt es keine Artikeldaten ${products[i].p_nr}`);

                    continue;
                }

                let comps = await componentRepo.getAllComponentPerID(products[i].p_nr);

                let item = {
                    p_nr: products[i].p_nr,
                    p_name: products[i].p_name,
                    p_text_de: products[i].p_text_de,
                    p_text_ee: products[i].p_text_ee,
                    p_brand: products[i].p_brand,
                    p_brand_key: products[i].p_brand_key,
                    p_cat_id: products[i].p_cat_id,
                    p_keyword1: products[i].p_keyword1,
                    p_keyword2: products[i].p_keyword2,
                    p_keyword3: products[i].p_keyword3,
                    p_3_rd: products[i].p_3_rd,
                    p_weight: products[i].p_weight,
                    articles: (article.length > 0) ? article : [],
                    components: (comps.length > 0) ? comps : [],
                };

                /**
                 * Ist das Produkt ein 3rd, müssen folgende Felder aufbereitet werden:
                 * p_brand_key
                 * p_text_de
                 * p_text_ee
                 * */
                if (products[i].p_3_rd) {
                    item.p_brand_key = item.p_brand_key + '_3rd';
                    if (!item.p_brand.includes('3rd'))
                        item.p_brand = item.p_brand + ' 3rd';
                    // if (!item.p_text_de.includes(', kompatibel'))
                    // 	item.p_text_de = item.p_text_de + ', kompatibel';
                    // if (!item.p_text_ee.includes(', compatible'))
                    // 	item.p_text_ee = item.p_text_ee + ', compatible';
                }

                let isValidResult = await this.isValid(TAG, item);
                if (isValidResult.successful) {
                    items.push(item);
                } else {
                    let missingData = isValidResult.message.join(', ');
                    messages.push(`Das Produkt ${item.p_name} wurde nicht an TbOne übertragen. `
                        + ` Es fehlen folgende Informationen "${missingData}"`)
                }

                numberOfProductsProcessed++;

                if (i % 500 === 0 && i > 0) {
                    logger.log(TAG, 'Tb.all', `${i} verarbeitet von ${productsNumber}`);
                    await useCaseTbOneSendXML.DoUpload(TAG, items, params);
                    items = [];
                }
            }

            if (messages.length > 0) {
                logger.err(
                    TAG
                    , 'Fehler bei Übertragung'
                    , `Bei der Übertragung an TradeByte ist ein Fehler aufgetreten.\nHier eine Übersicht:`
                    , messages.join(`\n`)
                );
            }

            /** Restliche Artikel noch übertragen */
            if (items.length > 0) {
                await useCaseTbOneSendXML.DoUpload(TAG, items, params);
            }

        } catch (e) {
            logger.err(TAG, products[i].p_name, e.message);
            logger.log(TAG,`Abgebrochen`, `Fehler beim Produkt ${products[i].p_name}`);
        }

        message = `Es wurden ${numberOfProductsProcessed} Produkte von ${productsNumber} verarbeitet.`;
        logger.log(TAG, 'Job closed', message);
        result.messages.push(message);

        return result
    }

    /**
     *
     * @param TAG
     * @param id
     * @param articleNoUpdateData
     * @returns {Promise<Array>}
     */
    async getArticlePerID(TAG, id, articleNoUpdateData){
        let result = [];
        let article = await articleRepo.getAllArticlePerID(id);
        for (let i = 0; i < article.length; i++) {
            if (articleNoUpdateData['a_nr-' + article[i]['a_nr']] !== undefined){
                continue;
            }

            let a_cond = article[i].a_cond;

            /** nur new und ref aber auch E2-new und E2-ref Artikel */
            if (!
                (
                    a_cond.toLowerCase() === 'new'
                    || a_cond.toLowerCase() === 'ref'
                    || a_cond.toLowerCase() === 'e2_new'
                    || a_cond.toLowerCase() === 'e2_ref'
                )
            ) {
                continue;
            }

            let item = {
                a_nr: article[i].a_nr,
                a_nr2: article[i].a_nr2,
                p_nr: article[i].p_nr,
                a_ean: article[i].a_ean,
                a_prod_nr: article[i].a_prod_nr,
                // a_uvp: article[i].a_uvp,
                // a_price: article[i].a_price,
                // a_stock_marketplace: article[i].a_stock_marketplace,
                a_cond: article[i].a_cond,
                a_delivery_time: article[i].a_delivery_time,
//				a_media: images.length > 0 ? images : []
            };
            result.push(item);
        }

        /**
         * Da die Bilder unter dem Produkt abgelegt sind und dann für alle Artikel des Produkts gleich sind,
         * wird die Bildsuche hier zentral ausgewertet und zugeordnet.
         * */

        let useCaseGetImagesForProduct = new UseCaseGetImagesForProduct();
        let images = await useCaseGetImagesForProduct.go(id);

        for (let index in result) {
            result[index].a_media = images;
        }

        return result;
    }


    /**
     * @param TAG
     * @param item
     * @returns {{}}
     */
    async isValid(TAG, item) {
        let result = {
            successful: true,
            message: [],
            messageLog: '',
            messageMail: ''
        };

        switch (true) {
            case (item.p_nr === undefined || item.p_nr === null):
                result.successful = false;
                result.message.push('p_nr');
                break;
            case (item.p_name === undefined || item.p_name === null):
                result.successful = false;
                result.message.push('p_name');
                break;
            /**
             * Wird in "C:\workspace\WebstormProjects\karl\src\services\service.build.product.template.js" abgefangen.
             * */
            // case (item.p_text_de === undefined || item.p_text_de === null):
            // 	result.successful = false;
            // 	result.message.push('p_text_de');
            // 	break;
            case (item.p_text_ee === undefined || item.p_text_ee === null):
                result.successful = false;
                result.message.push('p_text_ee');
                break;
            case (item.p_brand === undefined || item.p_brand === null):
                result.successful = false;
                result.message.push('p_brand');
                break;
            case (item.p_brand_key === undefined || item.p_brand_key === null):
                result.successful = false;
                result.message.push('p_brand_key');
                break;
            case(item.p_cat_id === undefined || item.p_cat_id === null || item.p_cat_id.length < 1):
                result.successful = false;
                result.message.push('p_cat_id');
                break;
            case (item.articles === undefined || item.articles === null || item.articles.length < 1):
                result.successful = false;
                result.message.push('articles');
                break;
        }

        /** Um die Frage zu umgehen, ob Artikel vorhanden sind */
        if (!result.successful) {
            return result;
        }

        if (!(item.articles === null || item.articles === undefined)) {
            for (let i = 0; i < item.articles.length; i++) {
                if (item.articles[i].a_ean === null ||
                    item.articles[i].a_ean === undefined ||
                    item.articles[i].a_ean === 0 ||
                    item.articles[i].a_ean === '0' ||
                    item.articles[i].a_ean.length > 13
                ) {
                    result.successful = false;
                    result.message.push('a_ean');
                }
            }
        }

        /** Testen, ob der Ref-Code zum Produkt passt */
        let newItemArticles = [];
        for (let i = 0; i < item.articles.length; i++){
            let currentItem = item.articles[i];
            if (
                !(
                    //testem
                    currentItem.a_nr2 !== undefined
                    && currentItem.a_nr2 !== null
                    && currentItem.a_nr2.toLowerCase().indexOf(item.p_name.toLowerCase()) !== -1
                )
            ){
                result.message.push('Ref-Code');
                result.messageLog = `Der Ref-Code "${currentItem.a_nr2}" des Artikels "${currentItem.a_nr}" `;
                result.messageLog += `passt nicht zu seiner MPN "${item.p_name}" und wird aussortiert.\n`;
                logger.err(TAG
                    , item.p_name
                    , result.messageLog
                    , `Hallo,\n\nhier ist etwas schief gelaufen.`
                    , 'produktmanagement@cybertrading.de,s.wiehe@cybertrading.de'
                );
                result.messageMail += result.messageLog

                /**  3 Sekunden warten, um den Mailserver nicht zu überlasten. */
                await new Promise(resolve => setTimeout(resolve, 3000));
            } else {
                newItemArticles.push(currentItem);
            }
        }

        item.articles = newItemArticles;

        if (newItemArticles.length === 0) {
            result.successful = false;
        }

        /** Hiermit werden doppelte Einträge entfernt */
        result.message = [...new Set(result.message)]

        return result;
    }

    async dataPrepare(eDeliveries) {
        let result = [];
        for (let eDelivery of eDeliveries) {
            result.push(eDelivery.p_cat_id);
        }

        return result;
    }
}

module.exports = Index;