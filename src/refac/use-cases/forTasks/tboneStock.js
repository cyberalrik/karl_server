'use strict';

const pliRepo = require('../../../repository/repo.pricelist');
const UseCaseTbOneSendStock = require('../tbone/sendStock');
const logger = require('../../../helper/util/logger.util');

let TAG = ''
class Index {
    constructor(tag) {
        TAG = tag;
    }

    async call(delta = true) {
        let result = {
            successful: true,
            messages: []
        };
        let date = new Date();
        date.setMinutes( date.getMinutes() - 120);
        let stockUpdate = await pliRepo.getModifiedPriceListItem(date, delta)


        let message = `Von ${stockUpdate.length} Artikel wurde die Stückzahl in Lynn geändert.`;
        await logger.log(TAG,`TbOne Mengenupdate`, message);
        result.messages.push(message);

        let useCaseTbOneSendStock = new UseCaseTbOneSendStock();
        result = await useCaseTbOneSendStock.workUpload(TAG, result, stockUpdate);

        return result
    }
}

module.exports = Index;
