'use strict';

const DB = require('../../data-access');

const logger = require('../../../helper/util/logger.util');
const dbConstants = require('../../constants');

const UseCaseGetProductsFromLynn = require('../products/get-products-from-lynn');
const UseCaseAddProductsIntoKarl = require('../products/add-products');
const UseCaseAddComponentsToKarl = require('../components/add-components');
const FisherYates = require('../../helper/algorithm/fisheryates');
const productRepo = require("../../../repository/repo.product");
const articleRepo = require("../../../repository/article/repo.article");

let TAG = '';

class Index {
	constructor(tag) {
		TAG = tag;
	}

	async call(p_nr = null, date = null) {
// p_nr = '1454501';
		let db_lynn = new DB(dbConstants.LYNN);
		let db_karl = new DB(dbConstants.KARL);
		let result = {};
		let useCaseGetProductsFromLynn = new UseCaseGetProductsFromLynn(db_lynn);
		let useCaseAddProductsIntoKarl = new UseCaseAddProductsIntoKarl(db_karl, TAG);
		let useCaseAddComponentsToKarl = new UseCaseAddComponentsToKarl(db_karl);
		let shuffle = new FisherYates();
		let itemsNumber = 0;

		logger.log(TAG, 'Hole Produkte', `Produktdaten werden aus Lynn ermittelt.`)
		let items = await useCaseGetProductsFromLynn.getAllProducts(TAG, p_nr, date);

		let numberProductsFromLynn = (items === null || items === undefined) ? 0 : items.length;

		if (numberProductsFromLynn === 0) {
			logger.log(TAG,
				'Kein Produkt gefunden',
				`Es wurde in Lynn kein passendes Produkt gefunden, der Vorgang wird beendet.`
			);

			result = {successful:p_nr !== null && itemsNumber > 0}
			result.message = `Es wurde in Lynn kein passendes Produkt gefunden, der Vorgang wird beendet.`;

			return result
		}
		logger.log(TAG, numberProductsFromLynn + ' Produkte', `Aus Lynn wurden ${numberProductsFromLynn} Produkte ermittelt.`)
		logger.log(TAG, 'Produkte schütteln', `Produkte werden geschüttelt.`);
		items = await shuffle.shuffle(items);

		logger.log(TAG,
			'Produkte aus Karl ermitteln',
			`Es werden alle Produkte aus Karl ermittelt.`);

		let resultGetAllProducts = await productRepo.getAllProducts();
		logger.log(TAG,
			'Produkte aus Karl aufbereiten',
			`Es wurden ${resultGetAllProducts.length} Produkte aus Karl ermittelt. Sie werden jetzt aufbereitet.`);

		let karlProducts = await this.getPrepareProductData(resultGetAllProducts);

		logger.log(TAG, 'Produktkorrektur', `Vergleichen der Produkte mit dem Karl Datenbestand (korrektur/ neuanlage).`);
		await useCaseAddProductsIntoKarl.compareProducts(items, karlProducts);

		karlProducts = null;

		logger.log(TAG,
			'Artikel aus Karl ermitteln',
			`Es werden alle Artikel aus Karl ermittelt, um diese mit denen aus LYNN zu vergleichen.`);
		let resultGetAllArticle = await articleRepo.getAllArticle();
		logger.log(TAG,
			'Artikel aus Karl ermittelt',
			`Es wurden ${resultGetAllArticle.length} Artikel aus Karl ermittelt. Sie werden jetzt aufbereitet.`);
		let karlArticles = await this.getPrepareArticleData(resultGetAllArticle);

		logger.log(TAG, 'Artikelkorrektur', `Vergleichen der Artikel mit dem Karl Datenbestand (korrektur/ neuanlage).`);
		await useCaseAddProductsIntoKarl.compareArticles(items, karlArticles);

		logger.log(TAG, 'Produktkomponenten', `Standardkomponente werden korrigiert/ angelegt`);
		let resultDefaultComponents = {create: 0, update: 0, delete: 0}
		let resultExternalProductInformation = {create: 0, update: 0, delete: 0}
		for (let i = 0; i < items.length; i++) {
			itemsNumber++;
			resultDefaultComponents = await useCaseAddComponentsToKarl.addDefaultComponents(
				items[i]['p_nr']
				, items[i]['p_3_rd']
				, resultDefaultComponents
			);

			resultExternalProductInformation = await useCaseAddComponentsToKarl.setExternalProductInformation(
				items[i]
				, resultExternalProductInformation
			);

			if (itemsNumber % 5000 === 0) {
				logger.log(TAG, `${itemsNumber} verarbeitet`,
					`Es wurden ${
					itemsNumber} Produkte kontrolliert - Standardkomponente ${
					resultDefaultComponents.update} korrigiert - ${
					resultDefaultComponents.create} angelegt und ExProduktInfos (${
					resultExternalProductInformation.update} korrigiert - ${
					resultExternalProductInformation.create} angelegt (${
					items.length}).`);
			}
		}

		// result.message = `Es wurden von ${itemsNumber} Produkte die Standardkomponente kontrolliert/ korrigiert/ angelegt.`;
		result = {successful:p_nr !== null && itemsNumber > 0}
		result.message = `Es wurden ${
			itemsNumber} Produkte kontrolliert - Standardkomponente ${
			resultDefaultComponents.update} korrigiert - ${
			resultDefaultComponents.create} angelegt und ExProduktInfos (${
			resultExternalProductInformation.update} korrigiert - ${
			resultExternalProductInformation.create} angelegt.`;
		logger.log(TAG, `${itemsNumber} verarbeitet`, result.message);

		return result;
	}

	async getPrepareProductData(karlProdukts) {
		let result = [];
		for (let product of karlProdukts) {
			result['p_nr_' + product.p_nr] = product;
		}

		return result;
	}

	async getPrepareArticleData(karlArticles) {
		let result = [];
		for (let article of karlArticles) {
			result['a_nr_' + article.a_nr] = article;
		}

		return result;
	}
}

module.exports = Index;