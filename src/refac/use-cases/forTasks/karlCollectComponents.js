'use strict';

const DB = require('../../data-access');

const logger = require('../../../helper/util/logger.util');
const dbConstants = require('../../constants');
const constants = require('../../constants');
const repoComponents = require('../../../repository/repo.components');

const GetComponents = require('../components/get-components-from-cci');
const GetProductFromLynn = require('../products/get-products-from-lynn');
const AddComponentsToKarl = require('../components/add-components');
const FisherYates = require('../../helper/algorithm/fisheryates');


class Index {
    /** Hängt an den Artikeln die zugehörigen cNet - Komponenten an */
    async addComponentsToArticle(TAG, full = false) {
        let db_cci = new DB(dbConstants.CCI);
        let db_lynn = new DB(dbConstants.LYNN);
        let db_karl = new DB(dbConstants.KARL);

        let result = {
            successful: true,
            messages: []
        }

        let getComponent = new GetComponents(db_cci);
        let getProducts = new GetProductFromLynn(db_lynn);
        let addComponents = new AddComponentsToKarl(db_karl);
        let shuffle = new FisherYates();

        logger.log(TAG, 'Produktdaten holen', 'Produktdaten werden aus der Lynn-DB geholt.');
        let products = await getProducts.getAllProducts(TAG);

        let message = `Es wurden ${products.length} Produkte aus der Lynn-DB übernommen.`;
        logger.log(TAG, 'Produkte gesammelt', message);
        result.messages.push(message);

        logger.log(TAG, 'Produkte schütteln',
            'Alle Produkte werden kräftig durcheinander geschüttelt.');
        products = shuffle.shuffle(products);
        logger.log(TAG, 'Produkte geschüttelt', 'Es wurden '
            + products.length + ' Produkte geschüttelt.');

        let addProductsComponentCount = 0;
        let articleSkipped = 0;

        let i;
        for (i = 0; i < products.length; i++){
            let workingList = [];
            let addedProductComponent = false;
            if (products[i]['p_name'] === undefined || products[i]['p_nr'] === undefined)
                continue;
            // let compsDE = await getComponent.getComponentsPerMpn(products[i].p_name, 'de');
            // let compsEE = await getComponent.getComponentsPerMpn(products[i].p_name, 'ee');
            //
            //
            // workingList = (compsDE.concat(compsEE));

            /** Besitzt der Artikel bereits eine cNet Komponente, werden sie nur bei "full" aktualisiert. */
            let isNotCNetComponentInArticle = false;
            if (!full) {
                /**
                 * Sind für dieses Produkt schon cNet Komponenten (Pos < 100) vorhanden,
                 * dann werden nur bei "full" alle überprüft, da sie sich selten ändern.
                 * */
                isNotCNetComponentInArticle = !await repoComponents.isCNetComponentFromProductId(products[i].p_nr);
            }

            if (
                full ||
                (!full && isNotCNetComponentInArticle)
            ) {
                workingList = await getComponent.getAllLanguageComponentsFromMpn(products[i].p_name);
                for (let j = 0; j < workingList.length; j++) {

                    /** cNet Daten über 'Service & Support' mit dem Inhalt 'Begrenzte Garantie' entfernen. */
                    if (workingList[j]['Body'].includes('arantie') || workingList[j]['Body'].includes('arrant'))
                        continue;

                    let item = {
                        p_nr: products[i].p_nr,
                        p_component_key: workingList[j]['Header'],
                        p_component_value: workingList[j]['Body'],
                        p_component_language: workingList[j]['Lang'],
                        p_component_position: workingList[j]['DisplayOrder']
                    }
                    await addComponents.addComponents(constants.PRODUCTCOMPONENT, item)
                    addedProductComponent = true
                }
                await addComponents.addDefaultComponents(products[i]['p_nr'], products[i]['p_3_rd']);

                if (addedProductComponent) {
                    addProductsComponentCount++;
                }
            } else {
                articleSkipped++;
            }

            if (i % 500 === 0 && i > 0) {
                if (full) {
                    logger.log(TAG, 'cNet Komponenten vorhanden',
                        `Es waren für ${addProductsComponentCount} von ${
                        i} Produkten cNet Komponenten vorhanden. (${products.length})`);
                } else {
                    logger.log(TAG, 'cNet Komponenten neu',
                        `${i} Produkte bearbeitet, ${articleSkipped} vorhandene Komponenten beibehalten. Bei ${
                        addProductsComponentCount} Produkten wurden Komponenten hinzugefügt. (${products.length})`);
                }
            }
        }
        if (full) {
            message = `Es waren für ${addProductsComponentCount} von ${i} Produkten cNet Komponenten vorhanden. (${products.length})`;
            logger.log(TAG, 'cNet Komponenten vorhanden', message);
        } else {
            message = `${i} Produkte bearbeitet, ${articleSkipped} vorhandene Komponenten beibehalten. Bei ${
                addProductsComponentCount} Produkten wurden Komponenten hinzugefügt. (${products.length}) Abgeschlossen`;
            logger.log(TAG, 'cNet Komponenten neu', message);
        }

        result.messages.push(message);

        return result
    }
}

module.exports = Index;