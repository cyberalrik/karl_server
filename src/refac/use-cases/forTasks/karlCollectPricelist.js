'use strict';

const DB = require('../../data-access');

const logger = require('../../../helper/util/logger.util');
const dbConstants = require('../../constants');

const GetPriceListItemsFromLynn = require('../pricelist/get-pricelistitems-from-lynn');
const AddPriceListItemsToKarl = require('../pricelist/add-pricelistitem');
const artRepo = require('../../../repository/repo.pricelist');

let TAG = '';

class Index {
    constructor(tag) {
        TAG = tag;
    }

    async call(delta, p_nr) {

        let db_lynn = new DB(dbConstants.LYNN);
        let db_karl = new DB(dbConstants.KARL);

        let getAllPriceListItemsFromLynn = new GetPriceListItemsFromLynn(db_lynn);
        let addPriceListItemsToKarl = new AddPriceListItemsToKarl(db_karl);

        logger.log(TAG,'Hole Preise aus Lynn', 'Hole Preise der unterschiedlichen Kanäle von Lynn');
        let itemsLynn = await getAllPriceListItemsFromLynn.getAllPriceListItems(TAG, delta, p_nr);

        logger.log(TAG,'Hole Preise aus Karl', 'Hole Preise der unterschiedlichen Kanäle von Karl');
        let itemsKarl = await artRepo.getAllPriceList();
        let itemsKarlPrepare = await this.prepareItem(itemsKarl);
        itemsKarl = null;

        logger.log(TAG, 'Preis/ Mengen abgleichen', `${itemsLynn.length} Preise/ Mengen werden jetzt abgeglichen.`)

        return await addPriceListItemsToKarl.addPriceListItems(TAG, itemsKarlPrepare, itemsLynn);
    }

    async prepareItem(items) {
        let result = [];
        for (let index in items) {
            if (items[index].dataValues !== undefined) {
                let currentItem = items[index].dataValues;
                result[`${currentItem.a_nr}_${currentItem.channel}`] = currentItem;
            }
        }
        return result;
    }
}

module.exports = Index;