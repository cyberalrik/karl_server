'use strict';

const DB = require('../../data-access');
const RepoProduct2productImages = require('../../../repository/repo.product2productImages');
const logger = require('../../../helper/util/logger.util');
const dbConstants = require('../../constants');
const UseCaseImageForOther = require('../image/image-for-other');
const config = require('../../../configs/app.config').getConfig();

let TAG = '';

class Index {
    constructor(tag) {
        TAG = tag;
    }

    /**
     * Es werden Fotorelevante Verlinkungen in Lynn an Karl übertragen, und korrigiert.
     * */
    async call() {
        let result = {successful: true, messages: []};
        let message;
        let repoProduct2productImages = new RepoProduct2productImages();

        message = `Ermittle alle Fotoverbindungen in Karl.`;
        result.messages.push(message);
        logger.log(TAG, 'Ermittle Fotoverbindungen', message);

        let product2productImages = await repoProduct2productImages.getAllImagesConnection();
        message = `Es wurden ${product2productImages.length} Fotoverbindungen in Karl gefunden.`;
        result.messages.push(message);
        logger.log(TAG, 'Fotoverbindungen gefunden', message);

        message = `Präpariere alle Fotoverbindungen aus Karl.`;
        result.messages.push(message);
        logger.log(TAG, 'Präpariere Fotoverbindungen', message);

        let resultPrepareData = this.prepareData(product2productImages);
        product2productImages = undefined;
        let pNrLink = resultPrepareData.pNrLink;
        let useCaseImageForOther = new UseCaseImageForOther(TAG, pNrLink);

        /** Verlinkungen aus Lynn in Array übertragen */
        message = `Es werden alle verlinkte Produkte aus Lynn ermittelt, die über relevante Fotoverlinkungen verbunden sind.`;
        result.messages.push(message);
        logger.log(TAG, 'Ermittle Produkte', message);

        let db_lynn = new DB(dbConstants.LYNN);
        let linkListDataAccess = db_lynn.getLinkListDataAccess();
        /**
         * Welche Verlinkungen sind Fotorelevant?
         *
         *    1 = Fotoverlinkung
         *    2 = Komplettverlinkung 1 - gleiche Preise
         * 1003 = gleiche Artikelalternativen
         * 1007 = Master-Slave
         * */
        let resultLinkListOfArticleNumber = await linkListDataAccess.getLinkForType(config.FOTORELEVANTE_VERLINKUNGEN);
        if (!resultLinkListOfArticleNumber.successful) {
            message = 'Es kamen keine Daten von der Lynn-DB zurück.';
            result.messages.push(message);
            logger.log(TAG, 'Hole Produkte', message);

            return result
        }
        let linkListOfArticleNumber = resultLinkListOfArticleNumber.result;
        logger.log(TAG, 'Verlinkungen aus Lynn', `Es wurden ${linkListOfArticleNumber.length} ursprüngliche Verlinkungen gefunden.`);

        message = `Doppelte Querverweise herausfiltern`;
        result.messages.push(message);
        logger.log(TAG, 'clearReziproke', message);
        linkListOfArticleNumber = this.clearReziproke(linkListOfArticleNumber);

        message = `Auch der Schwager gehört zur Familie, P_Nr in Eimern einsortieren.`;
        result.messages.push(message);
        logger.log(TAG, 'getLinkBrothers', message);
        linkListOfArticleNumber = this.getLinkBrothers(linkListOfArticleNumber);

        message = `verlinkt alle P_Nr aus einen Eimer rekursiv`;
        result.messages.push(message);
        logger.log(TAG, 'buildObjectLinks', message);
        linkListOfArticleNumber = this.buildObjectLinks(linkListOfArticleNumber);

        message = `Vereine alle rekursiven verlinkungen aus den Bildern in ein Array`;
        result.messages.push(message);
        logger.log(TAG, 'uniteAllBuckets', message);
        linkListOfArticleNumber = this.uniteAllBuckets(linkListOfArticleNumber);

        let newAndRefLinked = 0;
        let newImageLinksCreated = 0
        let index = 0;

        message = `Gefundene und bereinigte Verlinkungen in Lynn: ${linkListOfArticleNumber.length}`;
        result.messages.push(message);
        logger.log(TAG, 'Start Verarbeitung', message);

        message = `In Karl schon vorhandene Verlinkungen: ${resultPrepareData.count}.`;
        result.messages.push(message);
        logger.log(TAG, 'vorhandene Verlinkungen', message);

        /**
         * gehe alle Verlinkungen einzeln durch
         * */
        for (index in linkListOfArticleNumber) {

            let currentLinkingPair = linkListOfArticleNumber[index];

            /** Es dürfen nicht zwei vom gleichen Produkt verknüpft werden. (new <-> ref) */
            if (currentLinkingPair.parent === currentLinkingPair.child) {
                newAndRefLinked++;

                continue
            }

            /** prüfe, ob schon verlinkt */
            newImageLinksCreated += await useCaseImageForOther.checkImagesShare(currentLinkingPair);

            if (index % 1000 === 0 && index > 0) {
                logger.log(TAG, `${(index)} Verlinkungen verarbeiten`, `Verarbeitet: ${(index)} von ${linkListOfArticleNumber.length} - neu angelegt: ${newImageLinksCreated} `);
            }
        }

        let numberDeleted = await useCaseImageForOther.deleteNotUsedImageLink();
        result.messages.push(`Gelöscht Verlinkungen: ${numberDeleted}`);
        logger.log(TAG, 'Auswertung-1', `Gelöscht Verlinkungen: ${numberDeleted}`);

        result.messages.push(`Hinzugefügte Verlinkungen: ${newImageLinksCreated}`);
        logger.log(TAG, 'Auswertung-2', `Hinzugefügte Verlinkungen: ${newImageLinksCreated}`);

        result.messages.push(`Verlinkungen, die auf sich selber zeigen: ${newAndRefLinked}`);
        logger.log(TAG, 'Auswertung-3', `Verlinkungen, die auf sich selber zeigen: ${newAndRefLinked}`);

        index++;
        result.messages.push(`Insgesamt überprüfte Verlinkungen: ${index}`);
        logger.log(TAG, 'Ende Verarbeitung', `Insgesamt überprüfte Verlinkungen: ${index}`);

        return result
    }

    prepareData(product2productImages) {
        let pNrLink = [];
        let count = 0;

        for (let index in product2productImages) {
            let currentPNr = product2productImages[index].p_nr;
            let currentPNrImage = product2productImages[index].p_nr_image;

            /**
             * Gleiche PNr sind keine Verlinkungen sondern Einträge auf die durchleiten.
             * Also für Produkte werden ihre eigenen Bilder benutzt.
             * */
            if (currentPNr === currentPNrImage) {
                continue;
            }
            /**
             * Hier werden alle Verlinkungen aufgenommen.
             * Verlinkungen sind Einträge, die aus unterschiedlichen PNr bestehen.
             * Dabei zeigt eine PNr auf eine andere.
             * */
            if (pNrLink['p_nr-' + currentPNr + '-p_nr_image-' + currentPNrImage] === undefined) {
                pNrLink['p_nr-' + currentPNr + '-p_nr_image-' + currentPNrImage] = {
                    delete: true,
                    pNr: currentPNr,
                    pNrImage: currentPNrImage
                };

                count++;
            }
        }

        return {pNrLink: pNrLink, count: count};
    }

    /** Doppelte Querverweise herausfiltern */
    clearReziproke(linkList) {
        let result = []
        let temp = []
        for (let index in linkList) {
            let current = linkList[index];
            if (temp[current.child + '-' + current.parent] === undefined) {
                result.push({parent: current.parent, child: current.child});
                temp[current.parent + '-' + current.child] = true;
            }
        }

        return result;
    }

    /**
     * Auch der Schwager gehört zur Familie
     * Bekommt ein Produkt über eine zweite Verlinkung als einziger einer anderen Verlinkung ein Bild,
     * müssen alle dieses Bild bekommen.
     * */
    getLinkBrothers(linkList) {
        let linkBucket = [];
        let number = 0;
        for (let index in linkList) {
            number++;
            let items = linkList[index];
            /** Ist das p oder c im Eimer? */
            let result = this.getBucket(linkBucket, items);

            switch (result.successful) {
                case 1 :
                    /** Wenn JA, dann hinzufügen */
                        // eslint-disable-next-line no-case-declarations
                    let currentBucket = result.bucket;
                    currentBucket.push(result.value);
                    linkBucket[result.index] = currentBucket;
                    break;
                case 2 :
                    /** Wenn NEIN, dann neu anlegen */
                        // eslint-disable-next-line no-case-declarations
                    let bucket = [items.parent,items.child];
                    // bucket.push(items.parent);
                    // bucket.push(items.child);
                    linkBucket.push(bucket);
                    break;
                case 3 :
                    /** nichts machen, ist schon vorhanden. */
                // eslint-disable-next-line no-fallthrough
                default:
            }

            if (number % 10000 === 0 && number > 0) {
                logger.log(TAG, `Schwager verarbeiten`, `Schon ${(number)} Verlinkungen zu Schwäger verarbeitet.`);
            }
        }

        return linkBucket;
   }

    /**
     * Kontrolliert, ob es ein Eintrag mit dem übergebenen Items gibt (Parent/ Child).
     * Gibt das Ergebnis in einem Object zurück.
     * {suc}
     * */
    getBucket(linkBucket, items) {
        let result = {successful: 2, bucket: null, index: null, value: null};

        for (let index in linkBucket) {
            let bucket = linkBucket[index];

            /** Parent überprüfen, ob es einen Eimer mit seinem Inhalt gibt. */
            if (bucket.includes(items.parent)) {
                /** Ist der gegenpart schon dabei? */
                if (bucket.includes(items.child)) {
                    /** dann nichts damit machen */
                    result.successful = 3;

                    return result;
                }

                result.successful = 1;
                result.bucket = bucket;
                result.index = index;
                result.value = items.child;
                break;
            }

            /** Parent überprüfen, ob es einen Eimer mit seinem Inhalt gibt. */
            if (bucket.includes(items.child)) {
                /** Ist der gegenpart schon dabei? */
                if (bucket.includes(items.parent)) {
                    /** dann nichts damit machen */
                    result.successful = 3;

                    return result;
                }

                result.successful = 1;
                result.bucket = bucket;
                result.index = index;
                result.value = items.parent;
                break;
            }
        }
        return result;
    }

    /**
     * Erstellt aus jedem Eimer eine rekursive Verlinkung.
     * */
    buildObjectLinks(linkBucket) {
        let result = [];
        let number = 0;
        for (let index in linkBucket) {
            number++;
            let currentBucket = linkBucket[index];
            let bucketLinks = this.getBucketLinks(currentBucket);
            result.push(bucketLinks)

            if (number % 1000 === 0 && number > 0) {
                logger.log(TAG, `Rekursive Verlinkung`, `Schon ${(number)} Eimer zu rekursiven Verlinkungen verarbeitet.`);
            }
        }

        return result;
    }

    getBucketLinks(items) {
        let result = [];
        let runThrough = 0;
        for (let x = 0; x < items.length; x++) {
            runThrough++;
            let itemParent = items[x];
            for (let y = runThrough; y < items.length; y++) {
                let itemChild = items[y];
                result.push({parent: itemParent, child: itemChild});
            }
        }

        return result;
    }

    /**
     * Verbindet alle Verlinkungen aus den Eimern wieder in ein großes Array
     * */
    uniteAllBuckets(items) {
        let result = []
        let number = 0;
        for (let index in items) {
            number++;
            let currentItem = items[index];
            for (let index_2 in currentItem) {
                let item = currentItem[index_2];
                result.push(item);
            }

            if (number % 1000 === 0 && number > 0) {
                logger.log(TAG, `Eimer zusammen gießen`, `Verlinkungen aus ${(number)} Eimern wurden schon zusammen gegossen.`);
            }
        }

        return result;
    }
 }

module.exports = Index;