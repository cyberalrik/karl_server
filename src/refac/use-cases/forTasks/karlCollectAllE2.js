'use strict';

const DB = require('../../data-access');
const logger = require('../../../helper/util/logger.util');
const dbConstants = require('../../constants');
const UseCaseGetE2ArticleFromLynn = require('../E2/get-article');
const FisherYates = require('../../helper/algorithm/fisheryates');
const articleRepo = require("../../../repository/article/repo.article");
const priceListRepo = require('../../../repository/repo.pricelist');
const articleChannelRepo = require('../../../repository/article/repo.article.channel');
const ServiceDestroyOldArticleData = require('../../../services/service.destroy.old.article.data');
const MailData = require('../../entity/mailData');

let TAG = '';
let result;
let aNrInArrayWithData = [];
let aNrInArrayWithPriceData = [];
let aNrInArrayWithChannelData = [];
let articlesChange = [];
let articlesChangeComparison = [];
let articleInfoForMail= [];
let newE2ArticleInfo = [];
let newPriceQuantInfo = [];
let newPriceQuantInfoHTML = [];
let newE2Articles = [];
let delE2ArticlePriceQuant = [];
let delE2ArticlePriceQuantHTML = [];
let delE2Articles = [];
let e2ChannelLock = [];


class Index {
	constructor(tag) {
		TAG = tag;
	}

	async call(p_nr = null) {
		result = {
			messages: []
		};
		aNrInArrayWithData = [];
		aNrInArrayWithPriceData = [];
		aNrInArrayWithChannelData = [];
		articlesChange = [];
		articlesChangeComparison = [];
		articleInfoForMail = [];
		newE2ArticleInfo = [];
		newPriceQuantInfo = [];
		newPriceQuantInfoHTML = [];
		newE2Articles = [];
		delE2ArticlePriceQuant = [];
		delE2ArticlePriceQuantHTML = [];
		delE2Articles = [];
		e2ChannelLock = [];

		let db_lynn = new DB(dbConstants.LYNN);
		let useCaseGetE2ArticleFromLynn = new UseCaseGetE2ArticleFromLynn(db_lynn);
		let shuffle = new FisherYates();

		if (p_nr !== null) {
			await logger.log(TAG, 'Hole E2-Artikel', `E2-Artikel für die KarlNr.: ${p_nr} werden aus Lynn ermittelt.`);
		} else {
			await logger.log(TAG, 'Hole E2-Artikel', `E2-Artikel werden aus Lynn ermittelt.`);
		}
		let resultE2FromLynn = await useCaseGetE2ArticleFromLynn.getAllE2Article(TAG, p_nr);
		if (!resultE2FromLynn.successful) {
			result.messages.push(resultE2FromLynn.message);
			await logger.err(TAG
				, 'keine E2-Artikel erhalten'
				, `Es konnten keine E2-Artikel aus Lynn ermittelt werden.`
				, `Der Fehler der übermittelt wurde ist: \n\n${resultE2FromLynn.message}`
				);

			return result;
		}

		let items = resultE2FromLynn.result;
		await logger.log(TAG, 'E2-Artikel erhalten', `Es konnten ${items.length} E2-Artikel aus Lynn ermittelt werden.`);

		/** Alle in Karl vorhandene eBay2 Artikel ermitteln, um eventuelle löschungen vorzunehmen */
		let allKarlEBay2Article = await articleRepo.getAllEBay2Article();
		let allEBay2Articles = await this.getANrInArray(allKarlEBay2Article);
		aNrInArrayWithData = await this.getANrInArrayWithData(allKarlEBay2Article);

		/** Alle in Karl vorhandene eBay2 Preise ermitteln, um eventuelle löschungen vorzunehmen */
		let allKarlEBay2PriceQuant = await priceListRepo.getAllEBay2PriceList();
		let allEBay2PriceQuant = await this.getANrInArray(allKarlEBay2PriceQuant);
		aNrInArrayWithPriceData = await this.getANrInArrayWithData(allKarlEBay2PriceQuant);

		/** Alle in Karl vorkommende eBay2 Artikel-Channel Kombinationen heraussuchen */
		let allKarlE2ArticleChannel = await articleChannelRepo.getAllE2ANrChannel();
		aNrInArrayWithChannelData = await this.getANrInArrayWithData(allKarlE2ArticleChannel);

		items = await shuffle.shuffle(items);

		for (let item of items) {
			/**
			 * Artikel anlegen/ korrigieren und aktualisieren.
			 * Bei Neuanlage wird TRUE zurückgegeben.
			 * */
			if (await this.createE2Article(item)) {
				await this.addArticleNumberToTradeByte(item.a_nr);
				newE2Articles.push(item);
				articleInfoForMail.push(item.a_nr);
			}

			/**
			 * E2-Artikel, die es schon in Karl gibt und hier auch bearbeitet wurden, werden mit FALSE gekennzeichnet.
			 * Damit werden sie als nicht entfernte E2-Artikel gekennzeichnet und bleiben enthalten
			 * */
			if (allEBay2Articles[item.a_nr] !== undefined) {
				allEBay2Articles[item.a_nr] = false;
			}
			if (allEBay2PriceQuant[item.a_nr] !== undefined) {
				allEBay2PriceQuant[item.a_nr] = false;
			}
		}

		let message;
		switch(articleInfoForMail.length) {
			case 0:
				message = `Es wurden keine neuen E2-Artikel aufgenommen.`;
				break;
			case 1:
				message = `Es wurde 1 neuer E2-Artikel aufgenommen.`;
				break;
			default:
				message = `Es wurden ${articleInfoForMail.length} neue E2-Artikel aufgenommen.`;
				break;
		}

		await logger.log(TAG, 'E2-Artikel', message);
		// newE2ArticleInfo.push(message);
		result.messages.push(message);

		if (articleInfoForMail.length > 0) {
			/** Mit Mailversenden */
			let articleNumbersList = articleInfoForMail.join(', ');

			await logger.log(TAG, 'E2-Artikel aufgenommen', articleNumbersList);
		}

		/**
		 * !!! ACHTUNG !!!
		 * ES IST SEHR WICHTIG
		 *
		 * Wird eine p_nr übergeben, muss hier nachgesehen werden, welche E2-Artikel es noch gibt.
		 * Diese müssen aus der weiteren Bearbeitung heraus genommen, also auf FASLE gesetzt werden.
		 * */
		if (p_nr !== null) {
			let result = (await useCaseGetE2ArticleFromLynn.getAllANrFromProductNr(TAG, p_nr));

			if (result.successful) {
				let a_nrs = result.result;
				let currentA_nr = [];
				for (let index in a_nrs) {
					currentA_nr.push('E2-' + a_nrs[index].a_nr);
				}

				for (let a_nr in allEBay2Articles) {
					if (!currentA_nr.includes(a_nr)) {
						allEBay2Articles[a_nr] = false;
					}
				}

				for (let a_nr in allEBay2PriceQuant) {
					if (!currentA_nr.includes(a_nr)) {
						allEBay2PriceQuant[a_nr] = false;
					}
				}
			}
		}

		/** E2-Artikel, die nicht mehr gebraucht werden, bekommen eine channel Sperre. */
		for (let a_nr in allEBay2Articles) {
			if (allEBay2Articles[a_nr]) {
				await this.setArticleChannel(a_nr, 'ebde', 0);
			}
		}

		/** Setze Preise und Mengen der nicht mehr vorhandene E2-Artikel auf 0 */
		for (let index in allEBay2PriceQuant) {
			if (allEBay2PriceQuant[index]
				&& aNrInArrayWithPriceData[index].price !== '0'
				&& aNrInArrayWithPriceData[index].quant !== '0'
			) {
				/** Preise/ Mengen des Artikels können auf 0 gesetzt werden */
				let priceResult = await priceListRepo.editPriceListItem({a_nr: index, channel: 'ebde', price: 0, quant: 0})
				if (priceResult) {
					message = `Der E2-Artikel ${index} wurde auf Preis und Menge 0 gestellt.`;
					await logger.log(TAG, 'Preis/ Menge für E2-Artikel', message);
					result.messages.push(message);
					delE2ArticlePriceQuant.push(message);
					delE2ArticlePriceQuantHTML.push(`<tr><td>${index}</td></tr>`);
				}

				await this.addArticleNumberToTradeByte(index);
			}
		}

		/**
		 * E2-Artikel, die einen eBay Preis von 0 haben und deren Änderungsdatum älter als heute - 30 Tage ist,
		 * werden aus Karl gelöscht.
		 * */
		//let date2 = new Date(new Date() - 30 * 24 * 60 * 60 * 1000)
		let date = new Date();
		date.setDate( date.getDate() - 30);
		let oldPriceList = await priceListRepo.getAllANrOfPriceQuantIs0(date);
		let oldANumbers = await this.getANrInArray(oldPriceList);
		if (oldPriceList.length > 0) {
			let serviceDestroyOldArticleData = new ServiceDestroyOldArticleData();
			let allPartNumbers = await serviceDestroyOldArticleData.destroyOldArticleData(TAG, oldANumbers);

			message = `Folgende E2-Artikel wurden aus Karl entfernt:`;
			await logger.log(TAG, 'E2-Artikel entfernt', message);
			delE2Articles.push(message);
			result.messages.push(message);

			await logger.log(TAG, 'E2-Artikel entfernt', allPartNumbers);
			delE2Articles.push(allPartNumbers);
			result.messages.push(allPartNumbers);
		}

		result.articlesChange = articlesChange;

		await this.sendInfoMail();

		return result
	}

	/**
	 * Gibt es den übergebenen E2-Artikel noch nicht, wird er angelegt.
	 * Alle anderen Tabellen werden mit aktuellen Daten entweder erweitert oder aktualisiert.
	 *
	 * Musste der Artikel angelegt werde, wird ein TRUE zurückgegeben.
	 * Andernfalls ein FALSE
	 *
	 * @param e2Article
	 * @return Promise {<boolean>}
	 * */
	async createE2Article(e2Article) {
		let isCreate = false;

		/**
		 * Gibt es den E2-Artikel schon in Karl?
		 * Oder müssen die Daten aktualisiert werden?
		 * */
		if (
			(
				aNrInArrayWithData[e2Article.a_nr] === null
				|| aNrInArrayWithData[e2Article.a_nr] === undefined
			) || (
				aNrInArrayWithData[e2Article.a_nr] !== null
				&& aNrInArrayWithData[e2Article.a_nr] !== undefined
				&& (
					aNrInArrayWithData[e2Article.a_nr].getDataValue('a_nr2') !== e2Article.a_nr2
					|| aNrInArrayWithData[e2Article.a_nr].getDataValue('a_prod_nr') !== e2Article.a_prod_nr
					|| aNrInArrayWithData[e2Article.a_nr].getDataValue('a_cond') !== e2Article.a_cond
					|| aNrInArrayWithData[e2Article.a_nr].getDataValue('a_delivery_time') + '' !== e2Article.a_delivery_time + ''
				)
			)
		) {
			/** Legt einen neuen E2-Artikel an. Wenn schon vorhanden wird kontrolliert, ob er korrigiert werden muss. */
			let articleData = await articleRepo.createIfNotAvailable(e2Article);
			isCreate = articleData.create;
			aNrInArrayWithData[e2Article.a_nr] = articleData.data;
		}
		/** EAN prüfen */
		if (
			aNrInArrayWithData[e2Article.a_nr] !== null
			&& aNrInArrayWithData[e2Article.a_nr] !== undefined
			&& aNrInArrayWithData[e2Article.a_nr].getDataValue('a_ean') + '' === '0'
		) {
			/** Die EAN ist noch nicht vergeben, es kann aber die vom Original-Artikel benutzt werden */
			let originalArticle = await articleRepo.getArticlePerID(e2Article.a_nr_original);
			if (
				originalArticle !== null
				&& originalArticle !== undefined
				&& originalArticle.getDataValue('a_ean') + '' !== '0'
			) {
				aNrInArrayWithData[e2Article.a_nr].setDataValue('a_ean', originalArticle.getDataValue('a_ean'));
				await articleRepo.createIfNotAvailable(aNrInArrayWithData[e2Article.a_nr]);
			}
		}
		/**
		 * Gibt es den E2-Artikel schon in priceList?
		 * Oder müssen die Daten aktualisiert werden?
		 * */
		if (
			(
				aNrInArrayWithPriceData[e2Article.a_nr] === null
				|| aNrInArrayWithPriceData[e2Article.a_nr] === undefined
			) || (
				aNrInArrayWithPriceData[e2Article.a_nr] !== null
				&& aNrInArrayWithPriceData[e2Article.a_nr] !== undefined
				&& (
					aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('price') !== e2Article.price + ''
					|| aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('quant') !== e2Article.quant
				)
			)
		) {
			await priceListRepo.addPriceListItem(e2Article);
			let message1 = `Die Werte für Preis/ Menge für den Artikel ${
				e2Article.a_nr} wurde von: ${
				aNrInArrayWithPriceData[e2Article.a_nr] !== undefined ?
					aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('price') !== undefined 
						? aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('price') 
						: 0
					: 0}€ auf: ${
				e2Article.price}€/ und die Menge von: ${
				aNrInArrayWithPriceData[e2Article.a_nr] !== undefined ?
					aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('quant') !== undefined 
						? aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('quant') 
						: 0
					: 0} auf: ${
				e2Article.quant} Stück gesetzt.`;
			await logger.log(TAG, 'E2-Artikel Preis/ Menge', message1);

			let message2 = `Artikel-Nr.: ${
				e2Article.a_nr} Preis von: ${
				aNrInArrayWithPriceData[e2Article.a_nr] !== undefined ?
					aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('price') !== undefined
						? aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('price')
						: 0
					: 0}€ auf: ${
				e2Article.price}€/, Menge von: ${
				aNrInArrayWithPriceData[e2Article.a_nr] !== undefined ?
					aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('quant') !== undefined
						? aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('quant')
						: 0
					: 0} auf: ${
				e2Article.quant} Stück gesetzt.`;
			newPriceQuantInfo.push(message2)

			/** HTML */
			let oldPrice = aNrInArrayWithPriceData[e2Article.a_nr] !== undefined ?
				aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('price') !== undefined
					? aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('price')
					: 0
				: 0;

			let oldQuant = aNrInArrayWithPriceData[e2Article.a_nr] !== undefined ?
				aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('quant') !== undefined
					? aNrInArrayWithPriceData[e2Article.a_nr].getDataValue('quant')
					: 0
				: 0;
			let priceStyle = oldPrice === e2Article.price + '' ? '' : 'style="color: #002aff;"';
			let quantStyle = oldQuant === e2Article.quant + '' ? '' : 'style="color: #002aff;"';

			//	<td style="font-size: 24px; font-family: Arial, sans-serif; color: #ff0000;">

			let messageHTML = `<tr><td>${
				e2Article.a_nr}</td><td class="numbers" ${priceStyle}>${
				oldPrice}€</td><td class="numbers" ${priceStyle}>${
				e2Article.price}€</td><td class="numbers" ${quantStyle}>${
				oldQuant}</td><td class="numbers" ${quantStyle}>${
				e2Article.quant}</td></tr>`;

			newPriceQuantInfoHTML.push(messageHTML);
		}

		/**
		 * Channel Einstellungen schon vorhanden?
		 * Für ebde soll bei Neuanlage nichts enthalten sein.
		 * Löschen wenn vorhanden.
		 * */
		if (
			aNrInArrayWithChannelData[e2Article.a_nr + '-ebde'] !== null
			&& aNrInArrayWithChannelData[e2Article.a_nr + '-ebde'] !== undefined
			&& !aNrInArrayWithChannelData[e2Article.a_nr + '-ebde'].getDataValue('is_active')
		) {
			/** artikelChannel korrigieren */
			await this.setArticleChannel(e2Article.a_nr, 'ebde', 1);
		}

		/** Für ITM Sperre setzen, wenn noch nicht vorhanden. */
		if (
			(
				aNrInArrayWithChannelData[e2Article.a_nr + '-cucybertr1'] === null
				|| aNrInArrayWithChannelData[e2Article.a_nr + '-cucybertr1'] === undefined
			) || (
				aNrInArrayWithChannelData[e2Article.a_nr + '-cucybertr1'] !== null
				&& aNrInArrayWithChannelData[e2Article.a_nr + '-cucybertr1'] !== undefined
				&& aNrInArrayWithChannelData[e2Article.a_nr + '-cucybertr1'].getDataValue('is_active')
			)
		) {
			/** artikelCannel korrigieren */
			await this.setArticleChannel(e2Article.a_nr, 'cucybertr1', 0);
		}

		return isCreate;
	}

	/**
	 * Überführt die übergebenen Daten in ein Array, wo als Key die a_nr benutzt wird
	 * und der Wert ist überall = TRUE;
	 *
	 * @param items
	 * @return Promise {<Array>}
	 * */
	async getANrInArray(items) {
		let result = [];
		for (let item of items) {
			result[item.getDataValue('a_nr')] = true;
		}

		return result
	}

	/**
	 * Überführt die übergebenen Daten in ein Array, wo als Key die a_nr benutzt wird.
	 * Gibt es einen 'channel_key', wird auch dieser zusätzlich im Key verwendet.
	 * Der Wert ist jeweils der Artikel
	 *
	 * @param items
	 * @return Promise {<Array>}
	 * */
	async getANrInArrayWithData(items) {
		let result = [];
		let addition = '';
		for (let item of items) {
			if (item.getDataValue('channel_key') !== undefined) {
				addition = '-' + item.getDataValue('channel_key');

			}
			result[item.getDataValue('a_nr') + addition] = item;
		}

		return result
	}

	/**
	 * Ist eine Sperrung für den übergebenen Artikel für den übergebenen Channel erforderlich,
	 * wird der Artikel in der Tabelle articleChannels aufgenommen.
	 * Ansonsten kann der Artikel aus der Tabelle gelöscht werden.
	 *
	 * @param a_nr
	 * @param channel
	 * @param value
	 * @return Promise {<>}
	 * */
	async setArticleChannel(a_nr, channel, value) {
		let message;
		let channelResult;

		/** Ist der Channel schon so eingestellt wie gewünscht, dann muss nichts verändert werden. */
		if (aNrInArrayWithChannelData[a_nr + '-' + channel] !== undefined
			&& aNrInArrayWithChannelData[a_nr + '-' + channel].is_active !== undefined
			&& aNrInArrayWithChannelData[a_nr + '-' + channel].is_active === !!value
		) {
			channelResult = 4;
		} else {
			channelResult = await articleChannelRepo.setArtChannelData(a_nr, channel, value);
			await this.addArticleNumberToTradeByte(a_nr);
		}
		if (channelResult !== 0) {
			switch (channelResult) {
				// 0 = nichts geändert; 1 = neuanlage; 2 = angepasst; 3 = gelöscht
				case 1:
					message = `Der E2-Artikel ${a_nr} wurde für dem Channel '${channel}' deaktiviert. (Neuanlage)`;
					break;
				case 2:
					message = `Der E2-Artikel ${a_nr} wurde für dem Channel '${channel}' deaktiviert. (geändert)`;
					break;
				case 3:
					message = `Der E2-Artikel ${a_nr} wurde aus der Sperre des Channels '${channel}' gelöscht.`;
					break;
				case 4:
					// message = `Der E2-Artikel ${a_nr} war auf dem Channel '${channel}' schon deaktiviert. (nichts geändert).`;
					message = ``;
					break;
				default:
			}
			if (message !== '') {
				await logger.log(TAG, 'Channel für E2-Artikel gesperrt', message);
				e2ChannelLock.push(message);
				result.messages.push(message);
			}
		}
	}

	/**
	 * Diese Artikel sollen an TradeByte übermittelt werden
	 * Artikeländerungen
	 * */
	async addArticleNumberToTradeByte(a_nr) {
		if (articlesChangeComparison[a_nr] === undefined) {
			articlesChangeComparison[a_nr] = a_nr;
			articlesChange.push(a_nr);
		}
	}

	async sendInfoMail() {
		let resultGenerateData = '', messageText = '', messageHTML = '';
		/** Zusammenstellen der Informationen für die InfoMail */

		resultGenerateData = await this.generateDataNewArticle();
		if (resultGenerateData.successful) {
			messageText +=  resultGenerateData.messageText
			messageHTML +=  resultGenerateData.messageHTML
		}

		/** Neuer Preise */
		resultGenerateData = await this.generateDataNewPriceAndQuant();
		if (resultGenerateData.successful) {
			messageText +=  resultGenerateData.messageText
			messageHTML +=  resultGenerateData.messageHTML
		}

		/** Artikel preis genullt */
		resultGenerateData = await this.generateDataPriceAndQuantSetNull();
		if (resultGenerateData.successful) {
			messageText +=  resultGenerateData.messageText
			messageHTML +=  resultGenerateData.messageHTML
		}

		/** Artikel Kanal sperren */
		resultGenerateData = await this.generateDataChannelLock();
		if (resultGenerateData.successful) {
			messageText +=  resultGenerateData.messageText
			messageHTML +=  resultGenerateData.messageHTML
		}

		/** Daten in MailData Object eintragen über Konstruktor */
		if (messageHTML.length > 0) {
			// eslint-disable-next-line no-mixed-spaces-and-tabs
			 let mailData = new MailData(
				'Änderungen an E2-Artikel'
				, messageText
				,'s.wiehe@cybertrading.de'
				, null
				, true
				, messageHTML
			);

			await logger.log(
				TAG
				, 'Versende Info-Mail'
				, `Informationen von Änderungen an E2-Artikel als eMail verschickt.`
				, mailData
			);
		}
	}

	async generateDataNewArticle() {
		let result = {successful: false}
		let newE2ArticleInfoHTML = [];

		let headline = `<div><h3>Neu angelegte Artikel</h3></div>`;
		let tableHead = `<tr><th>Artikel-Nr.</th><th>Karl-Nr.</th><th>Part-Nr.</th><th>Preis in €</th><th>Anzahl</th></tr>`;

		for (let newE2Article of newE2Articles) {
			newE2ArticleInfo.push(
				`Part-Nr.: ${
					newE2Article.a_nr
				}, Karl-Nr.: ${
					newE2Article.p_nr
				}, Artikel-Nr.: ${
					newE2Article.a_prod_nr
				} mit einem Preis von: ${
					newE2Article.price
				} € und Anzahl: ${
					newE2Article.quant
				} Stück.`
			)
			/** Aufnahme neu angelegter Artikel */
			newE2ArticleInfoHTML.push(
				`<tr><td>${newE2Article.a_nr
				}</td><td class="numbers">${newE2Article.p_nr
				}</td><td>${newE2Article.a_prod_nr
				}</td><td class="numbers">${newE2Article.price
				} €</td><td class="numbers">${newE2Article.quant
				}</td></tr>`
			)
		}

		/** Zeilenweise umbrechen */
		let newE2ArticleInfoList = newE2ArticleInfo.join(`\n`);


		let newE2ArticleInfoListHTML = '';
		if (newE2ArticleInfoHTML.length > 0) {
			newE2ArticleInfoListHTML = `<tr><td>${headline}<table>\n${
				tableHead}${newE2ArticleInfoHTML.join(`\n`)}</table></td></tr>\n`;
		}

		if (newE2ArticleInfo.length > 0) {
			result.messageText = `\nFür folgende Produkte wurden E2-Artikel angelegt:\n${newE2ArticleInfoList}\n\n`;
			result.messageHTML = newE2ArticleInfoListHTML;
			result.successful = true;
		}

		return result;
	}

	async generateDataNewPriceAndQuant() {
		let result = {successful: false}

		let headline = `<div><h3>Preis-/ Mengenänderungen</h3></div>`;
		let tableHead = `<tr><th>Artikel-Nr.</th><th>alter Preis</th><th>neuer Preis</th><th>alte Menge</th><th>neue Menge</th></tr>`;
		let footer = `<tr><td colspan="5" style="color: #002aff;">Farbige Werte sind Änderungen.</td></tr>`;
		let newPriceQuantInfoListHTML = `<tr><td>${headline}<table>${tableHead}${newPriceQuantInfoHTML.join(`\n`)}${footer}</table></td></tr>`;
		let newPriceQuantInfoList = newPriceQuantInfo.join(`\n`);
		if (newPriceQuantInfo.length > 0) {
			result.messageText = `Folgende Änderungen wurden übernommen:\n${newPriceQuantInfoList}\n\n`;
			result.messageHTML = newPriceQuantInfoListHTML;
			result.successful = true;
		}

		return result
	}

	async generateDataPriceAndQuantSetNull() {
		let result = {successful: false}

		let delE2ArticlePriceQuantInfoList = delE2ArticlePriceQuant.join(`\n`);
		if (delE2ArticlePriceQuant.length > 0) {
			result.messageText = `Artikel, bei denen der Preis und die Anzahl auf 0 gesetzt wurden:\n${delE2ArticlePriceQuantInfoList}\n\n`;
			let headline = `<h3>Artikel, bei denen der Preis und die Anzahl auf 0 gesetzt wurden.</h3>`;
			let tableHead = `<tr><th>Artikel-Nr.</th></tr>`;

			result.messageHTML = `<tr><td>${headline}<table>${tableHead}${delE2ArticlePriceQuantHTML.join(`\n`)}</table></td></tr>`;
			result.successful = true;
		}

		return result
	}

	async generateDataChannelLock() {
		let result = {successful: false}

		let e2ChannelLockInfoList = e2ChannelLock.join(`\n`);
		let e2ChannelLockInfoListHTML = e2ChannelLock.join(`</br>`);
		let headline = `<h3>Für folgende Artikel wurde die Kanalsperre gesetzt/ gelöscht:</h3>`;
		if (e2ChannelLock.length > 0) {
			result.messageText = `Folgende Artikel wurden deaktiviert:\n${e2ChannelLockInfoList}\n\n`;
			result.messageHTML = `<tr><td>${headline}<table>${e2ChannelLockInfoListHTML}</table></td></tr>`;
			result.successful = true;
		}

		return result
	}
}

module.exports = Index;