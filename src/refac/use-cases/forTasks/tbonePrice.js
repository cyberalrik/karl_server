'use strict';

const priceList = require('../../../repository/repo.pricelist');
const UseCaseTbOneSendPrice = require('../tbone/sendPrice');
const logger = require('../../../helper/util/logger.util');

let TAG = '';

/***
 *
 * @returns {Promise<{}>}
 */
class Index {
    constructor(tag) {
        TAG = tag;
    }

    /**
     * Es werden die Preise an TB.One geschickt.
     * Übergeben wirt
     *  true = alle Preise werden benutzt
     *  false = die Preise die in den letzten 10 Stunden geändert wurden
     *
     * @param delta
     * @return {Promise<>}
     * */
    async call(delta = true) {
        let result = {
            successful: true,
            messages: []
        }

        let message = `Wurde mit delta = ${delta} gestartet.`;
        logger.log(TAG,`TbOne Preisupdate`,message);
        result.messages.push(message);

        let date = new Date();
        date.setMinutes( 0);
        date.setHours( 0);
        date.setSeconds( 1);
        let priceUpdate = await priceList.getModifiedPriceListItem(date, delta);

        message = `Es gibt ${priceUpdate.length} Artikelpreise in Karl.`;
        if (delta) {
            message = `Es gibt ${priceUpdate.length} Preisänderungen in Karl.`;
        }
        await logger.log(TAG,`TbOne Preisupdate`, message);
        result.messages.push(message);

        if (priceUpdate.length === 0) {
            return result
        }

        let useCaseTbOneSendPrice = new UseCaseTbOneSendPrice();
        result = await useCaseTbOneSendPrice.workUpload(TAG, result, priceUpdate);

        return result
    }
}

module.exports = Index;