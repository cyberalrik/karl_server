'use strict';

const productRepo = require('../../../repository/repo.product');
const DataAccessForJoin = require('../../data-access/karl/articleProduct');
const UseCaseTbOneBuildDataForXml = require('../tbone/buildDataForXml');

class Index {
	/***
	 * @param TAG
	 * @param p_nr
	 * @param date
	 * @param newArticle
	 * @returns {Promise<{}>}
	 */
	async call(TAG, p_nr = null, date = null, newArticle = false) {
		/**
		 * Sollen alle Produkte nach ihren neu angelegten Artikeln bestimmt werden? 'delta'
		 * Übergeben wird dann ein Datum, ab dem Produkte mit neuen Artikeln ermittelt werden sollen.
		 *  */
		let products;
		if (newArticle) {
			let dataAccessForJoin = new DataAccessForJoin();
			products = await dataAccessForJoin.getAllProductOfTheNewArticle(date);
		} else {
			products = await productRepo.getAllProductAfterTime(p_nr, date);
		}

		let useCaseTbOneBuildDataForXml = new UseCaseTbOneBuildDataForXml();
		let result = await useCaseTbOneBuildDataForXml.buildDataForXml(TAG, products);

		return result
	}
}

module.exports = Index;

