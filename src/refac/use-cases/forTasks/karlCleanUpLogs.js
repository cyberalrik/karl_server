'use strict';

const logger = require('../../../helper/util/logger.util');
const logRepo = require('../../../repository/repo.log');
const ServiceDateFormat = require("../../../services/service.date.format");
let TAG = '';

class Index {
	constructor(tag) {
		TAG = tag;
	}

	async call() {
		let result = {messages: []}
		let serviceDateFormat = new ServiceDateFormat();
		let messageUpDate = await serviceDateFormat.getStringDateAddDayAndHoursGermanFormat(-184,0); // 184 Tage = ca. 1/2 Jahr

		let message = `Alle Log-Einträge, die älter sind als '${messageUpDate}' werden gelöscht.`;
		logger.log(TAG, 'Tabelle logs', message);
		result.messages.push(message);

		try {
			let cleanUpDate = await serviceDateFormat.getStringDateAddDayAndHours(-184,2);
			let destroyCount = await logRepo.deleteByDate(cleanUpDate);
			message = `Es wurden ${destroyCount} Einträge gelöscht.`;
			logger.log(TAG, 'Tabelle logs', message);
			result.messages.push(message);
		} catch (e) {
			message = `Beim Löschen von Log-Einträge ist ein Fehler aufgetreten.`;
			logger.err(TAG, 'Tabelle logs', message);
			result.messages.push(message);
		}

		return result;
	}
}

module.exports = Index;