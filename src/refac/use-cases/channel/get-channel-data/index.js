'use strict'
const repoChannel = require('../../../../repository/repo.channel')
const constants = require('../../../../constants/images.constant');
let channels = null;

class Index {

    async getAllChannels() {
        if (channels === null) {
            channels = repoChannel.getChannels();
        }

        return channels;
    }

    async getChannelTypeValue() {
        let channelTypValue = [];
        if (channels === null) {
            channels = await repoChannel.getChannels();
        }

        for (let channel of channels) {
            if (channel.channel_key === 'default')
                continue

            channelTypValue.push(
                {
                    key: channel.channel_key,
                    name: channel.channel_name,
                    value: await constants.getChannelTypeValue(channel.channel_key),
                    height: channel.image_height,
                    width: channel.image_width,
                });
        }

        return channelTypValue;
    }
}

module.exports = Index