'use strict';

const Download = require('../../../data-access/sftp/tbOne/order');

class Index {
    async loadOrderFiles(param) {

        let download = new Download();

        return await download.loadFromTbOne(param);
    }
}

module.exports = Index;