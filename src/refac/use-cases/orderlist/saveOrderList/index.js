'use strict'

const SaveOrder = require('../../../data-access/order/order/index');

class index {
    async saveOrderList(params) {
        let saveOrder = new SaveOrder();
        let data = params.orderList;

        if (
            data === undefined || data === null ||
            data.ORDER_LIST === undefined || data.ORDER_LIST === null ||
            data.ORDER_LIST.ORDER === undefined || data.ORDER_LIST.ORDER === null
        ) {
            return params;
        }

        for (let i = 0; i < data.ORDER_LIST.ORDER.length; i++) {
            params.currentOrderList = data.ORDER_LIST.ORDER[i];
            params = await saveOrder.saveOrder(params);
        }

        return params;
    }
}

module.exports = index;