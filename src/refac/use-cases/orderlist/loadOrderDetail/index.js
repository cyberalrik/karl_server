'use strict';

const OrderData = require('../../../data-access/order/orderData');
const SellTo = require('../../../data-access/order/sellTo');
const ShipTo = require('../../../data-access/order/shipTo');
const Shipment = require('../../../data-access/order/shipment');
const Payment = require('../../../data-access/order/payment');
const References = require('../../../data-access/order/references');
const History = require('../../../data-access/order/history');
const Services = require('../../../data-access/order/service');
const Items = require('../../../data-access/order/items');

class Index {

    // OrderData
    async getOrder(orderId) {
        let orderData = new OrderData();

        return await orderData.getOrderDetails(orderId);
    }

    // SellTos
    async getSellTo(orderId) {
        let sellTo = new SellTo();

        return await sellTo.getSellToDetails(orderId);
    }

    // ShipTo
    async getShipTo(orderId) {
        let shipTo = new ShipTo();

        return await shipTo.getShipToDetails(orderId);
    }

    // Shipment
    async getShipment(orderId) {
        let shipment = new Shipment();

        return await shipment.getShipmentDetails(orderId);
    }

    // Payment
    async getPayment(orderId) {
        let payment = new Payment();

        return await payment.getPaymentDetails(orderId);
    }

    // References
    async getReferences(orderId) {
        let references = new References();

        return await references.getReferencesDetails(orderId);
    }

    // History
    async getHistory(orderId) {
        let history = new History();

        return await history.getHistoryDetails(orderId);
    }

    // Services
    async getServices(orderId) {
        let services = new Services();

        return await services.getServicesDetails(orderId);
    }

    // Items
    async getItems(orderId) {
        let items = new Items();

        return await items.getItemsDetails(orderId);
    }


}

module.exports = Index;