'use strict';

// const xml-js = require('xml-js');
const xml2js = require('xml2js');
const fs = require('fs');
const OrderData = require('../../../../refac/use-cases/orderlist/saveOrderList/index');
const orderConfig = require('../../../../configs/order.config');
const localPath = orderConfig.localPath;
const renamePath = orderConfig.renamePath;
const logger = require('../../../../helper/util/logger.util');
const TAG = 'use-cases.readOrderData';

class Index {
    async readXmlFiles(params) {
        try {
            let parser = new xml2js.Parser();
            let orderData = new OrderData();

            for (let i = 0; i < params.localFiles.length; i++) {
                params.currentFileName = params.localFiles[i];

                let content;
                try {
                    content = fs.readFileSync(localPath + params.currentFileName);
                } catch (e) {
                    logger.err(TAG, 'File Error ', e.message);
                    params.result.numberOfFilesNotFound++;
                    continue;
                }

                /**
                 * XML-String in JSON umwandeln
                 * "data/ orderList" beinhaltet das JSON-Objekt
                 */

                await parser.parseString(content, function (err, data) {
                    if (err === null) {
                        params.orderList = data;
                        params.result.successful = true;
                        params.result.numberOfGoodFiles++;
                    } else {
                        logger.err(TAG, 'XML-Parser Error', 'Datei ' + localPath + params.currentFileName +
                            ' konnte nicht verarbeitet werden');

                        params.result.successful = false;
                        params.result.numberOfBadFiles++;
                    }
                });

                if (params.result.successful) {
                    params = await orderData.saveOrderList(params);
                }

                parser.reset();

                // Datei nach Verarbeitung löschen
                if (params.result.successful) {
                    fs.unlink(localPath + params.currentFileName, (err) => {
                        if (err) {
                            logger.err(TAG, 'File Error', 'Die Datei ' + localPath + params.currentFileName +
                                ' konnte nicht gelöscht werden.');
                        }
                    });
                } else {
                    // Bei Fehler lokale Datei in Ordner errorFiles sichern
                    logger.err(TAG, 'XML-Datei fehlerhaft', 'Fehler in ' + localPath + params.currentFileName);
                    fs.rename(localPath + params.currentFileName, renamePath + params.currentFileName ,
                        (err) => {
                            if (err) {
                                logger.err(TAG, 'File Error', 'Die Datei ' + localPath +
                                    params.currentFileName + ' konnte nicht verschoben werden.');
                            }
                    });
                }
            }
        } catch (e) {
            logger.err(TAG, 'XML-Datei Fehler', 'Die Datei ' + localPath + params.currentFileName +
                ' konnte nicht gelesen werden.');
        }

        return params;
    }
}

module.exports = Index;