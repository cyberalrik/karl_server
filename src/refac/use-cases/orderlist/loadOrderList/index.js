'use strict';

const OrderData = require('../../../data-access/order/orderData');

class Index {
    async loadOrderList(params) {

        let orderData = new OrderData();

        return await orderData.getOrderData(params);
    }

    async getNumberOfEntries(params) {
        let orderData = new OrderData();

        return await orderData.getNumberOfEntries(params);

    }

    async getFilterValue(groupName) {
        let orderData = new OrderData();

        return await orderData.getFilterValue(groupName);
    }
}

module.exports = Index;