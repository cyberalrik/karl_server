'use strict';

const TableDescription = require('../../../data-access/order/tableDescription/');

class Index {

    async getTableDescription() {
        let tableDescription = new TableDescription();

        return await tableDescription.getTableDescription();
    }

}

module.exports = Index;