'use strict'

const logger = require('../../../../helper/util/logger.util');

class Index {
    constructor(db) {
        this.db = db;
    }

    async getAllE2Article(TAG, p_nr){
        let articles = [];
        let result = [];

        try {
            /** Holt alle Artikel die einen eBay-2 Preis/ Megen Eintrag haben. */
            let access = this.db.getE2DataAccess();
            articles = await access.getE2Article(p_nr);

            if (articles === null) {
                let message = 'Es wurde NULL zurück gegeben';
                await logger.err(TAG, 'E2 - Abbruch', message);

                return {successful: false, result: result, message: message};
            }

            // logger.log(TAG
            //     , 'E2-Artikel erhalten'
            //     , `Es wurden ${articles.length} E2-Artikel von Lynn erhalten. Daten werden aufbereitet.`
            // );
            //
            // for (let currentArticle of articles) {
            //     let item = {
            //         a_nr_original: currentArticle['a_nr'],
            //         a_nr : 'E2-' + currentArticle['a_nr'],
            //         a_nr2 : 'E2-' + currentArticle['a_nr2'],
            //         p_nr: currentArticle['p_nr'],
            //         a_prod_nr: currentArticle['a_prod_nr'],
            //         a_cond: currentArticle['a_cond'],
            //         a_is_blocked: false,
            //         a_delivery_time: 2, // currentArticle['a_delivery_time'],
            //         a_ean: 0,
            //         channel: 'ebde',
            //         price: currentArticle['eBay2Preis'],
            //         quant: currentArticle['eBay2Menge'],
            //     }
            //     result.push(item);
            // }
        } catch (e) {
            await logger.err(TAG, 'Abbruch', e.message);

            return {successful: false, result: articles, message: e.message};
        }

        return {successful: true, result: articles};
    }

    async getAllANrFromProductNr(TAG, p_nr) {
        let access = this.db.getE2DataAccess();
        let result = null;
        try {
            result = await access.getAllArticleFromProduct(p_nr);

            return {successful: true, result: result};
        } catch (e) {
            await logger.err(TAG, 'Abbruch', e.message);

            return {successful: false, result: result, message: e.message};
        }
    }
}

module.exports = Index