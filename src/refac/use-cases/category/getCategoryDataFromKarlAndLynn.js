'use strict'

const logger = require("../../../helper/util/logger.util");
const dbConstants = require("../../constants");
const DB = require("../../data-access");
const RepoCNetCategory2LynnAndShopCategory = require("../../../repository/repo.cNetCategory2LynnAndShopCategory");

class Index {
    constructor(tag) {
        this.TAG = tag;
    }

    async getAllCategoryData() {
        let result = {};
        /** Ermittle alle Produkte und die zugehörenden Kategorie-Id´s aus Karl */
        await logger.log(this.TAG,
            'Kategorie-Id´s aus Karl',
            'Ermittle alle Produkte und die zugehörenden Kategorie-Id´s aus Karl'
        );

        result.karlCategoryData = await this.getCategoryData(dbConstants.KARL);

        await logger.log(this.TAG,
            'Kategorie-Id´s aus Karl',
            `${result.karlCategoryData.count} Produkte aus Karl ermittelte.`
        );

        /** Ermittle alle Produkte und die zugehörenden Kategorie-Id´s aus Lynn */
        await logger.log(this.TAG,
            'Kategorie-Id´s aus Lynn',
            'Ermittle alle Produkte und die zugehörenden Kategorie-Id´s aus Lynn'
        );

        result.lynnCategoryData = await this.getCategoryData(dbConstants.LYNN);

        await logger.log(this.TAG,
            'Kategorie-Id´s aus Lynn',
            `${result.lynnCategoryData.count} Produkte aus Lynn ermittelte.`
        );

        /** Ermittelt alle Lynn und Shop Kategorien aus Karl. */
        let repoCNetCategory2LynnAndShopCategory = new RepoCNetCategory2LynnAndShopCategory();
        let categoryData = await repoCNetCategory2LynnAndShopCategory.getAllLynnCategoryAndShopCategories();

        let resultPrepareCategoryArray = await this.getPrepareCategoryArray(categoryData);
        result.lynnAndShopCategoryData = resultPrepareCategoryArray.lynnAndShopCategoryData;
        result.shopToLynnAndViceVersa = resultPrepareCategoryArray.shopToLynnAndViceVersa;

        return result;
    }

    /**
     * Gibt die p_nr <--> cat_id Kombination zurück
     * */
    async getCategoryData(dbConst) {
        let db = new DB(dbConst);
        let access = db.getCategoryAccess();
        let data = {};
        if (access.successful) {
            data = await access.access.getAllProductCategory()
        } else {
            await logger.err(this.TAG,
                `Fehler bei Zugriff auf ${dbConst}-DB`,
                `Es konnten keine Kategorie Daten gelesen und zurück gegeben werden.`,
                `Die Datenbank Verbindung ist nicht sichergestellt.`,
            )
        }
        let result = {
            count: data.length !== undefined ? data.length : 0,
            data: await this.getPrepareArray(data)
        }
        return  result;
    }

    /**
     * Bereitet die Daten auf.
     * Gibt ein Array zurück, was die Produktnummer als Key besitzt.
     * */
    async getPrepareArray(data) {
        let result = []
        for (let index in data) {
            result['p_nr-' + data[index].p_nr] = {
                lynnCategoryId: data[index].lynnCategoryId === null ? null : data[index].lynnCategoryId + '',
                shopCategoryId: data[index].shopCategoryId === null ? null : data[index].shopCategoryId + '',
                productId: data[index].productId,
                p_nr: data[index].p_nr,
                a_nr: data[index].a_nr
            };
        }

        return result
    }

    async getPrepareCategoryArray(categories) {
        let result = {};
        let lynnAndShopCategoryData = [];
        let shopToLynnAndViceVersa = [];
        shopToLynnAndViceVersa['ShopId-null'] = 'null';
        shopToLynnAndViceVersa['LynnId-null'] = 'null';

        for (let category of categories) {
            /** Namen auflösen */
            if (lynnAndShopCategoryData['Shop-' + (category.getDataValue('shopCategoryId') !== null ? category.getDataValue('shopCategoryId') : 'null')] === undefined) {
                lynnAndShopCategoryData['Shop-' + (category.getDataValue('shopCategoryId') !== null ? category.getDataValue('shopCategoryId') : 'null')] = category; //.getDataValue('shopCategory');
            }
            if (lynnAndShopCategoryData['Lynn-' + (category.getDataValue('lynnCategoryId') !== null ? category.getDataValue('lynnCategoryId') : 'null')] === undefined) {
                lynnAndShopCategoryData['Lynn-' + (category.getDataValue('lynnCategoryId') !== null ? category.getDataValue('lynnCategoryId') : 'null')] = category; //.getDataValue('lynnCategory');
            }

            /** IDs auflösen */
            if (shopToLynnAndViceVersa['ShopId-' + (category.getDataValue('shopCategoryId') !== null ? category.getDataValue('shopCategoryId') : 'null')] === undefined) {
                shopToLynnAndViceVersa['ShopId-' + (category.getDataValue('shopCategoryId') !== null ? category.getDataValue('shopCategoryId') : 'null')]
                    = (category.getDataValue('lynnCategoryId') !== null ? category.getDataValue('lynnCategoryId') : 'null');
            }
            if (shopToLynnAndViceVersa['LynnId-' + (category.getDataValue('lynnCategoryId') !== null ? category.getDataValue('lynnCategoryId') : 'null')] === undefined) {
                shopToLynnAndViceVersa['LynnId-' + (category.getDataValue('lynnCategoryId') !== null ? category.getDataValue('lynnCategoryId') : 'null')]
                    = (category.getDataValue('shopCategoryId') !== null ? category.getDataValue('shopCategoryId') : 'null');
            }
        }

        result.lynnAndShopCategoryData = lynnAndShopCategoryData;
        result.shopToLynnAndViceVersa = shopToLynnAndViceVersa;

        return result
    }
}

module.exports = Index;