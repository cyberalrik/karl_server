'use strict';

const DB = require('../../data-access');
const dbConstants = require('../../constants');
const UseCaseShopCategoryFromLynnEdit = require('./shopCategoryFromLynnEdit');
const UseCaseLynnCategoryFromLynnEdit = require('./lynnCategoryFromLynnEdit');
const UseCaseGetCategoryDataFromKarlAndLynn = require("./getCategoryDataFromKarlAndLynn");
const UseCaseCreateInfo = require('./createInfo');
const logger = require("../../../helper/util/logger.util");


class Index {
    constructor(tag) {
        this.TAG = tag;
        let db = new DB(dbConstants.LYNN);
        this.dbAccess = db.getCategoryAccess();

    }

    async matchingCategory() {
        /** Ermitteln der gegenwärtigen KategorieDaten aus Karl und Lynn */
        let useCaseGetCategoryDataFromKarlAndLynn = new UseCaseGetCategoryDataFromKarlAndLynn(this.TAG);
        let categoryData = await useCaseGetCategoryDataFromKarlAndLynn.getAllCategoryData();

        /**
         * Alle geänderten Shop-Kategorien aus Lynn übernehmen,
         * in Karl aktualisieren und die passende Lynn-Kategorie in Lynn setzen
         * */
        let useCaseShopCategoryFromLynnEdit = new UseCaseShopCategoryFromLynnEdit(this.TAG, categoryData);
        let result = await useCaseShopCategoryFromLynnEdit.processChangesToTheShopCategoryInLynn();

        if (result.modifyData) {
            await logger.log(this.TAG,
                'KategorieId´s aus Karl',
                'Ermittle erneut die KategorieId´s aus Karl'
            );

            categoryData = await useCaseGetCategoryDataFromKarlAndLynn.getAllCategoryData();
        }

        /**
         * Wurde, bei einem Artikel der keiner Kategorie zugeordnet ist, eine Lynn-Kategorie gesetzt, so wird,
         * die Standard Shop-Kategorie der ausgewählten Lynn-Kategorie, in Karl und Lynn übernommen.
         * */
        let useCaseLynnCategoryFromLynnEdit = new UseCaseLynnCategoryFromLynnEdit(this.TAG, categoryData, result);
        result = await useCaseLynnCategoryFromLynnEdit.processChangesToTheLynnCategoryInLynn();
        result.messages = 'Alles super durch gelaufen. Wenn du mehr erfahren möchtest, gucke in die Logs oder deinen eMails.';

        if (result.mailData.length > 0) {
            let useCaseCreateInfo = new UseCaseCreateInfo(this.TAG, {})
            await useCaseCreateInfo.createReportMail(result.mailData);
            logger.log(this.TAG,
                'versende eMail',
                'Ein Report über geänderte Kategorien wurde per eMail versand.'
            );
        }

        return result
    }
}

module.exports = Index;