'use strict';

const DB = require('../../../refac/data-access');
const dbConstants = require('../../../refac/constants');
const logger = require('../../../helper/util/logger.util');

class ShopCategory {
    constructor(tag) {
        this.TAG = tag
    }

    /**
     * Übergibt die einzelnen Daten, um die Shop-Kategorie in Karl zu setzen
     * */
    async setShopCategoryInKarl(shopCatDiffToKarl) {
        let successful = false;
        for (let index in shopCatDiffToKarl) {
            successful = await this.setShopCategory(
                shopCatDiffToKarl[index].p_nr,
                shopCatDiffToKarl[index].shopCategoryId
            );

            if (!successful) {
                break
            }
        }

        return successful;
    }

    /**
     * Setzt in Karl, anhand der übergebenen shopCategoryId, die p_cat_id im Produkt p_nr
     * */
    async setShopCategory(p_nr, shopCategoryId) {
        // setze cNetCatId in Karl-Produkt
        let dbConst = dbConstants.KARL;
        let db = new DB(dbConst);
        let result = db.setCategoryAccess();
        if (result.successful) {
            await result.access.setShopCategoryIdInKarl(p_nr, shopCategoryId);
            await logger.log(this.TAG,
                'Shop-Kategorie geändert',
                `Für das Produkt ${p_nr} wurde die ShopKategorie-Id ${shopCategoryId} in Karl gesetzt.`)
        } else {
            await logger.err(this.TAG,
                `Fehler bei Zugriff auf ${dbConst}-DB`,
                `Es konnten keine Shop-Kategorie Daten geschrieben werden.`,
                `Die Datenbank Verbindung ist nicht sichergestellt.`,
            )
        }

        return result.successful;
    }
}

module.exports = ShopCategory;