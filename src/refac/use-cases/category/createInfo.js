'use strict';

const logger = require("../../../helper/util/logger.util");
const MailData = require("../../entity/mailData");

class Index {
    constructor(tag, lynnAndShopCategoryData) {
        this.TAG = tag;
        this.lynnAndShopCategoryData = lynnAndShopCategoryData
    }

    async getListOfChangeCategories (products) {
        let result = [];
        let p_nrList = [];
        let p_nrListHTML = [];
        let importFileData = ['Artikelnummer;LynnKategorie;ShopKategorie'];
        for (let index in products) {
            let product = products[index];
            /** Text-Format */
            let info = `p_nr: ${
                product.p_nr} alte ShopCategoryId: '${
                this.lynnAndShopCategoryData['Shop-' + product.oldShopCategoryId] !== undefined
                    ? this.lynnAndShopCategoryData['Shop-' + product.oldShopCategoryId].getDataValue('shopCategory')
                    : 'unbekannt'
            }' (${product.oldShopCategoryId}),  neue ShopCategoryId: '${
                this.lynnAndShopCategoryData['Shop-' + product.shopCategoryId] !== undefined
                    ? this.lynnAndShopCategoryData['Shop-' + product.shopCategoryId].getDataValue('shopCategory')
                    : 'unbekannt'
            }' (${product.shopCategoryId})`;

            p_nrList.push(info);

            /** Import-Format */
            let importData = `${product.a_nr
            };${this.lynnAndShopCategoryData['Lynn-' + product.oldLynnCategoryId].lynnCategory
            };${this.lynnAndShopCategoryData['Lynn-' + product.oldLynnCategoryId].shopCategory}`;

            importFileData.push(importData);

            /** HTML-Format */
             let infoHTML = `<tr>
<!-- Shop-Category-->
                    <td class="numbers">${product.p_nr}</td>
                    ${product.shopCategoryId === product.oldShopCategoryId
                 ? '<td class="zentriert" colspan="2"> * unverändert * </td>'
                 + (this.lynnAndShopCategoryData['Shop-' + product.shopCategoryId] !== undefined
                     ? '<td>' + this.lynnAndShopCategoryData['Shop-' + product.shopCategoryId].getDataValue('shopCategory') + '</td>'
                     + '<td class="numbers">' + product.shopCategoryId + '</td>'
                     : '<td class="zentriert" colspan="2">unbekannt-1</td>')

                 : this.lynnAndShopCategoryData['Shop-' + product.oldShopCategoryId] !== undefined
                     ? '<td>' + this.lynnAndShopCategoryData['Shop-' + product.oldShopCategoryId].getDataValue('shopCategory') + '</td>'
                     + '<td class="numbers">' + product.oldShopCategoryId + '</td>'
                     // <!-- Zweite Abfrage muss in () gesetzt werden -->
                     + (this.lynnAndShopCategoryData['Shop-' + product.shopCategoryId] !== undefined
                         ? '<td>' + this.lynnAndShopCategoryData['Shop-' + product.shopCategoryId].getDataValue('shopCategory') + '</td>'
                         + '<td class="numbers">' + product.shopCategoryId + '</td>'
                         : '<td class="zentriert" colspan="2">unbekannt-2</td>')

                     : '<td class="zentriert" colspan="2">unbekannt-3</td>'
             }
<!-- Lynn-Category--> <td>|</td>
                    ${product.lynnCategoryId === product.oldLynnCategoryId
                 ? '<td class="zentriert" colspan="2"> * unverändert * </td>'
                 + (this.lynnAndShopCategoryData['Lynn-' + product.lynnCategoryId] !== undefined
                     ? '<td>' + this.lynnAndShopCategoryData['Lynn-' + product.lynnCategoryId].getDataValue('lynnCategory') + '</td>'
                     + '<td class="numbers">' + product.lynnCategoryId + '</td>'
                     : '<td class="zentriert" colspan="2">unbekannt-4</td>')
                 : this.lynnAndShopCategoryData['Lynn-' + product.oldLynnCategoryId] !== undefined
                     ? '<td>' + this.lynnAndShopCategoryData['Lynn-' + product.oldLynnCategoryId].getDataValue('lynnCategory') + '</td>'
                     + '<td class="numbers">' + product.oldLynnCategoryId + '</td>'
                     // <!-- Zweiter Wert muss in () gesetzt werden -->
                     + (this.lynnAndShopCategoryData['Lynn-' + product.lynnCategoryId] !== undefined
                         ? '<td>' + this.lynnAndShopCategoryData['Lynn-' + product.lynnCategoryId].getDataValue('lynnCategory') + '</td>'
                         + '<td class="numbers">' + product.lynnCategoryId + '</td>'
                         : '<td class="zentriert" colspan="2">unbekannt-5</td>')

                     : '<td class="zentriert" colspan="2">unbekannt-6</td>'
             }
</tr>`;

            p_nrListHTML.push(infoHTML);
        }
        result.text = p_nrList;
        result.importFileData = importFileData;
        result.html = p_nrListHTML;

        return result;
    }

    async generateMailDataForCategoryChange(data, headlineText, BackgroundColor = '#ffffff', importText = false) {
        let result = {successful: false, number: data.text.length}

        let headline = `<div><h3 style="Background-color: ${BackgroundColor};">${headlineText}</h3></div>`;
        let tableHead = `<tr><th>Karl-Nr.</th><th>alte Shop-Kategorie</th><th>alte ID</th><th>neue Shop-Kategorie</th><th>neue ID</th><th>|</th>
                                                     <th>alte Lynn-Kategorie</th><th>alte ID</th><th>neue Lynn-Kategorie</th><th>neue ID</th></tr>`;
        // let footer = `<tr><td colspan="5" style="color: #002aff;">Farbige Werte sind Änderungen.</td></tr>`;
        let newCategoryHTML = `<tr><td>${headline}<table style="Background-color: ${BackgroundColor};">${tableHead}${data.html.join(`\n`)}</table></td></tr>`;
        let newCategoryText = data.text.join(`\n`);

        if (data.text.length > 0) {
            result.messageText = `Folgende Änderungen wurden übernommen:\n${newCategoryText}\n\n`;
            result.messageHTML = newCategoryHTML;
            result.successful = true;
            result.fileText = '';
            result.createImportFile = false;
            if (importText) {
                result.fileText = data.importFileData;
                result.createImportFile = true;
            }
        }

        return result
    }

    async createReportMail(mailDataCollection) {
        let number = 0;
        let messageText = [];
        let messageHTML = [];
        let fileText = '';
        let createImportFile = false
        for (let index in mailDataCollection) {
            number += mailDataCollection[index].number;
            messageText.push(mailDataCollection[index].messageText);
            messageHTML.push(mailDataCollection[index].messageHTML);

            if (mailDataCollection[index].createImportFile) {
                fileText = mailDataCollection[index].fileText;
                createImportFile = true;
            }
        }

        /** Daten in MailData Object eintragen über Konstruktor */
        let mailData = new MailData(
            `Es sind ${number} Produkte in Lynn einer anderen ShopKategorie zugeordnet wurden.`,
            `Für folgende p_nr wurden neue Shop-Kategorien in Karl von Lynn übernommen.\n${messageText.join('\n')}`,
            'produktmanagement@cybertrading.de,s.wiehe@cybertrading.de',
            // null,
            null,
            true,
            messageHTML.join('\n',)
        );

        if (createImportFile) {
            await mailData.setAttachments([{   // binary buffer as an attachment
                filename: 'Update Kategorien (SW).csv',
                content: Buffer.alloc(fileText.join('\r\n').toString('Latin1').length, fileText.join('\r\n'), 'Latin1')
            }]);
        }

        await logger.log(this.TAG,
            `Es wurden Produktkategorien angepasst und korrigiert.`,
            `Es wurden ${number} Kategorien angepasst`,
            mailData);
    }
}

module.exports = Index;