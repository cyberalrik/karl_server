'use strict'

const logger = require("../../../helper/util/logger.util");
const UseCaseSetCategoryInLynn = require("./setCategoryInLynn");
const UseCaseCreateInfo = require('./createInfo');
const RepoCNetCategory2LynnAndShopCategory = require("../../../repository/repo.cNetCategory2LynnAndShopCategory");
const repoProduct = require("../../../repository/repo.product");
const dbConstants = require("../../constants");
const DB = require("../../data-access");

class Index {
    constructor(tag, categoryData, result) {
        this.TAG = tag;
        this.categoryData = categoryData;
        this.result = result;

        let db = new DB(dbConstants.LYNN);
        this.dbAccess = db.getCategoryAccess();
    }

    async processChangesToTheLynnCategoryInLynn() {
        /**
         * Lynn Kategorien werden nur übernommen, wenn das Produkt in Karl in der standard Kategorie "CY0001" zugeordnet ist.
         * Ansonsten wird die Kategorie aus Karl wieder benutzt.
         *
         * 1. Welche Produkte aus der standard Kategorie "CY0001" wurden in Lynn einer anderen Kategorie zugeordnet?
         *    Diese müssen dann in Karl, nach bestimmten Bedingungen, übernommen werden.
         * 2. Welche Produkte, die in Karl nicht in der standard Kategorie "CY0001" zugeordnet sind, wurde geändert?
         *    Diese müssen wieder zurückgeändert werden.
         * */
        /** 1. */
        await logger.log(this.TAG,
            'Kategorie differenzen bilden',
            `Es werden Kategorie Differenzen aus ${this.categoryData.karlCategoryData.count} Karl und ${
            this.categoryData.lynnCategoryData.count} Lynn Produkte gebildet.`
        );

        let resultDifferencesKategorie = await this.findOutDifferencesFromTheCategory(
            this.categoryData.karlCategoryData.data,
            this.categoryData.lynnCategoryData.data
        );
        let resetKategorieInLynn = resultDifferencesKategorie.reset;
        let changeKategorieOfKarlAndLynn = resultDifferencesKategorie.change;

        await logger.log(this.TAG,
            `Kategorie Differenzen`,
            `Es wurden ${resetKategorieInLynn.length + changeKategorieOfKarlAndLynn.length} Kategorie Differenzen festgestellt.`
        );

        let useCaseSetCategoryInLynn = new UseCaseSetCategoryInLynn(this.TAG, this.dbAccess);

        /**
         * Lynn Kategorien dürfen nur aus einer leeren Anzeige, also nicht kategorisiert, einer Kategorie zugewiesen werden.
         * Wird in Lynn die Lynn-Kategorie gewechselt, ist das nicht erlaubt und wird hiermit wieder zurückgesetzt.
         * */
        if (resetKategorieInLynn.length > 0) {
            /** ACHTUNG!!! Wieder aktivieren */
            await useCaseSetCategoryInLynn.setCategory(resetKategorieInLynn);
            await logger.log(this.TAG,
                'Differenzen korrigiert',
                `${resetKategorieInLynn.length} Lynn-Kategorien mussten wieder zurück gesetzt werden.`
            );

            let useCaseCreateInfo = new UseCaseCreateInfo(this.TAG, this.categoryData.lynnAndShopCategoryData);
            let mailDataNew = await useCaseCreateInfo.getListOfChangeCategories(resetKategorieInLynn)
            let resultGenerateData = await useCaseCreateInfo.generateMailDataForCategoryChange(mailDataNew
                , 'Produktart (Lynn-Kategorie): Produkte, mit gesetzter Shop-Kategorie, sollen durch setzen einer Lynn-Kategorie geändert werden. ' +
                'Das geht so nicht! Die Lynn-Kategorien wird in Lynn wieder zurückgesetzt.'
                , '#f6d0e0'
                , true);

            if (resultGenerateData.successful) {
                this.result.mailData[3] = resultGenerateData;
            }
        }

        /**
         * Produkte, bei denen eine Lynn-Kategorie zugeordnet wurde, die zuvor nicht kategorisiert waren.
         * Anhand der Lynn-Kategorie muss die Shop-Kategorie in Karl und in Lynn geändert werden.
         * */
        let resultCall = await this.setLynnCategoryInKarlAndReturnShopCategoryForLynn(changeKategorieOfKarlAndLynn);
        let changeCategoryInLynn = resultCall.changeShopCategoryInLynn;
        let errorChangeCategory = resultCall.errorCategory;
        await this.reportIncorrectData(errorChangeCategory);

        if (changeCategoryInLynn.length > 0) {
            /** geänderte LynnKategorien in Lynn wieder zurücksetzen */
            /** ACHTUNG!!! Wiedere aktivieren */
            await useCaseSetCategoryInLynn.setCategory(changeCategoryInLynn);

            let useCaseCreateInfo = new UseCaseCreateInfo(this.TAG, this.categoryData.lynnAndShopCategoryData);
            let mailDataNew = await useCaseCreateInfo.getListOfChangeCategories(changeKategorieOfKarlAndLynn)
            let resultGenerateData = await useCaseCreateInfo.generateMailDataForCategoryChange(mailDataNew
                , 'Produktart (Lynn-Kategorie): Produkte, bei denen eine Lynn-Kategorie zugeordnet wurde, die zuvor nicht kategorisiert waren. ' +
                'Anhand der Lynn-Kategorie muss die Shop-Kategorie in Karl und in Lynn geändert werden.'
                , '#def1cd'
                , false);

            if (resultGenerateData.successful) {
                this.result.mailData[0] = resultGenerateData;
            }
        }

        return this.result;
    }

    /**
     * Konnte keine Standard-Kategorie in Karl ermittelt werden, bitte melden.
     * */
    async reportIncorrectData(errorChangeCategory) {
        if (errorChangeCategory.length > 0) {
            let dataList = [];
            for (let index in errorChangeCategory) {
                let currentData = errorChangeCategory[index];
                dataList.push(`p_nr: ${currentData.p_nr}, productId: ${currentData.productId}, lynnCategoryId: ${currentData.lynnCategoryId}`);
            }
            let data = dataList.join(`\n`);

            logger.err(this.TAG,
                `Standardkategorie fehlt`,
                `Eine Markierung einer Kategorie als Standard fehlt. Info per Mail ist raus`,
                `Es geht um die Produkte/ Kategorien: \n${data}`
            );
        }
    }

    /**
     * Produkte, bei denen aus der standard Lynn-Kategorie eine ander ausgewählt wurde,
     * muss in Karl und die Shop-Kategorie in Lynn geändert werden.
     *
     * @param changeKategorieOfKarlAndLynn
     * @return {Promise<Object>}
     * */
    async setLynnCategoryInKarlAndReturnShopCategoryForLynn(changeKategorieOfKarlAndLynn) {
        let result = {};
        let errorCategory = [];
        let changeShopCategoryInLynn = [];
        let repoCNetCategory2LynnAndShopCategory = new RepoCNetCategory2LynnAndShopCategory();
        for (let index in changeKategorieOfKarlAndLynn) {
            let currentProduct = changeKategorieOfKarlAndLynn[index];

            /**
             * Wenn verfügbar, gib mir die standard shopCategoryId von der LynnCategoryId zurück,
             * ansonsten mull.
             * */
            let result = await repoCNetCategory2LynnAndShopCategory.getDefaultLynnCategoryId(currentProduct.lynnCategoryId);
            if (result === null) {
                errorCategory.push(currentProduct);
                continue;
            }
            currentProduct.p_cat_name = result.getDataValue('shopCategory');
            currentProduct.p_cat_id = result.getDataValue('p_cat_id');
            currentProduct.shopCategoryId = result.getDataValue('shopCategoryId');

            /** setzt die neu p_cat_id im Karl-Product */
            await repoProduct.setNewCategory(currentProduct);

            changeShopCategoryInLynn.push(currentProduct);
        }

        result.changeShopCategoryInLynn = changeShopCategoryInLynn;
        result.errorCategory = errorCategory;

        return result;
    }

    /**
     * Lynn-Kategorie
     *
     * 1. Welche Produkte aus der standard Kategorie "CY0001" wurden in Lynn einer anderen Kategorie zugeordnet?
     * Diese müssen dann in Karl nach bestimmten Bedingungen übernommen oder in Lynn zurückgesetzt werden.
     * @param karlData
     * @param lynnData
     * @return {Promise<Object>}
     * */
    async findOutDifferencesFromTheCategory(karlData, lynnData) {
        let result = {reset: [], change: []};
        let reset = [];
        let change = [];
        for (let karlIndex in karlData) {
            let currentLynnData = lynnData[karlIndex];
            let currentKarlData = karlData[karlIndex];

            /**
             * 1. currentLynnData ist gesetzt
             * 2. lynnCategoryId ist in Lynn und Karl unterschiedlich
             * */
            if (
                currentLynnData !== undefined
                &&
                currentKarlData.lynnCategoryId !== currentLynnData.lynnCategoryId
            ) {
                /**
                 * 3. Die lynnCategoryId in Karl ist die standard lynnCategoryId 'Sonstiges' = NULL (alt= '22'),
                 * jetzt auch noch die lynnCategoryId 'unbekannt' = 1079.
                 * Wenn sie anders ist, dann sammeln, damit sie wieder zurückgesetzt werden kann.
                 */
                if (
                    (currentKarlData.lynnCategoryId === null || currentKarlData.lynnCategoryId === '1079')//'22' oder 1079=In Lynn unbekannt
                    && currentLynnData.lynnCategoryId !== null
                ) {
                    /**
                     * Eine standard Kategorie wurde geändert,
                     * diese muss in Karl eingetragen werden und die Shop-Kategorie an Lynn zurückgegeben werden.
                     * */

                    change.push({
                        p_nr: currentKarlData.p_nr,
                        a_nr: currentLynnData.a_nr,
                        shopCategoryId:
                            this.categoryData.shopToLynnAndViceVersa['LynnId-' + currentLynnData.lynnCategoryId] !== undefined
                            ? this.categoryData.shopToLynnAndViceVersa['LynnId-' + currentLynnData.lynnCategoryId]
                            : null,

                        oldShopCategoryId: currentKarlData.shopCategoryId,
                        lynnCategoryId: currentLynnData.lynnCategoryId,
                        oldLynnCategoryId: currentKarlData.lynnCategoryId,
                        productId: currentLynnData.productId,
                        message: 'Eine standard Kategorie wurde geändert,' +
                            'diese muss in Karl eingetragen werden und die Shop-Kategorie an Lynn zurückgegeben werden.'
                    })
                } else {
                    /**
                     * Eine in Karl gesetzte (vorhandene) Lynn-Kategorie wurde geändert, das soll nicht sein.
                     * Sie muss wieder zurückgesetzt werden
                     * */
                    reset.push({
                        p_nr: currentKarlData.p_nr,
                        a_nr: currentLynnData.a_nr,
                        shopCategoryId: currentKarlData.shopCategoryId,
                        oldShopCategoryId: currentKarlData.shopCategoryId,
                        lynnCategoryId: currentKarlData.lynnCategoryId,
                        oldLynnCategoryId: currentLynnData.lynnCategoryId,
                        productId: currentLynnData.productId,
                        message: 'Eine in Karl gesetzte (vorhandene) Lynn-Kategorie wurde geändert, das soll nicht sein.' +
                            'Sie muss wieder zurückgesetzt werden'
                    })
                }
            }
        }
        result.reset = reset;
        result.change = change;

        return result;
    }
}

module.exports = Index;