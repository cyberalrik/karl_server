'use strict'

const logger = require("../../../helper/util/logger.util");
const MailData = require("../../entity/mailData");

class Index {
    constructor(tag, lynnDbAccess ) {
        this.TAG = tag;
        this.lynnDbAccess = lynnDbAccess
    }
    /**
     * Baut ein Query mit CASE zusammen.
     *
     * Wird aber im CASE alles auf "NULL" gesetzt,
     * währt sich die DB dagegen und es muss ein einfaches "set ... = NULL" erzeugt werden.
     * */
    async setCategory(categoryDifference) {
        if (this.lynnDbAccess.successful) {
            /** So kann auch im Bulk alles andere auf NULL weisen. */
            let lynnCategoryCase = [`WHEN 0 THEN 0`];
            let shopCategoryCase = [`WHEN 0 THEN 0`];
            let whereArray = [];
            let number = 0;
            let count = 0;

            let longMessage = `Es müssen ${categoryDifference.length} Kategorien in Lynn korrigiert werden.`;

            if (categoryDifference.length > 200) {
                longMessage = `Es müssen insgesamt ${categoryDifference.length
                } Kategorien (Lynn/ Shop) in Lynn korrigiert werden. Dies geschieht in 200er Schritte.`;
            }

            await logger.log(this.TAG,
                `${categoryDifference.length} Kategorien korrigieren`,
                longMessage + ''
            );

            for (let index in categoryDifference) {
                number++;
                count++;
                let currentData = categoryDifference[index];
                lynnCategoryCase.push(`WHEN ${currentData.productId} THEN ${currentData.lynnCategoryId}`)
                shopCategoryCase.push(`WHEN ${currentData.productId} THEN ${currentData.shopCategoryId}`)
                whereArray.push(currentData.productId);

                if (number === 200) {
                    await logger.log(this.TAG,
                        'Kategorien korrigieren',
                        `Es werden 200, von schon ${count} bearbeiteten, Kategorien in Lynn korrigiert/ eingetragen. (${categoryDifference.length})`
                    );
                    let bulkSql = await this.createBulkSql(lynnCategoryCase, shopCategoryCase, whereArray);
                    await this.setLynnCategoryInLynnDb(bulkSql);

                    /** Variablen müssen wieder zurückgesetzt werden, Daten wurden weggeschrieben */
                    lynnCategoryCase = [`WHEN 0 THEN 0`]
                    shopCategoryCase = [`WHEN 0 THEN 0`]
                    whereArray = [];
                    number = 0;
                }
            }
            /** Für alle die über geblieben sind und nicht in der maximalen begrenzung reingelaufen sind. */
            if (whereArray.length > 0) {
                await logger.log(this.TAG,
                    'Kategorien korrigieren',
                    `Es werden ${whereArray.length}, von schon ${count} bearbeiteten, Kategorien in Lynn korrigiert/ eingetragen. (${categoryDifference.length})`
                );
                let bulkSql = await this.createBulkSql(lynnCategoryCase, shopCategoryCase, whereArray);
                await this.setLynnCategoryInLynnDb(bulkSql);
            }
        } else {
            await logger.err(this.TAG,
                `Fehler bei Zugriff auf DB`,
                `Es konnten keine Kategorie Änderungen in die Lynn DB geschrieben werden`,
                `Die Datenbank Verbindung ist nicht sichergestellt.`,
            )
        }
    }

    /**
     * Erstellt das SQL-Skript aus den zugewiesenen Daten.
     *
     * @param {*[]} lynnCategoryCase
     * @param {*[]} shopCategoryCase
     * @param {{}} whereArray
     * */
    async createBulkSql(lynnCategoryCase, shopCategoryCase, whereArray) {
        let lynnCategoryCaseString = 'LynnKategorie = CASE Id ' + lynnCategoryCase.join(' ') + ' END';
        let shopCategoryCaseString = 'ShopKategorie = CASE Id ' + shopCategoryCase.join(' ') + ' END';
        let whereString = whereArray.join(', ');
        let sql = `UPDATE [erp].[Product] SET ${lynnCategoryCaseString}, ${shopCategoryCaseString} WHERE Id IN (${whereString})`;

        let mailData = new MailData(
            `Es wurden bei ${whereArray.length} Produkten die Kategorie geändert/ korrigiert.`
            ,`Das Query, was benutzt wurde, war folgendes:\n${sql}`
        );
        // Es geht auch so. :)
        // await mailData.setAddMailTo('a.linde@cybertrading.de');
        // await mailData.setSubject(`Es wurden bei ${whereArray.length} Produkten die Kategorie geändert/ korrigiert.`)
        // await mailData.setMailBody(`Das Query, was benutzt wurde, war folgendes:\n${sql}`);
        await logger.log(this.TAG,
            `Kategorien korrigiert`,
            `Es wurden bei ${whereArray.length} Produkten die Kategorie geändert/ korrigiert.`,
            mailData);

        return sql;
    }

    async checkDataFromNull(data, category) {
        let bulkOk = false;
        for (let index in data){
            if (data[index][category] !== null) {
                bulkOk = true;
                break;
            }
        }

        return bulkOk;
    }

    async setLynnCategoryInLynnDb(sql) {
        if (this.lynnDbAccess.successful) {
            /** Aus Sicherheitsgründen 1 sec warten bis es wieder losgeht. */
            await new Promise(resolve => setTimeout(resolve, 1000));

            try {
                /** Übergeben eines SQL-Skripts zum Ausführen */
                await this.lynnDbAccess.access.runUpdateQuery(sql);
            } catch (e) {
                await logger.err(this.TAG,
                    `Fehler bei Zugriff auf Lynn-DB`,
                    `Es konnten keine Kategorie Änderungen in die Lynn DB geschrieben werden`,
                    `Die Fehlermeldung lautet: \n${e.message
                }\nDie Datenbank Verbindung ist nicht sichergestellt. \nDas Query ist:\n${sql}`)
            }
        } else {
            await logger.err(this.TAG,
                `Fehler bei Zugriff auf Lynn-DB`,
                `Es konnten keine Kategorie Änderungen in die Lynn DB geschrieben werden`,
                `Die Datenbank Verbindung ist nicht sichergestellt. \nDas Query ist:\n${sql}`,
            )
        }
    }
}

module.exports = Index;