'use strict'

const DB = require("../../data-access");
const dbConstants = require("../../constants");
const logger = require("../../../helper/util/logger.util");
const UseCaseSetCategoryInKarl = require("./setCategoryInKarl");
const UseCaseSetCategoryInLynn = require('./setCategoryInLynn');
const UseCaseCreateInfo = require('./createInfo');

class Index {
    constructor(tag, categoryData) {
        this.TAG = tag;
        this.productCategoryFromKarl = categoryData.karlCategoryData.data;
        this.productCategoryFromLynn = categoryData.lynnCategoryData.data;
        this.lynnAndShopCategoryData = categoryData.lynnAndShopCategoryData;
        this.shopToLynnAndViceVersa = categoryData.shopToLynnAndViceVersa;

        let db = new DB(dbConstants.LYNN);
        this.dbAccess = db.getCategoryAccess();
    }

    /**
     * Gleicht die im Constructor übergebenen Kategorie-Daten und gleicht sie, wenn nötig, in Karl und Lynn an.
     * Wurden Daten angeglichen, wird TRUE zurückgegeben,
     * ansonsten FALSE.
     *
     * Damit wird festgelegt, ob die Kategorie Daten erneut eingelesen werden müssen.
     * */
    async processChangesToTheShopCategoryInLynn() {
        let result = {
            modifyData: false
            , mailData: []
        };
        /** Bei welchem Lynn Produkte weicht die ShopKategorie-Id in Karl ab und muss in Karl angepasst werden? */
        await logger.log(this.TAG,
            'ShopKategorie Vergleich',
            `Bei welchem Lynn Produkte weicht die ShopKategorie-Id in Karl ab und muß in Karl angepasst werden?`
        );

        let shopCatDiffToKarl = await this.getChangeShopCategory(this.productCategoryFromKarl, this.productCategoryFromLynn);
        let resetShopCategoryInLynn = shopCatDiffToKarl.reset;
        let changeShopCategoryInKarl = shopCatDiffToKarl.change;

        let useCaseCreateInfo = new UseCaseCreateInfo(this.TAG, this.lynnAndShopCategoryData);

        if (changeShopCategoryInKarl.length > 0) {
            logger.log(this.TAG,
                'Bericht aufbereiten',
                `Erstelle Bericht über die Änderung von ${changeShopCategoryInKarl.length} ShopKategorie in Lynn.`
            );

            result.modifyData = true;

            /** Wenn eine ShopKategorie in Lynn geändert wurde, sollen diese Daten in Karl übernommen werden. */
            let useCaseSetCategoryInKarl = new UseCaseSetCategoryInKarl(this.TAG)
            /** ACHTUNG!!! Wieder aktivieren */
            let successful = await useCaseSetCategoryInKarl.setShopCategoryInKarl(changeShopCategoryInKarl);
            // let successful = true;

            if (successful) {
                /**
                 * Die Lynn-Kategorie muss anhand der neuen Shop-Kategorie angepasst werden.
                 * Lynn-Kategorie ermitteln.
                 * Nicht wundern, die neuen IDs werden gesetzt und müssen nicht zurückgegeben werden.
                 * */
                await this.setAllLynnCategoryFromShopCategories(changeShopCategoryInKarl);
                let mailDataNew = await useCaseCreateInfo.getListOfChangeCategories(changeShopCategoryInKarl)
                let resultGenerateData = await useCaseCreateInfo.generateMailDataForCategoryChange(mailDataNew
                    , 'Produktkategorie (Shop-Kategorie): In Lynn wurde die Shop-Kategorie gesetzt/ geändert. ' +
                    'Sie muss in Karl übernommen werden. ' +
                    'Die zugehörigen Lynn-Kategorie wird in Lynn angepasst.'
                    , '#fdf8c9'
                    , false);

                if (resultGenerateData.successful) {
                    result.mailData[1] = resultGenerateData;
                }
            }


            let useCaseSetCategoryInLynn = new UseCaseSetCategoryInLynn(this.TAG, this.dbAccess);
            /** Lynn-Kategorie in Lynn setzen */
            /** ACHTUNG!!! Wieder aktivieren */
            await useCaseSetCategoryInLynn.setCategory(changeShopCategoryInKarl);

        } else {
            await logger.log(this.TAG,
                'ShopKategorie vergleich',
                `Es weichen keine ShopKategorie-Id's in Lynn von denen in Karl ab.`
            );
        }

        if (resetShopCategoryInLynn.length > 0) {
            await logger.log(this.TAG,
                `${resetShopCategoryInLynn.length} ShopKategorien zurück setzen`,
                `${resetShopCategoryInLynn.length} ShopKategorien müssen zurück, auf NULL oder alten Wert, gesetzt werden.`
            );
            result.modifyData = true

            let useCaseSetLynnCategory = new UseCaseSetCategoryInLynn(this.TAG, this.dbAccess);
            /** ACHTUNG!!! Wieder aktivieren */
            await useCaseSetLynnCategory.setCategory(resetShopCategoryInLynn);

            let mailDataNew = await useCaseCreateInfo.getListOfChangeCategories(resetShopCategoryInLynn)
            let resultGenerateData = await useCaseCreateInfo.generateMailDataForCategoryChange(mailDataNew
                , 'Produktkategorie (Shop-Kategorie): In Lynn ist die Shop-Kategorie auf NULL gesetzt wurden, ' +
                'obwohl in Karl eine Shop-Kategorie gesetzt ist. ' +
                'Alles in Lynn zurück setzen.'
                , '#cadff1'
                , false);
                // , '#cadff1'); blau

            if (resultGenerateData.successful) {
                result.mailData[2] = resultGenerateData;
            }
        }

        return result;
    }

    /**
     * Shop-Kategorie
     *
     * Welche Shop-Kategorien wurden in Lynn gesetzt und müssen in Karl angepasst werden?
     *
     * Zurück gegeben wird ein Object mit zwei enthaltenen Arrays.
     * In "reset" sind Produkte enthalten,
     * bei denen die Shop-Kategorie auf NULL zurückgesetzt werden müssen.
     * Oder es wurde bei bestehender Shop-Kategorie eine nicht passende Lynn-Kategorie gesetzt.
     * Die Lynn Produkte werden auf die Werte in diesem Array gesetzt.
     *
     * In "change" enthaltenen Produkte wurden geändert und müssen in Karl und in Lynn geändert werden.
     * Auch in Karl muss die Shop-Kategorie übernommen werden
     * und in Lynn muss die passende Lynn-Kategorie gesetzt werden.
     * */
    async getChangeShopCategory(karlData, lynnData) {
        let result = {};
        let change = [];
        let reset = [];
        for (let karlIndex in karlData) {
            let currentLynnData = lynnData[karlIndex];
            let currentKarlData = karlData[karlIndex];
            if (
                currentLynnData !== undefined
                /** In Lynn ist eine Kategorie gesetzt */
                && currentLynnData.shopCategoryId !== null
                && currentKarlData.shopCategoryId !== currentLynnData.shopCategoryId
            ) {
                /**
                 * In Lynn steht die Shop-Kategorie auf "Komponenten | Neuzugänge" = 1031
                 * und muss mit "NULL" überschrieben werden.
                 * */
                if (currentLynnData.shopCategoryId === '1031') {
                    reset.push({
                        p_nr: currentKarlData.p_nr,
                        a_nr: currentLynnData.a_nr,
                        shopCategoryId: null,
                        oldShopCategoryId: currentLynnData.shopCategoryId,
                        lynnCategoryId: null,
                        oldLynnCategoryId: currentLynnData.lynnCategoryId,
                        productId: currentLynnData.productId,
                        message: 'Shop-Kategorie: In Lynn steht die Shop-Kategorie auf "Komponenten | Neuzugänge" = 1031 ' +
                            'und muss mit "NULL" überschrieben werden. In Lynn so setzen, wie hier übergeben.'
                    })

                } else {
                    /** In Lynn wurde die Shop-Kategorie gesetzt/ geändert. Kategorie muss in Karl übernommen werden. */
                    change.push({
                        p_nr: currentKarlData.p_nr,
                        a_nr: currentLynnData.a_nr,
                        shopCategoryId: currentLynnData.shopCategoryId,
                        oldShopCategoryId: currentKarlData.shopCategoryId,
                        lynnCategoryId: currentKarlData.lynnCategoryId,
                        oldLynnCategoryId: currentKarlData.lynnCategoryId,
                        productId: currentLynnData.productId,
                        message: 'Shop-Kategorie: In Lynn wurde die Shop-Kategorie gesetzt/ geändert. ' +
                            'Shop-Kategorie muss in Karl übernommen werden.'
                    })
                }
            } else if (
                currentLynnData !== undefined
                /** In Lynn ist die Shop-Kategorie auf NULL gesetzt worden. */
                && currentLynnData.shopCategoryId === null
                && currentKarlData.shopCategoryId !== null
            ) {

                reset.push({
                    p_nr: currentKarlData.p_nr,
                    a_nr: currentLynnData.a_nr,
                    shopCategoryId: currentKarlData.shopCategoryId,
                    oldShopCategoryId: null,
                    lynnCategoryId: this.shopToLynnAndViceVersa['ShopId-' + currentKarlData.shopCategoryId] + '',
                    oldLynnCategoryId: currentLynnData.lynnCategoryId,
                    productId: currentLynnData.productId,
                    message: 'Shop-Kategorie: In Lynn ist die Shop-Kategorie auf NULL gesetzt wurden, ' +
                        'obwohl in Karl eine Shop-Kategorie gesetzt ist. ' +
                        'Alles in Lynn zurück setzen, so wie hier übergeben.'
                })
            }
        }
        result.reset = reset;
        result.change = change;

        return result;
    }

    /**
     * Setzt anhand der Shop-Kategorie die Lynn-Kategorie.
     * */
    async setAllLynnCategoryFromShopCategories(data) {
        for (let index in data) {
            let currentData = data[index];

            if (this.shopToLynnAndViceVersa['ShopId-' + currentData.shopCategoryId] !== undefined) {
                currentData.oldLynnCategoryId = currentData.lynnCategoryId;
                currentData.lynnCategoryId = this.shopToLynnAndViceVersa['ShopId-' + currentData.shopCategoryId] + '';
            } else {
                currentData.lynnCategoryId = 'unbekannt';
            }
        }
    }

}

module.exports = Index;