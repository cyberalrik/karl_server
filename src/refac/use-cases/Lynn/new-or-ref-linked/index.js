'use strict';

/**
 * Diese Klasse wird nach der Strukturänderung der DB (erp.ProductPropertyLink)
 * von ArticleForeignId hinzu ProductForeignId nicht mehr benötigt.
 * */

// const DB = require('../../../../refac/data-access');
// const dbConstants = require('../../../../refac/constants');
// const fs = require('fs');
//
// let linkTypeName = [];
// let articles
// let linked
//
// class Index {
//     /** Aufruf mit http://localhost:8080/newOrRefLinked */
//     async newOrRefLinked() {
//         let numbers = 0;
//         linkTypeName['T-1'] = 'nur Foto';
//         linkTypeName['T-2'] = 'Komplettverlinkung 1 - gleiche Preise';
//         linkTypeName['T-102'] = 'Master-Slave';
//         linkTypeName['T-1001'] = 'Alternative';
//
//         // numbers += (await this.forDifferentType(1)).count;
//         // numbers += (await this.forDifferentType(2)).count;
//         numbers += (await this.forDifferentType(1001)).count;
//
//         return {count: numbers};
//     }
//
//     async forDifferentType(linkType) {
//         let db_lynn = new DB(dbConstants.LYNN);
//
//         /** Welche gleichen Artikel sind verschiedenen Produkten zugeordnet */
//         console.log('Hole alle new und ref komplett Artikel');
//         let productsDataAccess = db_lynn.getProductsDataAccess();
//         let allNewAndRefArticle = await productsDataAccess.getAllNewAndRefArticlesOfAProduct();
//         console.log(`Habe ${allNewAndRefArticle.length} Artikel gefunden.`);
//
//         console.log('Präpariere die Artikeldaten und überführe sie in einem Array');
//         articles = await this.prepareArticleData(allNewAndRefArticle);
//
//         console.log('Hole alle Verlinkungen');
//         let linkListDataAccess = db_lynn.getLinkListDataAccess();
//         let linkList = await linkListDataAccess.getPrepareDataOfLinks(linkType);
//         console.log(`Habe ${linkList.length} Verlinkungen gefunden.`);
//
//         console.log('Bereite Verlinkungen auf');
//         linked = await this.prepareLinkedData(linkList);
//
//         console.log('Durchlaufen aller Verlinkungen');
//         let missingLinked = [];
//         let singleArticle = [];
//         for (let index in linked) {
//             let currentData = linked[index];
//             let result = await this.ifCrossLinked(currentData);
//
//             if (!result.successful) {
//                 if (result.isSingle) {
//                     /** Der gegenartikel ist nicht verlinkt */
//                     singleArticle.push(currentData);
//                 } else {
//                     /** Der gegenartikel ist nicht verlinkt */
//                     currentData.missingLinks = result.missingLinks;
//                     missingLinked.push(currentData);
//                 }
//             }
//         }
//
//         console.log(`Es wurden ${missingLinked.length} fehlende Verlinkungen gefunden.`);
//
//         await this.writeCsvFile(missingLinked, linkType);
//
//         console.log(`FERTIG.`);
//
//         return {count: missingLinked.length}
//     }
//
//     /** Artikeldaten aufbereiten */
//     async prepareArticleData(allNewAndRefArticle) {
//         let articles = []
//         for (let index in allNewAndRefArticle) {
//             if (allNewAndRefArticle.hasOwnProperty(index)) {
//                 let currentData = allNewAndRefArticle[index];
//
//                 articles['d-' + currentData.newArticleForeignId] = {
//                     crossArticleId: currentData.refArticleForeignId,
//                     mpn: currentData.mpn,
//                     thirdParty: currentData.thirdParty,
//                     manufacturer: currentData.manufacturer,
//                     condition: 'new'
//                 }
//
//                 articles['d-' + currentData.refArticleForeignId] = {
//                     crossArticleId: currentData.newArticleForeignId,
//                     mpn: currentData.mpn,
//                     thirdParty: currentData.thirdParty,
//                     manufacturer: currentData.manufacturer,
//                     condition: 'ref'
//                 }
//             }
//         }
//
//         return articles;
//     }
//
//     /** Verlinkungsdaten aufbereiten */
//     async prepareLinkedData(linkList) {
//         let linked = []
//         let count = 0;
//         for (let index in linkList) {
//             count++;
//             if (linkList.hasOwnProperty(index)) {
//                 let currentData = linkList[index];
//
//                 linked[currentData.parent + '-' + currentData.child]  = {
//                     parent: currentData.parent,
//                     child: currentData.child
//                 }
//             }
//         }
//
//         return linked;
//     }
//
//     async ifCrossLinked(currentData) {
//         let result = {
//             successful: false,
//             missingLinks: [],
//             isSingle: true
//         };
//
//         /** Sind die Artikel vorhanden? */
//         if (
//             articles['d-' + currentData.parent] === undefined
//             || articles['d-' + currentData.child] === undefined
//         ) {
//             return result;
//         }
//
//         result.isSingle = false;
//
//         let parent = articles['d-' + currentData.parent].crossArticleId;
//         let child = articles['d-' + currentData.child].crossArticleId;
//
//         /** Sind die Gegenartikel auch verlinkt? */
//         if (linked[parent + '-' + child] === undefined) {
//             result.missingLinks = {
//                 parent: parent,
//                 child: child
//             };
//
//             return result;
//         }
//
//         // if (linked[child + '-' + parent] === undefined) {
//         //     result.missingLinks = {
//         //         parent: parent,
//         //         child: child
//         //     };
//         //
//         //     return result;
//         // }
//         //
//         result.successful = true;
//
//         return result;
//     }
//
//     async getArticleData(articleId) {
//         let currentArticle = articles['d-' + articleId]
//
//         return {
//             mpn: currentArticle.mpn,
//             thirdParty: currentArticle.thirdParty,
//             manufacturer: currentArticle.manufacturer,
//             condition: currentArticle.condition,
//         }
//     }
//
//     async writeCsvFile(missingLinked, linkType) {
//         let fileName = `D:\\Daten\\karl\\nur ein Zustand verlinkt vom Typ - ${linkTypeName['T-' + linkType]}.csv`;
//         // let fileName = '/services/report/nur ein Zustand verlinkt.csv';
//         //let fileName = '/Users/Andy/Cybertrading/nur ein Zustand verlinkt.csv';
//         console.log(`Das Ergebnis wird in die Datei ${fileName} geschrieben. `);
//         try {
//             if (fs.existsSync(fileName)) {
//                 fs.unlinkSync(fileName);
//             }
//             let writeStream = fs.createWriteStream(fileName, {
//                 flags: 'a' // 'a' means appending (old data will be preserved)
//             })
//
//             writeStream.write('verlinkt sind:;;;;;;;;;;;vermisst werden:' + "\n");
//             writeStream.write('Artikel-ID;MPN;Hersteller;ThirdParty;Zustand;;Artikel-ID;MPN;Hersteller;ThirdParty;Zustand;;Artikel-ID;MPN;Hersteller;ThirdParty;Zustand;;Artikel-ID;MPN;Hersteller;ThirdParty;Zustand;' + "\n");
//
//             for (let index in missingLinked) {
//                 let parentArticleData = await this.getArticleData(missingLinked[index].parent);
//                 let childArticleData = await this.getArticleData(missingLinked[index].child);
//                 let missingParentArticleData = await this.getArticleData(missingLinked[index].missingLinks.parent);
//                 let missingChildArticleData = await this.getArticleData(missingLinked[index].missingLinks.child);
//                 writeStream.write(`${missingLinked[index].parent};${parentArticleData.mpn};${parentArticleData.manufacturer};${parentArticleData.thirdParty};${parentArticleData.condition};-->;${missingLinked[index].child};${childArticleData.mpn};${childArticleData.manufacturer};${childArticleData.thirdParty};${childArticleData.condition};||;${missingLinked[index].missingLinks.child};${missingChildArticleData.mpn};${missingChildArticleData.manufacturer};${missingChildArticleData.thirdParty};${missingChildArticleData.condition};-->;${missingLinked[index].missingLinks.parent};${missingParentArticleData.mpn};${missingParentArticleData.manufacturer};${missingParentArticleData.thirdParty};${missingParentArticleData.condition}` + '\n');
//             }
//
//             writeStream.end();
//         } catch (e) {
//             let message = e.message;
//             console.log(`Fehler: ${message}. `);
//         }
//     }
//
// }
//
// module.exports = Index;