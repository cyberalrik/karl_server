'use strict';

const DB = require('../../../../refac/data-access');
const dbConstants = require('../../../../refac/constants');
const logger = require('../../../../helper/util/logger.util');
const Mail = require("../../../../services/service.send.mail");

class Index {
    /** Werden doppelt angelegte Produkte in Lynn bemerkt, soll eine eMail versendet werden. */
    async doubleProductsAlert(TAG) {
        let db_lynn = new DB(dbConstants.LYNN);
        let result = '';
        /** Gibt alle doppelten Produkte aus Lynn zurück */
        let access = db_lynn.getProductsDataAccess();
        let doubleLynnProduct = await access.getDoubleLynnProduct();

        if (doubleLynnProduct.length > 0) {
            let productList = [];
            for (let index in doubleLynnProduct) {
                productList.push(doubleLynnProduct[index].Number);
            }
            let products = productList.join(', ');
            logger.err(TAG, 'Doppelte Lynn Produkte', `Es gibt ${doubleLynnProduct.length} doppelte(n) Produkte in Lynn`);
            let mail = new Mail();
            await mail.sendEMail(
                `Es gibt ${doubleLynnProduct.length} doppelte Produkte in Lynn`
                ,'Folgende Produkte sind in Lynn doppelt angelegt:'
                , products
                , TAG
                ,'Hallo,\n\nkönnt ihr bitte die Produkte ansehen.\n\nDanke, euer Karl ;)\n\n\n\n'
                ,'produktmanagement@cybertrading.de,s.wiehe@cybertrading.de'
            );
            result = `Folgende Produkte sind in Lynn doppelt angelegt: ${products}`;
        } else {
            logger.log(TAG, 'Doppelte Lynn Produkte', `Es gibt keine doppelten Produkte in Lynn.`);
        }

        return result
    }

    async doubleProductsCleanUp() {
        let db_lynn = new DB(dbConstants.LYNN);

        /** welche gleiche Artikel sind verschiedenen Produkten zugeordnet */
        let access = db_lynn.getProductsDataAccess();
        let allSingleArticles = await access.getAllSingleArticles();

        let result = {};
        if (allSingleArticles.recordset !== undefined)
        {
            result = allSingleArticles.recordset
        }

        let preselection = [];
        let doubleSingleProducts = [];
        /**
         * Suche alle doppelten Einträge heraus, zu berücksichtigen ist 3rd und Hersteller
         * */
        for (let index in result) {
            let currentData = result[index];
            let arrayKey = currentData.mpn + '_' + currentData.mfg + '_' + currentData.thirdParty;
            if (
                preselection[arrayKey] !== undefined
                && preselection[arrayKey].productForeignId === currentData.productForeignId
            ) {
                let ProduktData = {
                    mpn: currentData.mpn,
                    productId: {
                        1: preselection[arrayKey],
                        2: {productId: currentData.productId}
                    },
                    mfg: currentData.mfg,
                    ThirdParty: currentData.thirdParty
                }
                doubleSingleProducts.push(ProduktData);
            } else {
                preselection[arrayKey] = {
                    productId: currentData.productId,
                    productForeignId: currentData.productForeignId
                };
            }

        }

        /** Daten in Lynn-DB anpassen */
        for (let index in doubleSingleProducts) {
            let currentData = doubleSingleProducts[index];
            // await access.updateSingleArticles(currentData oldProductId, currentData newProductId)
            await access.updateSingleArticles(currentData.productId[2].productId, currentData.productId[1].productId)
        }
    }
}

module.exports = Index;