'use strict'

const logger = require("../../../../helper/util/logger.util");
const MailData = require("../../../entity/mailData");

class Index {
    constructor(tag, result ) {
        this.TAG = tag;
        this.result = result
        this.sqlForMail = []
    }
    /**
     * Baut ein Query mit CASE zusammen.
     *
     * Wird aber im CASE alles auf "NULL" gesetzt,
     * währt sich die DB dagegen und es muss ein einfaches "set ... = NULL" erzeugt werden.
     * */
    async setEan(eanDifference) {
        if (this.result.successful) {
            /** So kann auch im Bulk alles andere auf NULL weisen. */
            let lynnEanCase = [`WHEN 0 THEN '0'`];
            let whereArray = [];
            let number = 0;
            let count = 0;
            let longMessage = `Es müssen ${eanDifference.number} EAN in Lynn Artikel zugeordnet werden.`;

            if (eanDifference.number > 500) {
                longMessage = `Es müssen insgesamt ${eanDifference.number
                } EAN in Lynn zugeordnet werden, dies geschieht in 500er Schritte.`;
            }

            await logger.log(this.TAG,
                `${eanDifference.number} EAN zugeordnet`,
                longMessage + ''
            );

            for (let index in eanDifference.result) {
                number++;
                count++;
                let currentData = eanDifference.result[index];
                lynnEanCase.push(`WHEN ${currentData.erpProductPropertyId} THEN '${currentData.a_ean}'`)
                whereArray.push(currentData.erpProductPropertyId);

                if (number === 500) {
                    await logger.log(this.TAG,
                        'EAN korrigieren',
                        `Es werden ${number}, von schon ${count} bearbeiteten, EAN in Lynn korrigiert/ eingetragen. (${eanDifference.number})`
                    );
                    let bulkSql = await this.createBulkSql(lynnEanCase, whereArray);
                    await this.setEanInLynnDb(bulkSql);
                    /** Variablen müssen wieder zurückgesetzt werden, Daten wurden weggeschrieben */
                    lynnEanCase = [`WHEN 0 THEN '0'`]
                    whereArray = [];
                    number = 0;

                    /** Wenn ein Fehler bei der Auführung des Skripts entsanden ist, Abbrechen. */
                    if (!this.result.successful) {
                        break;
                    }
                }
            }
            /** Für alle die über geblieben sind und nicht in der maximalen begrenzung reingelaufen sind. */
            if (whereArray.length > 0) {
                await logger.log(this.TAG,
                    'EAN korrigieren',
                    `Es werden ${number}, von schon ${count} bearbeiteten, EAN in Lynn korrigiert/ eingetragen. (${eanDifference.number})`
                );
                let bulkSql = await this.createBulkSql(lynnEanCase, whereArray);
                await this.setEanInLynnDb(bulkSql);
            }
            this.result.messages.push(`Es wurden ${eanDifference.number} fehlende EAN ergänzt.`);
        } else {
            await logger.err(this.TAG,
                `Fehler bei Zugriff auf DB`,
                `Es konnten keine EAN Änderungen in die Lynn DB geschrieben werden`,
                `Die Datenbank Verbindung ist nicht sichergestellt.`,
            )
        }

        if (eanDifference.number > 0) {
            let mailData = new MailData(
                `Es werden bei ${eanDifference.number} Artikel die EAN geändert/ korrigiert.`
                , this.sqlForMail.join('\r\n')
            );
            await logger.log(this.TAG,
                `EAN korrigiert`,
                // `Das SQL, für die Änderung der EAN bei ${whereArray.length} Artikel, wurde per eMail gesendet.`);
                `Das SQL, für die Änderung der EAN von ${eanDifference.number} Artikel, wurde per eMail gesendet.`,
                mailData);
        }

        return this.result;
    }

    /**
     * Erstellt das SQL-Skript aus den zugewiesenen Daten.
     *
     * @param lynnEanCase
     * @param {{}} whereArray
     * */
    async createBulkSql(lynnEanCase, whereArray) {
        let lynnEanCaseString = 'EAN = CASE Id ' + lynnEanCase.join(' ') + ' END';
        let whereString = whereArray.join(', ');
        let sql = `UPDATE [erp].[ProductProperty] SET ${lynnEanCaseString} WHERE Id IN (${whereString})`;

        this.sqlForMail.push(`\n\nDas Query, was benutzt wurde, war folgendes:\n\n${sql}`);

        return sql;
    }

    async setEanInLynnDb(sql) {
        if (this.result.successful) {
            /** Aus Sicherheitsgründen 30 sec warten bis es wieder losgeht. */
            await new Promise(resolve => setTimeout(resolve, 1000));

            try {
                /** Übergeben eines SQL-Skripts zum Ausführen */
                await this.result.access.runUpdateQuery(sql);
                await logger.log(this.TAG, 'EAN korrigiert', `EAN's wurden in Lynn-Artikel korrigiert.`);
            } catch (e) {
                await logger.err(this.TAG,
                    `Fehler bei Zugriff auf Lynn-DB`,
                    `Es konnten keine EAN Änderungen in die Lynn DB geschrieben werden`,
                    `Die Fehlermeldung lautet: \n${e.message
                }\nDie Datenbank Verbindung ist nicht sichergestellt. \nDas Query ist:\n${sql}`)
                this.result.successful = false;
            }
        } else {
            await logger.err(this.TAG,
                `Fehler bei Zugriff auf Lynn-DB`,
                `Es konnten keine EAN Änderungen in die Lynn DB geschrieben werden`,
                `Die Datenbank Verbindung ist nicht sichergestellt. \nDas Query ist:\n${sql}`,
            )
        }
    }
}

module.exports = Index;