'use strict';

const DB = require('../../../../refac/data-access');
const dbConstants = require('../../../../refac/constants');
const articleRepo = require('../../../../repository/article/repo.article');
const UseCaseWriteEanInLynn = require('./setEanInLynn');
const logger = require('../../../../helper/util/logger.util');
// const Mail = require('../../../../services/service.send.mail');

class Index {
    /** Fehlende oder sich geänderte EAN's sollen von Karl zu Lynn übertragen werden. */
    async call(TAG, result) {
        this.TAG = TAG;
        this.result = result
        let db_lynn = new DB(dbConstants.LYNN);

        await logger.log(TAG, 'Lynn- Karl-Daten ermittelt', `Es werden Artikel aus Lynn und Karl ermittelt.`)
        /** Gibt alle Artikel mit EAN-Nr zurück aus Lynn zurück */
        this.result = db_lynn.getEanFromArticleAccess(this.result);
        let data = await this.result.access.getAllArticleWithEanData();
        await logger.log(TAG, 'Lynn-Daten ermittelt', `Es wurden ${data.length} Artikel aus Lynn ermittelt.`)
        let lynnEan = await this.getPrepareArticleData(data);

        /** Gibt alle Artikel mit EAN-Nr zurück aus Karl zurück */
        data = await articleRepo.getAllArticleWhitEan()
        await logger.log(TAG, 'Karl-Daten ermittelt', `Es wurden ${data.length} Artikel aus Karl ermittelt.`)
        let karlEan = await this.getPrepareArticleData( data );
        data = null;

        /**
         * Vergleichen der Datenbestände, wobei die Daten aus Karl in Lynn übernommen werden sollen.
         * Als Ergebniss kommt ein Array zurück mit den Daten die geändert werden müssen.
         * */
        let transferredEan = await this.forWhomTheEanDeviates(karlEan, lynnEan)

        let useCaseWriteEanInLynn = new UseCaseWriteEanInLynn(TAG, this.result)
        this.result = await useCaseWriteEanInLynn.setEan(transferredEan);

        return this.result;
    }

    async getPrepareArticleData(articles) {
        let result = [];
        for (let article of articles) {
            result['a_nr_' + article.a_nr] = article;
        }

        return result;
    }

    /** bei wem weicht die ean ab */
    async forWhomTheEanDeviates(karlArticles, lynnArticles) {
        let result = [];
        let number = 0;
        for (let index in karlArticles) {
            if (
                lynnArticles[index] !== undefined
                && lynnArticles[index].a_ean !== karlArticles[index].a_ean
            ) {
                result[index] = {
                    erpProductPropertyId: lynnArticles[index].erpProductPropertyId,
                    a_ean: karlArticles[index].a_ean
                };
                number++;
            }
        }

        return {result: result, number: number};
    }
}

module.exports = Index;