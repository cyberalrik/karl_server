'use strict'

const DB = require('../../../data-access');
const dbConstants = require('../../../constants');
const DataAccessKarl = require('../../../../refac/data-access/karl/images');

const logger = require('../../../../helper/util/logger.util');
let TAG = '';
let db_lynn;

/**
 * Vergleicht und setzt CT-Foto und cNet-Foto Kennzeichnungen.
 * */
class Index {
    constructor(tag) {
        TAG = tag;
    }

    async setImageFeatureInLynn() {

        let result = {
            successful: false,
            errorCNetFotoInLynn: 0,
            missingCNetFotoInLynn: 0,
            errorCtFotoInLynn: 0,
            missingCtFotoInLynn: 0
        }

        await this.setDbConnection();
/********************** CNet-Fotos *****************************/
        /**xx-- Daten ermitteln --xx*/
        /** LYNN.
         * Alle Lynn ProductForeignId = p_nr, mit CNet Kennzeichen.
         * Auch alle inaktive, da es ansonsten immer zu Korrekturversuche kommt,
         * nur weil die Produkte nicht mit herangezogen wurden.
         * */
        let lynnCNetP_nr = await this.accessLynn.getAllCNetImagesFeature();

        if (lynnCNetP_nr === null) {
            logger.err(TAG,'Lynn-DB Verbindungsfehler','Es wurden keine Daten (Produkte mit cNet-Foto Kennzeichnung) zurück gegeben.');

            return result;
        }

        /**xx-- Daten verarbeiten --xx*/
        let cNetFeatureInLynn = await this.getPrepareArray(lynnCNetP_nr, []);
        lynnCNetP_nr = null;

        /** KARL */
        /**
         * Alle ProductId die ein CantoFoto haben, diese aber nicht von uns sind und als CTFoto = true gekennzeichnet sind.
         * Hier sind es aber keine 24er- oder 4er-Fotoserie. Nach etwas anderem lassen sie sich nicht unterscheiden.
         * */
        let karlCt_CNetP_nr = await this.dataAccessKarl.getAllProductIdsFromCantoForeignImages();

        /**xx-- Daten verarbeiten --xx*/
        let cNetFeatureInKarl = await this.getPrepareArray(karlCt_CNetP_nr, []);
        karlCt_CNetP_nr = null;

        /** Alle Karl ProductId = p_nr, mit aktiven CNetFoto's */
        let karlCNetP_nr = await this.dataAccessKarl.getAllProductIdsFromCNetImages();

        /** Die Daten lynnCNetP_nr und karlCt_CNetP_nr verbinden */
        cNetFeatureInKarl = await this.getPrepareArray(karlCNetP_nr, cNetFeatureInKarl);
        karlCNetP_nr = null;

        /** Welche Kennzeichen sind falsch gesetzt? */
        let errorCNetFotoInLynn = await this.compare(cNetFeatureInKarl, cNetFeatureInLynn);

        /** Bilder String-Listen */
        let errorCNetP_Nr = await this.arrayInToStringGroup(errorCNetFotoInLynn);
        result.errorCNetFotoInLynn = errorCNetFotoInLynn.length;
        logger.log(TAG, `Falsche Kennz. CNet-Foto`, `${errorCNetFotoInLynn.length} falsche Kennzeichnung von CNet-Foto in Lynn korrigiert`)
        errorCNetFotoInLynn = null;

        /** setze nicht mehr vorhandene Kennzeichen zurück */
        await this.setCNetFeature(errorCNetP_Nr, 0);
        errorCNetP_Nr = null;

        /** Welche Kennzeichen fehlen? */
        let missingCNetFotoInLynn = await this.compare(cNetFeatureInLynn, cNetFeatureInKarl);
        cNetFeatureInKarl = null;

        /** Bilder String-Listen */
        let missingCNetP_Nr = await this.arrayInToStringGroup(missingCNetFotoInLynn);
        result.missingCNetFotoInLynn = missingCNetFotoInLynn.length;
        logger.log(TAG, `Fehlende Kennz. CNet-Foto`, `${missingCNetFotoInLynn.length} fehlende Kennzeichnung von CNet-Foto in Lynn korrigiert`)
        missingCNetFotoInLynn = null;

        /** setze neue Kennzeichen in Lynn */
        await this.setCNetFeature(missingCNetP_Nr, 1);
        missingCNetP_Nr = null;

/********************** CT-Fotos *****************************/
        /**xx-- Daten ermitteln --xx*/
        /** LYNN */

        /**
         * Alle ProductForeignId = p_nr, mit CT Kennzeichen.
         * Auch alle inaktive, da es ansonsten immer zu Korrekturversuche kommt,
         * nur weil die Produkte nicht mit herangezogen wurden.
         * */
        let lynnCtP_nr = await this.accessLynn.getAllCTImagesFeature();
        if (lynnCtP_nr === null) {
            logger.err(TAG,'Lynn-DB Verbindungsfehler','Es wurden keine Daten (Produkte mit CT-Foto Kennzeichnung) zurück gegeben.');

            return result;
        }

        let ctFeatureInLynn = await this.getPrepareArray(lynnCtP_nr, []);
        lynnCtP_nr = null;

        /** KARL */
        /** Alle ProductForeignId = p_nr, die als CTFoto = true gekennzeichnet sind */
        let karlCtP_nr = await this.dataAccessKarl.getAllProductIdsFromCtImages();

        /**xx-- Daten verarbeiten --xx*/
        let ctFeatureInKarl = await this.getPrepareArray(karlCtP_nr, []);

        /** Welche Kennzeichen sind falsch gesetzt? */
        let errorCtFotoInLynn = await this.compare(ctFeatureInKarl, ctFeatureInLynn);

        /** Bilde String-Listen */
        let errorCtP_Nr = await this.arrayInToStringGroup(errorCtFotoInLynn);
        result.errorCtFotoInLynn = errorCtFotoInLynn.length;
        logger.log(TAG, `Falsche Kennz. CT-Foto`, `${errorCtFotoInLynn.length} falsche Kennzeichnung von CT-Foto in Lynn korrigiert`)
        errorCtFotoInLynn = null;

        /** setze nicht mehr vorhandene Kennzeichen zurück */
        await this.setCtFeature(errorCtP_Nr, 0);
        errorCtP_Nr = null;

        /** Welche Kennzeichen fehlen? */
        let missingCtFotoInLynn = await this.compare(ctFeatureInLynn, ctFeatureInKarl);
        ctFeatureInKarl = null;

        /** Bilder String-Listen */
        let missingCtP_Nr = await this.arrayInToStringGroup(missingCtFotoInLynn);
        result.missingCtFotoInLynn = missingCtFotoInLynn.length;
        logger.log(TAG, `Fehlende Fehlende Kennz. von CT-Foto`, `${missingCtFotoInLynn.length} fehlende Kennzeichnung von CT-Foto in Lynn korrigiert`)
        missingCtFotoInLynn = null;

        /** setze neue Kennzeichen in Lynn */
        await this.setCtFeature(missingCtP_Nr, 1);
        missingCtP_Nr = null;

        db_lynn = null;

        result.successful = true;
        return result
    }

    async setDbConnection() {
        /** Datenverbindung zu Lynn */
        db_lynn = new DB(dbConstants.LYNN);
        this.accessLynn = db_lynn.getProductsDataAccess();

        /** Datenverbindung zu Karl (JOIN) */
        this.dataAccessKarl = new DataAccessKarl();
    }

    /**
     * Bereitet die Daten auf.
     * Gibt ein Array zurück, was die Artikelnummer als Key besitzt.
     * */
    async getPrepareArray(data, returnData) {
        for (let index in data) {
            returnData.push(data[index].productId);
        }

        return returnData;
    }

    /**
     * Alle in dataSearchIn nicht vorkommenden Daten aus dataSearch werden zurückgegeben.
     * */
    async compare(dataSearchIn, dataSearch){
        let result = []
        for (let index in dataSearch) {
            let p_nr = dataSearch[index];
            if (!dataSearchIn.includes(p_nr)) {
                result.push(p_nr);
            }
        }

        return result
    }

    async arrayInToStringGroup(data) {
        let result = [];
        let cache = [];
        let counter = 0;

        for (let index in data) {
            cache.push(data[index])
            counter++;
            if (counter % 50 === 0) {
                let resultString = cache.join(',');
                result.push(resultString);
                cache = [];
            }
        }

        if (counter % 10 !== 0) {
            let resultString = cache.join(',');
            result.push(resultString);
        }

        return result;
    }

    async setCNetFeature(list, value) {
        for (let i in list) {
            let productIds = list[i];
            if (productIds.length > 0) {
                console.log(`CNetFeature setzen auf ${value}`);
                console.log(productIds);
                await this.accessLynn.setCNetFeature(productIds, value);
            }
        }
    }

    async setCtFeature(list, value) {
        for (let i in list) {
            let productIds = list[i];
            if (productIds.length > 0) {
                console.log(`CtFeature setzen auf ${value}`);
                console.log(productIds);
                await this.accessLynn.setCTFeature(productIds, value);
            }
        }
    }
}

module.exports = Index;