'use strict'

const constants  = require('../../../constants');
const comRepo = require('../../../../repository/repo.components')

class AddComponents {
    constructor(db) {
        this.db = db;
        this.allComponents = [];
        this.allComponents['empty'] = true;
        this.allComponentsFrom100To199 = [];
        this.allComponentsFrom100To199['empty'] = true;
    }

    /**
     * Stelle sicher, dass die Komponente für dieses Produkt so wie übergeben existiert.
     * Ist die newComponent für das Produkt nicht vorhanden oder muss korrigiert werden?
     * Wenn ja, dann bitte anlegen/ korrigiert
     * */
    async makeSureYouComponent(p_nr, type, newComponent, result) {
        /** ist die shouldComponent für das Produkt schon angelegt? */
        let existingComponent = this.allComponentsFrom100To199[
        p_nr
        + '-' + newComponent.p_component_position
        + '-' + newComponent.p_component_language
            ];
        if (!(existingComponent !== undefined
            && existingComponent.p_component_key === newComponent.p_component_key
            && existingComponent.p_component_value === newComponent.p_component_value)
        ) {
            result = await this.addComponents(type, newComponent, result);
        }

        return result;
    }

    async addComponents(type, component, result){
        if(type === constants.PRODUCTCOMPONENT){
            result = await comRepo.addComponent(
                component,
                result
            );
        }

        return result;
    }

    async getAllComponentsFromPosition(position) {
        if (this.allComponents['empty']) {
            console.log('Komponenten 200 werden geladen');
            let result = await comRepo.getAllComponentPerPosition(position);
            for (let index in result) {
                this.allComponents[result[index].p_nr + '-' + result[index].p_component_language] = true;
            }
            this.allComponents['empty'] = false;
        }

        return this.allComponents;
    }

    async setAllComponentsFromPosition100To199() {
        if (this.allComponentsFrom100To199['empty']) {
            console.log('Komponenten 100To199 werden geladen');
            let components =  await comRepo.getAllComponentsFrom100To199();
            for (let component of components) {
                this.allComponentsFrom100To199[
                    component.p_nr
                    + '-' + component.p_component_position
                    + '-' + component.p_component_language
                    ] = component;
            }
            this.allComponentsFrom100To199['empty'] = false;
        }
    }


    async addDefaultComponents(p_nr, is3rd, result = {create: 0, update: 0, delete: 0}){
        let type = constants.PRODUCTCOMPONENT;
        await this.setAllComponentsFromPosition100To199();
        let newComponent = {
            p_nr: p_nr,
            p_component_key: '*Garantiebedingungen',
            p_component_value: 'Beachten Sie die vollständ. Artikelbeschreibung',
            p_component_language: 'de',
            p_component_position: 100
        };
        result = await this.makeSureYouComponent(p_nr, type, newComponent, result);

        newComponent = {
            p_nr: p_nr,
            p_component_key: '*Warranty condition',
            p_component_value: 'Please note the full product description',
            p_component_language: 'ee',
            p_component_position: 100
        };
        result = await this.makeSureYouComponent(p_nr, type, newComponent, result);

        newComponent = {
            p_nr: p_nr,
            p_component_key: 'Produktart',
            p_component_value: 'Netzwerkgerät',
            p_component_language: 'de',
            p_component_position: 102
        };
        result = await this.makeSureYouComponent(p_nr, type, newComponent, result);

        newComponent = {
            p_nr: p_nr,
            p_component_key: 'Product Type',
            p_component_value: 'Network device',
            p_component_language: 'ee',
            p_component_position: 102
        };
        result = await this.makeSureYouComponent(p_nr, type, newComponent, result);

        newComponent = {
            p_nr: p_nr,
            p_component_key: 'Ausführung',
            p_component_value: await this.calcVersionForProductsDe(is3rd),
            p_component_language: 'de',
            p_component_position: 103
        };
        result = await this.makeSureYouComponent(p_nr, type, newComponent, result);

        newComponent = {
            p_nr: p_nr,
            p_component_key: 'Version',
            p_component_value: await this.calcVersionForProductsEe(is3rd),
            p_component_language: 'ee',
            p_component_position: 103
        };
        result = await this.makeSureYouComponent(p_nr, type, newComponent, result);

        return result;
    }

    /**
     * Sind in Lynn Externe Produktinformationen gesetzt, werden sie kontrolliert.
     * - Sind sie in Karl nicht angelegt, werden sie angelegt.
     * - Sind in Lynn keine mehr angelegt aber in Karl, so werden sie gelöscht.
     * */
    async setExternalProductInformation(item, result = {create: 0, update: 0, delete: 0}) {
        let type = constants.PRODUCTCOMPONENT;
        if (item.p_infoExternGerman !== '') {
            let newComponent = {
                p_nr: item.p_nr,
                p_component_key: 'Wichtige Produktinformationen',
                p_component_value: item.p_infoExternGerman,
                p_component_language: 'de',
                p_component_position: 200
            }
            result = await this.addComponents(type, newComponent, result);
        } else {
            let allComponents = await this.getAllComponentsFromPosition(200);
            if (allComponents[item.p_nr + '-' + 'de']) {
                result = await comRepo.deleteComponent({
                    p_nr: item.p_nr,
                    p_component_position: 200,
                    p_component_language: 'de'
                }
                , result);
            }
        }

        if (item.p_infoExternGerman !== '') {
             let newComponent = {
                p_nr: item.p_nr,
                p_component_key: 'Important product information',
                p_component_value: item.p_infoExternEnglish,
                p_component_language: 'ee',
                p_component_position: 200
            };
            result = await this.addComponents(type, newComponent, result);
        } else {
            let allComponents = await this.getAllComponentsFromPosition(200);
            if (allComponents[item.p_nr + '-' + 'ee']) {
                result = await comRepo.deleteComponent({
                    p_nr: item.p_nr,
                    p_component_position: 200,
                    p_component_language: 'ee'
                }
                , result);
            }
        }

        return result;
    }

    async calcVersionForProductsDe(is3rd){
        if (is3rd) {
            return 'Third Party || kompatibel'
        } else {
            return 'original'
        }
    }

    async calcVersionForProductsEe(is3rd){
        if (is3rd) {
            return 'Third Party || compatible'
        } else {
            return 'original'
        }
    }
}

module.exports = AddComponents
