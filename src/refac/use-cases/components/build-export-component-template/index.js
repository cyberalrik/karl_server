'use strict';

const Componenthelper = require('../../../helper/component');

class BuildComponentTemplate{
    constructor() {
        this.helper = new Componenthelper();
    }

    build_old(components){
        let data = [];
        if (components === undefined)
            return data;

        for (let i = 0; i < components.length; i++) {
            if (components[i].p_component_position > 15 && components[i].p_component_position < 100)
                continue;
            let item = {
                '@': {
                    identifier: 'key',
                    key: components[i]['p_component_key']
                },
                '#': {
                    VALUE: {
                        '@': {
                            'xml:lang': this.helper.serializeLanguageToTbone(components[i].p_component_language)
                        },
                        '#': components[i]['p_component_value']
                    }
                }
            };
            data.push(item);
        }
        return data;
    }

    build(components){
        let data = [];
        if (components === undefined)
            return data;
        let ids = [];
        for (let i = 0; i < components.length; i++){
            if (components[i].p_component_position > 15 && components[i].p_component_position < 100)
                continue;
            if (!ids.includes(components[i].p_component_position)){
                ids.push(components[i].p_component_position);
            }
        }
        let compData = [];
        for (let i = 0; i < ids.length; i++){
            let storage = [];
            for (let j = 0; j < components.length; j++){
                if (
                    components[j].p_component_position === ids[i]
                    && components[j].p_component_value !== ''
                ) {
                    storage.push(components[j]);
                }
            }
            compData.push(storage);
        }

        for (let i = 0; i < compData.length; i++){
            if (compData[i].length > 0) {
                let key = this.getKey(compData[i]);
                if (key !== '') {
                    let item = {
                        '@': {
                            identifier: 'key',
                            key: key
                        },
                        '#': {
                            VALUE: this.calcInterface(compData[i])
                        }
                    };
                    data.push(item);
                }
            }
        }

        return data;
    }

    getKey(data){
        /** Der Key soll vorrangig aus der deutschen Komponente gewonnen werden. */
        for (let i = 0; i < data.length; i++){
            if (data[i].p_component_language === 'de' && data[i].p_component_value !== ''){
                return data[i].p_component_key;
            }
        }
        /** Gibt es aber nur eine englische Komponente, dann lieber diesen Key benutzen als eine irre Zahl als Key */
        for (let i = 0; i < data.length; i++){
            if (data[i].p_component_language === 'ee' && data[i].p_component_value !== ''){
                return data[i].p_component_key;
            }
        }
    }

    calcInterface(data) {
        let result = []
        for (let i = 0; i < data.length; i++) {
            let item = {
                '@': {
                        'xml:lang': this.helper.serializeLanguageToTbone(data[i].p_component_language)
                },

                '#': data[i]['p_component_value']
                };
            result.push(item);
        }
        return result;
    }
}

module.exports = BuildComponentTemplate;