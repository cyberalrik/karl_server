'use strict';

class GetComponentsFromCCI{
    constructor(db) {
        this.db = db;
    }

    // async getComponentsPerMpn(mpn, lang){
    //     let access = this.db.getComponentsDataAccess();
    //     let components = await access.getComponents(mpn, lang);
    //     return components;
    // }

    /** Läd aus der ct_components (cci) alle Komponenten der übergebenen MPN */
    async getAllLanguageComponentsFromMpn(mpn){
        let access = this.db.getComponentsDataAccess();
        let components = await access.getAllLanguageComponentsFromMpn(mpn);
        return components;
    }
}

module.exports = GetComponentsFromCCI;