'use strict';

const prodRepo = require('../../../../repository/repo.product');
const artRepo = require('../../../../repository/article/repo.article');

class GetProduct {
    constructor(db) {
        this.db = db;
    }

    async getProductsPerValue(key, value){
        return await prodRepo.getAllProductPerValue(key, value);
    }

    /**
     * Gibt alle Produkte zurück, wo die CatId Null ist.
     * */
    async getAllProductWhereCatIdIsNullOrEmpty(){
        return await prodRepo.getAllProductWhereCatIdIsNullOrEmpty();
    }

    async getArticlesPerValue(key, value){
        return await artRepo.getAllArticlePerValue(key, value);
    }
}

module.exports = GetProduct;