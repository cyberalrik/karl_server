'use strict';

const logger = require('../../../../helper/util/logger.util');
class GetProductsFromLynn {
    constructor(db) {
        this.db = db;
    }

    async getAllProducts(TAG, p_nr, date){
        let products = [];
        let article = [];
        let result = [];

        try {
            let access = this.db.getProductsDataAccess();
            products = await access.getProducts(p_nr, date);
            article = await access.getArticle(p_nr, date);

            // for (let i = 0; i < products.length; i++) {
            for (let product of products) {
                let item = {
                    p_nr: product.p_nr,
                    p_name: product.p_name,
                    p_text_de: product.p_text_de,
                    p_text_ee: product.p_text_ee,
                    p_brand: product.p_brand,
                    p_brand_key: product.p_brand_key,
                    p_cat_id: product.p_cat_id,
                    p_3_rd: product.p_3_rd,
                    p_keyword1: product.p_keyword1,
                    p_keyword2: product.p_keyword2,
                    p_keyword3: product.p_keyword3,
                    p_infoExternGerman: product.p_infoExternGerman,
                    p_infoExternEnglish: product.p_infoExternEnglish,
                    p_weight: product.p_weight,
                    articles: article[product.p_nr + '-i'],
                }
                result.push(item);
            }
        } catch (e) {
            logger.err(TAG, 'Abbruch', e.message)
            return result;
        }


        return result;
    }
}

module.exports = GetProductsFromLynn;