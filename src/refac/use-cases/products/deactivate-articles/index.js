'use struct';

const logger = require('../../../../helper/util/logger.util');
const TAG = 'task.karl.article.deactivate';

const repoPriceList = require('../../../../repository/repo.pricelist');
const repoArticle = require('../../../../repository/article/repo.article');
const repoChannel = require('../../../../repository/repo.channel');
const serviceChannel = require('../../../../api/channel/service/channel.service');
const UseCaseSendDataToTb = require("../../../use-cases/products/send-data-to-tb");
const MailData = require("../../../entity/mailData");

class Index {
    constructor(db_lynn) {
        this.db_lynn = db_lynn;
    }

    /**
     * Alle die in Lynn als gelöscht, inaktiv und SelOnly gekennzeichnete Artikel werden für alle Kanäle deaktiviert
     * */
    async deactivateArticle() {
        /** Hole Daten von Lynn */
        logger.log(TAG, 'Hole Daten von Lynn', 'Alle die in Lynn als gelöscht, inaktiv und SelOnly gekennzeichnete Artikel werden für alle Kanäle deaktiviert.')
        let access = this.db_lynn.getProductsDataAccess();
        let allArticleNumbers = await access.getDeletedAndSelOnlyLynnArticle()

        if (allArticleNumbers.length === 0) {
            logger.log(TAG, 'Hole Daten von Lynn', 'keine daten erhalten')
            return 0;
        }

        logger.log(TAG, 'Hole Daten von Lynn', `Es wurden ${allArticleNumbers.length} passende Artikel gefunden.`)
        /** Nur die Artikel behalten die es bei Karl gibt */
        let articleNumbers = [];
        for (let index in allArticleNumbers) {
            if(await repoArticle.isArticleAvailable(allArticleNumbers[index])) {
                articleNumbers.push(allArticleNumbers[index]);
            }
        }
        if (articleNumbers.length === 0) {
            return 0;
        }

        logger.log(TAG, 'Artikel auf Löschung prüfen', 'Es werden '
            + articleNumbers.length
            + ' Artikel überprüft, ob sie in Karl auf gelöscht gesetzt werden müssen.');

        /** Welche Kanäle gibt es bei Karl*/
        let channelKeys = await repoChannel.getAllChannelKey();

        /**
         * 1. Schreibe alle Artikel in die Tabelle 'articleChannels', wenn noch nicht vorhanden.
         * 2. Setze Bestand und Preis der Artikel auf 0
         *
         * Daten werden nur in der db geändert, wenn sie vorher anders waren.
         * Es sollen auch nur Artikel im Log aufgeführt werden, die geändert wurden.
         * */
        let number = 0;
        let numberChecked = 0;
        let numberSend = 0;
        let products = [];
        let allProducts = [];
        let articleNumberChange = [];
        let isSend = false;

        for (let indexA in articleNumbers) {
            let articleNumber = articleNumbers[indexA];
            let articleDataChange = false;

            let resultChannel = false;
            let resultPrice = false;
            for (let indexC in channelKeys) {
                /** articleChannel */
                resultChannel = await serviceChannel.setArticleChannelWithResponse(articleNumber, channelKeys[indexC], 0);

                /** Bestand und Preis auf 0 setzen */
                let articleItem = {
                    a_nr: articleNumber,
                    channel: channelKeys[indexC],
                    price: '0',
                    quant: '0',
                }
                resultPrice = await repoPriceList.editPriceListItem(articleItem);

                if (resultChannel || resultPrice) {
                    let infoText = [];
                    if (resultChannel) {
                        infoText.push(`wurde der Channel ${articleItem.channel} deaktiviert`);
                    }

                    if (resultPrice) {
                        infoText.push(`wurde der Preis für den Kanal ${articleItem.channel} auf 0 gesetzt`);
                    }

                    logger.log(TAG, 'Artikeldaten verarbeitet', `Für den Artikel mit der Nr.:${articleItem.a_nr} ${infoText.join(', ')}.`)

                    articleDataChange = true;
                }
            }

            /** Daten an TB senden und Daten merken, um sie per Mail zu versenden */
            if (articleDataChange) {
                logger.log(TAG,'Daten verarbeiten', `Daten der Artikels ${articleNumber} an TB senden und merken, um sie per Mail zu versenden.`);
                let articleObject = await repoArticle.getArticlePerID(articleNumber);
                products.push(articleObject.p_nr);
                allProducts['p_nr-' + articleObject.p_nr] = articleObject.p_nr;
                articleNumberChange.push(articleNumber);

                logger.log(TAG
                    , 'Artikel deaktiviert'
                    , 'Die Artikelnummer ' + articleNumber + ' wurde für alle Kanäle deaktiviert.');

                numberSend++;
                /**
                 * Im Anschluss sollen alle geänderten Produkte mit ihrem Artikel an TB gesendet werden.
                 * Maximal 10 auf einmal.
                 * */
                if (numberSend >= 10) {
                    isSend = true;
                    numberSend = 0;
                    await this.send(products);
                    logger.log(TAG
                        , `Löschdaten an TB gesendet`
                        , `Löschung für p_nr: ${products.join(',')} an TB gesendet`);

                    products = [];
                }

                number++;
            }

            numberChecked++;
            if (numberChecked % 50 === 0) {
                logger.log(
                    TAG,
                    `Artikel überprüfen`,
                    `Es wurden schon ${numberChecked} Artikel überprüft und ${number} Artikel deaktiviert.`);
            }
        }

        if (numberChecked > 0) {
            logger.log(
                TAG,
                `Artikel überprüfen`,
                `Es wurden ${numberChecked} Artikel überprüft und ${number} Artikel deaktiviert.`);
        }

        /** Zum Anschluss die restlich geänderten Produkte mit ihrem Artikel an TB senden. */
        if (numberSend > 0) {
            isSend = true;
            await this.send(products);
            logger.log(TAG
                ,`Löschdaten an TB gesendet`
                , `Löschung für p_nr: ${products.join(',')} an TB gesendet`);
        }

        let productsInfo = [];
        for (let index in allProducts) {
            productsInfo.push(allProducts[index]);
        }

        if (isSend) {
            /** Info per eMail versenden. */
            let mailData = new MailData(
                `Folgende Daten wurden zum Löschen/ Deaktivieren an TB gesendet.`,
                `\nDiese Artikel wurden in Lynn auf einen inaktiven Zustand gesetzt.\n\nArtikelnummern: ${
                    articleNumberChange.join(',')}\n\nKarl-ProductId's: ${productsInfo.join(',')}`,
                's.wiehe@cybertrading.de'
            );

            logger.log(TAG,
                `Artikel bei TB deaktiviert`,
                `Produkt- und Artikeldaten wurde zum Deaktivieren an TB gesendet.`,
                mailData);
        }

        logger.log(TAG, 'deaktivieren abgeschlossen', `Das deaktivieren von ${number} Artikel ist abgeschlossen.` )
        return number;
    }

    async send(products) {
        let useCaseSendDataToTb = new UseCaseSendDataToTb();
        await useCaseSendDataToTb.sendProductAndArticleDataToTb(products);
    }
}

module.exports = Index