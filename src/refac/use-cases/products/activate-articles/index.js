'use struct';

const logger = require('../../../../helper/util/logger.util');
const TAG = 'task.karl.article.activate';
const repoArticleChannel = require('../../../../repository/article/repo.article.channel');
const Mail = require("../../../../services/service.send.mail");

class Index {
    constructor(db_karl, db_lynn) {
        this.db_lynn = db_lynn;
        this.db_karl = db_karl;
    }

    /**
     * Alle aktiven Artikel in Lynn aus alle der Tabelle "articleChannels" löschen
     * */
    async activateArticle(result) {
        let messages;
        if (result.messages === undefined) {
            result.messages = [];
        }
        /**
         * Löscht alle nicht mehr benötigte Einträge in der Tabelle 'articleChannel'.
         * Grund: Artikel sind nicht mehr vorhanden.
         * */
        let resultAllGroupBy = await repoArticleChannel.getAllGroupBy('a_nr');
        let countOfDeaktivateKarlArticle_1 = resultAllGroupBy.length;

        messages = 'Entfernen aller nicht mehr vorhandenen Artikel aus der Tabelle "articleChannels".';
        logger.log(TAG, 'Starte Job', messages);
        result.messages.push(messages);
        result = this.db_karl.aktivateOfArticle(result);
        if (result.successful) {
            await result.access.deleteAllEntriesTheIsNotInArticle();
            messages = 'Es wurden alle nicht mehr vorhandenen Artikel aus der Tabelle "articleChannels" entfernt.';
            logger.log(TAG, 'cleanup von articleChannels', messages);
        } else {
            messages = 'Beim Versuch alle nicht mehr vorhandenen Artikel aus der Tabelle "articleChannels" zu entfernen, ist ein Fehler aufgetreten.';
            logger.err(TAG, 'cleanup von articleChannels', messages);
        }
        result.messages.push(messages);

        resultAllGroupBy = await repoArticleChannel.getAllGroupBy('a_nr');
        let countOfDeaktivateKarlArticle_2 = resultAllGroupBy.length;
        messages = `Aus Karl wurde(n) ${countOfDeaktivateKarlArticle_1 - countOfDeaktivateKarlArticle_2} deaktivierte(r) Artikelleichen entfernt.`;
        logger.log(TAG, 'deaktivierte Artikel entfernt', messages);
        result.messages.push(messages);

        /**++==-- Vergleichen aller in Lynn aktiven und in Karl inaktiven Artikel --==++*/
        /** Hole alle inaktive Artikel aus Karl */
        let resultData = await repoArticleChannel.getAllGroupBy('a_nr');
        let karlInaktivData = await this.manipulateDataFromKarl(resultData, 'a_nr');
        messages = `Aus Karl wurde(n) ${karlInaktivData.length} deaktivierte Artikel ermittelt.`;
        logger.log(TAG, 'deaktivierte Artikel ermittelt', messages);
        result.messages.push(messages);

        /** Hole alle aktive Artikel aus Lynn */
        resultData = {};
        let lynnAktivData = [];
        result = this.db_lynn.aktivateOfArticle(result);
        if (result.successful) {
            resultData = await result.access.getAllActiveArticleNumber();
            lynnAktivData = await this.manipulateDataFromLynn(resultData);
            messages = `Aus Lynn wurde(n) ${lynnAktivData.length} aktive Artikel ermittelt.`;
            logger.log(TAG, 'aktive Artikel aus Lynn', messages);
        } else {
            messages = 'Beim Versuch alle aktive Artikel aus Lynn zu ermitteln, ist ein Fehler aufgetreten.';
            logger.err(TAG, 'Fehler bei Artikelabfrage aus Lynn', messages);
        }
        result.messages.push(messages);

        /**
         * Vergleichen und Liste erstellen von Artikel,
         * die aus der Tabelle Karl-DB Tabelle 'articleChannels' entfernt werden muss.
         * Es darf kein aktiver Lynn Artikel in der Karl-DB der Tabelle 'articleChannels' enthalten sein.
         * */
        let deleteArticleOfKarl = [];
        for (let index in karlInaktivData) {
            if (lynnAktivData.includes(karlInaktivData[index])) {
                deleteArticleOfKarl.push(karlInaktivData[index]);
            }
        }

        messages = `Es wurden ${deleteArticleOfKarl.length} deaktivierte Artikel in Karl ermittelt, die in Lynn aktiv sind.`;
        logger.log(TAG, 'aktiven Artikel aus Karl', messages);
        result.messages.push(messages);

        /**++==-- Entfernen aller Artikel aus der Tabelle 'articleChannels'. --==++*/

        /**
         * Damit das Query nicht zu lang wird, unterteile ich die zu löschenden Artikel in 50er-Teile.
         * */
        let deleteBulk = [];
        for (let index in deleteArticleOfKarl) {
            deleteBulk.push(deleteArticleOfKarl[index]);

            if ((parseInt(index) + 1) % 2 === 0) {
                /** Hier muss gelöscht werden */
                await this.deleteArticleChannels(deleteBulk);
                deleteBulk = [];
            }
        }

        if (deleteBulk.length > 0) {
            await this.deleteArticleChannels(deleteBulk);
        }

        /** Nachricht versenden */
        if (deleteArticleOfKarl.length > 0) {
            let mail = new Mail();
            await mail.sendEMail(
                `Es wurden ${
                    deleteArticleOfKarl.length
                    } deaktivierte Artikel aus Karl entfernt`,
                'Artikel, die in Karl wieder aktiviert wurden.',
                `Es wurden in der Karl-DB Tabelle 'articleChannels' ${
                    deleteArticleOfKarl.length
                    } Artikel entfernt, die in Lynn aktiv waren.\nEs handelt sich um folgende Artikel:\n${
                    deleteArticleOfKarl.join(', ')
                    }`,
                `${TAG}`,
                `Protokoll:\n${result.messages.join('\n')}`
            );
        }

        return result;
    }

    /** Übergebene Daten werden in Array überführt */
    async manipulateDataFromKarl(data, dataName) {
        let result = [];
        for (let index of data) {
            result.push(index.getDataValue(dataName));
        }

        return result;
    }

    /** Übergebene Daten werden in Array überführt */
    async manipulateDataFromLynn(data) {
        let result = [];
        for (let index of data) {
            result.push(Object.values(index)[0]);
        }

        return result;
    }

    /**
     * Alle a_nr, die in dem übergebenen Array enthalten sind, werden gelöscht.
     *
     * @param deleteBulk muss ein Array sein.
     * */
    async deleteArticleChannels(deleteBulk) {
        await repoArticleChannel.delete(deleteBulk);
    }
}

module.exports = Index