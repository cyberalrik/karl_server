'use strict';

const cciSQL = require('../../../../helper/maria.cci.database.helper');
const sql = require('../../../../helper/sql/query.collect');
const prodRepo = require('../../../../repository/repo.product');
const ServiceConfirmCategory = require('../../../../services/service.confirm.category');
const logger = require('../../../../helper/util/logger.util');

class Index {
    constructor(db) {
        this.ss = db;
    }

    async changeCategory(TAG, list, result) {
        let serviceConfirmCategory = new ServiceConfirmCategory();
        let confirmCategory = await serviceConfirmCategory.getConfirmCategoryPrepared();

        let itemList = [];
        let noFound = 0;
        let newCategory = 0;
        for (let i = 0; i < list.length; i++) {
            let dbItem = (await cciSQL.sql(await sql.getCciSQL(list[i].p_name)));
            if (dbItem === undefined || dbItem === null || dbItem[0] === undefined) {
                noFound++;
                if ((i + 1) % 500 === 0) {
                    logger.log(
                        TAG,
                        `p_cat_id`,
                        `Es wurden schon ${i + 1
                        } von ${list.length
                        } Produkte überprüft, davon konnten ${noFound
                        } keiner Kategorien zugeordnet werden.`
                    );
                }

                continue;
            }

            /** Ist die Kategorie nicht */
            if (!confirmCategory.includes(dbItem[0]['cat_id'])) {
                logger.err(TAG,
                    `Neue CNet Kategorie`,
                    `Es ist eine neue CNet Kategorie aufgetreten`,
                    `Hallo Silke, ` +
                    `\n\ndies ist eine automatische eMail aus Karl heraus. ` +
                    `\nEs wurde eine neue Kategorie benutzt, die wir noch nicht in TB angelegt oder gematscht haben.` +
                    `\nBitte lege die Kategorie in TB an.` +
                    `\nEs handelt sich um die cat-Id: "${dbItem[0]['cat_id']
                    }" mit der Bezeichnung: "${dbItem[0]['p_cat_name']
                    }" und das dazugehörende Produkt ist: "${list[i].p_name
                    }" mit der Karl-ProduktNr.: "${list[i].p_nr
                    }"\n\n\nFolgenden Workaround durchführen:\n` +
                    `- Kategorie suchen in TB\n` +
                    `- wenn nicht vorhanden, dann neu anlegen (ID vergeben nicht vergessen)\n` +
                    `- Zuordnung der Shop-Kategorien und Ebay-Kategorien ausführen` +
                    `\n\nDanke ;)`,
                    's.wiehe@cybertrading.de'
                );
            }

            newCategory++;
            let item = {
                p_cat_id: dbItem[0]['cat_id'],
                p_cat_name: dbItem[0]['p_cat_name'],
                p_nr: list[i].p_nr
            }
            itemList.push(item);

            /** Wurden 500 Kategorien überprüft, Ergebnis abspeichern. */
            if ((i + 1) % 500 === 0) {
                logger.log(
                    TAG,
                    `p_cat_id`,
                    `Es wurden schon ${i + 1
                    } von ${list.length
                    } Produkte überprüft und bei ${newCategory
                    } Produkten konnten eine Kategorie zugeordnet werden.`
                );

                for (let k = 0; k < itemList.length; k++) {
                    await prodRepo.updateProd(itemList[k]);
                }

                itemList = [];
            }
            // break;
        }

        let message = `Es konnte von ${list.length
        } übergebenen Produkten ${newCategory
        } einer Kategorie zugeordnet werden. Für ${noFound
        } wurde keine Kategorie gefunden.`;

        logger.log(TAG, `p_cat_id`, message);
        result.messages.push(message);

        for (let i = 0; i < itemList.length; i++) {
            await prodRepo.updateProd(itemList[i]);
        }

        return result
    }
}

module.exports = Index