'use struct';

const UseCaseTbOneAll = require("../../forTasks/tboneAll");
const UseCaseGetProduct = require("../get-product");
const UseCaseArticlePriceToTbOne = require("../../pricelist/articleDataToTbOne/price");
const UseCaseArticleStockToTbOne = require("../../pricelist/articleDataToTbOne/stock");
let TAG = 'send-data-to-tb';

class AddProductsIntoKarl {
    async sendProductAndArticleDataToTb(products){
        for (let index in products) {
            let p_nr = products[index];
            /** Übergibt Produktdaten an TB */
            let useCaseTbOneAll = new UseCaseTbOneAll();
            await useCaseTbOneAll.call(TAG, p_nr);

            /** Welche Artikel hat das Produkt? */
            let useCaseGetProduct = new UseCaseGetProduct();
            let articles = await useCaseGetProduct.getArticlesPerValue('p_nr', p_nr);

            /** Übergibt alle Preise und Mengen der Artikel */
            let useCaseArticlePriceToTbOne = new UseCaseArticlePriceToTbOne(TAG);
            let useCaseArticleStockToTbOne = new UseCaseArticleStockToTbOne(TAG);

            for (let currentArticle of articles) {
                await useCaseArticlePriceToTbOne.sendArticlePriceToTbOne(currentArticle.a_nr, '');
                await useCaseArticleStockToTbOne.sendArticleStockToTbOne(currentArticle.a_nr, '');
            }
        }
    }
}

module.exports = AddProductsIntoKarl