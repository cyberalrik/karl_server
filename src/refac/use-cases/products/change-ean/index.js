'use strict';

const cciSQL = require('../../../../helper/maria.cci.database.helper');
const sql = require('../../../../helper/sql/query.collect');
const artRepo = require('../../../../repository/article/repo.article');
const prodRepo = require('../../../../repository/repo.product');
const logger = require('../../../../helper/util/logger.util');

class ChangeEAN {
    constructor(db, max) {
        this.db = db;
        this.max = max;
    }

    async changeEANs(TAG, list, result) {
        let working = [];
        let product;
        let eanAvailable = true;
        let e2NewEan = 0;

        try {
            for (let i = 0; i < list.length; i++) {
                product = (await prodRepo.getAllProductPerValue('p_nr', list[i].getDataValue('p_nr')))[0];

                if (product === null || product === undefined) {
                    logger.err(TAG, `Übersprungen`, `Kein Produkt ${list[i].getDataValue('p_nr')} gefunden`);
                    continue
                }

                let ean = (await cciSQL.sql(sql.getEanFromCciSQL(list[i].getDataValue('a_prod_nr'))));
                if (ean !== undefined && ean[0] !== undefined && ean[0]['UPCEAN_Code'])
                    ean = ean[0]['UPCEAN_Code'];
                else {
                    ean = undefined;
                }

                if (product.getDataValue('p_3_rd') || list[i].getDataValue('a_cond') !== 'new' || ean === undefined || ean === null) {
                    ean = await this.calcEAN(list[i].getDataValue('a_nr'));
                }

                /** Wenn keine EAN' mehr vorhanden sind, abbrechen und melden. */
                if (ean === null) {
                    eanAvailable = false;

                    break;
                }

                let item = {
                    anr: list[i].getDataValue('a_nr'),
                    ean: ean
                }
                working.push(item);
                // if (working.length > this.max) {
                //     break;
                // }
            }

            let message = `Für ${working.length} Artikel konnte eine EAN von cNet ermittelt werde.`;
            logger.log(TAG,`EAN speichern`, message);
            result.messages.push(message);

            if (e2NewEan > 0) {
                message = `Für ${e2NewEan} E2-Artikel konnte eine EAN von seinem Originalartikel ermittelt werden.`;
                logger.log(TAG, `EAN speichern`, message);
                result.messages.push(message);
            }

            for (let i = 0; i < working.length; i++) {
                await artRepo.updateArt(working[i].anr, 'a_ean', working[i].ean);
            }

            if (!eanAvailable) {
                message = `Es gibt keine freien EAN's mehr. Bitte neu bestellen und in Tabelle "cyberstats.cyber_ean" einpflegen.`;
                logger.err(TAG,`keine freien EAN's mehr`, message,
                    `Es müssen neue EAN's bei antrag@gs1-germany.de per Mail bestellt werden.
Betreff: "Zuteilung eines neuen GLN Nummernkreis"
und folgendem Inhalt:

>>>>
Sehr geehrte Damen und Herren,

da unsere derzeitigen Nummernkreise aufgebraucht sind, benötigen wir einen neuen GLN Nummernkreis.
Ich bitte um Zuteilung dessen und eine dazugehörige Prüfziffernliste von 100.000 EANs zum Download. 
MfG ...
<<<<
`, 's.wnendt@cybertrading.de');
                result.messages.push(message);
            }
        } catch (e) {
            logger.err(TAG, product.p_name, e.message);
            let message = `Fehler beim Produkt ${product.p_name}`;
            logger.log(TAG,`Abgebrochen`, message);
            result.messages.push(message);
        }

        return result
    }

    async calcEAN(a_nr){
        return (await cciSQL.sql(`call cyberstats.cy_get_ean('${a_nr}')`))['0']['0']['var_output'];
    }
}

module.exports = ChangeEAN;

