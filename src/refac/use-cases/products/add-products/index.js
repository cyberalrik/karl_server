'use struct';

const productRepo = require('../../../../repository/repo.product');
const articleRepo = require('../../../../repository/article/repo.article');
const artCompRepo = require('../../../../repository/article/repo.article.components');
const logger = require('../../../../helper/util/logger.util');
let TAG = '';

class AddProductsIntoKarl {
    constructor(db, tag) {
        this.db = db;
        TAG = tag;
    }

    async compareProducts(lynnProducts, karlProducts) {
        if (lynnProducts === null
            || lynnProducts === undefined
            || karlProducts === null
            || karlProducts === undefined
        ) {
            return false;
        }
        let result = {create: 0, update: 0, unnecessary: 0};

        logger.log(TAG, `${lynnProducts.length} Produkte`, `Es werden ${lynnProducts.length} Produkte verarbeitet.`)
        let productNumber = 0;
        for (let lynnProduct of lynnProducts) {
            productNumber++;
            if (productNumber % 5000 === 0 && productNumber !== 0) {
                logger.log(TAG, `${productNumber} verarbeitet`,
                    `Es wurden schon ${productNumber} verarbeitet. ${result.create} angelegt, ${result.update} geändert (${lynnProducts.length})`);
                if (result.unnecessary > 0) {
                    logger.log(TAG, `${productNumber} verarbeitet`,
                        `Es wurden ${result.unnecessary} Produkte unnötig überprüft.`);
                }
            }

            /** Prüfen, ob das Produkt schon in Karl existiert - wenn nicht, dann anlegen */
            if (karlProducts['p_nr_' + lynnProduct.p_nr] === undefined) {
                result = await productRepo.addOrUpdateProduct(lynnProduct, result);

                continue;
            }
            let karlProduct = karlProducts['p_nr_' + lynnProduct.p_nr];

            /** Prüfen, ob das lynnProduct und karlProduct übereinstimmt - wenn nicht, dann korrigieren */
            if (!(await this.isItTheSameProduct(lynnProduct, karlProduct))) {
                result = await productRepo.addOrUpdateProduct(lynnProduct, result);
            }
        }
        logger.log(TAG, `${productNumber} verarbeitet`,
            `Es wurden ${productNumber} Produkte verarbeitet. ${result.create} angelegt, ${result.update} geändert`);

        if (result.unnecessary > 0) {
            logger.log(TAG, `${productNumber} verarbeitet`,
                `Es wurden ${result.unnecessary} Produkte unnötig überprüft.`);
        }

    }

    /**
     * Vergleicht, ob die Standartwerte der Produkte überein stimmen.
     * Und gibt das Ergebnis als Wahrheitswert zurück.
     *
     * @param lynnProduct
     * @param karlProduct
     *
     * @return {Promise <boolean>}
     * */
    async isItTheSameProduct(lynnProduct, karlProduct) {
        return (lynnProduct.p_name                       === karlProduct.p_name
            && lynnProduct.p_text_de.substring(0, 254)  === karlProduct.p_text_de
            && lynnProduct.p_text_ee.substring(0, 254)  === karlProduct.p_text_ee
            && lynnProduct.p_brand                      === karlProduct.p_brand
            && lynnProduct.p_brand_key                  === karlProduct.p_brand_key
            && lynnProduct.p_3_rd                       === karlProduct.p_3_rd
            && lynnProduct.p_weight                     === karlProduct.p_weight // Zum Vergleich in ein String gewandelt (+'')
            && lynnProduct.p_keyword1.substring(0, 50)  === karlProduct.p_keyword1
            && lynnProduct.p_keyword2.substring(0, 50)  === karlProduct.p_keyword2
            && lynnProduct.p_keyword3.substring(0, 50)  === karlProduct.p_keyword3);
    }
            // && lynnProduct.p_cat_id                    === karlProduct.p_cat_id
            // && lynnProduct.p_is_fake                   === karlProduct.p_is_fake

    /*** +++ --- Artikel --- +++ *** +++ --- Artikel --- +++ *** +++ --- Artikel --- +++ *** +++ --- Artikel --- +++ ***/
    async compareArticles(lynnProducts, karlArticles) {
        if (lynnProducts === null
            || lynnProducts === undefined
            || karlArticles === null
            || karlArticles === undefined
        ) {
            return false;
        }
        let result = {create: 0, update: 0, unnecessary: 0};

        logger.log(TAG, `${lynnProducts.length} Produkte`, `Es werden die Artikel von ${lynnProducts.length} Produkte verarbeitet.`)
        let productNumber = 0;
        let articleNumber = 0;
        for (let lynnProduct of lynnProducts) {
            productNumber++;
            for (let lynnArticle of lynnProduct.articles) {
                articleNumber++;
                if (articleNumber % 50000 === 0 && articleNumber !== 0) {
                    logger.log(TAG, `${articleNumber} Produkte verarbeitet`,
                        `Es wurden schon ${articleNumber} Artikel (${productNumber}) Produkte von ${lynnProducts.length} Produkten verarbeitet. ${result.create} angelegt, ${result.update} geändert`);
                }
                if (result.unnecessary > 0) {
                    logger.log(TAG, `${articleNumber} verarbeitet`,
                        `Es wurden ${result.unnecessary} Artikel unnötig überprüft.`);
                }

                /**
                 * Prüfen, ob der Artikel schon in Karl existiert - wenn nicht, dann anlegen
                 * */
                if (karlArticles['a_nr_' + lynnArticle.a_nr] === undefined) {
                    result = await articleRepo.addOrUpdateArticle(lynnArticle, result);

                    /** Die zugehörenden 'ebay_condition' anlegen */
                    await artCompRepo.addComponent(
                        lynnArticle.a_nr,
                        'ebay_condition',
                        lynnArticle.a_cond === 'ref' ? 'used' : 'new'
                    );

                    continue;
                }
                let karlArticle = karlArticles['a_nr_' + lynnArticle.a_nr];

                /** Prüfen, ob das lynnProduct und karlProduct übereinstimmt - wenn nicht, dann korrigieren */
                if (!(await this.isItTheSameArticle(lynnArticle, karlArticle))) {
                    result = await articleRepo.addOrUpdateArticle(lynnArticle, result);
                }
            }
        }
        logger.log(TAG, `${articleNumber} Artikel verarbeitet`,
            `Es wurden ${articleNumber} Artikel von ${lynnProducts.length} Produkten verarbeitet. ${result.create} angelegt, ${result.update} geändert`);
        if (result.unnecessary > 0) {
            logger.log(TAG, `${articleNumber} verarbeitet`,
                `Es wurden ${result.unnecessary} Artikel unnötig überprüft.`);
        }

    }

    /**
     * Vergleicht, ob die Standartwerte des Artikels übereinstimmen.
     * Und gibt das Ergebnis als Wahrheitswert zurück.
     *
     * @param lynnArticle
     * @param karlArticle
     * @return {Promise <boolean>}
     * */
    async isItTheSameArticle(lynnArticle, karlArticle) {
        return (lynnArticle.a_nr           === karlArticle.a_nr
            && lynnArticle.a_nr2           === karlArticle.a_nr2
            && lynnArticle.p_nr            === karlArticle.p_nr
            && lynnArticle.a_prod_nr       === karlArticle.a_prod_nr
            && lynnArticle.a_cond          === karlArticle.a_cond
            && lynnArticle.a_delivery_time === karlArticle.a_delivery_time
        );
    }
}

module.exports = AddProductsIntoKarl