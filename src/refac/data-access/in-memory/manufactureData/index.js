'use strict';

const ManuFac = require('../../../entity/manufacture')

class ManufacturerData {
    constructor() {
        this.MODELL = [];
    }

    addManufacturer(cnet, name){
        let fac = new ManuFac(cnet, name);
        let item = fac.getManufacture();
        this.MODELL.push(item);
    }

    getAllManufacturers(){
        return this.MODELL;
    }
}

module.exports = ManufacturerData
