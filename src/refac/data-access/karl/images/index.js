'use strict'

// const query = require('../../../../helper/sql/query.join.karl');
// import {QueryOptionsWithType} from "sequelize/types/lib/query-interface";
const { QueryTypes } = require('sequelize');
const SqlQueryJoinKarlHelper = require('../../../../helper/sql/query.join.karl');
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

const _ = require('underscore');

class ProductData {
    constructor() {
    }

    async getAllProductIdsFromCtImages(){
        let result;
        let sqlQueryJoinKarlHelper = new SqlQueryJoinKarlHelper();
        result = await seq.query(await sqlQueryJoinKarlHelper.getAllProductIdsFromCtImages(), { type: QueryTypes.SELECT })

        return result;
    }

    async getAllProductIdsFromCantoForeignImages(){
        let result;
        let sqlQueryJoinKarlHelper = new SqlQueryJoinKarlHelper();
        result = await seq.query(await sqlQueryJoinKarlHelper.getAllProductIdsFromCantoForeignImages(), { type: QueryTypes.SELECT })

        return result;
    }

     async getAllProductIdsFromCNetImages(){
        let result;
        let sqlQueryJoinKarlHelper = new SqlQueryJoinKarlHelper();
        result = await seq.query(await sqlQueryJoinKarlHelper.getAllProductIdsFromCNetImages(), { type: QueryTypes.SELECT })

        return result;
    }

}

module.exports = ProductData;