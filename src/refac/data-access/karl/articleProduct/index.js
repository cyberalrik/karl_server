'use strict'

const { QueryTypes } = require('sequelize');
const SqlQueryJoinKarlHelper = require('../../../../helper/sql/query.join.karl');
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

const _ = require('underscore');

class ProductData {
    constructor() {
    }

     async getAllProductOfTheNewArticle(date){
        let result;
        let sqlQueryJoinKarlHelper = new SqlQueryJoinKarlHelper();
        result = await seq.query(await sqlQueryJoinKarlHelper.getAllProductOfTheNewArticle(date), { type: QueryTypes.SELECT })

        return result;
    }

     async getAllProductOfArticleList(date){
        let result;
        let sqlQueryJoinKarlHelper = new SqlQueryJoinKarlHelper();
        result = await seq.query(await sqlQueryJoinKarlHelper.getAllProductOfArticleList(date), { type: QueryTypes.SELECT })

        return result;
    }

}

module.exports = ProductData;