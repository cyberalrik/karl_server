'use strict'

const { QueryTypes } = require('sequelize');
const SqlQueryJoinKarlHelper = require('../../../../helper/sql/query.join.karl');
const SqlQueryKarlHelper = require('../../../../helper/sql/query.karl');

class CategoryData {
    constructor(seq) {
        this.seq = seq
    }

     async getAllProductCategory(){
        let result;
        let sqlQueryJoinKarlHelper = new SqlQueryJoinKarlHelper();
        let query = await sqlQueryJoinKarlHelper.getQueryAllProductCategoryFromKarlForLynn();
        result = await this.seq.query(query, { type: QueryTypes.SELECT });

        return result;
    }

    async setShopCategoryIdInKarl(p_nr, shopCategoryId) {
        let sqlQueryKarlHelper = new SqlQueryKarlHelper();
        await this.seq.query(await sqlQueryKarlHelper.setShopCategoryIdInKarl(p_nr, shopCategoryId), {type: QueryTypes.UPDATE});
    }
}

module.exports = CategoryData;