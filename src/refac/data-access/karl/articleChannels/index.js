'use strict'

const { QueryTypes } = require('sequelize');
const SqlQueryJoinKarlHelper = require('../../../../helper/sql/query.join.karl');

class Index {
    constructor(seq) {
        this.seq = seq
    }

     async deleteAllEntriesTheIsNotInArticle(){
        let sqlQueryJoinKarlHelper = new SqlQueryJoinKarlHelper();
        let query = await sqlQueryJoinKarlHelper.getQueryDeleteAllEntriesTheIsNotInArticle();
        await this.seq.query(query, { type: QueryTypes.DELETE });
    }
}

module.exports = Index;