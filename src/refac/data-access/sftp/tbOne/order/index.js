'use strict'

const Client = require('ssh2-sftp-client');

const logger = require('../../../../../helper/util/logger.util');
const TAG = 'service.sftp.tbone.load.files';

// const streamToBuffer = require('stream-to-buffer');
// const xml2js = require('xml2js');
// const OrderData = require('../services/order/service.orderList')

class index {

    /**
     * Lädt nach Vorgabe des Pattern entsprechende Dateien aus dem sftpPath herunter und speichert sie im LocalPath.
     * Ein Array mit Dateinamen wird zurück gegeben.
     *
     * @returns {Promise<[]>}
     */
    async loadFromTbOne(params) {

        const sftpPath = params.tboneConfig.DOWNLOAD_SFTP_PATH;
        const pattern = params.tboneConfig.DOWNLOAD_PATTERN;

        let sftp = new Client();
        let files = []

        await sftp.connect({
            host: params.tboneConfig.SERVER,
            username: params.tboneConfig.USER,
            password: params.tboneConfig.PASSWORD,
        }).then(async () => {
            // Verzeichniss auf dem Ftp auslesen

            return sftp.list(sftpPath);
        }).then(async filesOfSftp => {
            // Dateinamen Filtern, nur bestimmte Namen und Dateien die größer 0 sind.
            let index = 0;
            for (let i = 0; i < filesOfSftp.length; i++) {
                if (filesOfSftp[i].name.match(pattern) !== null &&
                    filesOfSftp[i].size > 0
                ) {
                    files[index++] = filesOfSftp[i].name;
                }
            }

            return files;
        }).then( async (files) => {
            // Ermittelte Dateien herunterladen
            for (let i = 0; i < files.length; i++) {
                await sftp.fastGet(sftpPath + files[i], params.localPath + files[i], {})
            }
        }).then(() => {
            // Verbindung beenden
            sftp.end();
        }).catch(err => {
            // Loggen, falls Fehler auftreten
            logger.err(TAG, 'FTP Download Error', err.message)
        });
        params.localFiles = files;
        return params;
    }
}

module.exports = index;


