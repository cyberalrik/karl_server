'use strict'

const sql = require('../../../../helper/lynn.sql.database.helper');
const query = require('../../../../helper/sql/query.collect');

// const _ = require('underscore');

class ProductData {
    constructor() {
    }

    async getProducts(p_nr, date){
        let result;
        let lynnProducts = await sql.sql(query.getProductsFromLynn(p_nr, date));
        result = lynnProducts['recordset'];
        return result;
    }

    async getArticle(p_nr, date){
        let lynnArticles = await sql.sql(query.getArticleFromLynn(p_nr, date));
        let articles = lynnArticles['recordset'];

        let work = [];
        for (let article of articles){
            if (work[article.p_nr + '-i'] === undefined || work[article.p_nr] === null){
                work[article.p_nr+ '-i'] = [];
            }
            work[article.p_nr+ '-i'].push(article);
        }

        return work;
    }

    /**
     * Gibt alle in Lynn als "gelöscht" oder als "SelOnly" gekennzeichnete Artikel zurück.
     * ModifiedDate -* Tage
     *
     * @returns {Promise<Array>}
     * */
    async getDeletedAndSelOnlyLynnArticle() {
        let deletedArticles = await sql.sql(query.getDeletedAndInaktivArticleFromLynn(1));
        let articles = deletedArticles['recordset'];
        let articleNumbers = [];
        for (let i = 0; i < articles.length; i++){
            articleNumbers.push(articles[i].a_nr);
        }

        let selOnlyArticles = await sql.sql(query.getSelOnlyArticleFromLynn(1));
        articles = selOnlyArticles['recordset'];
        for (let i = 0; i < articles.length; i++){
            articleNumbers.push(articles[i].a_nr);
        }

        return articleNumbers;
    }

    /** Gibt alle ProductForeignId zurück, die ein CNetFoto Kennzeichen besitzen */
    async getAllCNetImagesFeature() {
        let result = null;
        let cNetImages = await sql.sql(query.getAllCNetFotoFeature());
        if (cNetImages !== null) {
            result = cNetImages['recordset'];
        }

        return result;
    }

    /** Gibt alle ProductForeignId zurück, die ein CTFoto Kennzeichen besitzen */
    async getAllCTImagesFeature() {
        let result = null;
        let cNetImages = await sql.sql(query.getAllCTFotoFeature());
        if (cNetImages !== null) {
            result = cNetImages['recordset'];
        }

        return result;
    }

    /** Setzt bei allen übergebenen p_nr die cNetFoto Kennzeichen in Lynn */
    async setCNetFeature(productIds, value) {
        // console.log(query.setCNetFoto(productIds, value));
        await sql.sql(query.setCNetFoto(productIds, value));
    }

    /** Setzt bei allen übergebenen p_nr die cTFoto Kennzeichen in Lynn */
    async setCTFeature(productIds, value) {
        // console.log(query.setCTFoto(productIds, value));
        await sql.sql(query.setCTFoto(productIds, value));
    }

    /** Gibt alle Prod. Daten zurück, die nur einen Artikel haben */
    async getAllSingleArticles() {
        let singleArticles = await sql.sql(query.getAllSingleArticles());

        return singleArticles;
    }

    /** Gibt alle Prod. Daten zurück, die nur einen Artikel haben */
    async updateSingleArticles(oldProductId, newProductId) {
        await sql.sql(query.updateSingleArticle(oldProductId, newProductId));
    }

    /** Gibt alle Produkte mit new und ref Artikelnr. zurück. Nur, wenn es beide Zustände gibt. */
    async getAllNewAndRefArticlesOfAProduct() {
        let allNewAndRefArticle = await sql.sql(query.getAllNewAndRefArticlesOfAProduct());

        let result = {};
        if (allNewAndRefArticle.recordset !== undefined)
        {
            result = allNewAndRefArticle.recordset
        }

        return result;
    }

    /** Gibt alle doppelten Produkte in Lynn zurück */
    async getDoubleLynnProduct() {
        let cNetImages = await sql.sql(query.getDoubleLynnProduct());
        let result = cNetImages['recordset'];

        return result;
    }
}

module.exports = ProductData;