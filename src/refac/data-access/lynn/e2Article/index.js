'use strict'

const sql = require('../../../../helper/lynn.sql.database.helper');
const query = require('../../../../helper/sql/query.collect');
const logger = require('../../../../helper/util/logger.util');

class ProductData {
    constructor() {
    }

    /**
     * E2 = eBay-2 Artikel werden gebildet, sobald ein Artikel einen eBay-2 Preis und Menge besitzt.
     * */
    async getE2Article(p_nr){
        try {
            let lynnArticles = await sql.sql(query.getE2ArticleFromLynn(p_nr), null, null, 'Manuell');

            if (lynnArticles !== null && lynnArticles['recordset'] !== null && lynnArticles['recordset'] !== undefined) {
                return lynnArticles['recordset'];
            }

            return null;
        } catch (e) {
            await logger.err(
                'getE2Article'
                , 'Fehler aufgetreten'
                , 'Beim Abrufen von E2-Artikel ist ein Fehler aufgetreten.'
                , `Folgernder Fehler ist beim Abruf von E2-Artikeln aufgetreten\n${
                    e.messages
                }`
            );
            console.log('Fehler beim Abrufen von E2-Artikel');
        }
    }

    async getAllArticleFromProduct(p_nr) {
        let lynnArticles = await sql.sql(query.getAllArticleFromProduct(p_nr));

        if (lynnArticles !== null) {
            return lynnArticles['recordset'];
        }

        return null;
    }
}

module.exports = ProductData;