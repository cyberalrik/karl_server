'use strict'

const { QueryTypes } = require('sequelize');
const query = require('../../../../helper/sql/query.collect');


class CategoryData {
    constructor(seq) {
        this.seq = seq
    }
     async getAllProductCategory() {
        let result;
         result = await this.seq.sql(query.getQueryAllCategoryFromLynn(), {type: QueryTypes.SELECT});

         return result.recordsets[0];
    }

    async runUpdateQuery(sql) {
        await this.seq.sql(sql, {type: QueryTypes.UPDATE});
    }
}

module.exports = CategoryData;