'use strict'

const { QueryTypes } = require('sequelize');
const query = require('../../../../helper/sql/query.collect');

class Index {
    constructor(seq) {
        this.seq = seq
    }

    async getAllActiveArticleNumber() {
        let result;
        result = await this.seq.sql(query.getQueryAllActiveArticleNumber(), {type: QueryTypes.SELECT});

        return result.recordsets[0];
    }
}

module.exports = Index;