'use strict'

const { QueryTypes } = require('sequelize');
const query = require('../../../../helper/sql/query.collect');

class Index {
    constructor(seq) {
        this.seq = seq
    }

    /** Wird benötigt, um alle Artikel mit EAN-Nummern aus Karl zu versorgen */
    async getAllArticleWithEanData() {
        let result;
        result = await this.seq.sql(query.getQueryAllArticleWithEan(), {type: QueryTypes.SELECT});

        return result.recordsets[0];
    }

    /** Der SQL-Bulk wird hier verarbeitet. */
    async runUpdateQuery(sql) {
        await this.seq.sql(sql, {type: QueryTypes.UPDATE});
    }
}

module.exports = Index;