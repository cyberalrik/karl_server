'use strict';

const sql = require('../../../../helper/lynn.sql.database.helper');
const query = require('../../../../helper/sql/query.collect');

class Index {

    /** Gibt alle Verlinkungen eines oder mehrere Typen aus Lynn als p_nr zurück. */
    async getLinkForType(type){
        let result = {successful: true, result: {}}
        let lynnLinks = await sql.sql(query.getLinksForType(type));
        if (lynnLinks === null) {
            result.successful = false;

            return result;
        }

        result.result = lynnLinks['recordset'];

        return result;
    }

    /** Gibt alle Komplett-Verlinkungen aus Lynn zurück. */
    async getLinkList() {
        let result = await sql.sql(await query.getLinkList());

        return result.recordsets[0];
    }

    /**
     * Gibt alle Daten, um einen Artikel eindeutig zu beschreiben, zurück.
     * Übergeben wird ein String in der die ArtikelNr, mit Komma getrennt, aufgelistet sind.
     *
     * @param a_nrs
     * @return {Promise<Object>}
     * */
    async getProductArticleFromLynn(a_nrs) {
        let result = {};
        if (a_nrs !== '') {
            let productArticleFromLynn = await sql.sql(await query.getProductArticleFromLynn(a_nrs));
            result = productArticleFromLynn.recordsets[0];
        }

        return result;
    }

    /**
     * Gibt alle Verlinkungen zurück.
     * */
    async getPrepareDataOfLinks(linkType) {
        let lynnLinks = await sql.sql(await query.getPrepareDataOfLinks(linkType));
        let result =  lynnLinks['recordset'];

        return result;
    }
}

module.exports = Index
