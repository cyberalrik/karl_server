'use strict';

const axios = require('axios');
const oauth = require('axios-oauth-client');
const rp = require('request-promise');
const sql = require('../../../../helper/lynn.sql.database.helper');
const query = require('../../../../helper/sql/query.collect');

class PriceListDataData {
    constructor() {
        this.token = '';
    }
    // funktioniert erst nach dem NDM
    async getAllPriceListItems(){
        let result = [];
        if(this.token === undefined ||  this.token.length < 1)
            this.token = await  this.lynnAuth();

        let list = await rp(this.getOptionforLynn('http://extern.apps.service.ct.local/Artikel/Artikelnummern',{Hersteller: 2}), async function (error, response, body) {
            if (error) throw new Error(error);
            return JSON.stringify(body);
        });


        for(let i = 0; i < list.length; i++){
            let article = await rp(this.getOptionforLynn('http://extern.apps.service.ct.local/Artikel/Artikel/' + list[i],{}), async function (error, response, body) {
                if (error) throw new Error(error);
                return JSON.stringify(body);
            });

            console.log(article);
        }
        return result;
    }

    async lynnAuth() {
        const getAuthorizationCode = await oauth.client(await axios.create(), {
            url: 'http://authentication.apps.service.ct.local/connect/token',
            grant_type: 'client_credentials',
            client_id: '05a97e70-b7c1-4086-9cc1-33c4312564cd',
            client_secret: 'HZ7K8u9fCNkDPtws5aYR3KeFxhzJF3',
        });
        return await getAuthorizationCode();
    }

    getOptionforLynn(url, body) {
        return { method: 'GET',
            url: url,
            headers: {
                'cache-control': 'no-cache',
                Authorization: 'Bearer ' + this.token['access_token'],
                'Content-Type': 'application/json' },
            body: body,
            json: true
        };
    }

    /**
     * @param channel {String}
     * @param delta {Boolean}
     * @param p_nr {string}
     * @return Promise {<>}
     * */
    async getAllPriceListItems_old(channel, delta, p_nr){
        let result = [];
        let data;

        switch (channel) {
            case 'ebde':
                data = await sql.sql(await query.getPriceList('erpProductPropertyEBayPreisBruttoEur', 'erpProductPropertyEBayMenge', channel, delta, p_nr));
                result = data['recordset'];
                break;
            case 'ebuk':
            //     data = await sql.sql(await query.getPriceList('erpProductPropertyeBayPreisBruttoGbp', 'erpProductPropertyBB1Menge', channel, delta, p_nr));
            //     result = data['recordset'];
                break;
            case 'cucybertr1':
                data = await sql.sql(await query.getPriceList('erpProductPropertyITMPreisBruttoEur', 'erpProductPropertyITMMenge', channel, delta, p_nr));
                result = data['recordset'];
                break;
            case 'amde':
                // data = await sql.sql(await query.getPriceList('AmazonPreisBruttoEur', 'LagerMenge', channel, delta, p_nr));
                // result = data['recordset'];
                break;
        }

        return result;
    }
}

module.exports = PriceListDataData
