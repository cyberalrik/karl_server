'use strict';

const constants = require('../constants');
const config = require('../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
let seqKarl = null;
const seqLynn = require('../../helper/lynn.sql.database.helper');

class DataAccess {
    constructor(type) {
        this.type = type;
    }

    getManufactureDataAccess(){
        switch(this.type){
            case constants.CCI:
                return new (require('./cci/manufacturerData'))();
            case constants.IN_MEMORY:
            case 'default':
                return new (require('./in-memory/manufactureData'))();
        }
    }

    getPriceListDataAccess(){
        switch(this.type){
            case constants.LYNN:
            case 'default':{
                let DataAccess = require('./lynn/priceListData');
                return new DataAccess();
            }
        }
    }

    getProductsDataAccess(){
        let DataAccess
        switch(this.type){
            case constants.ITM:
                DataAccess = require('./itm/media');
                return new DataAccess();

            case constants.LYNN:
            case 'default':{
                DataAccess = require('./lynn/productData');
                return new DataAccess();
            }
        }
    }

    getLinkListDataAccess(){
        switch(this.type){
            case constants.LYNN:
            case 'default':{
                let DataAccess = require('./lynn/linkListData');
                return new DataAccess();
            }
        }
    }

    getComponentsDataAccess(){
        switch(this.type){
            case constants.CCI:
            case 'default':{
                let DataAccess = require('./cci/componentsData');
                return new DataAccess();
            }
        }
    }

    getE2DataAccess(){
        switch(this.type){
            case constants.LYNN:
            case 'default':{
                let DataAccess = require('./lynn/e2Article')
                return new DataAccess();
            }
        }
    }

    setCategoryAccess() {
        let result = {successful: true, access: null}
        let DataAccess = null;
        switch(this.type){
            case constants.KARL:
                this.setKarlSeq();
                DataAccess = require('./karl/category');
                result.access = new DataAccess(seqKarl);
                break;

            default:
                result.successful = false;
        }

        return result;
    }

    getCategoryAccess() {
        let result = {successful: true, access: null}
        let DataAccess = null;
        switch(this.type){
            case constants.LYNN:
                DataAccess = require('./lynn/category');
                result.access = new DataAccess(seqLynn);
                break;

            case constants.KARL:
                this.setKarlSeq();
                DataAccess = require('./karl/category');
                result.access = new DataAccess(seqKarl);
                break;

            default:
                result.successful = false;
        }

        return result;
    }

    aktivateOfArticle(result) {
        result.successful = true;
        result.access = null;
        let DataAccess = null;
        switch(this.type){
            case constants.KARL:
                this.setKarlSeq();
                DataAccess = require('./karl/articleChannels');
                result.access = new DataAccess(seqKarl);
                break;

           case constants.LYNN:
               DataAccess = require('./lynn/article');
               result.access = new DataAccess(seqLynn);
               break;

            default:
                result.successful = false;
        }

        return result;
    }

    getEanFromArticleAccess(result) {
        result.successful = true;
        result.access = null;
        let DataAccess = null;
        switch(this.type){
           case constants.LYNN:
               DataAccess = require('./lynn/article/articleWithEan');
               result.access = new DataAccess(seqLynn);
               result.messages.push('Lynn-DB Verbindung übergeben.');
               break;

            default:
                result.successful = false;
                result.messages.push('Fehler bei der übergabe einer Lynn-DB Verbindung aufgetreten.');
        }

        return result;
    }

    setKarlSeq() {
        if (seqKarl === null) {
            seqKarl = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
        }
    }

}

module.exports = DataAccess;
