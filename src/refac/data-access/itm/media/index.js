'use strict'

const sql = require('../../../../helper/maria.itm.database.helper');
const query = require('../../../../helper/sql/query.itm.product');

class ProductData {
    constructor() {
    }

    /**
     * Liefert, anhand der mediaData aus dem ITM-Backend, das Produktdaten (MPN, Hersteller)
     * */
    async getProductsFromMediaId(mediaData){
        return await sql.sql(query.getProductsFromMediaId(mediaData));
    }
}

module.exports = ProductData;