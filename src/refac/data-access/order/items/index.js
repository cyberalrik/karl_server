'use strict'

const ServiceOrder = require('../../../../services/service.order');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'order.saveItem';
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;

class index
{
    constructor() {
        this.item = seq.define('order_item', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            orderId: {
                type:Sequelize.INTEGER,
            },
            tbId: {
                type:Sequelize.INTEGER,
            },
            channelId: {
                type:Sequelize.STRING,
            },
            sku: {
                type:Sequelize.STRING,
            },
            channelSku: {
                type:Sequelize.STRING,
            },
            ean: {
                type:Sequelize.STRING,
            },
            quantity: {
                type:Sequelize.INTEGER,
            },
            billingText: {
                type:Sequelize.STRING,
            },
            transferPrice: {
                type:Sequelize.DECIMAL,
            },
            itemPrice: {
                type:Sequelize.DECIMAL,
            },
            deliveryTime: {
                type:Sequelize.INTEGER,
            },
            dateCreated: {
                type:Sequelize.DATE,
            },
            service: {
                type:Sequelize.STRING,
            }
        })
    }

    async saveItems(params) {
        let data = params.currentItems;
        let serviceOrder = new ServiceOrder();

        if (data.ITEM === undefined || data.ITEM === null) {
            return;
        }

        try {
            for (let i = 0; i < data.ITEM.length; i++) {
                let sql = {orderId: params.result.orderDataId};
                let item = data.ITEM[i];

                /**
                 * Über alle Bestellungen hinweg eindeutige Positionsnummer aus TB.One
                 */
                if (!(item.TB_ID === undefined || item.TB_ID[0] === undefined)
                    && await serviceOrder.isInteger(item.TB_ID[0], TAG)
                ) {
                    sql.tbId = await serviceOrder.getInteger(item.TB_ID[0], TAG);
                }

                /**
                 * Positionsnummer des Vertriebskanals (falls übermittelt)
                 * oder laufende Nummer der Position innerhalb des Auftrags
                 */
                if (!(item.CHANNEL_ID === undefined || item.CHANNEL_ID[0] === undefined)) {
                    sql.channelId = await item.CHANNEL_ID[0];
                }

                /** Artikelnummer aus TB.One  */
                if (!(item.SKU === undefined || item.SKU [0] === undefined)) {
                    sql.sku = item.SKU [0];
                }

                /** Artikelnummer des Vertriebskanal */
                if (!(item.CHANNEL_SKU === undefined || item.CHANNEL_SKU[0] === undefined)) {
                    sql.channelSku = item.CHANNEL_SKU[0];
                }

                /** EAN */
                if (!(item.EAN === undefined || item.EAN [0] === undefined)) {
                    sql.ean = item.EAN [0];
                }

                /** Bestellmenge   */
                if (
                    !(item.QUANTITY === undefined || item.QUANTITY [0] === undefined)
                    && await serviceOrder.isInteger(item.QUANTITY [0], TAG)
                ) {
                    sql.quantity = await serviceOrder.getInteger(item.QUANTITY [0], TAG);
                }

                /**
                 * Rechnungstext der Auftragsposition (falls diese nicht vom
                 * Kanal übermittelt wird, wird hier der Artikelname aus TB.One
                 * geliefert)
                 */
                if (!(item.BILLING_TEXT === undefined || item.BILLING_TEXT[0] === undefined)) {
                    sql.billingText = item.BILLING_TEXT[0];
                }

                /**
                 * Netto Artikelpreis bei Bedarf nach Abzug der Provision des
                 * Vertriebskanals (falls dieser Wert nicht geliefert wird, findet
                 * die Berechnung seitens TB.One statt), pro Stück (Menge 1)
                 */
                if (
                    !(item.TRANSFER_PRICE === undefined || item.TRANSFER_PRICE[0] === undefined)
                    && await serviceOrder.isFloat(item.TRANSFER_PRICE[0], TAG)
                ) {
                    sql.transferPrice = await serviceOrder.getFloat(item.TRANSFER_PRICE[0], TAG);
                }

                /** Brutto Verkaufspreis (pro Stück, Menge 1) */
                if (
                    !(item.ITEM_PRICE === undefined || item.ITEM_PRICE[0] === undefined)
                    && await serviceOrder.isFloat(item.ITEM_PRICE[0], TAG)
                ) {
                    sql.itemPrice = await serviceOrder.getFloat(item.ITEM_PRICE[0], TAG);
                }

                /**
                 * Zum Zeitpunkt der Kundenbestellung angezeigte Lieferzeit in
                 * Werktagen (falls diese vom Kanal übermittelt wird)
                 */
                if (
                    !(item.DELIVERY_TIME === undefined || item.DELIVERY_TIME[0] === undefined)
                    && await serviceOrder.isInteger(item.DELIVERY_TIME[0], TAG)
                ) {
                    sql.deliveryTime = await serviceOrder.getInteger(item.DELIVERY_TIME[0], TAG);
                }

                /**
                 * Erstelldatum (Einlesedatum) der Auftragsposition in TB.One
                 * (z.B: 2010-10-19T22:07:21)
                 */
                if (!(item.DATE_CREATED === undefined || item.DATE_CREATED[0] === undefined)
                    && await serviceOrder.isDate(item.DATE_CREATED[0], TAG)
                ) {
                    sql.dateCreated = await serviceOrder.getDate(item.DATE_CREATED[0], TAG);
                }

                /** Positionsbezogene Services (z.B. Geschenkverpackung) */
                if (!(item.SERVICES === undefined || item.SERVICES[0] === undefined)) {
                    sql.service = item.SERVICES[0];
                }

                await this.item.create(sql);
            }

        } catch (e) {
            logger.err(TAG, 'Item unkorrekt','Fehler in Datei ' + params.currentFileName)
        }
    }

    async getItemsDetails(orderId) {
        let result;
        try {
            let sql = {
                where: {orderId: orderId},
            };

            result = await this.item.findOne(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }

        return  result
    }
}

module.exports = index;