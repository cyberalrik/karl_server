'use strict'

const SaveOrderData = require('../orderData/index');
const SaveSellTo = require('../sellTo/index');
const SaveShipTo = require('../shipTo/index');
const SaveSaveShipment = require('../shipment/index');
const SavePayment = require('../payment/index');
const SaveReferences = require('../references/index');
const SaveHistory = require('../history/index');
const SaveService = require('../service/index');
const SaveItems = require('../items/index');
// const logger = require('../../../../helper/util/logger.util');
// const TAG = 'order.order';


class index {
    async saveOrder(params) {
        let order = params.currentOrderList;
        let orderId;
        try {
            if (order === undefined || order === null) {
                params.result.successful = false;
                return params;
            }

            // ORDER_DATA - Pflichtfeld
            if (!(order.ORDER_DATA === undefined || order.ORDER_DATA === null))
            {
                let orderData = [];
                let saveOrderData = new SaveOrderData();

                if (Array.isArray(order.ORDER_DATA)) {
                    orderData = order.ORDER_DATA;
                } else {
                    orderData[0] = order.ORDER_DATA;
                }

                params.result.numberOfOrders += orderData.length;

                for (let i = 0; i < orderData.length; i++) {
                    params.currentOrderData = orderData[i]
                    params = await saveOrderData.saveOrderData(params)
                }
            }

            if (!params.result.successful) {
                return  params;
            }

            orderId = params.result.orderDataId;

            // SELL_TO - kein Pflichtfeld
            if (!(order.SELL_TO === undefined || order.SELL_TO === null))
            {
                let sellTo = [];
                let saveSellTo = new SaveSellTo();
                if (Array.isArray(order.SELL_TO)) {
                    sellTo = order.SELL_TO;
                } else {
                    sellTo[0] = order.SELL_TO;
                }

                for (let i = 0; i < sellTo.length; i++) {
                    params.currentSellTo = sellTo[i];
                    await saveSellTo.saveSellTo(params);
                }
            }

            // SHIP_TO - Pflichtfeld
            if (!(order.SHIP_TO === undefined || order.SHIP_TO === null))
            {
                let shipTo = [];
                let saveShipTo = new SaveShipTo();
                if (Array.isArray(order.SHIP_TO)) {
                    shipTo = order.SHIP_TO;
                } else {
                    shipTo[0] = order.SHIP_TO;
                }

                for (let i = 0; i < shipTo.length; i++) {
                    params.currentShipTo = shipTo[i];
                    await saveShipTo.saveShipTo(params);
                }
            }

            // SHIPMENT - kein Pflichtfeld
            if (!(order.SHIPMENT === undefined || order.SHIPMENT === null))
            {
                let shipment = [];
                let saveShipment = new SaveSaveShipment();
                if (Array.isArray(order.SHIPMENT)) {
                    shipment = order.SHIPMENT;
                } else {
                    shipment[0] = order.SHIPMENT;
                }

                for (let i = 0; i < shipment.length; i++) {
                    params.currentShipment = shipment[i];
                    await saveShipment.saveShipment(params);
                }
            }

            // PAYMENT - kein Pflichtfeld
            if (!(order.PAYMENT === undefined || order.PAYMENT === null))
            {
                let payment = [];
                let savePayment = new SavePayment();
                if (Array.isArray(order.PAYMENT)) {
                    payment = order.PAYMENT;
                } else {
                    payment[0] = order.PAYMENT;
                }

                for (let i = 0; i < payment.length; i++) {
                    params.currentPayment = payment[i];
                    await savePayment.savePayment(params);
                }
            }

            // REFERENCES - kein Pflichtfeld
            if (!(order.REFERENCES === undefined || order.REFERENCES === null))
            {
                let references = [];
                let saveReferences = new SaveReferences();
                if (Array.isArray(order.REFERENCES)) {
                    references = order.REFERENCES;
                } else {
                    references[0] = order.REFERENCES;
                }

                for (let i = 0; i < references.length; i++) {
                    params.currentReferences = references[i];
                    await saveReferences.saveReferences(params);
                }
            }

            // HISTORY - kein Pflichtfeld
            if (!(order.HISTORY === undefined || order.HISTORY === null))
            {
                let history = [];
                let saveHistory = new SaveHistory();
                if (Array.isArray(order.HISTORY)) {
                    history = order.HISTORY;
                } else {
                    history[0] = order.HISTORY;
                }

                for (let i = 0; i < history.length; i++) {
                    params.currentHistory = history[i];
                    await saveHistory.saveHistory(params);
                }
            }

            // SERVICE - kein Pflichtfeld
            if (!(order.SERVICES === undefined || order.SERVICES === null))
            {
                let services = [];
                let saveService = new SaveService();
                if (Array.isArray(order.SERVICES)) {
                    services = order.SERVICES;
                } else {
                    services[0] = order.SERVICES;
                }

                for (let i = 0; i < services.length; i++) {
                    params.currentServices = services[i]
                    await saveService.saveService(params);
                }
            }

            // ITEMS - Pflichtfeld
            if (!(order.ITEMS === undefined || order.ITEMS === null))
            {
                let items = [];
                let saveItem = new SaveItems();
                if (Array.isArray(order.ITEMS)) {
                    items = order.ITEMS;
                } else {
                    items[0] = order.ITEMS;
                }

                for (let i = 0; i < items.length; i++) {
                    params.currentItems = items[i]
                    await saveItem.saveItems(params);
                }
            }

        } catch (e) {
            params.result.successful = false;
        }

        return params;
    }
}

module.exports = index;