'use strict'

const ServiceOrder = require('../../../../services/service.order');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'order.orderData';
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;

class index
{
    constructor() {
        this.orderData = seq.define('order_orderData', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            orderDate: {
                type:Sequelize.DATE,
            },
            tbId: {
                type:Sequelize.INTEGER,
            },
            channelSign: {
                type:Sequelize.STRING,
            },
            channelId: {
                type:Sequelize.STRING,
            },
            channelNo: {
                type:Sequelize.STRING,
            },
            billNo: {
                type:Sequelize.STRING,
            },
            paid: {
                type:Sequelize.INTEGER,
            },
            approved: {
                type:Sequelize.INTEGER,
            },
            withdrawn: {
                type:Sequelize.INTEGER,
            },
            customerComment: {
                type:Sequelize.STRING,
            },
            itemCount: {
                type:Sequelize.INTEGER,
            },
            totalItemAmount: {
                type:Sequelize.DECIMAL,
            },
            dateCreated: {
                type:Sequelize.DATE,
            }
        })
    }

    async saveOrderData(param) {
        let data = param.currentOrderData
        let serviceOrder = new ServiceOrder();
        try {
            let sql = {};
            /**
             * Datum der Kundenbestellung, falls diese vom Kanal übermittelt
             * wird. Andernfalls wird von TB.One das Datum Imports eingetragen.
             */
            if (!(data.ORDER_DATE === undefined || data.ORDER_DATE[0] === undefined)
                && await serviceOrder.isDate(data.ORDER_DATE[0], TAG)
            ) {
                sql.orderDate = await serviceOrder.getDate(data.ORDER_DATE[0], TAG);
            }

            /**
             * Eindeutige, kanalübergreifende Auftragsnummer aus TB.One, mit
             * der Sie den Auftrag jederzeit eindeutig identifizieren können.
             */
            if (
                !(data.TB_ID === undefined || data.TB_ID[0] === undefined)
                && await serviceOrder.isInteger(data.TB_ID[0], TAG)
            ) {
                sql.tbId = await serviceOrder.getInteger(data.TB_ID[0], TAG);
            }

            /** Kanalkennzeichen */
            if (!(data.CHANNEL_SIGN === undefined || data.CHANNEL_SIGN[0] === undefined)) {
                sql.channelSign = data.CHANNEL_SIGN[0];
            }

            /** Eindeutige Auftragsnummer des Vertriebskanals */
            if (!(data.CHANNEL_ID === undefined || data.CHANNEL_ID[0] === undefined)) {
                sql.channelId = data.CHANNEL_ID[0];
            }

            /**
             * Auftragsnummer des Vertriebskanals (muss nicht eindeutig sein,
             * z.B. Auftragsnummer eines Webshops).
             * Je nach Vertriebskanal können CHANNEL_ID und CHANNEL_NO
             * auch den gleichen Wert enthalten
             */
            if (!(data.CHANNEL_NO === undefined || data.CHANNEL_NO[0] === undefined)) {
                sql.channelNo = data.CHANNEL_NO[0];
            }

            /**
             * Rechnungsnummer, die vom Vertriebskanal vergeben wurde
             * (nicht bei allen Kanälen vorhanden)
             */
            if (!(data.BILL_NO === undefined || data.BILL_NO[0] === undefined)) {
                sql.billNo = data.BILL_NO[0];
            }

            /**
             * Bezahlt-Kennzeichen. Kennzeichen:
             * 0 = nicht bezahlt (i.A. ein schwebender Auftrag)
             * 1 = bezahlt (bestätigter Auftrag)
             * 2 = Zahlungsausfall
             */
            if (
                !(data.PAID === undefined || data.PAID[0] === undefined)
                && await serviceOrder.isInteger(data.PAID[0], TAG)
            ) {
                sql.paid = await serviceOrder.getInteger(data.PAID[0], TAG);
            }

            /**
             * Freigabekennzeichen des Auftrags:
             * 0 = schwebend
             * 1 = bestätigter Auftrag
             */
            if (
                !(data.APPROVED === undefined || data.APPROVED[0] === undefined)
                && await serviceOrder.isInteger(data.APPROVED[0], TAG)
            ) {
                sql.approved = await serviceOrder.getInteger(data.APPROVED[0], TAG);
            }

            /**
             * Alternativ zu APPROVED kann auch der Knoten WITHDRAWN geliefert werden, wenn ein Auftrag zurückgezogen wird. Hier kann nur
             * der Wert 1 enthalten sein. Beide Angaben gemeinsam können
             * nicht auftreten.
             */
            if (
                !(data.WITHDRAWN === undefined || data.WITHDRAWN[0] === undefined)
                && await serviceOrder.isInteger(data.WITHDRAWN[0], TAG)
            ) {
                sql.withdrawn = await serviceOrder.getInteger(data.WITHDRAWN[0], TAG);
            }

            /** Kundenbemerkung zum Auftrag */
            if (!(data.CUSTOMER_COMMENT === undefined || data.CUSTOMER_COMMENT[0] === undefined)) {
                sql.customerComment = data.CUSTOMER_COMMENT[0];
            }

            /** Anzahl der Bestellpositionen */
            if (
                !(data.ITEM_COUNT === undefined || data.ITEM_COUNT[0] === undefined)
                && await serviceOrder.isInteger(data.ITEM_COUNT[0], TAG)
            ) {
                sql.itemCount = await serviceOrder.getInteger(data.ITEM_COUNT[0], TAG);
            }

            /**
             * Gesamtsumme der Auftragspositionen Brutto
             * (ohne Versandkosten)
             */
            if (
                !(data.TOTAL_ITEM_AMOUNT === undefined || data.TOTAL_ITEM_AMOUNT[0] === undefined)
                && await serviceOrder.isFloat(data.TOTAL_ITEM_AMOUNT[0], TAG)
            ) {
                sql.totalItemAmount = await serviceOrder.getFloat(data.TOTAL_ITEM_AMOUNT[0], TAG);
            }

            /**
             * Erstelldatum (Einlesedatum) des Auftrags in TB.One
             * (z.B.: 2010-10-19T22:07:21)
             */
            if (!(data.DATE_CREATED === undefined || data.DATE_CREATED[0] === undefined)
                && await serviceOrder.isDate(data.DATE_CREATED[0], TAG)
            ) {
                sql.dateCreated = await serviceOrder.getDate(data.DATE_CREATED[0], TAG);
            }

            let orderData = await this.orderData.create(sql);

            param.result.orderDataId = orderData.dataValues.id;

        } catch (e) {
            logger.err(TAG, 'Orderdaten unkorrekt', e.message)
            param.result.successful = false;
        }

        return param;
    }

    async getOrderData(params) {
        let result;
        try {
            let sql = {
                offset: params.offset,
                limit: params.limit,
                attributes: [
                    'id',
                    'tbId',
                    'channelSign',
                    'itemCount',
                    'totalItemAmount',
                    'paid',
                    'customerComment',
                    'orderDate'
                ],
                order: [
                    ['id', 'ASC']
                ],
            };

            sql = await this.setSqlWhere(sql, params);
            result = await this.orderData.findAll(sql);

        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }

        return result;
    }

    async getNumberOfEntries(params) {
        let result;
        try {
            let sql = new Object();
            sql = await this.setSqlWhere(sql, params);

            result = await this.orderData.count(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
            return;
        }

        return result;

    }

    async getFilterValue(groupName) {
        let result;
        try {
            let sql = {
                group: groupName,
                order: [groupName],
                attributes: [groupName]
            };

            result = await this.orderData.findAll(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }
        return  result
    }

    async getOrderDetails(orderId) {
        let result;
        try {
            let sql = {
                where: {id: orderId},
            };

            result = await this.orderData.findOne(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }
        return  result
    }

    async setSqlWhere(sql, params) {
        sql.where = {};

        if (
            params.where === undefined
            || params.where.length === 0
            || params.whereOption === undefined
            || params.whereOption.length === 0
        ) {
            return sql;
        }

        for (let [key, value] of Object.entries(params.whereOption)) {
            if (
                params.where[value] === undefined
                || params.where[value] === null
                || params.where[value] === ''
            ) {
                continue;
            }
            sql.where[value] = params.where[value];
        }

        // for (let [key, value] of Object.entries(params.where)) {
        //     sql.where[key] = value;
        // }
        //


        // sql.where = {};
        // for (var key in params.where) {
        //     sql.where[key] = params.where[key];
        // }

        return sql;
    }

}

module.exports = index;