'use strict'

const ServiceOrder = require('../../../../services/service.order');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'order.payment';
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;

class index
{
    constructor() {
        this.payment = seq.define('order_payment', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            orderId: {
                type:Sequelize.INTEGER,
            },
            type: {
                type:Sequelize.STRING,
            },
            costs: {
                type:Sequelize.DECIMAL,
            },
            paymentTransactionId: {
                type:Sequelize.STRING,
            },
            paymentTransactionNr: {
                type:Sequelize.STRING,
            },
            paymentTransactionName: {
                type:Sequelize.STRING,
            },
            paymentData: {
                type:Sequelize.STRING,
            },
            accountName: {
                type:Sequelize.STRING,
            },
            accountNr: {
                type:Sequelize.STRING,
            },
            bankName: {
                type:Sequelize.STRING,
            },
            bankCode: {
                type:Sequelize.STRING,
            },
            iban: {
                type:Sequelize.STRING,
            },
            bic: {
                type:Sequelize.STRING,
            }
        })
    }

    async savePayment(params) {
        let data = params.currentPayment;
        let serviceOrder = new ServiceOrder();
        try {
            let sql = {orderId: params.result.orderDataId};

            /**
             * Zahlart (Abhängig vom Vertriebskanal)
             * Bitte sprechen Sie mit Ihrem Integrationsbetreuer die
             * möglichen Zahlarten ab
             */
            if (!(data.TYPE  === undefined || data.TYPE [0] === undefined)
            ) {
                sql.type = data.TYPE[0];
            }

            /** Zahlkosten */
            if (
                !(data.COSTS === undefined || data.COSTS[0] === undefined)
                && await serviceOrder.isFloat(data.COSTS[0], TAG)
            ) {
                sql.costs = await serviceOrder.getFloat(data.COSTS[0], TAG);
            }

            /**
             * Transaktionsnummer der Zahlung (oder z.B. Freigabecode
             * zum Anstoß der Kundenbelastung, je nach Zahlart)
             */
            if (
                !(data.PAYMENT_TRANSACTION_ID === undefined
                    || data.PAYMENT_TRANSACTION_ID[0] === undefined)
            ) {
                sql.paymentTransactionId = data.PAYMENT_TRANSACTION_ID[0];
            }

            /**
             * Zusätzliches Kennzeichen zur Identifizierung der Zahlung
             * oder Kennzeichen des Kunden (z.B: Paypal Kundennummer)
             */
            if (
                !(data.PAYMENT_TRANSACTION_NR === undefined
                    || data.PAYMENT_TRANSACTION_NR[0] === undefined)
            ) {
                sql.paymentTransactionNr = data.PAYMENT_TRANSACTION_NR[0];
            }

            /** Evtl. abweichender Kundenname für Zahlung  */
            if (
                !(data.PAYMENT_TRANSACTION_NAME === undefined
                    || data.PAYMENT_TRANSACTION_NAME[0] === undefined)
            ) {
                sql.paymentTransactionName = data.PAYMENT_TRANSACTION_NAME[0];
            }

            /** Name des Kontoinhabers */
            if (
                !(
                    data.DIRECTDEBIT === undefined
                    || data.DIRECTDEBIT[0] === undefined
                    || data.DIRECTDEBIT[0].ACCOUNTNAME === undefined
                    || data.DIRECTDEBIT[0].ACCOUNTNAME[0] === undefined
                )
            ) {
                sql.accountName = data.DIRECTDEBIT[0].ACCOUNTNAME[0];
            }

            /** Kontonummer */
            if (
                !(
                    data.DIRECTDEBIT === undefined
                    || data.DIRECTDEBIT[0] === undefined
                    || data.DIRECTDEBIT[0].ACCOUNTNR === undefined
                    || data.DIRECTDEBIT[0].ACCOUNTNR[0] === undefined
                )
            ) {
                sql.accountNr = data.DIRECTDEBIT[0].ACCOUNTNR[0];
            }

            /** Name der Bank */
            if (
                !(
                    data.DIRECTDEBIT === undefined
                    || data.DIRECTDEBIT[0] === undefined
                    || data.DIRECTDEBIT[0].BANKNAME === undefined
                    || data.DIRECTDEBIT[0].BANKNAME[0] === undefined
                )
            ) {
                sql.bankName = data.DIRECTDEBIT[0].BANKNAME[0];
            }

            /** Bankleitzahl */
            if (
                !(
                    data.DIRECTDEBIT === undefined
                    || data.DIRECTDEBIT[0] === undefined
                    || data.DIRECTDEBIT[0].BANKCODE === undefined
                    || data.DIRECTDEBIT[0].BANKCODE[0] === undefined
                )
            ) {
                sql.bankCode = data.DIRECTDEBIT[0].BANKCODE[0];
            }

            /** IBAN */
            if (
                !(
                    data.DIRECTDEBIT === undefined
                    || data.DIRECTDEBIT[0] === undefined
                    || data.DIRECTDEBIT[0].IBAN === undefined
                    || data.DIRECTDEBIT[0].IBAN[0] === undefined
                )
            ) {
                sql.iban = data.DIRECTDEBIT[0].IBAN[0];
            }

            /** BIC */
            if (
                !(
                    data.DIRECTDEBIT === undefined
                    || data.DIRECTDEBIT[0] === undefined
                    || data.DIRECTDEBIT[0].BIC === undefined
                    || data.DIRECTDEBIT[0].BIC[0] === undefined
                )
            ) {
                sql.bic = data.DIRECTDEBIT[0].BIC[0];
            }

            await this.payment.create(sql);

        } catch (e) {
            logger.err(TAG, 'Payment-Daten unkorrekt', 'Fehler in Datei ' + params.currentFileName);
        }
    }

    async getPaymentDetails(orderId) {
        let result;
        try {
            let sql = {
                where: {orderId: orderId},
            };

            result = await this.payment.findOne(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }

        return  result
    }
}

module.exports = index;