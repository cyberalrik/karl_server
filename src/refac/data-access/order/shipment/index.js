'use strict'

const ServiceOrder = require('../../../../services/service.order');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'order.shipment';
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;

class index
{
    constructor() {
        this.shipment = seq.define('order_shipment', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            orderId: {
                type:Sequelize.INTEGER,
            },
            idCodeShip: {
                type:Sequelize.STRING,
            },
            idCodeReturn: {
                type:Sequelize.STRING,
            },
            routingCode: {
                type:Sequelize.STRING,
            },
            price: {
                type:Sequelize.DECIMAL,
            },
            isExpress: {
                type:Sequelize.INTEGER,
            }
        })
    }

    async saveShipment(params) {
        let data = params.currentShipment;
        let serviceOrder = new ServiceOrder();
        try {
            let sql = {orderId: params.result.orderDataId};

            /**
             * Falls der Vertriebskanal zu verwendende Shipcodes
             * vorschreibt (z.B: DHL-Sendungsnummern) werden diese in
             * diesen Feldern übertragen. Haben Sie in TB.One einen DHLNummernkreis hinterlegt und der Kanal sendet keinen
             * Shipcode, wird der von TB.One ermittelte Shipcode
             * eingetragen.
             */
            if (!(data.IDCODE_SHIP  === undefined || data.IDCODE_SHIP [0] === undefined)
            ) {
                sql.idCodeShip = data.IDCODE_SHIP[0];
            }

            /**
             * Siehe data.IDCODE_SHIP
             */
            if (!(data.IDCODE_RETURN === undefined || data.IDCODE_RETURN[0] === undefined)
            ) {
                sql.channelNo = data.IDCODE_RETURN[0];
            }

            /**
             * Anrede oder Titel
             */
            if (!(data.TITLE === undefined || data.TITLE[0] === undefined)
            ) {
                sql.title = data.TITLE[0];
            }

            /**
             * Leitcode (DHL) der Kundenadresse
             */
            if (!(data.ROUTING_CODE === undefined || data.ROUTING_CODE[0] === undefined)
            ) {
                sql.firstname = data.ROUTING_CODE[0];
            }

            /**
             * Versandkosten
             */
            if (
                !(data.PRICE === undefined || data.PRICE[0] === undefined)
                && await serviceOrder.isFloat(data.PRICE[0], TAG)
            ) {
                sql.price = await serviceOrder.getFloat(data.PRICE[0], TAG);
            }

            /** Expressversand-Kennzeichen */
            if (
                !(data.IS_EXPRESS === undefined || data.IS_EXPRESS[0] === undefined)
                && await serviceOrder.isInteger(data.IS_EXPRESS[0], TAG)
            ) {
                sql.isExpress = await serviceOrder.getInteger(data.IS_EXPRESS[0], TAG);
            }

            await this.shipment.create(sql);

        } catch (e) {
            logger.err(TAG, 'Shipment-Daten unkorrekt', 'Fehler in Datei ' + params.currentFileName);
        }
    }

    async getShipmentDetails(orderId) {
        let result;
        try {
            let sql = {
                where: {orderId: orderId},
            };

            result = await this.shipment.findOne(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }

        return  result
    }
}

module.exports = index;