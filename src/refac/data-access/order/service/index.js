'use strict'

const ServiceOrder = require('../../../../services/service.order');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'order.service';
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;

class index
{
    constructor() {
        this.service = seq.define('order_service', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            orderId: {
                type:Sequelize.INTEGER,
            },
            code: {
                type:Sequelize.STRING,
            },
            desc: {
                type:Sequelize.STRING,
            },
            price: {
                type:Sequelize.DECIMAL,
            }
        })
    }

    async saveService(params) {
        let data = params.currentServices;
        let serviceOrder = new ServiceOrder();

        if (data.SERVICE === undefined || data.SERVICE === null) {
            return;
        }

        try {
            for (let i = 0; i < data.SERVICE.length; i++) {
                let sql = {orderId: params.result.orderDataId};
                let service = data.SERVICE[i];

                if (
                    !(
                        service === undefined
                        || service.CODE  === undefined
                        || service.CODE[0] === undefined

                        || service.DESC  === undefined
                        || service.DESC[0] === undefined

                        || service.PRICE  === undefined
                        || service.PRICE[0] === undefined
                    )
                    && await serviceOrder.isFloat(service.PRICE[0], TAG)
                ) {
                    /** Kennzeichen der Serviceart */
                    sql.code = service.CODE[0];
                    /** Bezeichnung des Service  */
                    sql.desc = service.DESC[0];
                    /** Servicekosten  */
                    sql.price = await serviceOrder.getFloat(service.PRICE[0]);

                    await this.service.create(sql);
                } else {
                    logger.err(TAG, 'Service-Daten unkorrekt/ unvollständig', 'Fehler in Datei ' + params.currentFileName);
                }
            }

        } catch (e) {
            logger.err(TAG, 'Service-Daten unkorrekt', 'Fehler in Datei ' + params.currentFileName);
        }
    }

    async getServicesDetails(orderId) {
        let result;
        try {
            let sql = {
                where: {orderId: orderId},
            };

            result = await this.service.findAll(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }

        return  result
    }
}

module.exports = index;