'use strict'

const ServiceOrder = require('../../../../services/service.order');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'order.history';
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;

class index
{
    constructor() {
        this.history = seq.define('order_histories', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            orderId: {
                type:Sequelize.INTEGER,
            },
            eventId: {
                type:Sequelize.INTEGER,
            },
            eventType: {
                type:Sequelize.STRING,
            },
            dateCreated: {
                type:Sequelize.DATE,
            }
        })
    }

    async saveHistory(params) {
        let data = params.currentHistory;
        let serviceOrder = new ServiceOrder();

        if (data.EVENT === undefined || data.EVENT === null) {
            return;
        }

        try {
            for (let i = 0; i < data.EVENT.length; i++) {
                let sql = {orderId: params.result.orderDataId};
                let history = data.EVENT[i];

                if (
                    !(
                        history === undefined
                        || history.EVENT_ID === undefined
                        || history.EVENT_ID[0] === undefined

                        || history.EVENT_TYPE === undefined
                        || history.EVENT_TYPE[0] === undefined

                        || history.DATE_CREATED === undefined
                        || history.DATE_CREATED[0] === undefined
                    )
                    && await serviceOrder.isInteger(history.EVENT_ID[0], TAG)
                    && await serviceOrder.isDate(history.DATE_CREATED[0], TAG)
                ) {
                    /** ID des Ereignisses */
                    sql.eventId = await serviceOrder.getInteger(history.EVENT_ID[0]);
                    /** Art des Ereignisses  */
                    sql.eventType = history.EVENT_TYPE[0];
                    /** Datum des Ereignisses  */
                    sql.dateCreated = await serviceOrder.getDate(history.DATE_CREATED[0], TAG);

                    await this.history.create(sql);
                } else {
                    logger.err(TAG, 'History-Daten unkorrekt/ unvollständig');
                }
            }

        } catch (e) {
            logger.err(TAG, 'History-Daten unkorrekt', 'Fehler in Datei ' + params.currentFileName);
        }
    }

    async getHistoryDetails(orderId) {
        let result;
        try {
            let sql = {
                where: {orderId: orderId},
            };

            result = await this.history.findAll(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }

        return  result
    }
}

module.exports = index;