'use strict'

const ServiceOrder = require('../../../../services/service.order');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'order.sellTo';
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;

class index
{
    constructor() {
        this.sellTo = seq.define('order_sellTo', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            orderId: {
                type:Sequelize.INTEGER,
            },
            tbId: {
                type:Sequelize.INTEGER,
            },
            channelNo: {
                type:Sequelize.STRING,
            },
            title: {
                type:Sequelize.STRING,
            },
            firstname: {
                type:Sequelize.STRING,
            },
            lastname: {
                type:Sequelize.STRING,
            },
            name: {
                type:Sequelize.STRING,
            },
            nameExtension: {
                type:Sequelize.STRING,
            },
            streetNo: {
                type:Sequelize.STRING,
            },
            streetExtension: {
                type:Sequelize.STRING,
            },
            zip: {
                type:Sequelize.STRING,
            },
            city: {
                type:Sequelize.STRING,
            },
            country: {
                type:Sequelize.STRING,
            },
            phonePrivate: {
                type:Sequelize.STRING,
            },
            phoneOffice: {
                type:Sequelize.STRING,
            },
            phoneMobile: {
                type:Sequelize.STRING,
            },
            email: {
                type:Sequelize.STRING,
            },
            birthday: {
                type:Sequelize.DATE,
            },
            vatId: {
                type:Sequelize.STRING,
            }
        })
    }

    async saveSellTo(params) {
        let serviceOrder = new ServiceOrder();
        try {
            let sql = {orderId: params.result.orderDataId};
            let data = params.currentSellTo;
            /**
             * Eindeutige, kanalübergreifende Kundennummer aus TB.One.
             * Für abweichende Lieferanschriften wird eine neue TB_ID
             * vergeben.
             */
            if (
                !(data.TB_ID === undefined || data.TB_ID[0] === undefined)
                && await serviceOrder.isInteger(data.TB_ID[0], TAG)
            ) {
                sql.tbId = await serviceOrder.getInteger(data.TB_ID[0], TAG);
            }

            /**
             * Nummer des Kunden beim jeweiligen Vertriebskanal
             */
            if (!(data.CHANNEL_NO === undefined || data.CHANNEL_NO[0] === undefined)
            ) {
                sql.channelNo = data.CHANNEL_NO[0];
            }

            /**
             * Anrede oder Titel
             */
            if (!(data.TITLE === undefined || data.TITLE[0] === undefined)
            ) {
                sql.title = data.TITLE[0];
            }

            /**
             * Vorname des Kunden; kann auch leer geliefert werden
             */
            if (!(data.FIRSTNAME === undefined || data.FIRSTNAME[0] === undefined)
            ) {
                sql.firstname = data.FIRSTNAME[0];
            }

            /**
             * Nachname des Kunden; kann auch leer geliefert werden
             */
            if (!(data.LASTNAME === undefined || data.LASTNAME[0] === undefined)
            ) {
                sql.lastname = data.LASTNAME[0];
            }

            /**
             * Zusammengesetzter Name des Kunden (liefert der
             * Vertriebskanal nicht Vor- und Nachname getrennt, versucht
             * TB.One diese Felder automatisch zu füllen); kann auch leer
             * geliefert werden
             */
            if (!(data.NAME === undefined || data.NAME[0] === undefined)
            ) {
                sql.name = data.NAME[0];
            }

            /**
             * Namenszusatz
             */
            if (!(data.NAME_EXTENSION === undefined || data.NAME_EXTENSION[0] === undefined)
            ) {
                sql.nameExtension = data.NAME_EXTENSION[0];
            }

            /**
             * Straße und Hausnummer
             */
            if (!(data.STREET_NO === undefined || data.STREET_NO[0] === undefined)
            ) {
                sql.streetNo = data.STREET_NO[0];
            }

            /**
             * Adresszusatz
             */
            if (!(data.STREET_EXTENSION === undefined || data.STREET_EXTENSION[0] === undefined)
            ) {
                sql.streetExtension = data.STREET_EXTENSION[0];
            }

            /**
             * Postleitzahl
             */
            if (!(data.ZIP === undefined || data.ZIP[0] === undefined)
            ) {
                sql.zip = data.ZIP[0];
            }

            /**
             * Stadt
             */
            if (!(data.CITY === undefined || data.CITY[0] === undefined)
            ) {
                sql.city = data.CITY[0];
            }

            /**
             * Ländercode ALPHA 2 (z.B. "DE")
             */
            if (!(data.COUNTRY === undefined || data.COUNTRY[0] === undefined)
            ) {
                sql.country = data.COUNTRY[0];
            }

            /**
             * Private Telefonnummer
             */
            if (!(data.PHONE_PRIVATE === undefined || data.PHONE_PRIVATE[0] === undefined)
            ) {
                sql.phonePrivate = data.PHONE_PRIVATE[0];
            }

            /**
             * Geschäftliche Telefonnummer
             */
            if (!(data.PHONE_OFFICE === undefined || data.PHONE_OFFICE[0] === undefined)
            ) {
                sql.phoneOffice = data.PHONE_OFFICE[0];
            }

            /**
             * Mobilnummer
             */
            if (!(data.PHONE_MOBILE === undefined || data.PHONE_MOBILE[0] === undefined)
            ) {
                sql.phoneMobile = data.PHONE_MOBILE[0];
            }

            /**
             * E-Mail-Adresse
             */
            if (!(data.EMAIL === undefined || data.EMAIL[0] === undefined)
            ) {
                sql.email = data.EMAIL[0];
            }

            /**
             * Geburtsdatum des Kunden
             * (wird nur von wenigen Kanälen unterstützt)
             */
            if (!(data.BIRTHDAY === undefined || data.BIRTHDAY[0] === undefined)
                && await serviceOrder.isDate(data.ORDER_DATE[0], TAG)
            ) {
                sql.birthday = await serviceOrder.getDate(data.BIRTHDAY[0], TAG);
            }

            /**
             * Umsatzsteueridentifikationsnummer des Bestellers
             * (wird nur von wenigen Kanälen unterstützt)
             */
            if (!(data.VAT_ID === undefined || data.VAT_ID[0] === undefined)
            ) {
                sql.vatId = data.VAT_ID[0];
            }
            await this.sellTo.create(sql);

        } catch (e) {
            logger.err(TAG, 'SellTo-Daten unkorrekt', 'Fehler in Datei ' + params.currentFileName);
        }
    }

    async getSellToDetails(orderId) {
        let result;
        try {
            let sql = {
                where: {orderId: orderId},
            };

            result = await this.sellTo.findOne(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }

        return  result
    }
}

module.exports = index;