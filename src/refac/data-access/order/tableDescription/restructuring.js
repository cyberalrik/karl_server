'use strict'

class index {
// Convert array to object
    async convertArrayToObject(array) {
        let thisElementObj = new Object();
        if(typeof array == "object"){
            for(let i in array){
                let thisElement = await this.convertArrayToObject(array[i]);
                thisElementObj[i] = thisElement;
            }
        }else {
            thisElementObj = array;
        }
        return thisElementObj;
    }

    async restructuringOfTheTableDescription(tableDescription) {
        let result = [];
        let description = [];

        let tableName = '';
        for (let i = 0; i < tableDescription.length; i++) {

            if (!(tableName === tableDescription[i].tableName)) {
                description = [];
                tableName = tableDescription[i].tableName;
            }
            description[tableDescription[i].columnName] = [];
            description[tableDescription[i].columnName]['tooltip'] = tableDescription[i].tooltip;
            description[tableDescription[i].columnName]['node'] = tableDescription[i].node;
            description[tableDescription[i].columnName]['type'] = tableDescription[i].type;
            description[tableDescription[i].columnName]['multiple'] = tableDescription[i].multiple;
            result[tableDescription[i].tableName] = description;
        }

        let result2 = await this.convertArrayToObject(result);

        return result2;
    }
}

module.exports = index;
