'use strict'

const logger = require('../../../../helper/util/logger.util');
const Mapping = require('./restructuring');
const TAG = 'order.tableDescription';
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;

class index {
    constructor() {
        this.tableDesckriptions = seq.define('order_tableDescriptions', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            tableName: {
                type: Sequelize.STRING,
            },
            columnName: {
                type: Sequelize.STRING,
            },
            node: {
                type: Sequelize.STRING,
            },
            tooltip: {
                type: Sequelize.STRING,
            },
            type: {
                type: Sequelize.INTEGER,
            },
            multiple: {
                type: Sequelize.INTEGER,
            }
        })
    }

    async getTableDescription() {
        let result;
        try {
            let sql = {order: [
                ['tableName', 'ASC'],
                ['displayOrder', 'ASC']
            ]}

            result = await this.tableDesckriptions.findAll(sql);

        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }

         let mapping = new Mapping();
         result = mapping.restructuringOfTheTableDescription(result);

        return result;
    }
}

module.exports = index;
