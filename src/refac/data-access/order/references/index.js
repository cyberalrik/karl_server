'use strict'

const ServiceOrder = require('../../../../services/service.order');
const logger = require('../../../../helper/util/logger.util');
const TAG = 'order.sellTo';
const config = require('../../../../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;

class index
{
    constructor() {
        this.references = seq.define('order_references', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            orderId: {
                type:Sequelize.INTEGER,
            },
            type: {
                type:Sequelize.STRING,
            },
            tbId: {
                type:Sequelize.INTEGER,
            }
        })
    }

    async saveReferences(params) {
        let data = params.currentReferences;

        let serviceOrder = new ServiceOrder();

        if (data.REFERENCE === undefined || data.REFERENCE === null) {
            return;
        }

        try {
            for (let i = 0; i < data.REFERENCE.length; i++) {
                let sql = {orderId: params.result.orderDataId};
                let references = data.REFERENCE[i];

                if (
                    !(
                        references === undefined
                        || references.$ === undefined
                        || references.$.type === undefined
                        || references.TB_ID === undefined
                        || references.TB_ID[0] === undefined
                    )
                    && await serviceOrder.isInteger(references.TB_ID[0], TAG)
                ) {
                    sql.type = references.$.type;
                    sql.tbId = await serviceOrder.getInteger(references.TB_ID[0], TAG);
                    await this.references.create(sql);
                } else {
                    logger.err(TAG, 'References-Daten unkorrekt/ unvollständig', 'Fehler in Datei ' + params.currentFileName);
                }
            }


        } catch (e) {
            logger.err(TAG, 'References-Daten unkorrekt', 'Fehler in Datei ' + params.currentFileName);
        }
    }

    async getReferencesDetails(orderId) {
        let result;
        try {
            let sql = {
                where: {orderId: orderId},
            };

            result = await this.references.findAll(sql);
        } catch (e) {
            logger.err(TAG, 'SQL-Error', e.message);
        }

        return  result
    }
}

module.exports = index;