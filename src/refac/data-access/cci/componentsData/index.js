'use strict';

const sql = require('../../../../helper/maria.cci.database.helper');
const query = require('../../../../helper/sql/query.collect');
const QueryForPreparation = require('../../../../helper/sql/query.cci.components');
const logger = require('../../../../helper/util/logger.util');

class ComponentsData {
    constructor() {
    }

    // async getComponents(mpn, lang){
    //     let comps = await sql.sql(await query.getComponentsFromCCISQLandLanguage(mpn, lang));
    //     return comps;
    // }

    async getAllLanguageComponentsFromMpn(mpn){
        let comps = await sql.sql(await query.getAllLanguageComponentsFromMpn(mpn));
        return comps;
    }

    async prepareComponents(TAG){
        let queryForPreparation = new QueryForPreparation();

        logger.log(TAG,'Tabelle leeren','Um die Komponenten-Tabelle neu zu befüllen, muss sie zuvor geleert werden.');
        /** Leert die vorhandene Tabelle. */
        await sql.sql(await queryForPreparation.truncate());

        logger.log(TAG, 'de-Komponenten', 'Es werden alle deutschen Komponenten hinzugefügt.');
        /** Fügt die deutschen Komponenten in die Tabelle ein. */
        await sql.sql(await queryForPreparation.insertDe());

        logger.log(TAG, 'ee-Komponenten', 'Es werden alle englischen Komponenten hinzugefügt.');
        /** Fügt die englischen Komponenten in die Tabelle ein. */
        await sql.sql(await queryForPreparation.insertEe());
    }
}

module.exports = ComponentsData;