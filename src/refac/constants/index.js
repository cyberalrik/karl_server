'use strict';

module.exports = {
    IN_MEMORY: 'inMemory',
    CCI: 'cci',
    MARIA: 'maria',
    KARL: 'karl',
    LYNN: 'lynn',
    ITM: 'itm',
    PRODUCTCOMPONENT: 1,
    ARTICLECOMPONENT: 2
}