'use strict';

/** Prüft, ob die wichtigsten Daten enthalten sind. */
exports.isOk = async (data) => {
    return await data.getSubject() != null && await data.getMailBody() != null;
}
