'use strict';

class Index {
    constructor(subject = null
                , mailBody = null
                , addMailTo = null
                , addMailCc = null
                , isHtml = false
                , mailBodyHTML = null
                , attachments = null
    ) {
        this.subject = subject;
        this.mailBody = mailBody;
        this.addMailTo = addMailTo;
        this.addMailCc = addMailCc;
        this.isHtml = isHtml;
        this.mailBodyHTML = mailBodyHTML;
        this.attachments = attachments;
    }

    async setSubject(subject) {
        this.subject = subject;
    }

    async getSubject() {
        return this.subject;
    }

    async setMailBody(mailBody) {
        this.mailBody = mailBody;
    }

    async getMailBody() {
        return this.mailBody;
    }

    async setAddMailTo(addMailTo) {
        this.addMailTo = addMailTo;
    }

    async getAddMailTo() {
        return this.addMailTo;
    }

    async setAddMailCc(addMailCc) {
        this.addMailCc = addMailCc;
    }

    async getAddMailCc() {
        return this.addMailCc;
    }

    async setIsHtml(isHtml) {
        this.isHtml = isHtml;
    }

    async getIsHtml() {
        return this.isHtml;
    }

    async setMailBodyHTML(mailBodyHTML) {
        this.mailBoddyHTML = mailBodyHTML;
    }

    async getMailBodyHTML() {
        return this.mailBodyHTML;
    }

    async setAttachments(attachments) {
        this.attachments = attachments;
    }

    async getAttachments() {
        return this.attachments;
    }

    async isAttachments() {
        return !(this.attachments === null || this.attachments === undefined);
    }
}

module.exports = Index;
