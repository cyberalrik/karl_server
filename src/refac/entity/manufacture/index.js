'use strict';

class Manufacture {
    constructor(cnetID, name, id) {
        this.cnetID = cnetID;
        this.name = name;
        this.id = id;
    }

    getManufacture() {
        return {
            id: this.id,
            cnetID: this.cnetID,
            name: this.name
        }
    }
}

module.exports = Manufacture;
