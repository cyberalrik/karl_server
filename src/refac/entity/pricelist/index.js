'use strict';

class PriceList {
    constructor(_articleId, _channelId, _price, _stock) {
        this.aticleId = _articleId;
        this.channelId = _channelId;
        this.price = _price;
        this.stock = _stock;
    }

    getPriceListItem() {
        return {
            article: this.aticleId,
            channelId: this.channelId,
            price: this.price,
            stock: this.stock
        }
    }
}

module.exports = PriceList;
