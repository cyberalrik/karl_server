'use strict';

const imageService = require('../service/image.service');
const UseCaseSetImageChannel = require('../../../refac/use-cases/image/set-image-channel');
const UseCaseAddImage = require('../../../refac/use-cases/image/add-image');
const UseCaseGetProduct = require('../../../refac/use-cases/products/get-product');
const UseCaseGetImage = require('../../../refac/use-cases/image/get-images');
// const DB = require('../../../refac/data-access');
// const dbConstants = require('../../../refac/constants');
const AddImage = require('../../../refac/use-cases/image/add-image');
const ServiceMoveImage = require('../service/service.move.image');

exports.postDeleteImageAction = async (req, res) => {
	// let g = {};
	res.status(200).send({result:'nicht implementiert'});
};

/** Api wird von Otto benutzt, um Bilder in Karl anzulegen oder auch zu berichtigen */
exports.postAddImageAction = async (req, res) => {
	if (req.body === undefined)
		res.status(503).send({result:false});
	let mpn = req.body.mpn;
	let manufacturer = req.body['manufacturer'];
	let uri = req.body['uri'];
	let pos = req.body['pos'];
	let lightVersion = req.body['lightVersion'];

	if(mpn === undefined || manufacturer === undefined || uri === undefined || pos === undefined) {
		res.status(503).send({result:false});
		return;
	}

	let addImage = new AddImage();
	let result = await addImage.addOwnImageToProduct(mpn, manufacturer, uri, pos, true, lightVersion);

	res.status(200).send(result);
};

/** Wird von Karl selber benutzt, um aus dem Frontend Bilderlinks zu ändern. */
exports.postUpdateImageAction = async (req, res) => {
	if (req.body === undefined) {
		res.status(503).send({});
	}

	if (
		req.body.id === null || req.body.id === undefined
		|| req.body.pnr === null || req.body.pnr === undefined
		|| req.body.url === null || req.body.url === undefined
		|| req.body.pos_old === null || req.body.pos_old === undefined
		|| req.body.pos === null || req.body.pos === undefined
		|| req.body.active === null || req.body.active === undefined
		|| req.body.behave === null || req.body.behave === undefined
	) {
		res.status(503).send({});
	}
	let manually = false;
	if (!(req.body.manually === null || req.body.manually === undefined)) {
		manually = req.body.manually;
	}

	let id = parseInt(req.body.id);
	let p_nr = parseInt(req.body.pnr);
	let url = req.body.url;
	let posOld = parseInt(req.body.pos_old);
	let pos = parseInt(req.body.pos);
	let active = req.body.active;
	let behave = parseInt(req.body.behave);
	let result = {successful: false};
	/**
	 * Wurde die Position veränderet?
	 * behave = 0 = austauschen
	 * behave = 1 = verschieben
	 * Dann bring das Bild an die neue Position bevor es verändert wird.
	 *  */
	try {
		if (pos !== posOld) {
			let serviceMoveImage = new ServiceMoveImage();
			await serviceMoveImage.moveOrChangeImage(id, posOld, pos, p_nr, behave);
		}

		result.successful = await imageService.updateImage(p_nr, active, url, pos, manually);
	} catch (e) {
		result.message = e.message;
	}
	res.status(200).send(result);
};

/** Wird von Karl selber benutzt um aus dem Frontend Bilderlinks zu ändern. */
exports.postUpdateImageChannelAction = async (req, res) => {
	if (req.body === undefined)
		res.status(503).send({});

	let id = req.body.id;
	let channel = req.body.channel;
	let active = (req.body.active === "true");

	// let db_karl = new DB(dbConstants.KARL);

	let useCaseSetImageChannel = new UseCaseSetImageChannel();
	let result = await useCaseSetImageChannel.changesImageChannel(id, channel, active);

	res.status(200).send(result);
};

exports.postUpdateAllActiveImageAction = async (req, res) => {
	if (req.body === undefined)
		res.status(503).send({});
	let p_nr = req.body.pnr;
	let active = req.body.active;
	let manually = false;
	if (!(req.body.manually === null || req.body.manually === undefined)) {
		manually = req.body.manually;
	}

	let result = await imageService.updateAllActiveImage(p_nr, active, manually);
	res.status(200).send(result);
};

/** Aus Karl wird ein neues Bild übergeben */
exports.postNewImageForPNr = async (req, res) => {
	let result = {successful: false};

	if (req.body === undefined) {
		res.status(503).send(result);
		return
	}

	if (
		req.body.pnr === null || req.body.pnr === undefined
		|| req.body.url === null || req.body.url === undefined
	) {
		res.status(503).send({});
	}

	let p_nr = parseInt(req.body.pnr);
	let url = req.body.url;

	try {
		let useCaseGetProduct = new UseCaseGetProduct();
		let product = await useCaseGetProduct.getProductsPerValue('p_nr', p_nr);
		let mpn = product[0].p_name;
		let manufacturer = product[0].p_brand;

		let useCaseGetImage = new UseCaseGetImage();
		let maxPos = await useCaseGetImage.getImageMaxPosForPNr(p_nr);

		let pos = maxPos + 1;
		let useCaseAddImage = new UseCaseAddImage();

		result = await useCaseAddImage.addOwnImageToProduct(mpn, manufacturer, url, pos, false, false);
	} catch (e) {
		result.message = e.message;
	}
	res.status(200).send(result);
};