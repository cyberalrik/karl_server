'use strict';

const controller = require('./controller/image.controller');

module.exports = async function(app) {
	app.post('/api/product/image/add/', controller.postAddImageAction);
	app.post('/api/product/image/update/', controller.postUpdateImageAction);
	app.post('/api/product/image/new/', controller.postNewImageForPNr);
	app.post('/api/product/image/channel/update/', controller.postUpdateImageChannelAction);
	app.post('/api/product/image/delete/', controller.postDeleteImageAction);
	app.post('/api/product/image/update/allActive', controller.postUpdateAllActiveImageAction);
};