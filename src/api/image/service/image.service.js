'use strict';

const UseCaseGetImageSize = require('../../../refac/use-cases/image/get-image-size');
const UseCaseDetermineImageChannel = require('../../../refac/use-cases/image/determine-image-channel');
const UseCaseGetImageVersion = require('../../../refac/use-cases/image/get-image-version');
const imageRepo = require('../../../repository/repo.images');
const log = require('../../../helper/util/logger.util');
const imageValidation = require('../../../helper/validation/validation.image.js');
const TAG = 'image.service';

exports.updateImage = async (p_nr, active, url, pos, manually = false) =>{
	//TODO: Refactoring
	let done = false;
	if (!imageValidation.checkImageURL(url))
		return done;

	let useCaseGetImageSize = new UseCaseGetImageSize();
	let useCaseDetermineImageChannel = new UseCaseDetermineImageChannel();
	let useCaseGetImageVersion = new UseCaseGetImageVersion();

	let images = await imageRepo.getImagesPerProductNumber(p_nr);

	/**
	 * Suchen, ob der Link schon vorhanden ist.
	 * Wenn JA
	 * 		Dann wird er hier korrigiert.
	 * Wenn NEIN
	 * 		Dann wird er weiter untern neu angelegt.
	 * */
	for (let i = 0; i < images.length; i++){
		if(images[i]['image_url'].toLocaleLowerCase().includes(url.toLocaleLowerCase())){
			let imageSize = await useCaseGetImageSize.getImageSize(url);
			/** Wenn der Link "tot" ist, den Link deaktivieren */
			if (!imageSize.successful ) {
				active = false;
			}

			let channel = await useCaseDetermineImageChannel.determineChannel(imageSize);
			let version = await useCaseGetImageVersion.getImageVersion(url);
			await imageRepo.addOrUpdateImage(p_nr, url, pos, active, version, channel, imageSize, manually);
			done = true;
			log.log(TAG, 'Image Update', `Bilder-Update für ${p_nr}` );
			break;
		}
	}

	/**
	 * Wenn der Bilderlink nicht gefunden und korrigiert werden konnte,
	 * wird er hier neu angelegt.
	 * */
	if (!done){
		let imageSize = await useCaseGetImageSize.getImageSize(url);
		/** Wenn der Link "tot" ist dann Link deaktivieren */
		if (!imageSize.successful ) {
			active = false;
		}

		let channel = await useCaseDetermineImageChannel.determineChannel(imageSize);
		let version = await useCaseGetImageVersion.getImageVersion(url);
		await imageRepo.addOrUpdateImage(p_nr, url, pos, active, version, channel, imageSize, manually);
		done = true;
		log.log(TAG, 'Image Add', `Bild hinzugefügt für ${p_nr}` );
	}
	return done
};

exports.updateAllActiveImage = async (p_nr, active, manually) => {
	let useCaseGetImageSize = new UseCaseGetImageSize();
	let useCaseDetermineImageChannel = new UseCaseDetermineImageChannel();
	let useCaseGetImageVersion = new UseCaseGetImageVersion();

	let images = await imageRepo.getImagesPerProductNumber(p_nr);
	for (let i= 0; i<images.length; i++){
		let url = images[i].image_url;
		let imageSize = await useCaseGetImageSize.getImageSize(url);
		/** Wenn der Link "tot" ist dann Link deaktivieren */
		if (!imageSize.successful ) {
			active = false;
		}

		let channel = await useCaseDetermineImageChannel.determineChannel(imageSize);
		let version = await useCaseGetImageVersion.getImageVersion(url);

		await imageRepo.addOrUpdateImage(images[i].p_nr, url, images[i].image_pos, active, version, channel, imageSize, manually);
	}
	return {};
};