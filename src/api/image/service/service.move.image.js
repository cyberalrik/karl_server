'use strict';

const imageRepo = require('../../../repository/repo.images');
const logger = require('../../../helper/util/logger.util');
const TAG = 'service.task.aktiv.';

class Index {
    /**
     * behave = 0 = austauschen
     * behave = 1 = verschieben
     *  */
    async moveOrChangeImage(id, posOld, posNew, p_nr, behave) {
        let result = true;
        switch (true) {
            case behave === 0:
                result = await this.changeImage(id, p_nr, posOld, posNew);
                break
            case behave === 1:
                result = await this.moveImage(id, p_nr, posOld, posNew);
                break
            default:
                result = false;
                break
        }

        return result
    }

    async moveImage(id, p_nr, posOld, posNew) {
        let result = true;

        /** Bild herausnehmen */
        let currentImage = await imageRepo.getImagesById(id);

        /** Lücke schließen */
        await imageRepo.setNewPos(p_nr, posOld, -1);

        /** Platz machen für neue Bildposition */
        await imageRepo.setNewPos(p_nr, posNew, 1);

        /** Endgültige neue Position */
        await imageRepo.setForIdNewPos(currentImage.id, posNew)
        return result;
    }

    async changeImage(id, p_nr, posOld, posNew) {
        let result = true;
        let currentImage = await imageRepo.getImagesById(id);
        if (currentImage.image_pos !== posOld) {
            return false
        }

        let images = await imageRepo.getImagesPerProductNumberAndPos(p_nr, posNew)
        for (let index in images) {
            await imageRepo.setForIdNewPos(images[index].id, posOld)
        }

        await imageRepo.setForIdNewPos(currentImage.id, posNew)
        return result;
    }
}

module.exports = Index