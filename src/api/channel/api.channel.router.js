'use strict';
const controller = require("./controller/channel.controller.js");

module.exports = async function(app) {
    app.post('/api/channel/update/', controller.postUpdateChannelAction);
    app.post('/api/channel/new/', controller.postNewChannelAction);
};