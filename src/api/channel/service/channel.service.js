'use strict';

const channelRepo = require('../../../repository/repo.channel.js');
const artChannelRepo = require('../../../repository/article/repo.article.channel.js');

exports.getAllChannel = async () => {
	return channelRepo.getChannels();
};

exports.getAllChannelStockInStructure = async () => {
	let channelStructure = [];
	let channels = await channelRepo.getChannels();
	for (let currentChannel of channels) {
		channelStructure[currentChannel.channel_key] = currentChannel.stock;
	}

	return channelStructure;
};

exports.setArticleChannel = async (a_nr, channelKey, value) => {
	await artChannelRepo.setArtChannelData(a_nr, channelKey, value);
	return true;
};

exports.setArticleChannelWithResponse = async (a_nr, channelKey, value) => {
	return await artChannelRepo.setArtChannelDataWithResponse(a_nr, channelKey, value);
};

exports.updateChannel = async (channelKey, isActive, imageWidth, imageHeight) => {
	await channelRepo.updateChannel(channelKey, isActive, imageWidth, imageHeight);
	return true;
}

exports.createNewChannel = async (channelName, channelKey, isActive, channelSurcharge, imageType, stock, imageWidth, imageHeight) => {
	await channelRepo.setChannel(channelName, channelKey, isActive, channelSurcharge, imageType, stock, imageWidth, imageHeight);
	return true;
}