'use strict';
const channelService = require("../service/channel.service");
const channelValidation = require('../../../helper/validation/validation.channel.js');

/**
 * Steuert den Update-Prozess eines Kanals.
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.postUpdateChannelAction = async (req, res) => {

    let result = {successful: false};
    try {
        if (req.body === undefined) {
            res.status(503).send({});
        }

        let channelKey = req.body.channel_key;
        let isActive = req.body.channel_is_active;
        let imageWidth = req.body.image_width;
        let imageHeight = req.body.image_height;

        if (
            channelKey === null || channelKey === undefined || channelKey === ''
            || imageWidth === null || imageWidth === undefined || imageWidth === ''
            || imageHeight === null || imageHeight === undefined || imageHeight === ''
            || isActive === null || isActive === undefined
        ) {
            throw 'Mindestens ein Wert wurde nicht angegeben!';
        }

        if (channelValidation.checkChannelImageDimensions(imageWidth, imageHeight)) {
            throw 'Der angegebene Wert für Bildhöhe/-breite ist keine gültige Zahl!';
        }

        result.successful = await channelService.updateChannel(channelKey, isActive, imageWidth, imageHeight);
    } catch (e) {
        result.message = e;
    }
    res.status(200).send(result);
}

/**
 * Steuert den Create-Prozess eines Kanals.
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.postNewChannelAction = async (req, res) => {

    let result = {successful: false};
    try {
        if (req.body === undefined) {
            res.status(503).send({});
        }

        let channelName = req.body.channel_name;
        let channelKey = req.body.channel_key;
        let isActive = req.body.channel_is_active;
        let channelSurcharge = req.body.channel_surcharge;
        let imageType = req.body.image_type;
        let stock = req.body.stock;
        let imageWidth = req.body.image_width;
        let imageHeight = req.body.image_height;

        if (
            channelName === null || channelName === undefined || channelName === ''
            || channelKey === null || channelKey === undefined || channelKey === ''
            || isActive === null || isActive === undefined
            || channelSurcharge === null || channelSurcharge === undefined
            || imageType === null || imageType === undefined || imageType === ''
            || stock === null || stock === undefined || stock === ''
            || imageWidth === null || imageWidth === undefined || imageWidth === ''
            || imageHeight === null || imageHeight === undefined || imageHeight === ''
        ) {
            throw 'Mindestens ein Wert wurde nicht angegeben!';
        }

        if (channelValidation.checkChannelImageDimensions(imageWidth, imageHeight)) {
            throw 'Der angegebene Wert für Bildhöhe/-breite ist keine gültige Zahl!';
        }

        if (channelValidation.checkChannelSurcharge(channelSurcharge)){
            throw 'Der angegebene Aufschlag ist keine gültige Zahl!';
        }

        result.successful = await channelService.createNewChannel(channelName, channelKey, isActive, channelSurcharge, imageType, stock, imageWidth, imageHeight);
    } catch (e) {
        result.message = e;
    }
    res.status(200).send(result);
}