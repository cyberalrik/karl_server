'use strict';

const productRepo = require('../../../repository/repo.product');
const articleRepo = require('../../../repository/article/repo.article');
const imageRepo = require('../../../repository/repo.images');
const Product2productImages = require('../../../repository/repo.product2productImages');
const UseCaseGetImagesForProduct = require('../../../refac/use-cases/image/get-Images-for-Product');
const compRepo = require('../../../repository/repo.components');
const artChannelRepo = require('../../../repository/article/repo.article.channel');
const channelRepo = require('../../../repository/repo.channel');
const priceListRepo = require('../../../repository/repo.pricelist');
const ServiceDateFormatted = require('../service/../../../services/service.date.format');
const repoChannel = require('../../../repository/repo.channel')
const constants = require('../../../constants/images.constant');
// const _ = require('underscore');

let serviceDateFormatted = {};

class index
{
	async getListOfArticles(_off, _limit) {
		return '5'+ _off + ' ' + _limit;
	}

	async getProductList(_mpn, _limit, _offset) {
		let model = [];
		let products = await productRepo.getProductsPerMPN(_offset, _limit, _mpn);
		for (let i = 0; i < products.length; i++) {
			// let item = {
			// 	p_name: products[i]['p_name'],
			// 	p_brand: products[i]['p_brand'],
			// 	p_nr: products[i]['p_nr'],
			// 	p_text_de: products[i]['p_text_de'],
			// 	p_3_rd: products[i]['p_3_rd'] === true ? 'ja' : ''
			// };
			let data = await this.getProductInformation(products[i]['p_nr'])
			let articleData = await this.getArticleData(data.article);
			let item = {
				p_name: data.product['p_name'],
				p_brand: data.product['p_brand'],
				p_nr: data.product['p_nr'],
				p_text_de: data.product['p_text_de'],
				p_3_rd: data.product['p_3_rd'] === true ? 'ja' : '',
				articles: articleData
			}

			model.push(item)
		}
		return model;
	}

	/**
	 * Hier werden die Artikeldaten extrahiert und aufbereitet,
	 * die im Karl Frontend in der Produktliste angezeigt werden sollen.
	 * */
	async getArticleData(article) {
		if (article === undefined || article === null) {
			return [];
		}

		let result = [];
		for (let index in article) {
			let currentArticleData = article[index];

			let channelData = [];
			let deactivate = true;
			for (let index in currentArticleData.channelData) {
				let currentChannel = currentArticleData.channelData[index];
				if (currentChannel.name === 'TB') {
					continue;
				}

				let channel = {};
				channel = {name: currentChannel.name, active: currentChannel.is_active};
				channelData.push(channel);
				deactivate = deactivate && !currentChannel.is_active;
			}
			result.push({text:`ArtikelNr.: ${currentArticleData.a_nr}`, channelData: channelData, active: !deactivate});
		}

		return result;
	}

	async getProductInformation(p_nr) {
		let data = {};

		serviceDateFormatted = new ServiceDateFormatted();
		let DBProduct = await productRepo.getProductInformationPerPnr(p_nr);

		/** Wurde etwas für die p_nr zurückgegeben? */
		if (DBProduct === null) {
			return [];
		}

		let product2productImages = new Product2productImages();
		let p2pResult = await product2productImages.getImagesConnection(p_nr);
		let imagesSet = [];

		if (p2pResult.successful) {
			for (let index in p2pResult.p_nr_imagesArray) {
				let imageOwner = {};
				imageOwner.owner = true;
				imageOwner.p_nr = p2pResult.p_nr_imagesArray[index];

				let DBImages = await imageRepo.getImagesPerProductNumber(imageOwner.p_nr);

				/** Gehören die Bilder nicht dem Produkt, werden die Bild-Produktdaten mit zurückgegeben. */
				if (imageOwner.p_nr !== p_nr) {
					imageOwner.productData = await productRepo.getProductInformationPerPnr(imageOwner.p_nr);
					imageOwner.owner = false;
				}

				let resultImagesData = await this.getImageData(DBImages);

				let imageData = {};
				imageData.allChecked = resultImagesData.allActive;
				imageData.pnr = p_nr;

				if (
					resultImagesData.images !== undefined
					&& resultImagesData.images.length > 0
				) {
					imagesSet.push({
						DBImages: DBImages,
						imageOwner: imageOwner,
						imageData: imageData,
						images: resultImagesData.images
					})
				}
			}
		}

		let DBComponents = await compRepo.getAllComponentPerID(p_nr);
		let DBArticle = await articleRepo.getAllArticlePerID(p_nr);

		data.product = {
			p_nr: DBProduct['p_nr'],
			p_name: DBProduct['p_name'],
			p_text_de: DBProduct['p_text_de'],
			p_text_ee: DBProduct['p_text_ee'],
			p_brand: DBProduct['p_brand'],
			p_brand_key: DBProduct['p_brand_key'],
			p_cat_key: DBProduct['p_cat_id'],
			p_cat_name: DBProduct['p_cat_name'],
			p_3_rd: DBProduct['p_3_rd'],
			p_import_lock: DBProduct['p_import_block'],
			p_export_lock: DBProduct['p_export_block'],
			p_keyword1: DBProduct['p_keyword1'],
			p_keyword2: DBProduct['p_keyword2'],
			p_keyword3: DBProduct['p_keyword3'],
			p_weight: DBProduct['p_weight'].toString().replace(".", ","),  // Dezimalstelle nicht als Punkt, sondern als Komma anzeigen.
			imagesSet: imagesSet,
			p_createdAt: await serviceDateFormatted.getDateFormatted(DBProduct['createdAt']),
			p_updatedAt: await serviceDateFormatted.getDateFormatted(DBProduct['updatedAt']),
		};

		let component = [];
		for (let i = 0; i < DBComponents.length; i++) {
			let item = {
				key: DBComponents[i]['p_component_key'],
				value: DBComponents[i]['p_component_value'],
				language: DBComponents[i]['p_component_language'],
			};
			component.push(item);
		}
		data.comp = component;

		let article = [];
		for (let i = 0; i < DBArticle.length; i++) {
			let item = {
				a_nr2: DBArticle[i]['a_nr2'],
				a_nr: DBArticle[i]['a_nr'],
				a_ean: DBArticle[i]['a_ean'],
				a_delivery_time: DBArticle[i]['a_delivery_time'],
				a_createdAt: await serviceDateFormatted.getDateFormatted(DBArticle[i]['createdAt']),
				a_updatedAt: await serviceDateFormatted.getDateFormatted(DBArticle[i]['updatedAt']),
			};
			item.channelData = await this.getChannelInformation(item.a_nr);
			article.push(item);
		}
		data.article = article;

		/** Hier wird ermittelt, welche Bilder zu TB geschickt würden. */
		let useCaseGetImagesForProduct = new UseCaseGetImagesForProduct();
		data.usedImages = await useCaseGetImagesForProduct.go(p_nr);

		/** Wurden Bilder zurückgegeben, dann von welchem Produkt? */
		if (
			data.usedImages !== undefined
			&& data.usedImages.length > 0
		) {
			data.usedImagesP_nr = data.usedImages[0].getDataValue('p_nr');
		}

		return data;
	}

	async getImageData(DBImages) {
		let images = [];
		let allActive = true;
		for (let i = 0; i < DBImages.length; i++) {
			let currentImage = DBImages[i];
			let imagesChannelData = await this.getImagesFromChannel(currentImage);

			let item = {
				imageData: currentImage,
				p_createdAt: await serviceDateFormatted.getDateFormatted(currentImage['createdAt']),
				p_updatedAt: await serviceDateFormatted.getDateFormatted(currentImage['updatedAt']),
				imagesChannelData: imagesChannelData
			};

			if (item.imageData.is_active === false || allActive === false)
				allActive = false;

			images.push(item);
		}

		return {images: images, allActive: allActive}
	}

	async getImagesFromChannel(images) {
		let result = [];

		let channels = await repoChannel.getChannels();
		for (let channel of channels) {
			let channelValue = await constants.getChannelTypeValue(channel.channel_key);
			result[channel.channel_key] = (images.channel & channelValue) === channelValue;
		}

		return result;
	}

	async setProductsLock(p_nr, exportLock, importLock) {
		await productRepo.setProductLock(p_nr, importLock, exportLock);
	}

	async getChannelInformation(a_nr) {
		let result = [];
		let channelList = await channelRepo.getChannels();
		let channelArticle = await artChannelRepo.getArtChannelData(a_nr);
		let priceList = await priceListRepo.getPriceListPerANr(a_nr);
		let serviceDateFormatted = new ServiceDateFormatted();
		for (let i = 0; i < channelList.length; i++) {
			let articlePrice = '-,--';
			let articleQuantity = 0;
			let updatedAt = '';
			let j = -1;
			let articleChannelisActive = channelList[i]['channel_is_active'];
			for (j = 0; j < channelArticle.length; j++) {
				if (channelArticle[j].channel_key === channelList[i]['channel_key']) {
					articleChannelisActive = channelArticle[j].is_active;
				}
			}

			for (j = 0; j < priceList.length; j++) {
				if (priceList[j].channel === channelList[i]['channel_key']) {
					articlePrice = await this.getFormattedPrice(priceList[j].price);
					articleQuantity = priceList[j].quant;
					updatedAt = await serviceDateFormatted.getDateFormatted(priceList[j].updatedAt);
					break;
				}
			}

			let item = {
				price: articlePrice,
				quantity: articleQuantity,
				key: channelList[i]['channel_key'],
				name: channelList[i]['channel_name'],
				is_active: articleChannelisActive,
				disabled: !channelList[i]['channel_is_active'],
				stock: channelList[i]['stock'],
				// createdAt: await serviceDateFormatted.getDateFormatted(channelList[i]['createdAt']),
				updatedAt: updatedAt
			};
			result.push(item);
		}

		return result;
	}

	async getFormattedPrice(price) {
		let result = price;
		/** trennen nach dem dezimal Pumkt */
		let value = price.split('.');

		/** Preis wieder zusammen setzen */
		if (value.length === 1) {
			/** Es sind nur EURO */
			result = await this.getFormattedEuro(value[0]) + ',00';
		} else if (value.length === 2) {
			/** Es sind EURO und Cent */
			result = await this.getFormattedEuro(value[0]) + ',' + await this.getFormattedCent(value[1]);
		}

		return result;
	}

	/** Cent Zeistellig zurück geben */
	async getFormattedCent(cent) {
		if (cent.length === 1) {
			cent = cent + '0';
		}

		return cent;
	}

	/** Tausender Zeichen einfügen */
	async getFormattedEuro(euro) {
		let result = '';
		let euroList = [...euro];
		for (let i = 0; i < euroList.length; i++) {

			/** von hinten anfangen */
			let index = euroList.length - 1 - i;
			if (i % 3 === 0 && i > 0) {
				result = '.' + result;
			}
			result = +euroList[index] + result;
		}

		return result;
	}
}
module.exports = index;
