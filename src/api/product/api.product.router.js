'use strict';

const controller = require('./controller/product.controller');

module.exports = async function(app) {
	app.get('/api/article/:off/:limit', controller.getArticleAction);
	app.post('/api/product/lock', controller.productLockAction);
	app.post('/api/product/article/channel', controller.articleChannelAction)
};