'use strict';

const ProductService = require('../service/product.service.js');
const articleChannelService = require('../../channel/service/channel.service');

exports.getArticleAction = async (req, res) => {
	let off = parseInt(req.params.off);
	let limit = parseInt(req.params.limit);

	if (off < 0 || limit < 1){
		res.status(404).send([]);
	}

	let result;

	let productService = new ProductService();
	result = await productService.getListOfArticles(off, limit);

	res.send(result);
};

exports.productLockAction = async (req, res) => {
	if (req.body === undefined ||req.body === null){
		res.status(400).send();
		return;
	}
	let pnr = req.body['p_nr'] === undefined? 0: req.body['p_nr'];
	let exportLock = req.body['exportLock'] === undefined? 0: req.body['exportLock'];
	let importLock = req.body['importLock'] === undefined? 0: req.body['importLock'];

	let productService = new ProductService();
	await productService.setProductsLock(pnr, exportLock === 'true', importLock === 'true');
	res.status(200).send(true)
};


exports.articleChannelAction = async (req, res) => {
	if (req.body === undefined ||req.body === null){
		res.status(400).send();
		return;
	}
	let a_nr = req.body['a_nr'];
	let key = req.body['key'];
	let active = req.body['active'] === 'false' ? 0:1;

	await articleChannelService.setArticleChannel(a_nr, key, active);

	res.status(200).send(true);
};