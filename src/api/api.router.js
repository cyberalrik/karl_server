'use strict';

module.exports = function(app) {
	require('./log/api.log.router')(app);
	require('./product/api.product.router')(app);
	require('./html/html.router')(app);
	require('./image/image.router')(app);
	require('./channel/api.channel.router.js')(app);
};