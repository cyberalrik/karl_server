'use strict';

let controller = require('../html/controller/html.controller');
let postController = require('../html/controller/post.controller');
let manualController = require('../html/controller/manual.controller');

module.exports = async function(app) {
	app.get('/', controller.indexAction);
	app.get('/logs/:off?/:limit?/:type?/:tag?/:shortDescription?/:description?/:startDate?/:endDate?/', controller.logAction);
	app.get('/product', controller.artAction);
	app.get('/product/search/:mpn?/:limit?/:offset?/', controller.productSearchAction);
	app.get('/product/overview/:pnr?/', controller.productOverviewAction);
	// app.get('/order/:off?/:channelSign?/', controller.orderAction);
	app.get('/channel', controller.channelAction);
	app.get('/quality', controller.qualityAction);


	app.post('/logs/filterType/:parameter?/', postController.loadFilterTypeAction);
	app.post('/logs/filterTag/:parameter?/', postController.loadFilterTagAction);
	app.post('/logs/filterChannelSign/:parameter?/', postController.loadFilterChannelAction);
	app.post('/logs/orderDetails/:parameter?/', postController.loadOrderDetailsAction);
	app.post('/infobox/put/:parameter?/', postController.saveInfoboxAction);
	app.post('/infobox/get/:parameter?/', postController.getInfoboxAction);
	app.post('/priceToTbOne/:parameter?/', postController.articlePriceToTbOneAction);
	app.post('/stockToTbOne/:parameter?/', postController.articleStockToTbOneAction);
	app.post('/productDataRefresh/:parameter?/', postController.productDataRefreshAction);
	app.post('/productDataToTbOne/:parameter?/', postController.productDataToTbOneAction);
	app.post('/deleteImageToOtherProduct/:parameter?/', postController.deleteImageToOtherProductAction);

	/** CronJob aufrufe über web */
	// Deaktiviert Artikel die in Lynn als gelöscht gekennzeichnet sind (State=1058)
	app.get('/cronJob/:parameter?/', controller.cronJobAction);
	app.post('/cronJob/:parameter?/', controller.cronJobAction);
	// sync der Tabelle aufrufen
	app.get('/sync/:parameter?/', controller.syncAction);

	// Welcher Job läuft gerade?
	app.get('/jobStatus', controller.jobStatusAction);

	// nur zum Aufrufen
	// app.get('/loadOrderData', controller.loadOrderDataAction);

	// ruft das einmalige setzen der Bildgröße auf
	app.get('/imageSize', controller.imageSizeAction);
	app.get('/imageChannel', controller.setImageChannelAction);

	// Sind für jeweils new und ref Artikel separate Produkte angelegt, werden sie hiermit zu einem Produkt zusammengeführt.
	// app.get('/doubleProduct', controller.doubleProductAction);

	/**
	 * Diese Klasse wird nach der Strukturänderung der DB (erp.ProductPropertyLink)
	 * von ArticleForeignId hinzu ProductForeignId nicht mehr benötigt.
	 * */
	// Kontrolle, ob new und ref als Produkt verlinkt ist. (Altlasten, als new und ref einzeln verlinkt werden mussten.)
	// app.get('/newOrRefLinked', controller.newOrRefLinkedAction);

	// MANUAL
	app.get('/call/:parameter?/', manualController.indexAction);
};