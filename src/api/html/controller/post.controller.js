'use strict';

const logService = require('../../log/service/log.service');
const UseCaseSaveInfoboxText = require('../../../refac/use-cases/infobox/save-info')
const UseCaseGetInfoboxText = require('../../../refac/use-cases/infobox/get-info')
const UseCaseOrderData = require('../../../refac/use-cases/orderlist/loadOrderList');
const UseCaseOrderDetails = require('../../../refac/use-cases/orderlist/loadOrderDetail');
const UseCaseTableDescription = require('../../../refac/use-cases/orderlist/loadOrderList/tableDescription');
const UseCaseArticlePriceToTbOne = require('../../../refac/use-cases/pricelist/articleDataToTbOne/price');
const UseCaseArticleStockToTbOne = require('../../../refac/use-cases/pricelist/articleDataToTbOne/stock');
const UseCaseKarlCollectAll = require('../../../refac/use-cases/forTasks/karlCollectAll');
const UseCaseKarlCollectPricelist = require('../../../refac/use-cases/forTasks/karlCollectPricelist');
const UseCaseImageForOtherDelete = require('../../../refac/use-cases/image/image-for-other/delete');
const UseCaseKarlCollectAllE2 = require("../../../refac/use-cases/forTasks/karlCollectAllE2");
const UseCaseSendDataToTb = require("../../../refac/use-cases/products/send-data-to-tb");
const TaskKarlArticleDeactivate = require("../../../tasks/task.karl.article.deactivate");

exports.loadFilterTypeAction = async (req, res) => {
    if (req.body === undefined || req.body === null){
        res.status(400).send();
        return;
    }
    let result = {};
    if (
        req.body.type === undefined || req.body.type === null ||
        req.body.tag === undefined || req.body.tag === null ||
        req.body.shortDescription === undefined || req.body.shortDescription === null ||
        req.body.description === undefined || req.body.description === null ||
        req.body.startDate === undefined || req.body.startDate === null ||
        req.body.endDate === undefined || req.body.endDate === null
    ){
        result.messages = 'Es fehlen Parameter';
        result.successful = false;
        res.status(200).send(result);
        return;
    }

    let type = req.body.type === '-1' ? '' : req.body.type;
    let tag = req.body.tag === '-1' ? '' : req.body.tag;
    let shortDescription = req.body.shortDescription === '-1' ? '' : req.body.shortDescription;
    let description = req.body.description === '-1' ? '' : req.body.description;
    let unixStartDate = req.body.startDate === '-1' ? '' : req.body.startDate;
    let unixEndDate = req.body.endDate === '-1' ? '' : req.body.endDate;


    result.successful = true;
    result.type = await logService.getFilterValue("type", type, tag, shortDescription, description, unixStartDate, unixEndDate);

    res.status(200).send(result);
}

exports.loadFilterTagAction = async (req, res) => {
    if (req.body === undefined || req.body === null){
        res.status(400).send();
        return;
    }
    let result = {};
    if (
        req.body.type === undefined || req.body.type === null ||
        req.body.tag === undefined || req.body.tag === null ||
        req.body.shortDescription === undefined || req.body.shortDescription === null ||
        req.body.description === undefined || req.body.description === null ||
        req.body.startDate === undefined || req.body.startDate === null ||
        req.body.endDate === undefined || req.body.endDate === null
    ){
        result.messages = 'Es fehlen Parameter';
        result.successful = false;
        res.status(200).send(result);
        return;
    }

    let type = req.body.type === '-1' ? '' : req.body.type;
    let tag = req.body.tag === '-1' ? '' : req.body.tag;
    let shortDescription = req.body.shortDescription === '-1' ? '' : req.body.shortDescription;
    let description = req.body.description === '-1' ? '' : req.body.description;
    let unixStartDate = req.body.startDate === '-1' ? '' : req.body.startDate;
    let unixEndDate = req.body.endDate === '-1' ? '' : req.body.endDate;


    result.successful = true;
    result.tag = await logService.getFilterValue("tag", type, tag, shortDescription, description, unixStartDate, unixEndDate);

    res.status(200).send(result);
}


/**
 * Speichert die Übergebenen Daten der Infobox
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.saveInfoboxAction = async (req, res) => {
    let result = {};

    if (req.body === undefined || req.body === null){
        res.status(400).send();
        return;
    }
    if (
        req.body.nr === undefined || req.body.nr === null ||
        req.body.type === undefined || req.body.type === null ||
        req.body.infoText === undefined || req.body.infoText === null
    ){
        result.messages = 'Es fehlen Parameter';
        result.successful = false;
        res.status(200).send(result);
        return;
    }

    let useCaseSaveInfoboxText = new UseCaseSaveInfoboxText();

    result = await useCaseSaveInfoboxText.saveInfo(req.body.nr, req.body.type, req.body.infoText)

    res.status(200).send(result);
}

exports.loadFilterChannelAction = async (req, res) => {
    let useCaseOrderData = new UseCaseOrderData();

    let result = {
        successful: true,
        channelSign: await useCaseOrderData.getFilterValue("channelSign")
    };

    res.status(200).send(result);
}


/**
 * Gibt die Daten für die Infobox zurück
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.getInfoboxAction = async (req, res) => {
    if (req.body === undefined || req.body === null){
        res.status(400).send();
        return;
    }
    let result = {};

    if (
        (req.body.type === undefined || req.body.type === null ||
        req.body.nr === undefined || req.body.nr === null)
    ) {
        result.messages = 'Es fehlen Parameter';
        result.successful = false;
        res.status(200).send(result);
        return;
    }
    let useCaseGetInfoboxText = new UseCaseGetInfoboxText();

    result = await useCaseGetInfoboxText.getInfo(req.body.type, req.body.nr);

    res.status(200).send(result);
}

exports.loadOrderDetailsAction = async (req, res) => {
    let result = {};

    if (
        req.body.orderId === undefined || req.body.orderId === null
    ){
        result.messages = 'Es fehlen Parameter';
        result.successful = false;
        res.status(200).send(result);
        return;
    }

    let useCaseOrderDetails = new UseCaseOrderDetails();
    let useCaseTableDescription = new UseCaseTableDescription();

    let orderId = req.body.orderId;

    let tableDiscription = await useCaseTableDescription.getTableDescription();

    let order = {
        orderData: await useCaseOrderDetails.getOrder(orderId),
        sellTos: await useCaseOrderDetails.getSellTo(orderId),
        shipTos: await useCaseOrderDetails.getShipTo(orderId),
        shipments: await useCaseOrderDetails.getShipment(orderId),
        payments: await useCaseOrderDetails.getPayment(orderId),
        references: await useCaseOrderDetails.getReferences(orderId),
        histories: await useCaseOrderDetails.getHistory(orderId),
        services: await useCaseOrderDetails.getServices(orderId),
        items: await useCaseOrderDetails.getItems(orderId),
    }

    result = {
        successful: true,
        order: JSON.stringify(order),
        tableDiscription: JSON.stringify(tableDiscription)
    };
    // result.successful = true;
    // result.channelSign = await useCaseOrderData.getFilterValue("channelSign");
    res.status(200).send(result);
}

/**
 * Vom übergebenen Artikel wird der Preis vom Übergebenen Channel an TbOne übergeben
 * Wird kein Channel übergeben, werden alle Channels benutzt.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.articlePriceToTbOneAction = async (req, res) => {
    let TAG = 'articlePriceToTbOne';
    if (req.body === undefined || req.body === null){
        res.status(400).send();
        return;
    }
    let result = {successful:true};

    if (
        req.body.a_nr === undefined || req.body.a_nr === null ||
        req.body.channel === undefined || req.body.channel === null
    ) {
        result.messages = 'Es fehlen Parameter';
        result.successful = false;
        res.status(200).send(result);
        return;
    }
    let useCaseArticlePriceToTbOne = new UseCaseArticlePriceToTbOne(TAG);
    result = await useCaseArticlePriceToTbOne.sendArticlePriceToTbOne(req.body.a_nr, req.body.channel);

    res.status(200).send(result);
}

/**
 * Vom übergebenen Artikel wird die Menge vom Übergebenen Channel an TbOne übergeben
 * Wird kein Channel übergeben, werden alle Channels benutzt.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.articleStockToTbOneAction = async (req, res) => {
    let TAG = 'articleStockToTbOne';
    if (req.body === undefined || req.body === null){
        res.status(400).send();
        return;
    }
    let result = {successful:true};

    if (
        req.body.a_nr === undefined || req.body.a_nr === null ||
        req.body.channel === undefined || req.body.channel === null
    ) {
        result.messages = 'Es fehlen Parameter';
        result.successful = false;
        res.status(200).send(result);
        return;
    }
    let useCaseArticleStockToTbOne = new UseCaseArticleStockToTbOne(TAG);
    result = await useCaseArticleStockToTbOne.sendArticleStockToTbOne(req.body.a_nr, req.body.channel);

    res.status(200).send(result);
}

/**
 * Vom übergebenen Produkt werden alle Lynn-daten neu abgeholt
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.productDataRefreshAction = async (req, res) => {
    let TAG = 'single.product.update.Karl';
    if (req.body === undefined || req.body === null){
        res.status(400).send();
        return;
    }
    let result = {successful:true};

    if (
        req.body.p_nr === undefined || req.body.p_nr === null
    ) {
        result.messages = 'Es fehlen Parameter';
        result.successful = false;
        res.status(200).send(result);
        return;
    }

    /** ALLE Produkt/ Artikel in Karl deaktivieren und an TB senden, die in Lynn deaktiviert sind */
    let taskKarlArticleDeactivate = new TaskKarlArticleDeactivate();
    await taskKarlArticleDeactivate.cronTask();

    /** Holt alle Produkt/ Artikel Daten von Lynn für das übergebene Produkt */
    let useCaseKarlCollectAll = new UseCaseKarlCollectAll(TAG);
    result = await useCaseKarlCollectAll.call(req.body.p_nr);

    if (result.successful) {
        /** Holt alle Preis und Mengen Daten von Lynn für das übergebene Produkt */
        let useCaseKarlCollectPricelist = new UseCaseKarlCollectPricelist(TAG);
        await useCaseKarlCollectPricelist.call(false, req.body.p_nr);

        /** Aktualisiert alle E2-Artikel für das übergebene Produkt */
        let useCaseKarlCollectAllE2 = new UseCaseKarlCollectAllE2(TAG);
        await useCaseKarlCollectAllE2.call(req.body.p_nr);
    }
    res.status(200).send(result);
}

/**
 * Vom übergebenen Produkt werden alle Karl-daten an TB.One gesendet
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.productDataToTbOneAction = async (req, res) => {
    let TAG = 'single.product.update.TB.One';
    if (req.body === undefined || req.body === null){
        res.status(400).send();
        return;
    }
    let result = {successful:true};

    if (
        req.body.p_nr === undefined || req.body.p_nr === null
    ) {
        result.messages = 'Es fehlen Parameter';
        result.successful = false;
        res.status(200).send(result);
        return;
    }
    let useCaseSendDataToTb = new UseCaseSendDataToTb();
    await useCaseSendDataToTb.sendProductAndArticleDataToTb([req.body.p_nr]);

    res.status(200).send(result);
}

/**
 * Lösch die Bildverbindung des übergebenen Produkts
 *
 * @param req
 * @param res
 * @returns Promise{<void>}
 */
exports.deleteImageToOtherProductAction = async (req, res) => {
    if (req.body === undefined || req.body === null){
        res.status(400).send();
        return;
    }
    let result = {};

    if (
        req.body.p_nr === undefined || req.body.p_nr === null
        || req.body.p_nr_image === undefined || req.body.p_nr_image === null
    ) {
        result.message = 'Es ist ein Fehler aufgetreten. Es wurden zu wenig Parameter übermittelt.';
        result.successful = false;
        res.status(200).send(result);
        return;
    }

    let useCaseImageForOtherDelete = new UseCaseImageForOtherDelete();
    result = await useCaseImageForOtherDelete.deleteImageLink(req.body.p_nr, req.body.p_nr_image);

    res.status(200).send(result);
}

