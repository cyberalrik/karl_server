'use strict';

const logService = require('../../log/service/log.service');
const ProductService = require('../../product/service/product.service');
const channelService = require('../../channel/service/channel.service');
const UseCaseListAllTask = require('../../../refac/use-cases/sys/listAllTask');
const isTest = require('../../../configs/app.config').getConfig().TEST;
const UseCaseGetInfoForProductAndArticle = require('../../../refac/use-cases/infobox/get-info-for-product-and-article')
// const taskLoadOrderData = require('../../../tasks/task.tbone.loadOrderData');
// const UseCaseOrderData = require('../../../refac/use-cases/orderlist/loadOrderList/index');
const UseCaseSetAllImageSize = require('../../../refac/use-cases/image/set-all-image-size');
const UseCaseChannel = require('../../../refac/use-cases/channel/get-channel-data');
// const UseCaseSetImageFeature = require('../../../refac/use-cases/Lynn/set-image-feature');
// const UseCaseDoubleProducts = require('../../../refac/use-cases/Lynn/double-products');
// const UseCaseNewOrRefLinked = require('../../../refac/use-cases/Lynn/new-or-ref-linked');
const ServiceTaskRun = require('../../../services/service.task.run');
const UseCaseItmMediaId = require('../../../refac/use-cases/itm/media');

exports.indexAction = async (req, res) => {


	// let dbValue = 8 | 32;
	// let searchValue = 9;
	//
	// let result = (dbValue & 1) === 1;
	// result = (dbValue & 2) === 2;
	// result = (dbValue & 4) === 4;
	// result = (dbValue & 8) === 8;
	// result = (dbValue & 16) === 16;
	// result = (dbValue & 32) === 32;
	//
	//
	//
	// let date1 = new Date();
	// let date2 = new Date();
	// date1.setMinutes(date1.getMinutes() - 1);
	// date2.setMinutes(date2.getMinutes() - 1);
	// let startDate = Math.floor(date1 / 1000)
	// let endDate = Math.floor(date2 / 1000)



	let titleDashboard = [];
	titleDashboard.push('Herzlich Willkommen bei Karl');
	titleDashboard.push('');

	let viewData = {
		titleDashboard: titleDashboard,
	}

	res.status(200).render('index', {
		viewData: viewData,
		title: "Hello Karl",
		page: 'home',
		test: isTest,
	});
};

exports.logAction = async (req, res) => {
	if (req.params === undefined ||req.params === null){
		res.status(400).send();
		return;
	}

	let offset = parseInt(req.params.off);
	let limit = parseInt(req.params.limit);
	let type = req.params.type === '-1' || req.params.type === undefined ? '' : req.params.type;
	let tag = req.params.tag === '-1' || req.params.tag === undefined ? '' : req.params.tag;
	let shortDescription = req.params.shortDescription === '-1' || req.params.shortDescription === undefined ? '' : req.params.shortDescription;
	let description = req.params.description === '-1' || req.params.description === undefined ? '' : req.params.description;
	let unixStartDate = req.params.startDate === '-1' || req.params.startDate === undefined ? '' : req.params.startDate;
	let unixEndDate = req.params.endDate === '-1' || req.params.endDate === undefined ? '' : req.params.endDate;

	if (isNaN(offset) || isNaN(limit)){
		offset = 0;
		limit = 35
	}

	let parameterForJS = {
		offset: offset,
		limit: limit,
		type: req.params.type,
		tag: req.params.tag,
		shortDescription: req.params.shortDescription,
		description: req.params.description,
		startDate: req.params.startDate,
		endDate: req.params.endDate,
	};

	let numberOfEntries = await logService.getNumberOfEntries(type, tag, shortDescription, description, unixStartDate, unixEndDate);

	// Wenn die Anzahl der Einträge durch das Filtern reduziert wird, muss ein neuer Offset definiert werden.
	if (numberOfEntries < offset) {
		offset = (numberOfEntries - limit) >= 0 ? (numberOfEntries - limit) : 0;
	}

	let model = await logService.getLogFilterData(offset, limit, type, tag, shortDescription, description, unixStartDate, unixEndDate);

	// Wie viel Einträge können die Buttons springen
	let offsetLeft = offset >= limit ? offset - limit : 0;
	let offsetRight = offset + limit;
	let offsetToBottom = numberOfEntries - limit;

	// Anzeige zwischen der Button zum Blättern
	let firstNumber = numberOfEntries > 0 ? (offset + 1) : 0;
	let secondNumber = (offset + limit) > numberOfEntries ? numberOfEntries : (offset + limit);
	let buttonMiddelText = firstNumber + ' bis ' + secondNumber + ' von ' + numberOfEntries;
	if (numberOfEntries === 0) {
		buttonMiddelText = 'keine Daten gefunden';
	}

	let isDate = 'false';
	if (!(unixStartDate === undefined || unixStartDate === '-1' || isNaN(parseInt(unixStartDate))
		|| unixEndDate === undefined || unixEndDate === '-1' || isNaN(parseInt(unixEndDate)))
	) {
		isDate = 'true';
	}

	const viewData = {
		offsetRight: offsetRight,
		offsetToBottom: offsetToBottom,
		buttonMiddelText: buttonMiddelText,
		offsetLeft: offsetLeft,
		offset: offset,
		limit: limit,
		type: type,
		tag: tag,
		shortDescription: shortDescription,
		description: description,
		unixStartDate: unixStartDate,
		unixEndDate: unixEndDate,
		isDate: isDate,
		parameterForJS: parameterForJS
	};

	res.status(200).render('sysLog', {
		datepickerView: true,
		data: model,
		test: isTest,
		viewData: viewData,
		title: "Karl-Log's",
		page: 'logs',
		sysLogs: true
	});
};

exports.artAction = async (req, res) => {
	res.status(200).render('product/product', {
		data: [],
		mpn: "",
		limit: 0,
		offset: 0,
		test: isTest,
		page: 'product',
		title: "Karl-Produkte"
	});
};

/**
 * Der übergebene Wert in "req.params['mpn']" wird einmal in der Karl DB und in der ITM DB gesucht.
 * In der Karl DB als MPN.
 * In der ITM DB als s_media.id oder als s_media.name
 *
 * */
exports.productSearchAction = async (req, res) => {
	if (req.params === undefined ||req.params === null){
		res.status(400).send();
		return;
	}
	let limit = req.params.limit === undefined ? 20: req.params.limit;
	let mpn = req.params['mpn'] === undefined ? '' : req.params['mpn'];
	let offset = req.params.offset === undefined ? 0 : req.params.offset;

	let useCaseItmMediaId = new UseCaseItmMediaId();
	let itmResult = await useCaseItmMediaId.getProductsFromMediaId(mpn);
	let productService = new ProductService();
	let products = {
		itm:{isData: false},
		karl:{isData: false}
	}

	if (itmResult.successful && itmResult.itm.successful && itmResult.itm.isData && itmResult.itm.data[0].mpn !== undefined) {
		products.itm = await productService.getProductList(itmResult.itm.data[0].mpn, limit,	offset);
		products.itm.isData = products.itm.length > 0;
	}
	products.karl = await productService.getProductList(mpn, limit,	offset);
	products.karl.isData = products.karl.length > 0;

	res.status(200).render('product/product', {
		data: products,
		mpn: mpn,
		limit: limit,
		offset: offset,
		test: isTest,
		productOverview: true,
		page: 'product',
		title: "Karl-Produkte",
	});
};

exports.productOverviewAction = async (req, res) => {
	if (req.params === undefined ||req.params === null){
		res.status(400).send();
		return;
	}

	let pnr = req.params['pnr'] === undefined? 0: req.params['pnr'];
	let productService = new ProductService();
	let product = await productService.getProductInformation(pnr);

	/**
	 * Wird eine p_nr übergeben, die es nicht gibt.
	 * Hier der Abbruch.
	 * */
	if (product.length === 0) {
		let titleDashboard = [];
		titleDashboard.push('Schade, das hat nicht geklappt.');
		titleDashboard.push(`Für die Produktnummer ${pnr} wurde kein Produkt gefunden.`);

		let viewData = {
			titleDashboard: titleDashboard,
		}

		res.status(200).render('index', {
			viewData: viewData,
			title: 'Karl-Produkte',
			page: 'product',
			test: isTest,
		});

		return
	}

	let useCaseGetInfoForProductAndArticle = new UseCaseGetInfoForProductAndArticle();
	let infoboxData = await useCaseGetInfoForProductAndArticle.getAllInfo(product);
	let useCaseChannel = new UseCaseChannel();
	let channelTypeValue = await useCaseChannel.getChannelTypeValue();
	res.status(200).render('product/product_overview', {
		infobox: true,
		data: product,
		channelTypeValue: channelTypeValue,
		test: isTest,
		infoboxData: infoboxData,
		productOverview: true,
		title: 'Karl-Produkte',
		page: 'product',
	});
};

exports.channelAction = async (req, res) => {

	let channels = await channelService.getAllChannel();

	res.status(200).render('channel', {
		test: isTest,
		data: channels,
		channelsView: true,
		page: 'channel',
		title: "Karl-Kanäle",
	});
};


exports.qualityAction = async (req, res) => {

	res.status(200).render('quality', {
		test: isTest,
	});
};

/** Hierüber können CronJobs über WEB gestartet werden */
exports.cronJobAction = async (req, res) => {
	if (req.params === undefined ||req.params === null){
		res.status(400).send();
		return;
	}

	let serviceTaskRun = new ServiceTaskRun();
	let result = await serviceTaskRun.taskRun(req.params.parameter);

	if (!result.successful) {
		res.status(400).send();
		return;
	}

	let titleDashboard = [];
	if (result.successful) {
		titleDashboard.push('Der CronJob: "' + req.params.parameter + '" ist erfolgreich durchgelaufen.');
		if ((result.taskResult.successful !== 'undefined') && !result.taskResult.successful) {
			titleDashboard.push('Im Verlauf des Jobs ist ein Fehler aufgetreten oder es konnten keine Ergebnisse erzielt werden.');
		}
		if (Array.isArray(result.taskResult.messages)) {
			for (let index in result.taskResult.messages) {
				titleDashboard.push(result.taskResult.messages[index]);
			}
		} else {
			titleDashboard.push('Ergebnis: ' + result.taskResult.messages);
		}
	} else {
		titleDashboard.push('Es ist ein Fehler aufgetreten.');
		titleDashboard.push(result.taskResult.messages);
	}

	let viewData = {
		title: "Karl",
		titleDashboard: titleDashboard,
	}

	res.status(200).render('cronJobsCall', {
		viewData: viewData,
	});
};

/**
 * Hierüber können die Tabellen separat synchronisiert werden
 *
 * Aufzurufen über: http://localhost:8080/sync/article
 * */
exports.syncAction = async (req, res) => {
	if (req.params === undefined ||req.params === null){
		res.status(400).send();
		return;
	}

	let table;
	let Table;
	switch (req.params.parameter) {
		case 'article':
			table = require('../../../repository/article/repo.article');
			break;
		case 'product':
			table = require('../../../repository/repo.product');
			break;
		case 'sys':
			Table = require('../../../repository/repo.sys');
			table = new Table();
			break;
		case 'product2productImages':
			Table = require('../../../repository/repo.product2productImages');
			table = new Table();
			break;
		case 'infobox':
			Table = require('../../../repository/repo.infobox');
			table = new Table();
			break;
		case 'images':
			table = require('../../../repository/repo.images');
			break;
		case 'channel':
			table = require('../../../repository/repo.channel');
			break;
		case 'imagesBlacklist':
			Table = require('../../../repository/repo.images.blacklist');
			table = new Table();
			break;
		case 'category':
			Table = require('../../../repository/repo.category');
			table = new Table();
			break;
		case 'cNetCategory2LynnAndShopCategory':
			Table = require('../../../repository/repo.cNetCategory2LynnAndShopCategory');
			table = new Table();
			break;
		case 'temp':
			Table = require('../../../repository/repo.temp');
			table = new Table();
			break;
		// case '':
		// 	table = await require('../../../tasks/task.karl.collect.all');
		// 	break;
		default:
			res.status(400).send();
			return;
	}

	await table.sync();

	let titleDashboard = [];
	titleDashboard.push('Die Tabelle: "' + req.params.parameter + '" wurde erfolgreich synchronisiert.');
	titleDashboard.push('Danke für den Auftrag.');

	let viewData = {
		title: "Karl",
		titleDashboard: titleDashboard,
	}

	res.status(200).render('cronJobsCall', {
		viewData: viewData,
	});
};

/** Anzeige welcher Job (Task) gerade läuft */
exports.jobStatusAction = async (req, res) => {
	if (req.params === undefined ||req.params === null){
		res.status(400).send();
		return;
	}

	let useCaseListAllTask = new UseCaseListAllTask();
	let processes = await useCaseListAllTask.getAllProcesses();

	let viewData = {
		processes: processes,
	}

	res.status(200).render('jobStatus', {
		viewData: viewData,
		page: 'taskActive',
		taskActive: true,
		test: isTest,
		title: "Karl-Job's",
	});
};

/**
 * Setzt für ALLE Bilder die Bildgröße in der DB.
 * Sollte nur zur initialen Setzung der Bildergrößen benutzt werden.
 * */
exports.imageSizeAction = async (req, res) => {

	let useCaseSetAllImageSize = new UseCaseSetAllImageSize();
	await useCaseSetAllImageSize.setAllImageSize();

	let titleDashboard = [];
	titleDashboard.push('Die Bildgröße wurde in allen Bildern eingetragen.');
	titleDashboard.push('Danke für den Auftrag.');

	let viewData = {
		title: "Karl",
		titleDashboard: titleDashboard,
	}

	res.status(200).render('index', {
		test: isTest,
		page: 'traverse',
		viewData: viewData
	});
};

/**
 * Setzt für alle Bilder mit Bildgröße den passenden Channel.
 * Sollte nur zur initialen Setzung der Bildergrößen benutzt werden.
 * */
exports.setImageChannelAction = async (req, res) => {

	let useCaseSetAllImageSize = new UseCaseSetAllImageSize();
	await useCaseSetAllImageSize.setImageChannel();

	let titleDashboard = [];
	titleDashboard.push('Die Channel wurde in allen Bildern eingetragen.');
	titleDashboard.push('Danke für den Auftrag.');

	let viewData = {
		title: "Karl",
		titleDashboard: titleDashboard,
	}

	res.status(200).render('index', {
		test: isTest,
		page: 'traverse',
		viewData: viewData
	});
};

/**
 * Hängt bei doppelten Produkten die Artikel unter einem Produkt um
 * */
exports.doubleProductAction = async (req, res) => {

	// let useCaseDoubleProducts = new UseCaseDoubleProducts();
	// await useCaseDoubleProducts.doubleProductsCleanUp();

	let titleDashboard = [];
	titleDashboard.push('Bei doppelten Produkten wurden die Artikel umgehängt.');
	titleDashboard.push('Danke für den Auftrag.');

	let viewData = {
		title: "Karl",
		titleDashboard: titleDashboard,
	}

	res.status(200).render('index', {
		test: isTest,
		page: 'traverse',
		viewData: viewData
	});
};
