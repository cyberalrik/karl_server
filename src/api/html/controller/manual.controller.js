'use strict';

/**
 * Aufrufen z.B. mit 'http://localhost:8080/call/readFile'
 * */
exports.indexAction = async (req, res) => {
	if (req.params === undefined || req.params === null) {
		res.status(400).send();
		return;
	}
	let viewData = {
		title: "Karl-Call"
	}

	if (req.params.parameter === undefined || req.params.parameter === null) {
		let messages = [];
		messages.push('Es wurde nichts gemacht.');
		messages.push('Du hast vergessen Aufrufparameter zu übergeben.');
		viewData.titleDashboard = messages;
		res.status(200).render('cronJobsCall', {
			viewData: viewData,
		});
		return
	}

	let ManualJob;
	let manualJob;
	let trueCall = false;
	let view = 'cronJobsCall';
	switch (req.params.parameter) {
		case 'readFile':
			console.log('readFile - wurden aufgerufen.');
			ManualJob = require('../../../manual/readFile');
			manualJob = new ManualJob();
			trueCall = true;

			break;
		// case 'picturesShow':
		// 	ManualJob = require('../../../manual/picturesShow');
		// 	manualJob = new ManualJob();
		// 	trueCall = true;
		//
		// 	break;
		case 'pictureLink':
			console.log('pictureLink - wurden aufgerufen.');
			ManualJob = require('../../../manual/pictureLink');
			manualJob = new ManualJob();
			trueCall = true;

			break;
		case 'itmDelete':
			console.log('itmDelete - wurden aufgerufen.');
			ManualJob = require('../../../manual/itmArticleDelete');
			manualJob = new ManualJob();
			trueCall = true;

			break;
		case 'articleCategories':
			console.log('articleCategories - wurden aufgerufen.');
			ManualJob = require('../../../manual/articleCategorieAllocation');
			manualJob = new ManualJob();
			trueCall = true;

			break;
		case 'selWriteTable':
			console.log('selWriteTable - wurden aufgerufen.');
			/** Ändert übergebene Inventurdaten in der SEL-Tabelle SERIE. */
			ManualJob = require('../../../manual/selectLineWriteTable');
			manualJob = new ManualJob();
			trueCall = true;

			break;
		case 'lynnWriteTable':
			console.log('lynnWriteTable - wurden aufgerufen.');
			/** Schreibt Daten in einer Temp-Tabelle in Lynn. */
			ManualJob = require('../../../manual/LynnWriteTable');
			manualJob = new ManualJob();
			trueCall = true;

			break;
	}
	let titleDashboard = [];
	if (trueCall) {
		titleDashboard = await manualJob.call();
	} else {
			let messages = [];
			messages.push('Es wurde nichts gemacht.');
			messages.push(`Die übergebenen Aufrufparameter "${req.params.parameter}" sind fehlerhaft.`);
			viewData.titleDashboard = messages;
			res.status(200).render(view, {
				viewData: viewData,
			});
			return
	}

	titleDashboard.push(`Die übergebenen Aufrufparameter waren: "${req.params.parameter}".`);

	viewData.titleDashboard = titleDashboard;

	res.status(200).render(view, {
		viewData: viewData,
	});
};

