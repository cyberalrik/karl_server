'use strict';

const logRepo = require('../../../repository/repo.log');

exports.getLog = async (_off, _limit) => {
	let off = parseInt(_off);
	let limit = parseInt(_limit);

	if (isNaN(off) || isNaN(limit))
		return [];

	if (off < 0 || limit < 1)
		return [];

	let result = await logRepo.getLog(off, limit);

	result = await this.formatDate(result);

	return result;
};

exports.formatDate = async (logs) => {
	// let i = 0;
	logs.forEach(log => {
		// i++;
		log.displayCreatedAt =
			log.createdAt.getDate().toString().padStart(2, '0')
			+ '.' +(log.createdAt.getMonth()+1).toString().padStart(2, '0')
			+ '.' +log.createdAt.getFullYear().toString().padStart(2, '0')
			+ ' ' +log.createdAt.getHours().toString().padStart(2, '0')
			+ ':' +log.createdAt.getMinutes().toString().padStart(2, '0')
			+ ':' +log.createdAt.getSeconds().toString().padStart(2, '0');
	})

	return logs;
};

exports.getFilterValue = async (name, type, tag, shortDescription, description, unixStartDate, unixEndDate) => {
	let result = logRepo.getGroupOfName(name, type, tag, shortDescription, description, unixStartDate, unixEndDate);

	return result;
};

exports.getLogFilterData = async (_offset, _limit, type, tag, shortDescription, description, unixStartDate,
								  unixEndDate) => {
	let offset = parseInt(_offset);
	let limit = parseInt(_limit);
	let result = await logRepo.getLogFilterData(offset, limit, type, tag, shortDescription, description, unixStartDate,
		unixEndDate)
	result = await this.formatDate(result);
	return result;
}

exports.getNumberOfEntries = async (type, tag, shortDescription, description, unixStartDate, unixEndDate) => {
	let result = logRepo.getNumberOfEntries(type, tag, shortDescription, description, unixStartDate, unixEndDate);

	return result;
};