'use strict';

const controller = require('./controller/log.controller');

module.exports = async function(app) {
	app.get('api/log/:off/:limit', controller.logAction);
};