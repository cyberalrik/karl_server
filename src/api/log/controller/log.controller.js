'use strict';

let logService = require('../service/log.service');

exports.logAction = async (req, res) => {
	if (req.params === undefined ||req.params === null){
		res.status(400).send();
		return;
	}

	let offset = req.params.off;
	let limit = req.params.limit;

	if (offset === null || offset === undefined || limit === undefined || limit === null){
		res.status(400).send();
	}
	let result;
	result = await logService.getLog(offset, limit);

	res.send(result);
};