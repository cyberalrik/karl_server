'use strict'

const logger = require('../../helper/util/logger.util');
const TAG = 'manual.lynnWriteTable';
const fs = require('fs');
const readline = require('readline');
const sql = require('../../helper/lynn.sql.database.helper');

let fileName = '/services/karl/src/manual/LynnWriteTable/Artikel mit Gewicht fuer SEL.csv';
// const fileName = 'C:\\workspace\\WebstormProjects\\karl\\src\\manual\\LynnWriteTable\\Artikel mit Gewicht fuer SEL.csv';
let usedP_NR = [];

/**
 * Hier sollen kleine Hilfsprogramme gesammelt werden.
 * Dieses hier liest eine CSV Datei ein und speichert die Daten in eine Tebelle.
 *
 * Aufrufen mit 'http://localhost:8080/call/lynnWriteTable'

 * @return {Promise<Array>}
 * */

class Index {
    async call() {
        let result = [];

        /** Datei einlesen */
        let readCsvFileResult = await this.readCsvFileSaveInDB()
        if (!(readCsvFileResult.message === undefined || readCsvFileResult.message === null)) {
            result = readCsvFileResult.message;
        }

        return result;
    }

    /**
     * Die Datei wird eingelesen und Zeilenweise überprüft und angelegt.
     *
     * @return Promise <{array}>
     * */
    async readCsvFileSaveInDB() {
        let result = {
            message: []
        };
        try {

            if (!fs.existsSync(fileName)) {
                let message = `Die Datei: "${fileName}" konnte nicht gefunden werden.`;
                result.message.push(message);
                console.log(message)

                return result;
            }

            let fileStream = fs.createReadStream(fileName);

            let rl = readline.createInterface({
                input: fileStream,
                crlfDelay: Infinity
            });


            let data = {};
            let lynnAzuP_Nr = {};
            let number = 0;
            console.log('Beginne mit der Verarbeitung.')
            for await (const line of rl) {
                if (lynnAzuP_Nr.length === undefined) {
                    lynnAzuP_Nr = await this.getDataFromLynn();
                }
                number++;
                let firstData = line.split(';');
// console.log(firstData[0]);
                data = [lynnAzuP_Nr['a_nr_' + firstData[1]], parseFloat(firstData[2])];
                /** Daten ersetzen */


                /** Daten in DB-Tabelle aufnehmen */
                // await this.selUpdate(data);
                await this.lynnInsert(data);

                if ((number) % 500 === 0) {
                    console.log(`Es wurden schon ${number} bearbeitet.`);
                    await logger.log(TAG, 'Datenübernahme',`Es wurden schon ${number} bearbeitet.`);
                }
            }

            console.log(`Insgesamt wurden ${number} Datensätze aktualisiert.`);
            await logger.log(TAG, 'Datenübernahme',`Insgesamt wurden ${number} Datensätze aktualisiert.`);
        } catch (e) {
            logger.err(TAG, 'Datenübernahmefehler', 'Bei der Übernahme von Daten ist ein Fehler aufgetreten', e.message);
        }
        return result;
    }

    /**
     * Schreibt die übergebenen Daten in die DB-Tabelle
     * */
    // async selUpdate(data) {
    //     if (data[0] !== undefined && data[1] !== undefined) {
    //         await sql.sql(`update dbo.SERIE set _INVWERT21 = ${data[1]} where SerieCharge = '${data[0]}'`);
    //         // await sql.sql(`// INSERT INTO erp.temps (SNr, Wert) VALUES ('${data[0]}', '${data[1]}')`);
    //     }
    // }

    /**
     * Schreibt die übergebenen Daten in die DB-Tabelle
     * */
    async lynnInsert(data) {
        if (data[0] !== undefined) {
            try {
                if (!usedP_NR.includes('p_nr_' + data[0])) {
                    let qry = `INSERT INTO dbo.temp_alinde (pId, gewicht) VALUES (${data[0]}, ${data[1]})`;
                    await sql.sql(qry);

                    usedP_NR.push('p_nr_' + data[0]);
                }
            } catch (e) {
                let test = e.message;
            }
        }
    }

    /**
     * Schreibt die übergebenen Daten in die DB-Tabelle
     * */
    async getDataFromLynn() {
        let result = {};
        try {
            console.log('Hole Lynn Daten');
            let qry = `select erpProductPropertyArticleForeignId as a_nr, erpProductId as p_nr from bde.AllProperties`;
            // await sql.sql('select  erpProductPropertyArticleForeignId as a_nr, erpProductId as p_nr from bde.AllProperties');
            result = await sql.sql(qry);
            console.log('Habe Lynn Daten');
            // return sqlResult.recordset;

        } catch (e) {
            let test = e.message;
        }

        return await this.manipulateData(result.recordset);
        // return result;
    }

    async manipulateData(items){
        console.log('Manipuliere Lynn Daten');
        let result = [];

        for (let item of items) {
            result['a_nr_'+  item.a_nr] = parseInt(item.p_nr);
        }
        console.log('Fertig mit Manipuliere der Lynn Daten');

        return result
    }

}

module.exports = Index
