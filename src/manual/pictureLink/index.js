'use strict'

// const logger = require('../../helper/util/logger.util');
// const TAG = 'manual.readFile';
const fs = require('fs');
// const readline = require('readline');
const mkdirp = require('mkdirp');
const path = require('path');
const axios = require("axios");

const targetDirectory = 'D:\\Daten\\karl\\FotoLinks\\Ergebnis';

const filePath = 'D:\\Daten\\karl\\FotoLinks\\Files';
const subDirectory = 'desktop';

/**
 * Hier sollen kleine Hilfsprogramme gesammelt werden.
 * Dieses hier liest eine CSV Datei ein und speichert die Daten in eine Tebelle.
 *
 * Aufrufen mit 'http://localhost:8080/call/pictureLink'

 * @return {Promise<Array>}
 * */

class Index {
    async call() {
        // let fileNameArray = ['0.csv'];
        let fileNameArray = ['1.csv','2.csv','3.csv','4.csv','5.csv','6.csv','7.csv','8.csv','9.csv','10.csv','11.csv',
            '12.csv','13.csv','14.csv','15.csv','16.csv','17.csv','18.csv','19.csv','20.csv','21.csv','22.csv'];

        for (let index in fileNameArray) {
            let fullName = path.join(filePath, fileNameArray[index]);
            await this.processData(fullName);

            // 1 sec warten
            await new Promise(resolve => setTimeout(resolve, 1000));
        }

        return [];
    }

    async processData(fileName) {
        let result = [];

        console.log(fileName);

        /** Datei einlesen */
        let readCsvFileResult = await this.readCsvFile(fileName)

        if (!(readCsvFileResult.message === undefined || readCsvFileResult.message === null)) {
            result = readCsvFileResult.message;
        }

        let fileData = readCsvFileResult.data;
        console.log(`Es wurden ${fileData.length} Zeilen eingelesen.`);

        let pictureNumber = 0;
        let newPath = '';
        let resultSetPath = {};

        console.log('Bilder werden jetzt herunter geladen.');
        for (let index in fileData) {
            let data = fileData[index];

            /** Verzeichnis codieren */
            data.name = await this.setCode(data.name);

            /** legt nur ein neues Verzeichnis an */
            if (newPath !== path.join(data.name, subDirectory)) {
                newPath = path.join(data.name, subDirectory);
                if (!((resultSetPath = await this.setPath(newPath)).successful)) {
                    console.log(`\nERROR: Beim Anlegen der Verzeichnisses ${resultSetPath.newPath} ist ein Fehler aufgetreten.\nDatei: ${data.fileName}, Zeile: ${data.row}\n`);

                    continue;
                }
            }

            // console.log(`Bild geladen von: ${fileName} aus Zeile: ${index} mit link: ${data.url}`)

            /** Bild in Verzeichnis laden */
            await this.loadPicture(resultSetPath.newPath, data);
            pictureNumber++;

            if (pictureNumber % 100 === 0) {
                console.log(`Es wurden schon ${pictureNumber} Bilder herunter geladen.`);
                // 1 sec warten
                await new Promise(resolve => setTimeout(resolve, 1000));
            }

        }

        result.message = `Fertig!!! - Es wurden ${pictureNumber} Bilder herunter geladen.\n`;
        console.log(result.message);

        return result;
    }

    /**
     * Die Datei wird eingelesen und Zeilenweise überprüft und verarbeitet.
     *
     * @return Promise <{}>
     * */
    async readCsvFile(fileName) {
        let result = {
            message: [],
            successful: false
        };
        if (!fs.existsSync(fileName)) {
            let message = `Die Datei: "${fileName}" konnte nicht gefunden werden.`;
            result.message.push(message);
            console.log(message)

            return result;
        }
        let fileData = fs.readFileSync(fileName, 'utf8').split('\r\n');

        let data = [];
        for (let index in fileData) {
            if (fileData[index].length === 0 || index === '0') {
                continue;
            }

            let row = fileData[index].split(';');
            let currentData = {
                url: row[0],
                name: row[1],
                row: index,
                fileName: fileName
            }

            if (currentData.name !== undefined) {
                data.push(currentData);
            }
        }
        result.data = data;

        return result;
    }

    /** Die PartNumber codieren */
    async setCode(PartNumber) {
        return  PartNumber
            .replace("␣", "%20")
            .replace("!", "%21")
            .replace("\"", "%22")
            .replace("#", "%23")
            .replace("$", "%24")
            //.replace("%", "%25")
            //.replace("&", "%26")
            .replace("'", "%27")
            .replace("(", "%28")
            .replace(")", "%29")
            .replace("*", "%2A")
            .replace("+", "%2B")
            .replace(",", "%2C")
            // .replace("-", "%2D")
            .replace(".", "%2E")
            .replace("/", "%2F")
            .replace(":", "%3A")
            //.replace(";", "%3B")
            .replace("<", "%3C")
            .replace("=", "%3D")
            .replace(">", "%3E", )
            .replace("?", "%3F")
            //.replace("@", "%40")
            .replace("[", "%5B")
            .replace("\\", "%5C")
            .replace("]", "%5D")
            .replace("{", "%7B")
            .replace("|", "%7C")
            .replace("}", "%7D");
    }

    async setPath(directory) {
        // let newPath = path.join(targetDirectory,directory);
        let result = {
            newPath: path.join(targetDirectory,directory),
            successful: true
        };

        try {
            await mkdirp(result.newPath);
        } catch (e) {

            result.successful = false;
            result.message = e.message;

            return result
        }

        return result;
    }

    async loadPicture(targetPath, data) {
//        let download_image = async (url, image_path) =>
        let url = data.url;
        let image_path = await this.createFileName(targetPath, data);
        try {
            await axios({
                url,
                responseType: 'stream',
            }).then(
                response =>
                    new Promise((resolve, reject) => {
                        response.data
                            .pipe(fs.createWriteStream(image_path))
                            .on('finish', () => resolve())
                            .on('error', e => reject(e));
                    }),
            )
        } catch (e) {
            console.log(`\nERROR: In der Datei ${data.fileName} in der Zeile: ${data.row} ist ein Fehler aufgetreten.\n${e.message}\nDie URL ist: ${data.url}\n`)
        }
    }

    async createFileName(targetPath, data) {
        let pictureNumber = 0;
        let image_path;
        let pictureNumberString;
        while (true) {
            pictureNumberString = pictureNumber.toString().padStart(2, '0');
            image_path = path.join(targetPath, data.name + '_' + pictureNumberString + '.jpg');

            if (!fs.existsSync(image_path)) {
                break;
            }
            pictureNumber++;
        }

        return image_path;
    }
}

module.exports = Index
