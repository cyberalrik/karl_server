'use strict'

const logger = require('../../helper/util/logger.util');
const TAG = 'manual.readFile';
const fs = require('fs');
const readline = require('readline');
const mariaDb = require("mariadb");
const RepoCategory = require('../../repository/repo.category');
const cciConfig = require("../../configs/cci.config");
const Sequelize = require("sequelize");
const config = require("../../configs/app.config").getConfig();

// const fileName = 'D:\\Daten\\karl\\vorhandene CNet Kategorien in Tradebyte Stand 27_01_2022.csv';
// const fileName = 'D:\\Daten\\karl\\4099764000001.csv';
const fileName = 'C:\\Daten\\karl\\ArtikelLoeschen\\Artikelliste_inaktiv auf Tradebyte_nur a_nr.csv';
let conn = null;

/**
 * Hier sollen kleine Hilfsprogramme gesammelt werden.
 * Dieses hier liest eine CSV Datei ein und speichert die Daten in eine Tebelle.
 *
 * Aufrufen mit 'http://localhost:8080/call/readFile'

 * @return {Promise<Array>}
 * */

class Index {
    async call() {
        let result = [];

        /** Datei einlesen */
        // let readCsvFileResult = await this.readCsvFileSaveInDB()
        let readCsvFileResult = await this.readCsvFile()
        if (!(readCsvFileResult.message === undefined || readCsvFileResult.message === null)) {
            result = readCsvFileResult.message;
        }

        return result;
    }

    /**
     * Die Datei wird eingelesen und Zeilenweise überprüft und angelegt.
     *
     * @return Promise <{array}>
     * */
    async readCsvFile() {
        let result = {
            message: []
        };
        if (!fs.existsSync(fileName)) {
            let message = `Die Datei: "${fileName}" konnte nicht gefunden werden.`;
            result.message.push(message);
            console.log(message)

            return result;
        }

        let fileStream = fs.createReadStream(fileName);

        let rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity
        });

        let data = {};
        let available = 0;
        let newlyCreated = 0;

        for await (const line of rl) {
            data = line.split(';');
            if (conn === null) {
                // conn = await this.getCciDbConnection(); -- Ist für EAN neu einlesen
                conn = await this.setKarlSeq();
            }

            /** Testen, ob Daten in DB-Tabelle schon enthalten sind */
            if (await this.isDataInDb(data)) {
                available++;
            } else {
                /** Daten in DB-Tabelle aufnehmen */
                await  this.writeInDb(data);
                newlyCreated++;
            }
        }

        await this.closeDbConnection();

        result.message.push(`Es wurden ${newlyCreated} neue Datensätze angelegt.`)
        result.message.push(`Schon vorhanden waren ${available} Datensätze, sie wurden nicht erneut angelegt.`)

        return result;
    }

    /**
     * Die Datei wird eingelesen und Zeilenweise überprüft und angelegt.
     *
     * @return Promise <{array}>
     * */
    async readCsvFileSaveInDB() {
        let result = {
            message: []
        };

        try {

            if (!fs.existsSync(fileName)) {
                let message = `Die Datei: "${fileName}" konnte nicht gefunden werden.`;
                result.message.push(message);
                console.log(message)

                return result;
            }

            let fileStream = fs.createReadStream(fileName);

            let rl = readline.createInterface({
                input: fileStream,
                crlfDelay: Infinity
            });

            let data = {};
            let number = 0;

            let repoCategory = new RepoCategory();
            for await (const line of rl) {
                data = line.split(';');
                if (data.length > 0) {
                    await repoCategory.setCategory(data[0]);
                }

                number++;
            }

            result.message.push(`Es wurden ${number} Datensätze angelegt.`)
        } catch (e) {
            logger.err(TAG, 'Datenübernahmefehler', 'Bei der Übernahme von Daten ist ein Fehler aufgetreten', e.message);
        }
        return result;
    }

    /**
     * Guckt nach, ob der Eintrag in der Tabelle schon vorhanden ist.
     *
     * @return true or false
     * */
    async isDataInDb(data) {
        // let isEanAvailable = await cciSQL.sql("select * from cyberstats.cyber_ean where article = '4063403000004'");
        // let isEanAvailable = await conn.query(`SELECT * FROM cyberstats.cyber_ean WHERE ean = '${data[0]}'`);
        let isEanAvailable = await conn.query(`SELECT * FROM artikelLoeschen WHERE a_nr = '${data[0]}'`);

        return isEanAvailable[0].length > 0;
    }

    /**
     * Schreibt die übergebenen Daten in die DB-Tabelle
     * */
    async writeInDb(data) {
        // await conn.query(`INSERT INTO cyberstats.cyber_ean (ean) VALUES ('${data[0]}')`);
        await conn.query(`INSERT INTO artikelLoeschen (a_nr) VALUES ('${data[0]}')`);
    }

    /**
     * Datenbankverbindung herstellen
     * */
    async getCciDbConnection() {
        const pool = mariaDb.createPool({
            host: cciConfig.DB_HOST,
            user:cciConfig.USER,
            password: cciConfig.PW,
            database: cciConfig.DB,
            connectionLimit: 5
        });
        return await pool.getConnection();
    }

    async setKarlSeq() {
        if (conn === null) {
            conn = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
        }

        return conn;
    }


    /**
     * Datenbankverbindung schließen
     * */
    async closeDbConnection() {
        if (conn !== null) {
            try {
                conn.end();
            } catch (e) {
                e.message = 'CCI braucht es und Karl nicht.';
            }
        }
    }
}

module.exports = Index
