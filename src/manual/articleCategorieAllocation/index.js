'use strict'

const logger = require('../../helper/util/logger.util');
const TAG = 'manual.readFile';
const fs = require('fs');
const readline = require('readline');
const sql = require('../../helper/lynn.sql.database.helper');
// const fileName = 'C:\\Daten\\karl\\Daten einlesen\\Upload neuer InaktivGrund mit ID.csv';
const fileName = '/services/Upload neuer InaktivGrund mit ID.csv';

/**
 * Hier sollen kleine Hilfsprogramme gesammelt werden.
 * Dieses hier liest eine CSV Datei ein und speichert die Daten in eine Tebelle.
 *
 * Aufrufen mit 'http://localhost:8080/call/articleCategories'

 * @return {Promise<Array>}
 * */

class Index {
    async call() {
        let result = [];

        /** Datei einlesen */
        let readCsvFileResult = await this.readCsvFileSaveInDB()
        if (!(readCsvFileResult.message === undefined || readCsvFileResult.message === null)) {
            result = readCsvFileResult.message;
        }

        return result;
    }

    /**
     * Die Datei wird eingelesen und Zeilenweise überprüft und angelegt.
     *
     * @return Promise <{array}>
     * */
    async readCsvFileSaveInDB() {
        let result = {
            message: []
        };
        let available = 0;
        let newlyCreated = 0;
        let message = '';
        let number = 0;
        try {

            if (!fs.existsSync(fileName)) {
                message = `Die Datei: "${fileName}" konnte nicht gefunden werden.`;
                logger.err(TAG, 'Datei nicht gefunden', message);
                result.message.push(message);
                console.log(message)

                return result;
            }

            let fileStream = fs.createReadStream(fileName);

            let rl = readline.createInterface({
                input: fileStream,
                crlfDelay: Infinity
            });

            let data = {};

            message = 'Beginne mit der Verarbeitung.';
            logger.log(TAG, 'Verarbeitet Datenzeilen', message);
            result.message.push(message);
            console.log(message);

            for await (const line of rl) {
                number++;
                data = line.split(';');

                /** Testen, ob Daten in DB-Tabelle schon enthalten sind */
                if (await this.isDataInDb(data)) {
                    available++;
                } else {
                    /** Daten in DB-Tabelle aufnehmen */
                    await  this.writeInDb(data);
                    newlyCreated++;
                }

                if ((number) % 500 === 0) {
                    message = `Es wurden schon ${number} bearbeitet. ${available} übersprungen und ${newlyCreated} neue Datensätze angelegt.`;
                    logger.log(TAG, 'Verarbeitet Datenzeilen', message);
                    result.message.push(message);
                    console.log(message);
                }
            }
            message = `Es wurden ${newlyCreated} neue Datensätze angelegt.`;
            await logger.log(TAG, 'Datenübernahme',message);
            result.message.push(message);
            console.log(message);

            message = `Schon vorhanden waren ${available} Datensätze, sie wurden nicht erneut angelegt.`
            await logger.log(TAG, 'Datenübernahme', message)
            result.message.push(message);
            console.log(message);
        } catch (e) {
            message = 'Bei der Übernahme von Daten ist ein Fehler aufgetreten';
            result.message.push(message);
            logger.err(TAG, 'Datenübernahmefehler', message, e.message);
            console.log(message);
        }

        message = `Es wurden insgesamt ${number} bearbeitet. ${available} übersprungen und ${newlyCreated} neue Datensätze angelegt.`
        logger.log(TAG, 'Verarbeitet beendet', message);
        result.message.push(message);
        console.log(message);

        return result;
    }

    /**
     * Guckt nach, ob der Eintrag in der Tabelle schon vorhanden ist.
     *
     * @return true or false
     * */
    async isDataInDb(data) {

        let isEanAvailable = await sql.sql(`SELECT * FROM dabo.ct_temps WHERE a_nr = '${data[0]}'`);

        return !(isEanAvailable === undefined || isEanAvailable === null) && (isEanAvailable.recordset.length > 0);
    }

    /**
     * Schreibt die übergebenen Daten in die DB-Tabelle
     * */
    async writeInDb(data) {
        await sql.sql(`INSERT INTO dabo.ct_temps (a_nr, IdInaktivGrund) VALUES ('${data[0]}', '${data[1]}')`);
    }
}

module.exports = Index
