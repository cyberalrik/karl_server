'use strict'

const fs = require('fs');
const articleChannelRepo = require('../../repository/article/repo.article.channel');

const sourceFilePath = 'D:\\Daten\\ITM\\tbProductArticleNumber.json';
// const targetFilePath = 'D:\\Daten\\ITM\\deleteItm.json';
const targetFilePath = 'D:\\Daten\\ITM\\deleteItm.xml';
const TempRepo =  require('../../repository/repo.temp');

/**
 * Hier sollen kleine Hilfsprogramme gesammelt werden.
 * Dieses hier liest eine JSON Datei (1) ein und erstellt eine neue XML Datei (2) die in to2s eingelesen werden kann
 * und zum Löschen von Produkten dient..
 *
 * (1) - Die JSON-Datei wird von to2s befüllt und befindet sich im Ordner
 * /home/itmljtfo/www.it-market.com/custom/to2s/workingFolder/parameterFolder
 * auf dem Host von Profihost. Der Dateiname ist: tbProductArticleNumber.json
 *
 * (2) - Die Datei, die erzeugt wird, muss in einer, am besten kleinen, XML Datei von TB eingetragen werden.
 * Sie muss in den <Produkt> Teil kopiert werden.
 *
 * Aufrufen mit: 'http://localhost:8080/call/itmDelete'

 * @return {Promise<Array>}
 * */

class Index {
    async call() {
        if (1===1) {
            /** lese Json ein */
            let a_nrs = require("D:\\Daten\\ITM\\loeschenInKarl.json");
            /** Fülle Temp-Tabelle */
            let tempRepo = new TempRepo();
            for (let a_nr in a_nrs) {
                await tempRepo.setTemp(a_nr);
            }

            return ;
        }

        if (1===5) {
            console.log('Json-Datei einlesen');
            let jsonData = await this.readJsonFile(sourceFilePath);

            console.log('Gelöschte Artikel aus JSON-Datei holen');
            let deleteArticleJson = '';
            try {

                deleteArticleJson = require('D:\\Daten\\ITM\\deleteItm.json');
            } catch (e) {
                let test = e;
            }
            // let deleteArticle = await this.prepareDeleteData(deleteArticleJson);


            console.log('zu löschende Daten aufbereiten');
            let prepareDeleteArticle = await this.prepareDeleteData(deleteArticleJson);

            console.log('Erstelle <p_nr> Elemente');
            let mainPart = await this.buildForXml(prepareDeleteArticle, jsonData);

            console.log('Erstelle XML in string');
            let message = await this.buildXml(mainPart);

            console.log(message);
        }

        if (1===2) {
            console.log('Json-Datei einlesen');
            let jsonData = await this.readJsonFile(sourceFilePath);

            console.log('Gelöschte Artikel von Karl holen');
            let deleteArticle = await this.getDeleteArticle();

            console.log('Gelöschte Daten aufbereiten');
            let prepareDeleteArticle = await this.prepareDeleteArticleData(deleteArticle);

            console.log('Erstelle <p_nr> Elemente');
            let mainPart = await this.buildForXml(prepareDeleteArticle, jsonData);

            console.log('Erstelle XML in string');
            let message = await this.buildXml(mainPart);

            console.log(message);
        }

        if (1===5) {
            let tbIds = {};
            try {
                tbIds = require('D:\\Daten\\karl\\ITM\\neu.json');
                // let tbIds = require('D:\\Daten\\karl\\ITM\\noch_zu_loeschende_Artikel.json');
            } catch (e) {
                let et = e;
            }

            await this.buildXmlNeu(tbIds);
        }


        return [];
    }

    async prepareDeleteData(deleteArticleJson) {
        let result = [];
        for (let index in deleteArticleJson) {
            result['a_nr-' + index] = true;
        }

        return result;
    }

    async buildXmlNeu(mainPart) {
        let result = 'fertig';
        try {
            let writeStream = fs.createWriteStream(targetFilePath);
            for (let index in mainPart) {
                writeStream.write('<PRODUCT mode="delete">\n');
                writeStream.write('<P_NR>' + index + '</P_NR>\n');
                writeStream.write('</PRODUCT>\n');
            }
            writeStream.end();
        } catch (e) {
            console.log(e.message);
            result = 'Es ist ein Fehler aufgetreten';
        }

        return result;
    }

    async buildXml(mainPart) {
        let result = 'fertig';
        try {
            let writeStream = fs.createWriteStream(targetFilePath);
            for (let index in mainPart) {
                writeStream.write('<PRODUCT mode="delete">\n');
                writeStream.write(mainPart[index] + '\n');
                writeStream.write('</PRODUCT>\n');
            }
            writeStream.end();
        } catch (e) {
            console.log(e.message);
            result = 'Es ist ein Fehler aufgetreten';
        }

        return result;
    }

    async buildForXml(prepareDeleteArticle, jsonData) {
        let result1 = [];
        let result2 = [];
        for (let index in prepareDeleteArticle) {
            if (jsonData[index] !== undefined) {
                result1['p_nr-' + jsonData[index][0]] = jsonData[index][0];
            }
        }

        for (let index in result1) {
            result2.push('<P_NR>' + result1[index] + '</P_NR>');
        }

        console.log(`zu löschende P_NR: ${result2.length}`);

        return result2;
    }

    async prepareDeleteArticleData(deleteArticle) {
        let result = [];
        for (let index in deleteArticle) {
            result['a_nr-' + deleteArticle[index].getDataValue('a_nr')] = true;
        }

        return result;
    }

    async getDeleteArticle() {
        let result = await articleChannelRepo.getAllDeactivateANrFromChannelKeyAndActivity('cucybertr1', false); //, '2022-06-02');

        return result;
    }


    async readJsonFile(fileName) {
        let json = require(fileName);
        let data = [];
        let number = 0;

        for (let index in json) {
            let current = json[index];
            for (let a_nr in current) {
                data['a_nr-' + a_nr] = []
                data['a_nr-' + a_nr].push(index);
                number++;
            }
        }
        console.log(`In Json vorhandene Daten = ${number}`);

        return data;
    }
}

module.exports = Index
