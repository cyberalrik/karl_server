'use strict'

const logger = require('../../helper/util/logger.util');
const TAG = 'manual.selectLineWriteTable';
const fs = require('fs');
const readline = require('readline');
const sql = require('../../helper/selectLine.sql.database.helper');

let fileName = '/services/karl/src/manual/selectLineWriteTable/Inventur2021.csv';
// const fileName = 'C:\\workspace\\WebstormProjects\\karl\\src\\manual\\selectLineWriteTable/Inventur2021.csv';

/**
 * Hier sollen kleine Hilfsprogramme gesammelt werden.
 * Dieses hier liest eine CSV Datei ein und speichert die Daten in eine Tebelle.
 *
 * Aufrufen mit 'http://localhost:8080/call/selWriteTable'

 * @return {Promise<Array>}
 * */

class Index {
    async call() {
        let result = [];

        /** Datei einlesen */
        let readCsvFileResult = await this.readCsvFileSaveInDB()
        if (!(readCsvFileResult.message === undefined || readCsvFileResult.message === null)) {
            result = readCsvFileResult.message;
        }

        return result;
    }

    /**
     * Die Datei wird eingelesen und Zeilenweise überprüft und angelegt.
     *
     * @return Promise <{array}>
     * */
    async readCsvFileSaveInDB() {
        let result = {
            message: []
        };
        try {

            if (!fs.existsSync(fileName)) {
                let message = `Die Datei: "${fileName}" konnte nicht gefunden werden.`;
                result.message.push(message);
                console.log(message)

                return result;
            }

            let fileStream = fs.createReadStream(fileName);

            let rl = readline.createInterface({
                input: fileStream,
                crlfDelay: Infinity
            });

            let data = {};
            let number = 0;
            console.log('Beginne mit der Verarbeitung.')
            for await (const line of rl) {
                number++;
                data = line.split(';');

                /** Daten in DB-Tabelle aufnehmen */
                // await this.selUpdate(data);
                await this.selInsert(data);

                if ((number) % 500 === 0) {
                    console.log(`Es wurden schon ${number} bearbeitet.`);
                    await logger.log(TAG, 'Datenübernahme',`Es wurden schon ${number} bearbeitet.`);
                }
            }

            console.log(`Insgesamt wurden ${number} Datensätze aktualisiert.`);
            await logger.log(TAG, 'Datenübernahme',`Insgesamt wurden ${number} Datensätze aktualisiert.`);
        } catch (e) {
            logger.err(TAG, 'Datenübernahmefehler', 'Bei der Übernahme von Daten ist ein Fehler aufgetreten', e.message);
        }
        return result;
    }

    /**
     * Schreibt die übergebenen Daten in die DB-Tabelle
     * */
    async selUpdate(data) {
        if (data[0] !== undefined && data[1] !== undefined) {
            await sql.sql(`update dbo.SERIE set _INVWERT21 = ${data[1]} where SerieCharge = '${data[0]}'`);
            // await sql.sql(`// INSERT INTO erp.temps (SNr, Wert) VALUES ('${data[0]}', '${data[1]}')`);
        }
    }

    /**
     * Schreibt die übergebenen Daten in die DB-Tabelle
     * */
    async selInsert(data) {
        if (data[0] !== undefined && data[1] !== undefined) {
            await sql.sql(`INSERT INTO dbo.CT_INVENTUR_TEMP (SNr, Wert) VALUES ('${data[0]}', '${data[1]}')`);
        }
    }
}

module.exports = Index
