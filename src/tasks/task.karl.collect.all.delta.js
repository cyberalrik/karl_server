'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const ServiceDateFormat = require('../services/service.date.format');
const UseCaseKarlCollectAll = require('../refac/use-cases/forTasks/karlCollectAll');
const LockJob = 'task.karl.collect.all';
const TAG = 'task.karl.collect.all.delta';

exports.cronTask = async () => {
	let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, false, 1)
	let result = await serviceJobMonitoring.start();
	result.messages = []
	if (result.isActive) {
		return result;
	}

	let serviceDateFormat = new ServiceDateFormat();
	let newDate = await serviceDateFormat.getStringDate();

	let useCaseKarlCollectAll = new UseCaseKarlCollectAll(TAG);
	let resultCall = await useCaseKarlCollectAll.call(null, newDate);

	result.messages.push(resultCall.message);
	result.successful = resultCall.successful;

	await serviceJobMonitoring.stop(true);

	return result;
};

