'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseKarlCollectAllE2 = require('../refac/use-cases/forTasks/karlCollectAllE2');
const UseCaseConvertAndCallToTB = require('../refac/use-cases/tbone/convertAndCall');
const TAG = 'task.karl.collect.all.e2';

class Index {
	async cronTask() {
		let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
		let result = await serviceJobMonitoring.start();
		if (result.isActive) {
			return result;
		}

		// let result = {}
		let useCaseKarlCollectAllE2 = new UseCaseKarlCollectAllE2(TAG);
		let callResult = await useCaseKarlCollectAllE2.call();

		/** Wurden Artikeldaten geändert, werden diese Artikel an TB geschickt. */
		if (
			callResult.articlesChange !== undefined
			&& callResult.articlesChange.length > 0
		) {
			console.log(`Folgende E2-Artikel wurden geändert: ${callResult.articlesChange.join(',')}`);
			let useCaseConvertAndCallToTB = new UseCaseConvertAndCallToTB()
			await useCaseConvertAndCallToTB.prepareAndPassOn(TAG, callResult.articlesChange);
		}
		result.successful = true;
		result.messages = callResult.messages;

		await serviceJobMonitoring.stop(true);

		return result;
	}
}

module.exports = Index;
