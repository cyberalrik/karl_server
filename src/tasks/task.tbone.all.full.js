'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseTbOneAll = require('../refac/use-cases/forTasks/tboneAll');
const LockJob = 'task.tbone.all';
const TAG = 'task.tbone.all.full';

/***
 *
 * @returns {Promise<{}>}
 */
exports.cronTask = async () => {
	let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, true, 10)
	let result = await serviceJobMonitoring.start();
	if (result.isActive) {
		return result;
	}

	let useCaseTbOneAll = new UseCaseTbOneAll();
	result = await useCaseTbOneAll.call(TAG);

	await serviceJobMonitoring.stop(false);

	result.successful = true;

	return result;
}
