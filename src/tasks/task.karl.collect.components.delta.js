'use strict';

const UseCaseAddCciComponents = require('../refac/use-cases/forTasks/karlCollectComponents');
const ServiceJobMonitoring = require('../services/service.job.monitoring');

const LockJob = 'task.karl.collect.components';
const TAG = 'task.karl.collect.components.delta';

exports.cronTask = async () => {
    /** full oder delta sollte nie gleichzeitig laufen, deshalb nur "task.karl.collect.components" angeben. */
    let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, false, 4)
    let result = await serviceJobMonitoring.start();
    if (result.isActive) {
        return result;
    }

    let useCaseAddCciComponents = new UseCaseAddCciComponents();
    result = await useCaseAddCciComponents.addComponentsToArticle(TAG,false);

    await serviceJobMonitoring.stop(true);

    return result;
}
