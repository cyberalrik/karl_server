'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseKarlCleanUpLogs = require('../refac/use-cases/forTasks/karlCleanUpLogs');
const TAG = 'task.karl.cleanup.logs';
/** Aufrufen über: http://localhost:8080/cronJob/task.karl.cleanup.logs */
class Index {
	async cronTask() {
		let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
		let result = await serviceJobMonitoring.start();
		if (result.isActive) {
			return result;
		}

		let useCaseKarlCleanUpLogs = new UseCaseKarlCleanUpLogs(TAG);
		let callResult = await useCaseKarlCleanUpLogs.call();
		result.messages = callResult.messages;

		await serviceJobMonitoring.stop(true);

		return result;
	}
}

module.exports = Index;
