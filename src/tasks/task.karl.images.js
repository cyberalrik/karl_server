'use strict'

const productRepo = require('../repository/repo.product');
const imageRepo = require('../repository/repo.images');
const ImagesBlacklistRepo = require('../repository/repo.images.blacklist');
const UseCaseGetImageSize = require('../refac/use-cases/image/get-image-size');
const UseCaseDetermineImageChannel = require('../refac/use-cases/image/determine-image-channel');
const cciSQL = require('../helper/maria.cci.database.helper');
const query = require('../helper/sql/query.collect');
const logger = require('../helper/util/logger.util');
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const constants = require('../constants/images.constant');
const TAG = 'task.karl.images';

// const _ = require('underscore');

/**
 * Hiermit werden Bilder aus der cNet-DB gezogen und, wenn es keine Canto Bilder für das entsprechende Produkt gibt, neu angelegt.
 * Dazu wird der Link überprüft und das Bild geladen, vermessen und für Kanäle kategorisiert und abgespeichert.
 * Das Bild wird in der Image-Tabelle anhand der p_nr und image_pos identifiziert.
 * */
exports.cronTask = async () => {
	let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 7)
	let result = await serviceJobMonitoring.start();
	if (result.isActive) {
		return result;
	}

	let useCaseGetImageSize = new UseCaseGetImageSize();
	let useCaseDetermineImageChannel = new UseCaseDetermineImageChannel();

	logger.log(TAG, 'Produkte laden','Produkte werden aus der Karl-DB geholt.');
	//let pre = await articleRepo.getAllArticle();
	let products = await productRepo.getAllProductAfterTime();
	logger.log(TAG, 'Produkte geladen',`Es wurden ${products.length} Produkte gefunden.`);
	let workingList = [];
	for (let  i = 0; i < products.length; i++){
		let item = {
			a_prod_nr: products[i].p_name,
			p_nr: products[i].p_nr
		};
		workingList.push(item);
	}
	products = null;
	// workingList = await _.uniq(workingList, false, 'p_nr');
	let importList = [];
	let countProductImagesNumber = 0;
	let countNotProductImagesNumber = 0;
	let countCompanyLogos = 0;
	let countCtImages = 0;
	let cNetPicturesNumber = 0;
	let productNumber = workingList.length;
	let imagesBlacklistRepo = new ImagesBlacklistRepo();
	let imagesBlacklist = await imagesBlacklistRepo.getAllPrepared();

	for (let i = 0; i < productNumber; i++){
		/** Gibt es für ein Produkt ein Canto Bild, sollen keine cNet Bilder mehr hinzugefügt werden. */
		if (await imageRepo.cantoImageIsAvailable(workingList[i]['p_nr'])) {
			countCtImages++;
			continue;
		}

		importList = await cciSQL.sql(query.getImagesPerList(workingList[i]));
		if (importList === undefined){
			logger.err(TAG, 'CCI Database');
			return;
		}

		if (importList.length > 0) {
			countProductImagesNumber++;
		} else {
			countNotProductImagesNumber++;
			/** Für dieses Produkt gibt es kein cNet Bild, also weiter... */
			continue;
		}

		for (let j = 0; j < importList.length; j++){
			let currentImage = importList[j]['url'];

			/** Ist die URL geblacklistet? Dann nicht weiter bearbeiten.*/
			if (imagesBlacklist[currentImage]) {
				countCompanyLogos++;
				continue;
			}

			cNetPicturesNumber++;
			let imageSize = await useCaseGetImageSize.getImageSize(currentImage);
			if (imageSize.successful) {
				let channel = await useCaseDetermineImageChannel.determineChannel(imageSize);
				// p_nr, image_url, image_pos, is_active, version, channel = null, size = {height: 0, width: 0})
				await imageRepo.addOrUpdateImage(
					workingList[i]['p_nr'],
					currentImage,
					j,
					true,
					constants.IMAGES_CNET_TYPE_VALUE,
					channel,
					imageSize
				);
			}
		}
		//importList = [];
		if (i % 200 === 0 && i !== 0) {
			logger.log(TAG,`${i} Produkte verarbeitet`,
				`Es wurden schon ${cNetPicturesNumber} cNet Bilder für ${countProductImagesNumber} Produkte verarbeitet. (${productNumber})`);
		}
	}
	logger.log(TAG,`Ende Produktverarbeitung`,
		`Es wurden ${cNetPicturesNumber} cNet Bilder für ${countProductImagesNumber} Produkte verarbeitet. (${productNumber})`);

	logger.log(TAG,`Keine cNet Bilder`,
		`Für ${countNotProductImagesNumber} Produkte gab es keine cNet Bilder.`);

	logger.log(TAG,`cNet Bilder abgewiesen`,
		`Für ${countCtImages} Produkte gibt es CT-Fotos, dadurch wurden keine cNet Bilder angelegt.`);

	logger.log(TAG,`Hersteller-Logos abgewiesen`,
		`Es wurden ${countCompanyLogos} Hersteller-Logos abgewiesen.`);

	await serviceJobMonitoring.stop(true);

	return result;
};