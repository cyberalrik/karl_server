'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseLynnImageLink = require('../refac/use-cases/forTasks/task.lynn.image.link');
const TAG = 'task.lynn.image.link';

/**
 * http://localhost:8080/cronJob/task.lynn.image.link
 * */

class Index {
	async cronTask() {
		let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 2)
		let result = await serviceJobMonitoring.start();
		if (result.isActive) {
			return result;
		}

		let useCaseLynnImageLink = new UseCaseLynnImageLink(TAG);
		result = await useCaseLynnImageLink.call();

		await serviceJobMonitoring.stop(true);

		return result;
	}
}

module.exports = Index;