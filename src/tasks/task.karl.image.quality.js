'use strict';

const DB = require('../refac/data-access');
const dbConstants = require('../refac/constants');
const UseCaseGetAllActiveImageLinks = require('../refac/use-cases/image/get-image-links');
const UseCaseDeactivateImage = require('../refac/use-cases/image/deactivate-image');
const UseCaseGetImageSize = require('../refac/use-cases/image/get-image-size');
const UseCaseSetImageSize = require('../refac/use-cases/image/set-image-size');
const UseCaseDetermineImageChannel = require('../refac/use-cases/image/determine-image-channel');
const UseCaseSetImageChannel = require('../refac/use-cases/image/set-image-channel');
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const ImagesBlacklistRepo = require('../repository/repo.images.blacklist');

const logger = require('../helper/util/logger.util');
const FisherYates = require("../refac/helper/algorithm/fisheryates");
const TAG = 'task.karl.image.quality';

/**
 * Setzt, wenn abweichend, für alle AKTIVEN Bilder die Bildgröße in der DB.
 * Alle toten Bilderlinks werden deaktiviert.
 * */
exports.cronTask = async () => {
    let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 6)
    let result = await serviceJobMonitoring.start();
    if (result.isActive) {
        return result;
    }

    result.successful = true;
    result.messages = [];

    let db_karl = new DB(dbConstants.KARL);
    let useCaseGetAllActiveImageLinks = new UseCaseGetAllActiveImageLinks(db_karl);
    let useCaseDeactivateImage = new UseCaseDeactivateImage(db_karl);
    let useCaseGetImageSize = new UseCaseGetImageSize();
    let useCaseSetImageSize = new UseCaseSetImageSize(db_karl);
    let useCaseDetermineImageChannel = new UseCaseDetermineImageChannel();
    let useCaseSetImageChannel = new UseCaseSetImageChannel();
    let imagesBlacklistRepo = new ImagesBlacklistRepo();
    let imagesBlacklist = await imagesBlacklistRepo.getAllPrepared();

    await logger.log(TAG, 'Hole aktive Bilderlinks')
    let list = await useCaseGetAllActiveImageLinks.getAllActive();
    let message = `Es wurden ${list.length} aktive Bilderlinks ermittelt.`;
    await logger.log(TAG, `Bilderlinks gesammelt`, message);
    result.messages.push(message);

    message = `Bilderlinks werden geschüttelt`;
    await logger.log(TAG, `Bilderlinks schütteln`, message);
    result.messages.push(message);

    let shuffle = new FisherYates();
    list = await shuffle.shuffle(list);

    message = `Das Schütteln der Bilderlinks ist abgeschlossen.`;
    await logger.log(TAG, `Bilderlinks schütteln ok`, message);
    result.messages.push(message);

    let deactivateUrl = 0;
    let i = 0;
    for (i; i < list.length; i++) {
        let currentImage = list[i];

        /**
         * Ist die URL in der Blacklist enthalten,
         * soll die URL in der Tabelle 'images' auf is_active = 0 gesetzt werden.
         * */

        if (imagesBlacklist[currentImage.image_url] !== undefined) {
            await useCaseDeactivateImage.deactivateByUrl(currentImage.image_url);

            continue;
        }

        /** Bildgröße feststellen */
        let imageSize = await useCaseGetImageSize.getImageSize(currentImage.image_url);

        /** Wenn der Link "tot" ist dann Link deaktivieren */
        if (!imageSize.successful ) {
            await useCaseDeactivateImage.deactivateByUrl(currentImage.image_url);
            deactivateUrl++;
            await logger.log(TAG, 'Der link wird deaktiviert', currentImage.image_url);

            continue;
        }

        /** Hat sich das Format geändert, dann hier neu abspeichern */
        if (imageSize.height !== currentImage.height || imageSize.width !== currentImage.width) {
            await useCaseSetImageSize.setImageSize(currentImage.id, imageSize.height, imageSize.width);
        }

        /**
         * Hat sich die Einstellung der Channels durch die Bildgröße geändert
         * und ist jetzt für andere Channel zugelassen, muss auch das gespeichert werden.
         * */

        let channel = await useCaseDetermineImageChannel.determineChannel(imageSize);
        if (currentImage.channel !== channel) {
            await useCaseSetImageChannel.setChannelById(currentImage.id, channel);
        }

        if (i % 500 === 0 && i > 0) {
            await logger.log(TAG, '500 Links verarbeitet', `Schon ${i} aktive Bilderlinks überprüft. (${list.length})`);
        }
    }

    let additionalText = '';
    if (deactivateUrl > 0) {
        additionalText = `Außerdem wurden ${deactivateUrl} tote URL's gefunden und deaktiviert.`;
    }

    message = `Es wurden insgesamt ${i} aktive Bilderlinks überprüft. ${additionalText} (${list.length})`;
    logger.log(TAG, 'Ende Bilderlinks verarbeiten', message);
    result.messages.push(message);

    await serviceJobMonitoring.stop(true);

    return result;
};