'use strict'

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseLynnDoubleProducts = require('../refac/use-cases/Lynn/double-products');
const TAG = 'task.check.lynn.products';

class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        let useCaseLynnDoubleProducts = new UseCaseLynnDoubleProducts();
        result.messages = await useCaseLynnDoubleProducts.doubleProductsAlert(TAG);

        await serviceJobMonitoring.stop(true);

        return result
    }
}

module.exports = Index;