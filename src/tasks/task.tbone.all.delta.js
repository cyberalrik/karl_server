'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const ServiceDateFormat = require('../services/service.date.format');
const UseCaseTbOneAll = require('../refac/use-cases/forTasks/tboneAll');
const LockJob = 'task.tbone.all';
const TAG = "task.tbone.all.delta";

/***
 *
 * @returns {Promise<{}>}
 */
exports.cronTask = async () => {
	let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, false, 1)
	let result = await serviceJobMonitoring.start();
	if (result.isActive) {
		return result;
	}

	let serviceDateFormat = new ServiceDateFormat();
	let newDate = await serviceDateFormat.getStringDate();
	// newDate = '2023-03-01'
	let useCaseTbOneAll = new UseCaseTbOneAll();
	result = await useCaseTbOneAll.call(TAG, null, newDate, true);

	await serviceJobMonitoring.stop(false);

	result.successful = true;

	return result;
}
