'use strict';

const DB = require('../refac/data-access');
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const dbConstants = require('../refac/constants');
const GetProduct = require('../refac/use-cases/products/get-product');
const ChangeCategory = require('../refac/use-cases/products/change-category');
const logger = require('../helper/util/logger.util');
const TAG = 'task.karl.cci.category';

class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 4)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        result.successful = true;
        result.messages = [];

        let db_karl = new DB(dbConstants.KARL);
        let db_cci = new DB(dbConstants.CCI);

        let getProduct = new GetProduct(db_karl);

        let missingCategory = await getProduct.getProductsPerValue('p_cat_id', 'CY0001');
        // let missingCategory = await getProduct.getAllProductWhereCatIdIsNullOrEmpty();

        /** Für Testzwecke */
        // let newMissingCategory = [];
        // for (let index in missingCategory) {
        //     let item = missingCategory[index];
        //     if (item.p_nr === '890336' || item.p_nr === '100089'  || item.p_nr === '890337' ) {
        //         newMissingCategory.push(item);
        //     }
        // }
        // missingCategory = newMissingCategory;

        let message = `Es müssen für ${missingCategory.length} Produkte die Kategorie ermittelt werden.`;
        logger.log(TAG, `Kategorie hinzufügen`, message);
        result.messages.push(message);

        let changeCategory = new ChangeCategory(db_cci);
        result = await changeCategory.changeCategory(TAG, missingCategory, result);
        missingCategory = null;

        await serviceJobMonitoring.stop(true);

        return result;
    }
}

module.exports = Index