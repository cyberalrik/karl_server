'use strict'

const UseCaseKarlCollectPriceList = require('../refac/use-cases/forTasks/karlCollectPricelist');
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const LockJob = 'task.karl.collect.priceList';
const TAG = 'task.karl.collect.priceList.full';

exports.cronTask = async () => {
    let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, true, 1)
    let result = await serviceJobMonitoring.start();
    if (result.isActive) {
        return result;
    }

    let useCaseKarlCollectPriceList = new UseCaseKarlCollectPriceList(TAG);
    result = await useCaseKarlCollectPriceList.call(false, null);

    await serviceJobMonitoring.stop(true);

    return result;
}
