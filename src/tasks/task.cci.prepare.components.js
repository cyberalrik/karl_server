'use strict'

const DB = require('../refac/data-access');
const dbConstants = require('../refac/constants');
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCasePrepareComponents = require('../refac/use-cases/cci/prepare-components');
const TAG = 'task.cci.prepare.components';
/**
 * Füllt die Tabelle "ct_components" neu mit aktuellen cNet Daten aus der cNet DB.
 *
 * @return {Promise<Array>}
 * */

class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

         let db_cci = new DB(dbConstants.CCI);
         let useCasePrepareComponents = new UseCasePrepareComponents(db_cci);
         result = await useCasePrepareComponents.prepareComponents(TAG);

        await serviceJobMonitoring.stop(true);

        return result;
    }
}

module.exports = Index
