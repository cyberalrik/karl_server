'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseEan = require('../refac/use-cases/Lynn/ean');
const TAG = 'task.lynn.ean';

class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
        let result= await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        result.successful = true;
        result.messages = [];

        let useCaseEan = new UseCaseEan();
        result = await useCaseEan.call(TAG, result);

        await serviceJobMonitoring.stop(true);

        return result;
    }
}

module.exports = Index