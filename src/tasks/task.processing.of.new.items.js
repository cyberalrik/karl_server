'use strict'

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const ServiceTaskRun = require('../services/service.task.run');
const logger = require("../helper/util/logger.util");
const TAG = 'task.processing.of.new.items';

/**
 * Hier sollen alle Task, die für die Verarbeitung neu angelegter Artikel verantwortlich sind,
 * sinnvoll nacheinander aufgerufen werden.
 *
 * Daten holen:
 * 0.1 h - task.karl.collect.all.delta - Importiert Artikeldaten aus Lynn - Delta (keine Preise/ Mengen)
 *
 * Daten anreichern:
 * 5.0 s - task.karl.cci.ean - Fehlende EAN ermitteln
 *
 * Daten verschicken:
 * 0.1 h - task.tbone.all.delta - Schickt ein Delta an Daten, auch Bilder, zu TB.One.(keine Preise/Mengen)
 *
 * Alles zusammen: 12.6h
 * */
class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        result.messages = [];

        let resultCal;
        let serviceTaskRun = new ServiceTaskRun();

        try {
            resultCal = await serviceTaskRun.taskRun('task.karl.collect.all.delta');     //1
            await this.addMessage(result, resultCal);
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.karl.collect.all.delta" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            resultCal = await serviceTaskRun.taskRun('task.karl.cci.ean');                   //2
            await this.addMessage(result, resultCal);
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.karl.cci.ean" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            resultCal = await serviceTaskRun.taskRun('task.tbone.all.delta');                //3
            await this.addMessage(result, resultCal);
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.tbone.all.delta" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        await serviceJobMonitoring.stop(true);

        return result
    }

    async addMessage(result, resultCal) {
        if (resultCal.taskResult !== undefined && resultCal.taskResult.messages !== undefined) {
            for (let index in resultCal.taskResult.messages) {
                result.messages.push(resultCal.taskResult.messages[index]);
            }
        }
        return result;
    }
}

module.exports = Index;