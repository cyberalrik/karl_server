'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseKarlCollectPriceList = require('../refac/use-cases/forTasks/karlCollectPricelist');
const LockJob = 'task.karl.collect.pricelist';
const TAG = 'task.karl.collect.pricelist.delta';

exports.cronTask = async () => {
    let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, false, 1)
    let result = await serviceJobMonitoring.start();
    if (result.isActive) {
        return result;
    }

    let useCaseKarlCollectPriceList = new UseCaseKarlCollectPriceList(TAG);
    result = await useCaseKarlCollectPriceList.call(true, null);

    await serviceJobMonitoring.stop(true);

    return result;
}
