'use strict'

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseSetImageFeatureInLynn = require('../refac/use-cases/Lynn/set-image-feature');
const logger = require('../helper/util/logger.util');
const TAG = 'task.karl.images.feature.lynn';

/**
 * Setzt oder korrigiert die Infos für CTFoto und CNetFoto.
 *
 * @return {Promise<Array>}
 * */

class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        let useCaseSetImageFeatureInLynn = new UseCaseSetImageFeatureInLynn(TAG);
        let cronResult = await useCaseSetImageFeatureInLynn.setImageFeatureInLynn();

        let messages = [];
        messages.push('Folgende Bildmerkmale wurden in Lynn gesetzt.');
        messages.push(`${cronResult.missingCtFotoInLynn} Fehlende CT-Foto Kennzeichen auf 'ja' gesetzt.`);
        messages.push(`${cronResult.missingCNetFotoInLynn} Fehlende CNet-Foto Kennzeichen 'ja' gesetzt.`);
        messages.push(`${cronResult.errorCtFotoInLynn} Falsche CT-Foto Kennzeichen 'nein' gesetzt.`);
        messages.push(`${cronResult.errorCNetFotoInLynn} Falsche CNet-Foto Kennzeichen 'nein' gesetzt.`);
        messages.push('Danke für den Auftrag.');

        result.messages = messages;

        logger.log(TAG, 'Job closed');
        await serviceJobMonitoring.stop(false);

        return result;
    }
}

module.exports = Index
