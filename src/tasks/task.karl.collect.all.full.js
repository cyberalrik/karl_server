'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseKarlCollectAll = require('../refac/use-cases/forTasks/karlCollectAll');
const LockJob = 'task.karl.collect.all';
const TAG = 'task.karl.collect.all.full';

exports.cronTask = async () => {
	let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, true, 4)
	let result = await serviceJobMonitoring.start();
	if (result.isActive) {
		return result;
	}

	let useCaseKarlCollectAll = new UseCaseKarlCollectAll(TAG);
	await useCaseKarlCollectAll.call();

	await serviceJobMonitoring.stop(true);

	return result;
};

