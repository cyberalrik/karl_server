'use strict';

const DB = require('../refac/data-access');
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const dbConstants = require('../refac/constants');
const GetProduct = require('../refac/use-cases/products/get-product');
const ChangeEAN = require('../refac/use-cases/products/change-ean');
const logger = require('../helper/util/logger.util');
const TAG = 'task.karl.cci.ean';

class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 4)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        result.successful = true;
        result.messages = [];

        let db_karl = new DB(dbConstants.KARL);
        let db_cci = new DB(dbConstants.CCI);

        let getProduct = new GetProduct(db_karl);

        let missingEAN = await getProduct.getArticlesPerValue('a_ean', 0);
        let message = `Es müssen für ${missingEAN.length} Artikel EAN hinzugefügt werden.`;
        logger.log(TAG, `EAN hinzufügen`, message);
        result.messages.push(message);

        let changeEAN = new ChangeEAN(db_cci, 1000);
        result = await changeEAN.changeEANs(TAG, missingEAN, result);

        await serviceJobMonitoring.stop(true);

        return result;
    }
}

module.exports = Index