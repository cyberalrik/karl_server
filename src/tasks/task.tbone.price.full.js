'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseTbOnePrice = require('../refac/use-cases/forTasks/tbonePrice');
const LockJob = 'task.tbone.price';
const TAG = 'task.tbone.price.full';

exports.cronTask = async () => {
	let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, true, 10)
	let result = await serviceJobMonitoring.start();
	if (result.isActive) {
		return result;
	}

	let useCaseTbOnePrice = new UseCaseTbOnePrice(TAG);
	result = await useCaseTbOnePrice.call(false);

	await serviceJobMonitoring.stop(true);

	return result;
}

