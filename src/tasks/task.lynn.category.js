'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseLynnCategory = require('../refac/use-cases/category');
const TAG = 'task.lynn.category';

class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        result.successful = true;
        result.messages = [];

        let useCaseLynnCategory = new UseCaseLynnCategory(TAG);

        let matchingCategoryResult = await useCaseLynnCategory.matchingCategory();
        result.messages = matchingCategoryResult.messages;

        await serviceJobMonitoring.stop(true);

        return result;
    }
}

module.exports = Index