'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseTbOnePrice = require('../refac/use-cases/forTasks/tbonePrice');
const LockJob = 'task.tbone.price';
const TAG = 'task.tbone.price.delta';

exports.cronTask = async () => {
	let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, false, 1)
	let result = await serviceJobMonitoring.start();
	if (result.isActive) {
		return result;
	}

	let useCaseTbOnePrice = new UseCaseTbOnePrice(TAG);
	result = await useCaseTbOnePrice.call(true);

	await serviceJobMonitoring.stop(true);

	return result;
}

