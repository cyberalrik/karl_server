'use strict';

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseTbOneStock = require('../refac/use-cases/forTasks/tboneStock');
const LockJob = 'task.tbone.stock';
const TAG = 'task.tbone.stock.delta';

exports.cronTask = async () => {
	let serviceJobMonitoring = new ServiceJobMonitoring(LockJob, false, 1)
	let result = await serviceJobMonitoring.start();
	if (result.isActive) {
		return result;
	}

	let useCaseTbOneStock = new UseCaseTbOneStock(TAG);
	result = await useCaseTbOneStock.call(true);

	await serviceJobMonitoring.stop(true);

	return result;
};
