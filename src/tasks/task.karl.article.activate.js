'use strict'

const DB = require('../refac/data-access');
const dbConstants = require('../refac/constants');
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseActivateArticle = require('../refac/use-cases/products/activate-articles');
const logger = require('../helper/util/logger.util');
const TAG = 'task.karl.article.activate';

/**
 * Kontrolliert alle in Lynn aktive Artikel, ob sie auch in Karl aktiv sind.
 * Es müssen alle einträge von aktiven Artikel in der Tabelle "articleChannels"
 *
 * @return {Promise<Array>}
 * */

class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        let db_lynn = new DB(dbConstants.LYNN);
        let db_karl = new DB(dbConstants.KARL);
        let useCaseActivateArticle = new UseCaseActivateArticle(db_karl, db_lynn);
        result = await useCaseActivateArticle.activateArticle(result);

        logger.log(TAG, 'Job closed');
        let timeString = await serviceJobMonitoring.stop(false);
        result.messages.push(`Der Job hat ${timeString} gedauert.`);

        return result;
    }
}

module.exports = Index
