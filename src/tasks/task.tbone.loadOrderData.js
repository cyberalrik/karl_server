'use strict'

/**
 * Wird nicht benutzt !!!
 * */

// Es sollen die Auftragsdaten () per FTP von TB-One geholt werden.
const appConfig = require('../configs/app.config').getConfig();
const tboneConfig = require('../configs/tbone.config');
const ReadOrderData = require('../refac/use-cases/orderlist/readOrderData');
const LoadFilesFromFtp = require('../refac/use-cases/orderlist/loadFileFromFtp');
const localPath = require('../configs/order.config').localPath;
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const logger = require('../helper/util/logger.util');
const TAG = 'task.tbone.loadOrderData';

exports.cronTask = async () => {
    if (!appConfig.ACTIVE_TBONE_DOWNLOAD)
        return;

    let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
    let result = await serviceJobMonitoring.start();
    if (result.isActive) {
        return result;
    }

    /** Wird nicht bentzt */
    return result;




    let params = {};
    params.tboneConfig = tboneConfig;
    params.localPath = localPath;
    params.result = {
        successful: true,
        numberOfOrders: 0,
        numberOfGoodFiles: 0,
        numberOfBadFiles: 0,
        numberOfFilesNotFound: 0
    };

    try {
        logger.log(TAG, 'Job started');
        // let loadFilesFromFtp = new LoadFilesFromFtp();
        // param = await loadFilesFromFtp.loadOrderFiles(param)
        params.localFiles =  ['ORDERS_20200727_1353662.xml', 'ORDERS_20200727_135344.xml', 'ORDERS_20200727_135322.xml', 'ORDERS_20200727_135366.xml']

        if (params.localFiles.length === 0) {
            return 'Es wurden keine Daten gefunden.';
        }

        let readOrderData = new ReadOrderData();
        params = await readOrderData.readXmlFiles(params);

         let text1 = params.result.numberOfGoodFiles === 1 ?
            ' Es wurde aus ' + params.result.numberOfGoodFiles + ' Datei ' :
            ' Es wurden aus ' + params.result.numberOfGoodFiles + ' Dateien ';

        let text2 = params.result.numberOfOrders === 1 ?
            ' Auftrag' :
            ' Aufträge';

        let text3 = params.result.numberOfBadFiles > 0 ?
            params.result.numberOfBadFiles === 1 ?
                ' ACHTUNG!!! ' + params.result.numberOfBadFiles + ' Datei konnte nicht eingelesen werden.':
                    ' ACHTUNG!!! ' + params.result.numberOfBadFiles + ' Dateien konnten nicht eingelesen werden.':
            '';

        let text4 = params.result.numberOfFilesNotFound !== 0 ?
            ' Es wurden ' + params.result.numberOfFilesNotFound + ' Dateien nicht gefunden.' : '';

        // Es wurden aus 2 Dateien 23 Aufträge eingelesen.
        logger.log(TAG, 'Daten eingelesen', text1 + params.result.numberOfOrders + text2 + ' eingelesen.' +
            text3 + text4);

        params.result = text1 + params.result.numberOfOrders + text2 + ' eingelesen.' + text3 + text4;
    } catch (e) {
        logger.err(TAG, 'Fehlerhafte Verarbeitung', e.messages)
    }

    await serviceJobMonitoring.stop(true);

    return params;
}