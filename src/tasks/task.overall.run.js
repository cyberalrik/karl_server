'use strict'

const ServiceJobMonitoring = require('../services/service.job.monitoring');
const ServiceTaskRun = require('../services/service.task.run');
const TAG = 'task.overall.run';
const logger = require('../helper/util/logger.util');

/**
 * Hier sollen alle Task sinnvoll nacheinander aufgerufen werden.
 *
 * Daten holen:
 * 1.6 h - task.karl.collect.all.full - Importiert Artikeldaten aus Lynn (keine Preise/ Mengen)
 * 2.1 h - task.karl.collect.pricelist.full - Holt sich Daten von Lynn (Preis/Menge) (full)
 *
 * Daten anreichern:
 * 7.5 m - task.karl.collect.components - Komponenten von cNet für Produkten ermitteln (delta)
 * 5.0 s - task.karl.cci.ean - Fehlende EAN ermitteln
 * 9.0 s - task.lynn.image.link - "Foto" Verlinkung aus Lynn verarbeiten
 * 1.1 s - task.karl.images.feature.lynn - Foto Merkmale prüfen und in Lynn setzen
 *
 * Daten überprüfen:
 * 2.6 h - task.karl.image.quality - Überprüft Bilder (Format und Existenz)
 *
 * Daten verschicken:
 * 6.1 h - task.tbone.all - Schickt alle Daten, auch Bilder, zu TB.One.(keine Preise/Mengen)
 * 6.2 m - task.tbone.price.full - Preisänderungen zu TB.One (delta)
 * 10.4 s - task.tbone.stock.full - Mengen zu TbOne
 *
 * 0.0 s - task.check.lynn.products - Sucht nach doppelten Lynn Produkten
 *
 * Alles zusammen: 12.6h
 * */
class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 23)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        let serviceTaskRun = new ServiceTaskRun();

        try {
            await serviceTaskRun.taskRun('task.karl.collect.all.full');              //1
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.karl.collect.all.full" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.karl.collect.all.e2');           //2
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.karl.collect.all.e2" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.karl.collect.pricelist.full');   //3
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.karl.collect.pricelist.full" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.karl.collect.components.delta'); //4
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.karl.collect.components.delta" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.karl.cci.ean');                  //5
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.karl.cci.ean" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.lynn.ean');                  //5
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.lynn.ean" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.tbone.all.delta');                  //5a
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.tbone.all.delta" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.lynn.image.link');               //6
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.lynn.image.link" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.karl.images.feature.lynn');      //7
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.karl.images.feature.lynn" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.tbone.price.full');              //8
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.tbone.price.full" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.tbone.stock.full');              //9
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.tbone.stock.full" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.check.lynn.products');           //10
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.check.lynn.products" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }

        try {
            await serviceTaskRun.taskRun('task.tbone.all.delta');               //11
        } catch (e) {
            logger.err(
                TAG,
                'Job abgebrochen',
                `Der Job "task.tbone.all.delta" ist abgebrochen.`,
                `Die Fehlermeldung ist:\n${e.message}`
            );
        }
        // await serviceTaskRun.taskRun('task.karl.image.quality');
        // await serviceTaskRun.taskRun('task.tbone.all');

        await serviceJobMonitoring.stop(true);

        return result
    }
}

module.exports = Index;