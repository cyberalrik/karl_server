'use strict'

const DB = require('../refac/data-access');
const dbConstants = require('../refac/constants');
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const UseCaseDeactivateArticle = require('../refac/use-cases/products/deactivate-articles');
const logger = require('../helper/util/logger.util');
const TAG = 'task.karl.article.deactivate';

/**
 * Deaktiviert alle Artikel die in Lynn als gelöscht gekennzeichnet sind (state = 1058).
 * Neu hinzugekommen ist in Lynn der Status SelOnly. Artikel die SelOnly = True sind,
 * müssen wie gelöschte Artikel behandelt werden und werden hier mit aufgenommen.
 *
 * @return {Promise<Array>}
 * */

class Index {
    async cronTask() {
        let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
        let result = await serviceJobMonitoring.start();
        if (result.isActive) {
            return result;
        }

        let db_lynn = new DB(dbConstants.LYNN);
        let useCaseDeactivateArticle = new UseCaseDeactivateArticle(db_lynn);
        let articleNumber = await useCaseDeactivateArticle.deactivateArticle();
        let messages = 'Es wurden ' + articleNumber + ' Artikel deaktiviert.';
        result.messages = messages;

        logger.log(TAG, 'Job closed', messages);
        await serviceJobMonitoring.stop(false);

        return result;
    }
}

module.exports = Index
