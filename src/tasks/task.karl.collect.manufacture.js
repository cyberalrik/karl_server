'use strict'

/**
 * Wird nicht benutzt !!!
 * */

const _ = require('underscore');

const DB = require('../refac/data-access')
const GetAllManufacture = require('../refac/use-cases/manufacture/get-manufactures');
const GetAllManufactureFromCCI = require('../refac/use-cases/manufacture/get-manufacrture-from-cci');
const AddManufacturer = require('../refac/use-cases/manufacture/add-manufacture');
const ServiceJobMonitoring = require('../services/service.job.monitoring');
const dbConstants = require('../refac/constants');
const TAG = 'task.karl.collect.manufacturer';

exports.cronTask = async () => {
    let serviceJobMonitoring = new ServiceJobMonitoring(TAG, null, 1)
    let result = await serviceJobMonitoring.start();
    if (result.isActive) {
        return result;
    }

    let db_cci = new DB(dbConstants.CCI);
    let db_karl = new DB(dbConstants.IN_MEMORY);

    let getAllManufactureFromCCI = new GetAllManufactureFromCCI(db_cci);
    let getAllManufactureFromKarl = new GetAllManufacture(db_karl);

    let addManufacturer = new AddManufacturer(db_karl);

    let listFromCCI = await getAllManufactureFromCCI.getAll();
    let listFromKarl = await getAllManufactureFromKarl.getManufactures();

    for(let i = 0; i< listFromCCI.length; i++){
        let j = await _.where(listFromKarl, {name: listFromCCI[i].name})
        if (j.id === null || j.id === undefined){
            addManufacturer.addManufacturer(listFromCCI[i]);
        }
    }

    await serviceJobMonitoring.stop();

    return result;
}