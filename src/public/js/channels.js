/**
 * Liest die Felder des Modals des Kanals mit dem Schlüssel channelKey aus
 * und stellt eine POST-Request mit den ausgelesenen Werten an die API
 * @param channelKey
 */
function saveChannel(channelKey){
    let isActive = document.getElementById("channel_is_active_selection" + channelKey).value;
    let imageWidth = document.getElementById("image_width_input" + channelKey).value;
    let imageHeight = document.getElementById("image_height_input" + channelKey).value;

    $.post(
        '/api/channel/update/', {
            channel_key: channelKey,
            channel_is_active: isActive,
            image_width: imageWidth,
            image_height: imageHeight
        }, function (data) {
            if (data.successful) {
                $('#modal_'+channelKey).modal('hide');
                window.document.location.href ='/channel';
            } else {
                alert(data.message);
            }
        }
    )
}

/**
 * Bricht den Bearbeitungsvorgang ab und setzt die Werte der Input-Felder des Modals zurück.
 * @param channelKey
 * @param isActive
 * @param imageWidth
 * @param imageHeight
 */
function cancelChannelEditing(channelKey, isActive, imageWidth, imageHeight){
   document.getElementById("image_width_input" + channelKey).value = imageWidth;
   document.getElementById("image_height_input" + channelKey).value = imageHeight;

   if(isActive){
       document.getElementById("channel_is_active_selection" + channelKey).value = 1;
   }
   else{
       document.getElementById("channel_is_active_selection" + channelKey).value = 0;
   }
}

/**
 * Liest die Felder des Neu-Modals aus und stellt eine POST-Request mit den ausgelesenen Werten an die API
 */
function saveNewChannel(){
    let channelName = document.getElementById("channel_name_input0").value;
    let channelKey = document.getElementById("channel_key_input0").value;
    let isActive = document.getElementById("channel_is_active_selection0").value;
    let channelSurcharge = document.getElementById("channel_surcharge_input0").value;
    let imageType = document.getElementById("channel_type_input0").value;
    let stock = document.getElementById("stock_input0").value;
    let imageWidth = document.getElementById("image_width_input0").value;
    let imageHeight = document.getElementById("image_height_input0").value;

    $.post(
        '/api/channel/new/', {
            channel_name: channelName,
            channel_key: channelKey,
            channel_is_active: isActive,
            channel_surcharge: channelSurcharge,
            image_type: imageType,
            stock: stock,
            image_width: imageWidth,
            image_height: imageHeight
        }, function (data) {
            if (data.successful) {
                $('#modal_new').modal('hide');
                window.document.location.href ='/channel';
            } else {
                alert(data.message);
            }
        }
    )
}

/**
 * Bricht den Vorgang ab und setzt die Werte der Input-Felder des Modals zurück.
 */
function cancelChannelCreation(){
    document.getElementById("channel_name_input0").value = null;
    document.getElementById("channel_key_input0").value = null;
    document.getElementById("channel_is_active_selection0").value = 0;
    document.getElementById("channel_surcharge_input0").value = 0;
    document.getElementById("channel_type_input0").value = null;
    document.getElementById("stock_input0").value = null;
    document.getElementById("image_width_input0").value = null;
    document.getElementById("image_height_input0").value = null;
}