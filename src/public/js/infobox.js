function saveInfobox()
{
    let nr = document.getElementById("infoboxNr").value;
    let type = document.getElementById("infoboxType").value;
    let infoText = document.getElementById("infoboxText").value.substring(0, 255);

    if (infoText === '') {
        document.getElementById(type + '-' + nr).classList.remove('isAvailable');
    } else {
        document.getElementById(type + '-' + nr).classList.add('isAvailable');
    }

    // Tooltip anpassen
    document.getElementById(type + '-' + nr).setAttribute("data-original-title", infoText);

    // Daten speichern
    $.post(
        "/infobox/put/", {
            nr : nr,
            type : type,
            infoText: infoText,
        }, function (data) {
            console.log(data);
            if (data.successful) {
                clearTextArea();
                // Daten wurden gespeichert, Infobox kann geschlossen werden
                $('#modalInfobox').modal('hide')
            } else {
                // Fehler werden angezeigt.
                document.getElementById("infoboxErrorLabel").innerHTML = data.messageUser;
                console.log(data.messages);
            }
        }
    )
}

function openModal(nr, type)
{
    // Überschrift der Infobox
    if (type === 0) {
        document.getElementById("infoboxTitle").innerHTML = 'Infobox - Produktnummer ' + nr;
    } else {
        document.getElementById("infoboxTitle").innerHTML = 'Infobox - Artikelnummer ' + nr;
    }

    document.getElementById("infoboxNr").value = nr;
    document.getElementById("infoboxType").value = type;

    // Daten holen
    $.post(
        "/infobox/get/", {
            type : type,
            nr : nr,
        }, function (data) {
            console.log(data);
            if (data.successful) {
                let textDiv = document.getElementById("infoboxTextarea");
                let textArea = document.createElement("textarea");
                textArea.setAttribute("id", "infoboxText")
                textArea.setAttribute("class", "infobox")
                textArea.placeholder = "Maximal 255 Zeichen werden gespeichert."
                textDiv.appendChild(textArea);
                textLabel = document.createTextNode(data.infoText);
                // document.getElementById("infoboxText").innerHTML = '';
                textArea.appendChild(textLabel);
                // Speicher-Button aktivieren
                document.getElementById("infoboxSaveButton").removeAttribute("disabled", "disabled");
                // Fehlertext löschen
                document.getElementById("infoboxErrorLabel").innerHTML = '';
            } else {
                // speicher-Button deaktivieren
                document.getElementById("infoboxSaveButton").setAttribute("disabled", "disabled")
                document.getElementById("infoboxErrorLabel").innerHTML = data.messageUser;
                console.log(data.messages);
            }
            $('#modalInfobox').modal('show');
        }
    )
    // Infobox anzeigen
}
function clearTextArea() {
    //textArea löschen
    let textDiv = document.getElementById("infoboxTextarea");
    let textArea = document.getElementById("infoboxText");
    textDiv.removeChild(textArea);
}
window.onload = function () {
    // Für Tooltips
    // $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="tooltip"]').tooltip({html: true});
}