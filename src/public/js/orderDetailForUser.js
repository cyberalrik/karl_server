window.onresize = function () {
    setScreen();
}

function viewOpen(result) {
    if (document.getElementById('rawDataCheckbox').checked) {

        $('#modal_OrderRawData').modal({
            keyboard: true, //ESC-Taste schließt Modal
            backdrop: true, //beim Klick außerhalb des Modals, wird dieses geschlossen (sonst "static")
            show: true, //Modal wird nach dem Initialisieren angezeigt
        });
        viewOrderDetails(result)
    } else {
        $('#modal_OrderDetailMenu').modal({
            keyboard: true,
            backdrop: true,
            show: true,
        });
    }
}

function viewContactAddress() {
    console.log('Kontaktadresse');
    let index = 0;
    let viewData = {
        letterSalutation: {index: index++, label: 'Briefanrede', dataOrigin: null},
        salutation: {index: index++, label: 'Anrede', dataOrigin: {table:'sellTo', field: 'title'}},
        firstName: {index: index++, label: 'Vorname', dataOrigin: {table:'sellTo', field: 'firstname'}},
        lastName: {index: index++, label: 'Name/Pseudofir...', dataOrigin: {table:'sellTo', field: 'lastname'}},
    }

    createView(viewData);
}

function viewDeliveryAddress() {
    console.log('Lieferadresse');
}

function createView(viewData) {
    let screenRight = document.getElementById('screenRight');
    screenRight.innerHTML = '';
    for (let data in viewData) {
        let element = getElement(viewData[data]);
        screenRight.appendChild(element);
    }
}

function getElement(data) {
    let textNode = document.createTextNode(data.label);
    let label = document.createElement('label');
    label.appendChild(textNode);
    label.classList.add('label', 'label_' + data.index)

    let input = document.createElement('input');
    if (data.dataOrigin !== null) {
        // let dataOrigin = tbOne[data.dataOrigin.table];
        // textNode = dataOrigin[data.dataOrigin.field];
        // input.appendChild(textNode);
    }
    input.setAttribute('type', 'text');
    input.classList.add('input_' + data.index);

    let div = document.createElement('div');
    div.appendChild(label);
    div.appendChild(input);

    return div;
}