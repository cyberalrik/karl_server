// https://t3n.de/news/fullscreen-api-funktioniert-363613/

function getScreenHeight(screen, imgBottomSticky) {
    let r = { top:0, top2:0, height:0 };

    let b = document.getElementById(screen);
    let o = document.getElementById(imgBottomSticky);

    if( typeof o != 'object' || typeof b != 'object') return 0;

    if(typeof o.offsetTop != 'undefined')    {
        r.height = o.offsetHeight;
        while (o && o.tagName != 'BODY')         {
            r.top  += parseInt( o.offsetTop );
            o = o.offsetParent;
        }
    }

    if(typeof b.offsetTop != 'undefined')    {
        while (b && b.tagName != 'BODY')         {
            r.top2  += parseInt( b.offsetTop );
            b = b.offsetParent;
        }
    }
    let height = r.top + r.height - 49;// r.top2;

    return height;
}

function setScreen() {
    let height = getScreenHeight('screenLeft', 'imgBottomSticky') - 3;
    document.getElementById('screenLeft').style.height = height + 'px';
    document.getElementById('screenRight').style.height = height + 'px';
}

