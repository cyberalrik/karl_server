/** Aufruf, dass für dieses Produkt alle Daten von Lynn abgeholt und in Karl aktualisiert werden. */
function downloadProductData(p_nr) {
    /** Kennzeichnet den Button als gedrückt */
    let btn = window.document.getElementById('btnDownloadLynn');
    if (btn != null) {
        btn.classList.add('btn-info');
        btn.classList.remove('btn-light');
    }

    /** Ausblenden auf der Produkt Startseite */
    let divProductImport = window.document.getElementById('divProductImport');
    if (divProductImport != null) {
        divProductImport.classList.add('d-none');
    }

    let divGetProductData = window.document.getElementById('divGetProductData');
    if (divGetProductData != null) {
        divGetProductData.classList.remove('d-none');
    }

    $('#modal_wait').modal({
        keyboard: true,
        backdrop: true,
        show: true,
    });

    let xhr = new XMLHttpRequest();
    let onReady = function(e) {
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status !== 200) {
                console.log('Daten von Lynn abholen - Error - Status: ' + xhr.status);
            } else {
                let result = JSON.parse(xhr.response);
                console.log(result);

                if (result.successful) {
                    window.document.location.href = '/product/overview/' + p_nr;

                    console.log('Das Produkt ' + p_nr + ' wurde mit Lynn-Daten aktualisiert.');

                } else {
                    console.log('Es konnten keine Daten aus Lynn geholt werden.');
                    if (divProductImport != null) {
                        divProductImport.classList.remove('d-none');
                    }

                    if (divGetProductData != null) {
                        divGetProductData.classList.add('d-none');
                    }

                    if (btn != null) {
                        btn.classList.add('btn-light');
                        btn.classList.remove('btn-info');
                    }

                    alert('Das Produkt mit der Nummer: '
                        + p_nr
                        + ' konnte in Lynn nicht unter den AKTIVEN Produkten/ Artikeln gefunden werden.');

                    $('#modal_wait').modal('hide');
                }
            }
        }
    }

    let params = {
        p_nr: p_nr,
    }

    let onError = function(err) {
        // something went wrong with upload
        console.log('Es ist ein Fehler aufgetreten: ' + err.message);
    };

    xhr.open('post', "/productDataRefresh", true);
    xhr.addEventListener('error', onError, false);
    xhr.addEventListener('readystatechange', onReady, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(params))
}


/** Aufruf, dass für dieses Produkt alle Daten an TB.One geschickt werden. */
function uploadProductData(p_nr) {
    /** Kennzeichnet den Button als gedrückt */
    let btn = window.document.getElementById('btnUploadTbOne');
    btn.classList.add('btn-info');
    btn.classList.remove('btn-light');

    $('#modal_wait').modal({
        keyboard: true,
        backdrop: true,
        show: true,
    });

    let xhr = new XMLHttpRequest();
    let onReady = function(e) {
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status !== 200) {
                console.log('Produktdaten an TbOne senden - Error - Status: ' + xhr.status);
            } else {
                let result = JSON.parse(xhr.response);
                console.log(result);

                if (result.successful) {
                    window.document.location.href = '/product/overview/' + p_nr;

                    console.log('Das Produkt ' + p_nr + ' wurde mit Lynn-Daten aktualisiert.');

                } else {
                    console.log('Es konnten keine Daten aus Lynn geholt werden.');
                    alert('Das Produkt mit der Nummer: '
                        + p_nr
                        + ' konnte in Lynn nicht unter den AKTIVEN Produkten/ Artikeln gefunden werden.');
                    $('#modal_wait').modal('hide');
                }
            }
        }
    }

    let params = {
        p_nr: p_nr,
    }

    let onError = function(err) {
        // something went wrong with upload
        console.log('Es ist ein Fehler aufgetreten: ' + err.message);
    };

    xhr.open('post', "/productDataToTbOne", true);
    xhr.addEventListener('error', onError, false);
    xhr.addEventListener('readystatechange', onReady, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(params))
}

/**
 *  Aktiviert oder deaktiviert alle Bilder eines bestimmten Produkts
 *  Wird in der Überschrift der Bilderlinks in der Checkbox 'Aktiv' aufgerufen
 *  */
function updateActiveState(p_nr) {

    $('#modal_wait').modal({
        keyboard: true,
        backdrop: true,
        show: true,
    });

    let active = document.getElementById("allImageInterface").checked
    $.post(
        "/api/product/image/update/allActive", {
            pnr: p_nr,
            active: active,
            manually: true
        }, function (data) {
            if (data) {
                window.document.location.href = '/product/overview/' + p_nr
            }
        }
    )
}

/** Wird in modalem Fenster zum Anlegen eines neuen Bildes aufgerufen 'Bild bearbeiten' */
function saveNewURL(id, p_nr) {
    let newURL = document.getElementById("image_url_input" + p_nr).value

    $.post(
        "/api/product/image/new/", {
            pnr: p_nr,
            url: newURL,
        }, function (data) {
            console.log(data);
            if (data.successful) {
                $('#modal_image_'+id).modal('hide')
                window.document.location.href ='/product/overview/'+p_nr
            } else {
                alert(data.message);
            }
        }
    )
}

/** Wird im Modalem Fenster zur Änderung der Bilderinformationen aufgerufen 'Bild bearbeiten' */
function saveURL(id, p_nr, pos_old) {
    console.log(id);
    let newURL = document.getElementById("image_url_input" + id).value
    let pos = document.getElementById("image_pos_input" + id).value
    let active = document.getElementById("image_active_selection" + id).value
    let behave = document.getElementById("image_behave_selection" + id).value

    $.post(
        "/api/product/image/update/", {
            id: id,
            pnr: p_nr,
            url: newURL,
            active: active,
            pos_old: pos_old,
            pos: pos,
            behave: behave,
            manually: true
        }, function (data) {
            console.log(data);

            if (data.successful) {
                $('#modal_image_'+id).modal('hide');
                window.document.location.href ='/product/overview/'+p_nr
            } else {
                alert(data.message);
            }
        }
    )
}

/** Ein Bild für ein bestimmten Kanal aktivieren oder deaktivieren */
function updateImageActiveForChannel(p_nr, id, channel, active) {
    $('#modal_wait').modal({
        keyboard: true,
        backdrop: true,
        show: true,
    });
    $.post(
        "/api/product/image/channel/update/", {
            id: id,
            channel: channel,
            active: active,
        }, function (result) {
            if (result.successful) {
                window.document.location.href ='/product/overview/'+p_nr
            } else {
                $('#modal_wait').modal({
                    show: false,
                });

                console.log(result);
            }
        }
    )
}

/** Wechselt zu einem anderem Produkt. Übergeben wird die ProduktNummer */
function changeProduct(p_nr) {
    window.document.location.href ='/product/overview/' + p_nr
}

/** ruft die Löschung einer Bilderverlinkung auf. (KARL) */
function deleteImageLink(p_nr, p_nr_image) {
    /** Kennzeichnet den Button als gedrückt */
    let btn = window.document.getElementById('deleteImageLink');
    btn.classList.add('btn-info');
    btn.classList.remove('btn-light');

    let xhr = new XMLHttpRequest();
    let onReady = function(e) {
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status !== 200) {
                console.log('Bildverlinkung zu anderem Produkt löschen - Error - Status: ' + xhr.status);
            } else {
                let result = JSON.parse(xhr.response);
                console.log(result);

                if (result.successful) {
                    $('#modal_delete_product_image_link').modal('hide')
                    window.document.location.href = '/product/overview/' + p_nr;
                    console.log('Das Produkt ' + p_nr + ' wurde wurde entlinkt.');
                } else {
                    console.log('Die Verlinkung konnte nicht aufgehobern werden.');
                    alert(result.message);
                    $('#modal_delete_product_image_link').modal('hide')
                }
            }
        }
    }

    let params = {
        p_nr: p_nr,
        p_nr_image: p_nr_image
    }

    let onError = function(err) {
        // something went wrong with upload
        console.log('Es ist ein Fehler aufgetreten: ' + err.message);
    };

    xhr.open('post', "/deleteImageToOtherProduct", true);
    xhr.addEventListener('error', onError, false);
    xhr.addEventListener('readystatechange', onReady, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(params))

}