/** Aufruf, dass Preise eines Artikels zu TbOne übertragen werden*/
function updateArticlePrice(a_nr, channel) {
    let xhr = new XMLHttpRequest();
    let onReady = function(e) {
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status !== 200) {
                console.log('Price send to TbOne Error - Status: ' + xhr.status);
            } else {
                let result = JSON.parse(xhr.response);
                console.log(result);

                if (result.successful) {
                    let messages = channel === ''
                        ? 'Der Preis für den Artikel ' + a_nr + ' wurde für alle Channel geändert.'
                        : 'Der Preis für den Artikel ' + a_nr + ' wurde für den Channel ' + channel + ' geändert.';

                    console.log(messages);
                    let button = document.getElementById('updateArticlePrice' + a_nr + '_' + channel);
                    button.innerText = 'OK';
                    button.classList.add('btn-success');
                    button.classList.remove('btn-outline-secondary');
                    button.setAttribute('disabled','disabled');

                } else {
                    console.log('Der Preis konnte nicht geändert werden.');
                    alert('Es ist ein Fehler aufgetreten.');
                }
            }
        }
    }

    let params = {
        a_nr: a_nr,
        channel: channel,
    }

    let onError = function(err) {
        // something went wrong with upload
        console.log('Es ist ein Fehler aufgetreten: ' + err.message);
    };

    xhr.open('post', "/priceToTbOne", true);
    xhr.addEventListener('error', onError, false);
    xhr.addEventListener('readystatechange', onReady, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(params))
}

function updateChannelArticleState(a_nr, key, p_nr) {
    // console.log('test')
    $('#modal_wait').modal({
        keyboard: true,
        backdrop: true,
        show: true,
    });

    let active = document.getElementById('art_channel_'+a_nr+'_'+key).checked
    $.post(
        "/api/product/article/channel", {
            a_nr: a_nr,
            key: key,
            active: active
        }, function (data) {
            if (data) {
                window.document.location.href = '/product/overview/' + p_nr
            }
        }
    )
}

/** Aufruf, dass die Mengen eines Artikels zu TbOne übertragen werden*/
function updateArticleStock(a_nr, channel) {
    let xhr = new XMLHttpRequest();
    let onReady = function(e) {
        if (xhr.readyState === xhr.DONE) {
            if (xhr.status !== 200) {
                console.log('Stock send to TbOne Error - Status: ' + xhr.status);
            } else {
                let result = JSON.parse(xhr.response);
                console.log(result);

                if (result.successful) {
                    let messages = channel === ''
                        ? 'Die Menge für den Artikel ' + a_nr + ' wurde für alle Channel geändert.'
                        : 'Die Menge für den Artikel ' + a_nr + ' wurde für den Channel ' + channel + ' geändert.';

                    console.log(messages);
                    let button = document.getElementById('updateArticleStock' + a_nr + '_' + channel);
                    button.innerText = 'OK';
                    button.classList.add('btn-success');
                    button.classList.remove('btn-outline-secondary');
                    button.setAttribute('disabled','disabled');

                } else {
                    console.log('Die Menge konnte nicht geändert werden.');
                    alert('Es ist ein Fehler aufgetreten.');
                }
            }
        }
    }

    let params = {
        a_nr: a_nr,
        channel: channel,
    }

    let onError = function(err) {
        // something went wrong with upload
        console.log('Es ist ein Fehler aufgetreten: ' + err.message);
    };

    xhr.open('post', "/stockToTbOne", true);
    xhr.addEventListener('error', onError, false);
    xhr.addEventListener('readystatechange', onReady, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(params))
}
