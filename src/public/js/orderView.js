let tbOne = {};
window.onload = function () {
    setScreen();
    loadChannelFilter()
    $('[data-toggle="tooltip"]').tooltip({html: true});
}

function loadChannelFilter() {
    console.log('Filter "Channel" wird nachgeladen');
    $.post(
        "/logs/filterChannelSign/", {
            off: document.getElementById("jsOffset").value,
            channelSign: document.getElementById("jsChannelSign").value,
        }, function (result) {
            console.log(result);
            if (result.successful) {
                viewChannelFilter(result)
            } else {
                console.log(result.messages);
            }
        }
    );
}

function viewChannelFilter(result) {
    document.getElementById("channelSign").options[0] = new Option("", '-1');

    if (!(result.channelSign === undefined) && result.channelSign.length > 0) {
        for (let i = 0; i < result.channelSign.length; i++) {
            if (document.getElementById("jsChannelSign").value === result.channelSign[i].channelSign) {
                document.getElementById("channelSign").options[i + 1]
                    = new Option(result.channelSign[i].channelSign, result.channelSign[i].channelSign);

                document.getElementById("channelSign").selectedIndex = i + 1;
            } else {
                document.getElementById("channelSign").options[i + 1]
                    = new Option(result.channelSign[i].channelSign, result.channelSign[i].channelSign);
            }
        }
    }
}

function updateTheView(_offset) {
    let offset = parseInt(document.getElementById("jsOffset").value);
    let channelSign = '';

    if (document.getElementById("channelSign") === undefined || document.getElementById("channelSign").value === '') {
        channelSign = document.getElementById("jsChannelSign");
    } else {
        channelSign = document.getElementById("channelSign").value;
    }

    if (typeof _offset === 'number') {
        offset = _offset;
    }
    window.document.location.href = '/order/'
        + offset + '/'
        + channelSign + '/'
}

/**
 * Läd die Daten der Passenden orderId über XHR nach. Bei erfolgreichen Datenempfang werden sie Angezeigt.
 *
 * @param orderId
 */
function detailsLoad(orderId) {
    console.log('Nachladen der Orderdetails');
    $.post(
        "/logs/orderDetails/", {
            orderId: orderId,
        }, function (result) {
            console.log(result);
            if (result.successful) {
                // viewOrderDetails(result)
                tbOne = JSON.parse(result.order)
                viewOpen(result);
                console.log(tbOne);
            } else {
                console.log(result.messages);
            }
            document.getElementById("controlLabel").style.display = "none"
        }
    );
}

function viewOrderDetails(result) {
    document.getElementById("controlLabel").style.display = "block";
    let data = {};
    data.detailsData = JSON.parse(result.order)
    console.log(data.detailsData);
    data.tableDiscription = JSON.parse((result.tableDiscription))
    console.log(data.tableDiscription);
    if (!(data.detailsData === undefined)) {
        for (let [keyGroup, dataGroup] of Object.entries(data.detailsData)) {
            let description = data.tableDiscription[keyGroup];
            let multiple;
            if (description['id']["multiple"] === undefined || description['id']["multiple"] === null) {
                multiple = false
            } else {
                multiple = description['id']["multiple"] === 1
            }

            if (multiple) {
                // vorhandene Tabellen leeren
                let tableBody = document.getElementById('tbody_' + keyGroup);
                if (tableBody !== null) {
                    tableBody.innerHTML = '';
                }
            }

            if (dataGroup === null) {

                if (!(data.tableDiscription[keyGroup] === undefined || data.tableDiscription[keyGroup] === null)) {
                    console.log('Für ' + keyGroup +
                        ' gibt es keine Daten zu dieser Bestellung, deshalb wird eine leere Tabelle angelegt.');

                    // Hier wird eine leere Tabelle erstellt
                    createDetailedView(data, description, keyGroup, 1, multiple)
                } else {
                    console.log('Für ' + keyGroup +
                        ' gibt es Beschreibungsdaten, deshalb kann keine leere Tabelle angelegt werden.');
                }

                continue
            }

            if (Array.isArray(dataGroup) && dataGroup.length > 0) {
                console.log(keyGroup + ' ist ein Array');
                for (let i = 0; i < dataGroup.length; i++) {
                    writeRows(data, dataGroup[i], keyGroup, dataGroup.length, multiple, true)
                }
            } else if (Array.isArray(dataGroup) && dataGroup.length === 0) {
                let values;
                if (!(data['tableDiscription'][keyGroup] === undefined || data['tableDiscription'][keyGroup] === null)) {
                    values = data['tableDiscription'][keyGroup];
                } else {
                    continue
                }
                writeRows(data, values, keyGroup, 1, multiple, false)
            } else if (!(Array.isArray(dataGroup))) {
                writeRows(data, dataGroup, keyGroup, 1, multiple, true)
            }
        }
    }

}

/**
 * Befüllt die erstellte Tabelle mit Daten
 *
 * @param data
 * @param dataGroup
 * @param keyGroup
 * @param count
 */
function writeRows(data, dataGroup, keyGroup, count, multiple, isData) {
    createDetailedView(data, dataGroup, keyGroup, count, multiple);

    if (dataGroup === null || (Array.isArray(dataGroup)) && dataGroup.length === 0) {
        return
    }

    if (multiple && isData) {
        writeMultipleRows(data, dataGroup, keyGroup)
    } else {
        writeSingleRows(data, dataGroup, keyGroup, count)
    }
}

function writeMultipleRows(data, dataGroup, keyGroup) {
    let table = document.getElementById('table_' + keyGroup);
    if (table === null) {
        return ;
    }
    // let description = data['tableDiscription'][keyGroup];
    let tableTd;
    let tableTr;

    // Wenn es den TBody noch nicht gibt dann erzeugen
    let tableBody = document.getElementById('tbody_' + keyGroup);
    if (tableBody === null) {
        tableBody = document.createElement("tbody");
        tableBody.setAttribute("id","tbody_" + keyGroup);
    }

    tableTr = document.createElement("tr");

    /**
     * Spalte 2
     * Typen der Spalte 2
     * 0=nichts
     * 1=Datum
     * 2=Textarea
     * 3=Währung
     */
    // Reihenfolge aus tableDescription
    for (let [key] of Object.entries(dataGroup)) {
        let description = getDescription(data, keyGroup, key);
        tableTd = document.createElement("td"); // dataGroup[key]
        let textLabel;
        switch (description.type) {
            case 1:
                textLabel = document.createTextNode(getDateTimeString(dataGroup[key]));
                break;
            case 2:
            case 0:
            default:
                textLabel = document.createTextNode(dataGroup[key]);
                break;
        }

        // tableTd.setAttribute("class","nowrap");
        tableTd.classList.add('nowrap');
        tableTd.appendChild(textLabel);
        tableTr.appendChild(tableTd);
    }
    tableBody.appendChild(tableTr);
    table.appendChild(tableBody);
}

function writeSingleRows(data, dataGroup, keyGroup, count) {
    for (let number = 0; number < count; number++) {
        for (let [key, detailData] of Object.entries(dataGroup)) {
            let description = getDescription(data, keyGroup, key);

            // Wenn statt der dataGroup die description übergeben wurde und keine Daten zum Ausfüllen zur verfügung stehen.
            if (typeof (detailData) === 'object') {
                detailData = '';
            }

            switch (description.type) {
                case 1:
                    if (document.getElementById(keyGroup + '_' + key + '_' + number) === null) {
                        continue;
                    }

                    document.getElementById(keyGroup + '_' + key + '_' + number).textContent = getDateTimeString(detailData);
                    break;
                case 2:
                    if (document.getElementById('textarea_' + keyGroup + '_' + key + '_' + number) === null) {
                        continue;
                    }

                    document.getElementById('textarea_' + keyGroup + '_' + key + '_' + number).textContent = detailData;
                    break;
                case 0:
                default:
                    if (document.getElementById(keyGroup + '_' + key + '_' + number) === null) {
                        continue;
                    }

                    document.getElementById(keyGroup + '_' + key + '_' + number).textContent = detailData;
                    break;
            }
        }
    }
}
/**
 * Erstellt den Inhalt der Tabellen mit Überschrift, Feldnamen und Tooltip
 *
 * @param data
 * @param dataGroup
 * @param keyGroup
 * @param count
 */
function createDetailedView(data, dataGroup, keyGroup, count, multiple) {
    let firstElement = getFirstElement(dataGroup);

    if (!(document.getElementById(keyGroup + '_' + firstElement + '_0') === null)) {
        return
    }

    if (!(document.getElementById('horizontalTableHead_' + keyGroup) === null)) {
        return
    }
    console.log('View muss für ' + keyGroup + ' erstellt werden.')

    // Tabelle
    if (document.getElementById("table_" + keyGroup) === null) {
        console.log("ACHTUNG!!! Die Tabelle 'table_" + keyGroup + "' ist nicht vorhanden.")
        return
    }

    // Diese Tabelle ins in "modal_order_detail_oneView.pug" angelegt wurden.
    let newTable = document.getElementById("table_" + keyGroup)

    if (multiple) {
        // Tabellenspalten in Tabellenkopf
        let tableHead = createHorizontalTableHead(data, dataGroup, keyGroup);
        newTable.appendChild(tableHead);

    } else {
        // Tabellenspalten in Tabellenkopf
        let tableHead = createTableHead();
        newTable.appendChild(tableHead);

        let tableBody = createTableBody(data, dataGroup, keyGroup, count);
        newTable.appendChild(tableBody);
    }
    document.getElementById('container_' + keyGroup).appendChild(newTable);
}

/**
 * Erstellt die Struktur der Tabellenspalten und gibt sie zurück.
 *
 * @returns {HTMLTableSectionElement}
 */
function createTableHead() {
    // Tabellen Kopf - start
    let tableHead = document.createElement("thead");
    let tableTr = document.createElement("tr");
    tableHead.appendChild(tableTr)

    // Tabellenspalten-Überschrift 1
    let tableTh = document.createElement("th");
    let textLabel = document.createTextNode("Feldname");
    tableTh.appendChild(textLabel);
    tableTr.appendChild(tableTh);

    // Tabellenspalten-Überschrift 2
    tableTh = document.createElement("th");
    textLabel = document.createTextNode("Wert");
    tableTh.appendChild(textLabel);
    tableTr.appendChild(tableTh);

    // Tabellenspalten-Überschrift 3
    tableTh = document.createElement("th");
    textLabel = document.createTextNode("Info");
    tableTh.appendChild(textLabel);
    tableTr.appendChild(tableTh);
    // Tabellen Kopf - ende

    return tableHead;
}

/**
 * Erstellt die Struktur des Tabellenbodys und gibt ihn zurück
 *
 * @param data
 * @param dataGroup
 * @param keyGroup
 * @param count
 * @returns {HTMLTableSectionElement}
 */
function createTableBody(data, dataGroup, keyGroup, count) {
    //Tabellen Body
    let tableBody = document.createElement("tbody");
    for (let number = 0; number < count; number++) {
        for (let [key] of Object.entries(dataGroup)) {

            // Beschreibung ermitteln
            let description = getDescription(data, keyGroup, key);

            let tableTr = document.createElement("tr");

            // Spalte 1
            let tableTd = document.createElement("td");
            // tableTd.setAttribute("class", 'label_' + keyGroup);
            tableTd.classList.add('label_' + keyGroup);
            let lebelValue = description.node === undefined ? key : description.node;
            let textLabel = document.createTextNode(lebelValue);
            tableTd.appendChild(textLabel);
            tableTr.appendChild(tableTd);

            // Spalte 2
            // Typen der Spalte 2
            // 0=nichts
            // 1=Datum
            // 2=Textarea
            // 3=Währung
            let textarea;
            switch (description.type) {
                case 0:
                    tableTd = document.createElement("td");
                    tableTd.setAttribute("id", keyGroup + '_' + key + '_' + number);
                    // tableTd.setAttribute("class", 'value_' + keyGroup);
                    tableTd.classList.add('value_' + keyGroup);
                    tableTr.appendChild(tableTd);
                    break;
                case 1:
                    tableTd = document.createElement("td");
                    tableTd.setAttribute("id", keyGroup + '_' + key + '_' + number);
                    // tableTd.setAttribute("class", 'value_' + keyGroup);
                    tableTd.classList.add('value_' + keyGroup);
                    tableTr.appendChild(tableTd);
                    break;
                case 2:
                    textarea = document.createElement("textarea");
                    textarea.setAttribute("id", 'textarea_' + keyGroup + '_' + key + '_' + number);
                    textarea.readOnly = true;
                    tableTd = document.createElement("td");
                    tableTd.setAttribute("id", keyGroup + '_' + key + '_' + number);
                    // tableTd.setAttribute("class", 'value_' + keyGroup);
                    tableTd.classList.add('value_' + keyGroup);

                    tableTd.appendChild(textarea);
                    tableTr.appendChild(tableTd);
                    break;
                default:
                    tableTd = document.createElement("td");
                    tableTd.setAttribute("id", keyGroup + '_' + key + '_' + number);
                    tableTr.appendChild(tableTd);
            }

            //Spalte 3
            tableTd = document.createElement("td");
            if (!(description.tooltip === undefined || description.tooltip === null)) {
                let newImages = document.createElement("img");
                // newImages.setAttribute("class", "imgTooltip");
                newImages.classList.add('imgTooltip');
                newImages.setAttribute("src", "/images/info-circle-solid.svg");
                newImages.setAttribute("alt", "Info über Tooltip");
                newImages.setAttribute("data-toggle", "tooltip");
                newImages.setAttribute("title", description.tooltip);
                tableTd.appendChild(newImages);
            }
            tableTr.appendChild(tableTd);

            // alles in den Body
            tableBody.appendChild(tableTr);
        }

    }
    return tableBody;
}

function createHorizontalTableHead(data, dataGroup, keyGroup) {
    // Tabellen Kopf - start
    let tableHead = document.createElement("thead");
    tableHead.setAttribute("id", 'horizontalTableHead_' + keyGroup);
    // Tabellenspalten-Überschriften
    let tableTh;
    let tableTr = document.createElement("tr");
    for (let [key] of Object.entries(dataGroup)) {
        let description = getDescription(data, keyGroup, key);
        tableTh = document.createElement("th");

        let tableDiv = document.createElement("div");
        // tableDiv.setAttribute("class", " float-left");
        tableDiv.classList.add('float-left');
        let textLabel = document.createTextNode(key);
        tableDiv.appendChild(textLabel);
        tableTh.appendChild(tableDiv);

        let newImages = document.createElement("img");
        // newImages.setAttribute("class", "imgTooltip");
        newImages.classList.add('imgTooltip');
        newImages.setAttribute("src", "/images/info-circle-solid.svg");
        newImages.setAttribute("alt", "Info über Tooltip");
        // newImages.setAttribute("width", "20px");
        newImages.setAttribute("data-toggle", "tooltip");
        newImages.setAttribute("title", description.tooltip);
        tableTh.appendChild(newImages);

        tableTr.appendChild(tableTh);
    }
    tableHead.appendChild(tableTr)
    // Tabellen Kopf - ende

    return tableHead;
}


/**
 * Liefert das ersten Element und gibt es zurück
 *
 * @param dataGroup
 * @returns {string}
 */
function getFirstElement(dataGroup) {
    for (let [key] of Object.entries(dataGroup)) {
        return key;
    }
}

function getDescription(data, keyGroup, key) {
    let description = {type: 10};
    if (!(data.tableDiscription[keyGroup][key] === undefined || data.tableDiscription[keyGroup][key] === null)) {
        description = data.tableDiscription[keyGroup][key];
    }
    return description
}

/**
 * Erstellt aus einen uniformierten Datumstring ein Datumstring im deutschen Format zurück
 *
 * @param detailData
 * @returns {string|*}
 */
function getDateTimeString(detailData) {
    let date = new Date(detailData)
    if (date.toString() === 'Invalid Date') {
        return detailData;
    }

    let day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    let month = date.getMonth() +1 < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1;
    let hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    let minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    let secounds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();

    return day + '.' + month + '.' + date.getFullYear()
        + ' ' + hours + ':' + minutes + ':' + secounds
}