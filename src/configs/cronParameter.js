'use strict';

class Index {
    async getCronParameter() {
        let cronParameter = [
            {
                /**
                 * Hier werden alle Task sinnvoll nacheinander aufgerufen.
                 * */
                time: '40 18 * * 1,2,3,4,5',
                loc: '../../tasks/task.overall.run',
                name: 'task.overall.run',
                timeDescription: '0. Läuft Mo. bis Fr. um 18:40 Uhr.',
                description: 'Alle Task sinnvoll nacheinander aufgerufen',
                active: true,
                cronClass: true,
                pos: 201
            }, {
                /**
                 * Importiert Artikeldaten aus Lynn
                 * */
                time: '15 02 * * 2,3,4,5,6',
                loc: '../../tasks/task.karl.collect.all.full',
                name: 'task.karl.collect.all.full',
                timeDescription: '1. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: 'Importiert Artikeldaten aus Lynn (keine Preise/ Mengen)',
                active: false,
                cronClass: false,
                pos: 202
            }, {
                /**
                 * Anlegen, korrigieren, deaktivieren und löschen von E2-Artikel
                 * */
                time: '*_/13 7-17 * * 1,2,3,4,5',
                loc: '../../tasks/task.karl.collect.all.e2',
                name: 'task.karl.collect.all.e2',
                timeDescription: '2. Läuft im "overall.run" Job und Mo. bis Fr. alle 13 Minuten von 7-17 Uhr.',
                description: 'Holt und bildet alle eBay-2 Artikel',
                active: true,
                cronClass: true,
                pos: 203
            }, {
                /**
                 * Holt sich Daten von Lynn (Preis/Menge) full
                 * */
                time: '* * * * *',
                loc: '../../tasks/task.karl.collect.pricelist.full',
                name: 'task.karl.collect.pricelist.full',
                timeDescription: '3. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: 'Holt sich Daten von Lynn (Preis/Menge) (full)',
                active: false,
                cronClass: false,
                pos: 204
            }, {
                /**
                 * Holt sich Daten von Lynn (Preis/Menge) delta
                 * */
                time: '*_/20 7-17 * * 1,2,3,4,5',
                loc: '../../tasks/task.karl.collect.pricelist.delta',
                name: 'task.karl.collect.pricelist.delta',
                timeDescription: 'Läuft Mo. bis Fr. alle 20 Minuten von 7-17 Uhr.',
                description: 'Holt sich Daten von Lynn (Preis/Menge) DELTA -10h',
                active: true,
                cronClass: false,
                pos: 1
            }, {
                /**
                 * Komponenten von cNet für Produkten ermitteln. (ct_components)
                 * Wenn, Artikel schon Komponenten von cNet besitzt, wird er übersprungen.
                 * */
                time: '30 20 * * 1,2,3,4,5',
                loc: '../../tasks/task.karl.collect.components.delta',
                name: 'task.karl.collect.components.delta',
                // timeDescription: 'Mo. bis Fr. um 20:30 Uhr.',
                timeDescription: '4. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: 'Komponenten von cNet für Produkten ermitteln (delta)',
                active: false,
                cronClass: false,
                pos: 205
            }, {
                /**
                 * Fehlende EAN ermitteln
                 * */
                time: '*_/30 * * * 1,2,3,4,5',
                loc: '../../tasks/task.karl.cci.ean',
                name: 'task.karl.cci.ean',
                // timeDescription: 'Mo. bis Fr. alle 30 Minuten.',
                timeDescription: '5. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: 'Fehlende EAN ermitteln',
                active: false,
                cronClass: true,
                pos: 206
            }, {
                /**
                 * Fehlende EAN ermitteln
                 * */
                time: '*_/30 * * * 1,2,3,4,5',
                loc: '../../tasks/task.lynn.ean',
                name: 'task.lynn.ean',
                // timeDescription: 'Mo. bis Fr. alle 30 Minuten.',
                timeDescription: '6. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: 'EAN mit Lynn-Artikel abgleichen',
                active: false,
                cronClass: true,
                pos: 207
            }, {
                /**
                    * Verlinkung aus Lynn verarbeiten
                    * */
                time: '0 19 * * 1,2,3,4,5',
                loc: '../../tasks/task.lynn.image.link',
                name: 'task.lynn.image.link',
                // timeDescription: 'Mo. bis Fr. um 19:00 Uhr.',
                timeDescription: '7. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: '"Foto/ Komplett" Verlinkungen aus Lynn verarbeiten',
                active: false,
                cronClass: true,
                pos: 208
            }, {
                /**
                    * Hier werden alle Foto Merkmale geprüft und gesetzt.
                    * */
                time: '30 19 * * 1,2,3,4,5',
                loc: '../../tasks/task.karl.images.feature.lynn',
                name: 'task.karl.images.feature.lynn',
                // timeDescription: 'Mo. bis Fr. um 19:30 Uhr.',
                timeDescription: '8. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: 'Foto Merkmale prüfen und in Lynn setzen',
                active: false,
                cronClass: true,
                pos: 209
            }, {
                /**
                    * Preisänderungen zu TB.One - full
                    * */
                time: '* * * * *',
                loc: '../../tasks/task.tbone.price.full',
                name: 'task.tbone.price.full',
                // timeDescription: 'jeden Sonntag um 02:00 Uhr.',
                timeDescription: '9. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: 'Preisänderungen zu TB.One (full)',
                active: false,
                cronClass: false,
                pos: 210
            }, {
                /**
                    * Preisänderungen zu TB.One - delta
                    * */
                time: '*_/14 7-17 * * 1,2,3,4,5',
                loc: '../../tasks/task.tbone.price.delta',
                name: 'task.tbone.price.delta',
                timeDescription: 'Läuft Mo. bis Fr. alle 14 Minuten von 7-17 Uhr.',
                description: 'Preisänderungen zu TB.One (delta)',
                active: true,
                cronClass: false,
                pos: 2
            }, {
                /**
                    * Mengen zu TbOne - full
                    * */
                time: '* * * * *',
                loc: '../../tasks/task.tbone.stock.full',
                name: 'task.tbone.stock.full',
                // timeDescription: 'jeden Sonntag um 14:00 Uhr.',
                timeDescription: '10. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: 'Mengen zu TbOne (full)',
                active: false,
                cronClass: false,
                pos: 211
            }, {
                /**
                    * Mengen zu TbOne - delta
                    * */
                time: '*_/15 7-17 * * 1,2,3,4,5',
                loc: '../../tasks/task.tbone.stock.delta',
                name: 'task.tbone.stock.delta',
                timeDescription: 'Läuft Mo. bis Fr. alle 15 Minuten von 7-17 Uhr.',
                description: 'Mengen zu TbOne (delta = -2h)',
                active: true,
                cronClass: false,
                pos: 3
            }, {
                /**
                    * Prüft Lynn nach doppelt angelegten Produkten
                    * */
                time: '* * * * *',
                loc: '../../tasks/task.check.lynn.products',
                name: 'task.check.lynn.products',
                timeDescription: '11. Läuft im "overall.run" Job - Mo. bis Fr. um 18:30 Uhr.',
                description: 'Prüft Lynn nach doppelt angelegten Produkten',
                active: false,
                cronClass: true,
                pos: 212
            }, {
                /**
                 * Überprüft Bilder (Format, Existenz und Blacklist), ob sie für eBay zu klein sind.
                 * MedienType wegen bilder
                 * */
                time: '0 10 * * 0',
                loc: '../../tasks/task.karl.image.quality',
                name: 'task.karl.image.quality',
                // timeDescription: 'Mo. bis Fr. um 22:20 Uhr',
                timeDescription: 'Läuft jeden Sonntag um 10:00 Uhr.',
                description: 'Überprüft Bilder (Format, Existenz und Blacklist)',
                active: false,
                cronClass: false,
                pos: 104
                /**
                 * Diese Klasse wird nach der Strukturänderung der DB (erp.ProductPropertyLink)
                 * von ArticleForeignId hinzu ProductForeignId nicht mehr benötigt.
                 * */
            // }, {
            //     /**
            //      * Prüft die Komplettverlinkungen
            //      * */
            //     time: '0 7 * * 1',
            //     loc: '../../tasks/task.lynn.missing.link',
            //     name: 'task.lynn.missing.link',
            //     timeDescription: 'Läuft jeden Montag um 7:00 Uhr.',
            //     description: 'Prüft die Komplettverlinkungen',
            //     active: false,
            //     cronClass: true,
            //     pos: 105
            }, {
                /**
                 * Importiert Artikeldaten aus Lynn - Delta (keine Preise/ Mengen).
                 * */
                time: '* * * * *',
                loc: '../../tasks/task.karl.collect.all.delta',
                name: 'task.karl.collect.all.delta',
                timeDescription: 'Läuft in "task.processing.of.new.items" Job.',
                description: 'Importiert Artikeldaten aus Lynn - Delta (keine Preise/ Mengen)',
                active: false,
                cronClass: true,
                pos: 20
            }, {
                /**
                 * Schickt ein Delta an Daten, auch Bilder, zu TB.One.(keine Preise/Mengen).
                 * */
                time: '* * * * *',
                loc: '../../tasks/task.tbone.all.delta',
                name: 'task.tbone.all.delta',
                timeDescription: 'Läuft in "task.processing.of.new.items" & "task.overall.run" Job.',
                description: 'Schickt heute angelegte Artikel + Produkte, zu TB.One.(keine Preise/Mengen)',
                active: false,
                cronClass: true,
                pos: 21
            }, {
                /**
                 * Im- und exportiert heute angelegter Artikel - (keine Preise/ Mengen).
                 * */
                time: '30 7-18 * * 1,2,3,4,5',
                loc: '../../tasks/task.processing.of.new.items',
                name: 'task.processing.of.new.items',
                timeDescription: 'Läuft Mo. bis Fr. immer 30 min nach jeder vollen Stunde von 7-18 Uhr.',
                description: 'Im- und exportiert heute angelegte Artikel - (keine Preise/ Mengen)',
                active: true,
                cronClass: true,
                pos: 22
            }, {
                /**
                 * Schickt alle Daten, auch Bilder, zu TB.One.
                 * */
                time: '0 5 * * 6',
                loc: '../../tasks/task.tbone.all.full',
                name: 'task.tbone.all.full',
                timeDescription: 'Läuft jeden Samstag um 5:00 Uhr',
                description: 'Schickt alle Daten, auch Bilder, zu TB.One.(keine Preise/Mengen)',
                active: true,
                cronClass: false,
                pos: 101
            }, {
                /**
                 * Bereitet die cci Daten zum besseren Abruf auf (ct_components).
                 * */
                time: '0 17 * * 1,2,3,4,5',
                loc: '../../tasks/task.cci.prepare.components',
                name: 'task.cci.prepare.components',
                timeDescription: 'Läuft Mo. bis Fr. um 17:00 Uhr.',
                description: '!!! ACHTUNG !!! Befüllt die Tabelle ct_components neu.',
                active: true,
                cronClass: true,
                pos: 100
            }, {
                /**
                 * Deaktiviert alle in Lynn als "gelöscht" gekennzeichnete Artikel
                 * */
                time: '5 7-18 * * 1,2,3,4,5',
                loc: '../../tasks/task.karl.article.deactivate',
                name: 'task.karl.article.deactivate',
                timeDescription: 'Läuft Mo. bis Fr. immer 5 min nach jeder vollen Stunde von 7-19 Uhr.',
                description: 'Deaktiviert alle in Lynn als "gelöscht" gekennzeichnete Artikel',
                active: true,
                cronClass: true,
                pos: 4
            }, {
                /**
                 * Kontrolliert alle in Karl deaktivierte Artikel mit aktiven Lynn Artikel.
                 * Sind deaktivierte dabei, die in Lynn aktiv sind, werden sie in der Karl-DB Tabelle
                 * 'articleChannels' gelöscht.
                 * */
                time: '10 7-18 * * 1,2,3,4,5',
                loc: '../../tasks/task.karl.article.activate',
                name: 'task.karl.article.activate',
                timeDescription: 'Läuft Mo. bis Fr. immer 10 min nach jeder vollen Stunde von 7-18 Uhr.',
                description: 'Kontrolliert alle in Karl deaktivierte Artikel mit aktiven Lynn Artikel.',
                active: true,
                cronClass: true,
                pos: 5
            }, {
                /**
                 * Neue CNet Bilder einlesen
                 * */
                time: '0 14 * * 6',
                loc: '../../tasks/task.karl.images',
                name: 'task.karl.images',
                timeDescription: 'Läuft jeden Samstag um 14:00 Uhr.',
                description: 'Kontrolliert die CNet-DB auf neue Produkt-Bilder',
                active: true,
                cronClass: false,
                pos: 102
            }, {
                /**
                 * Kategorien aus cNet-DB bestimmen
                 * */
                time: '0 9 * * 0',
                loc: '../../tasks/task.karl.cci.category',
                name: 'task.karl.cci.category',
                timeDescription: 'Läuft jeden Sonntag um 9:00 Uhr.',
                description: 'Kategorien aus cNet-DB bestimmen',
                active: true,
                cronClass: true,
                pos: 103
            }, {
                /**
                 * Komponenten von cNet für Produkten ermitteln. (ct_components)
                 * */
                time: '* * * * *',
                loc: '../../tasks/task.karl.collect.components.full',
                name: 'task.karl.collect.components.full',
                timeDescription: 'Muss manuell gestartet werden.',
                description: 'Komponenten von cNet für Produkten ermitteln (full)',
                active: false,
                cronClass: false,
                pos: 301
            }, {
                /**
                 * Vergleicht und setzt die Kategorien in Lynn
                 * */
                time: '30 19 * * 1,2,3,4,5',
                loc: '../../tasks/task.lynn.category',
                name: 'task.lynn.category',
                timeDescription: 'Läuft Mo. bis Fr. um 19:30 Uhr.',
                description: 'Vergleicht und setzt die Kategorien in Lynn',
                active: true,
                cronClass: true,
                pos: 106
            }, {
                /**
                 * Löscht alte Log-Einträge
                 * */
                time: '0 1 1 * *',
                loc: '../../tasks/task.karl.cleanup.logs',
                name: 'task.karl.cleanup.logs',
                timeDescription: 'Läuft jeden 1. im Monat um 1:00 Uhr.',
                description: 'Löscht alte Log-Einträge.',
                active: true,
                cronClass: true,
                pos: 400
            }
        ];

        return cronParameter;
    }
}

module.exports = Index