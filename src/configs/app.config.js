'use strict';

module.exports = {
    getConfig(){
        let prod = require('./app.config.select').is_prod;
        let config = require('./app.config.data');

        if (prod) {
            config.DOMAIN = 'http://apps.service.lokal';
            config.DB = 'karl_prod';
            config.TEST = false;
            config.ACTIVE_CRON = true;
            config.ACTIVE_TBONE_UPLOAD = true;
            config.PORT = 8089;
            config.SERVER_URL = '/mnt/karl';
            config.REPORT_PATH = '/services/report';
            config.FS_WRITE_FILE_PATH = '/';
        } else {
            config.DOMAIN = 'http://localhost';
            config.DB = 'karl_dev';
            config.TEST = true;
            config.ACTIVE_CRON = false;
            config.ACTIVE_TBONE_UPLOAD = true;
            config.PORT = 8080;
            config.SERVER_URL = '\\\\STOR-1\\Otto\\karl';
            config.REPORT_PATH = 'C:\\\\Daten\\\\karl\\\\report';
            config.FS_WRITE_FILE_PATH = 'C:\\Users\\a.linde\\Downloads\\';
        }

        return config;
    }
};
