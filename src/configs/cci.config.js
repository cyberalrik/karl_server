'use strict';

module.exports = {
	DB_HOST: 'dbserver.service.lokal',
	DB: 'cci_prod',
	USER: 'karl',
	PW: 'qwertz',
	USER_2: 'root',
	PW_2: 'q1w2e3r4t5',
	SYNC: true,
	SYNC_FORCE: true,
	TEST: true,
	OPTIONS: {
		host: 'dbserver.service.lokal',
		dialect: 'mariadb',
		logging: false,
		pool: {
			max: 1005,
			min: 1,
			acquire: 10000,
			idle: 10000,
			evict: 10000,
			handleDisconnects: true,
			connectionLimit: 1000
		},
	},
};
