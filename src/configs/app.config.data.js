'use strict';

module.exports = {
	DB_HOST: 'dbserver.service.lokal',
	// DB: 'karl_prod',
	USER: 'karl',
	PW: 'qwertz',
	SYNC: false,
	SYNC_FORCE: false,
	BACKUP_DATA: true,
	// TEST: true,
	// ACTIVE_CRON: false,
	// ACTIVE_TBONE_UPLOAD: false,
	ACTIVE_TBONE_DOWNLOAD: false,
	// PORT: 8089,
	OPTIONS: {
		host: 'dbserver.service.lokal',
		dialect: 'mariadb',
		logging: false,
		pool: {
			max: 5100,
			min: 1,
			acquire: 10000,
			idle: 10000,
			evict: 10000,
			handleDisconnects: true
		},
	},
	FOTORELEVANTE_VERLINKUNGEN: '1,2,1003,1007'
};