'use strict';

const express = require('express');
const favicon = require('serve-favicon')
const path = require('path');
const config = require('./configs/app.config').getConfig();

const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const port = config.PORT;

app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname, 'public', 'images/karlderkaefer.png')));
// app.use(favicon(path.join(__dirname, 'public', 'images/favicon-alt.ico')));

app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	next();
});

app.set('view engine', 'pug');
app.set('views', path.join(__dirname + '/api/html/', 'views'));

app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "*");
	next();
});
app.use(cors({
	origin: 'http://apps.service.lokal'
}));

require('./router')(app);

app.listen(port, () => {
	// eslint-disable-next-line no-console
	console.log(`server is started on port ${port}`);
});
