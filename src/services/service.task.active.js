'use strict';

const RepoSys = require('../repository/repo.sys');
const logger = require('../helper/util/logger.util');
// const TAG = 'service.task.aktiv.';

class Index {
    /**
     * Prüft, ob der Job schon als laufend in der db registriert ist.
     * isFull bestimmt, ob hinter dem TAG als JobName noch ein '.delta' oder '.full' gesetzt wird.
     * Wenn ja: wird true zurückgegeben.
     * Wenn nein: wird false zurückgegeben und der Job in der db als laufend registriert.
     * Als Parameter kann eine Schutzzeit in Stunden Übergeben werden.
     *
     * @param isFull
     * @param job
     * @param hour
     * @return {Promise<boolean>}
     * */
    async isRun(job, isFull = null, hour = 4) {
        let jobName;
        switch (isFull) {
            case true:
                jobName = job + '.full';
                break;
            case false:
                jobName = job + '.delta';
                break;
            default:
                jobName = job;
                break;
        }

        let repoSys = new RepoSys();
        let sys = await repoSys.get(jobName);
        let date = new Date();

        /** Die übergebene Zeit in Stunden abziehen */
        date.setHours( date.getHours() - hour);

        /**
         * Wenn:
         * - kein Ergebnis zurückkommt
         * - isUploadPictureToCanto = true
         * - isUploadPictureToCanto Änderung älter als 4 Stunden
         * */
        if (
            (sys !== null
                && (sys.dataValues.status
                && sys.dataValues.updatedAt >= date)
            )
        ) {
            /** Der Job läuft schon */
            logger.log(jobName, 'is active' ,
                'Der Job "' + jobName + '" läuft schon, deshalb wird dieser Aufruf abgebrochen.');

            return true;
        }

        /** Den Job als aktiv in der DB kennzeichnen */
        if (isFull === null) {
            await repoSys.set(jobName, true, null, hour);
        } else {
            await repoSys.set(job + '.delta', true, null, hour);
            await repoSys.set(job + '.full', true, null, hour);
        }

        return false;
    }

    /**
     * @param jobName {string}
     * @param info
     * @return promise
     * */
    async setTheJobToStop(jobName, info= null) {
        /** Den Job als deaktiviert in der db kennzeichnen */
        let dataAccessSys = new RepoSys();

        await dataAccessSys.set(jobName, false, info);
    }
}

module.exports = Index