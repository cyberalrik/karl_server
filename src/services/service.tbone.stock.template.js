'use strict'

const channelService = require('../api/channel/service/channel.service');
let channels = {};

/**
 * Erstellt ein Template was zur Erzeugung der XML Struktur dient.
 *
 * @return {Promise<Array>}
 * */

class Index {
    async calcExportTemplate(list){
        channels = await channelService.getAllChannelStockInStructure();
        return {
            '?xml version="1.0" encoding="UTF-8"?': null,
            TBSTOCK: {
                '#': {
                    ARTICLE: await this.calcArticleStockTemplate(list)
                }
            },
        };
    }

    async calcArticleStockTemplate(list) {
        let data = [];
        if (list === undefined)
            return data;

        for (let i = 0; i < list.length; i++) {
            let item = {
                '#': {
                    A_NR: {
                        '#': list[i]['a_nr']
                    },
                    A_STOCK:
                        {
                            '@': {
                                'identifier': "key",
                                'key': channels[list[i]['channel']]
                            },
                            '#': list[i]['quant'] === null ? '0' : list[i]['quant']
                        }

                }
            };
            data.push(item);

            // /**temporär */
            // item = {
            //     '#': {
            //         A_NR: {
            //             '#': list[i]['a_nr']
            //         },
            //         A_STOCK:
            //             {
            //                 '@': {
            //                     'identifier': "key",
            //                     'key': 'standard'
            //                 },
            //                 '#': '0'
            //             }
            //
            //     }
            // };
            // data.push(item);
            // /** ENDE */
        }
        return data;
    }
}

module.exports = Index
