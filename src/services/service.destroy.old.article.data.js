'use strict';

const articleRepo = require("../repository/article/repo.article");
const articleChannelRepo = require("../repository/article/repo.article.channel");
const articleComponentsRepo = require("../repository/article/repo.article.components");
// const noUpdateRepo = require("../repository/article/repo.article.noupdate");
// const InfoboxRepo = require("../repository/repo.infobox");
const priceListRepo = require("../repository/repo.pricelist");
const logger = require('../helper/util/logger.util');


class Index {
    /**
     * Löscht alle Daten der übergebenen Artikel aus den Tabellen.
     * Gibt ein String aller ArtikelNummern zurück.
     *
     * @param TAG
     * @param oldANumbers
     * @return Promise {<String>}
     * */
    async destroyOldArticleData(TAG, oldANumbers) {
        let result = [];
        let numberArticle = 0;
        let usedArticle = [];
        // let infoboxRepo = new InfoboxRepo();
        for (let a_nr in oldANumbers) {
            usedArticle.push(`${a_nr}`);
            result.push(a_nr);
            if (numberArticle++ === 99) {
                await this.workArticle(TAG, usedArticle);
                numberArticle = 0;
                usedArticle = [];
            }
        }

        if (usedArticle.length > 0) {
            await this.workArticle(TAG, usedArticle);
        }

        return result.join(', ');
    }

    async workArticle(TAG, usedArticle) {
        try {
            /** Löschen aus articles */
            articleRepo.delete(usedArticle);

            /** Löschen von articleChannels */
            articleChannelRepo.delete(usedArticle);

            /** Löschen von articleComponents */
            articleComponentsRepo.delete(usedArticle);

            /** Löschen von noUpdate */
            // noUpdateRepo.delete(a_nr);

            /** Löschen von infobox */
            // Type 1
            // infoboxRepo.delete(a_nr, 1);

            /** Löschen von priceList */
            priceListRepo.delete(usedArticle);
        } catch (e) {
            logger.err(
                TAG,
                `Fehler beim Löschen`,
                `Bei den Artikeln ${usedArticle} ist ein Fehler beim Löschen aufgetreten.`,
                `Aufgetretener Fehler;\n${e.message}`,
            )
        }
    }
}

module.exports = Index;