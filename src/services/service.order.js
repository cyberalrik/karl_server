'use strict'

const logger = require('../helper/util/logger.util');
const TAG = 'service.order';

class index
{
    /**
     * Prüft ob der übergebene String in ein Datum umgewandelt werden kann
     *
     * @param date
     * @returns {Promise<*>} true/ false
     */
    async isDate(date, tag) {
        // let regExp1 = new RegExp('([0-9]{4,4})-([0-9]{2,2})-([0-9]{2,2})');
        // let match1 = await regExp1.exec(date);
        // let regExp2 = new RegExp('([0-9]{4,4})-([0-9]{2,2})-([0-9]{2,2})T([0-9]{2,2}):([0-9]{2,2}):([0-9]{2,2})');
        // let match2 = await regExp2.exec(date);
        //
        // if (Array.isArray(match1) || Array.isArray(match2)) {
        //     return true;
        // }

        let newDate = new Date(date);
        if (
            newDate instanceof Date
            && Number.isInteger(newDate.getDay())
        ) {
            return true;
        }

        logger.err(TAG, 'kein Datumswert', 'In "' + tag + ' aus ' + date)
    }

    /**
     * Gibt ein Datum-Object zurück
     * @param date
     * @returns {Promise<Date>} new Date()
     */
    async getDate(date, tag) {
        if(await this.isDate(date)) {
            return new Date(date);
        }

        logger.err(TAG, 'kein Datumswert', 'In "' + tag + ' aus ' + date)
    }

    /**
     * Prüft ob der übergebene String in ein Integer gewandelt werden kann.
     * Prüft auch, ob der ganze String aus einer Zahl bestand.
     * '3x' -> 3 - hier ist die Quelle unsauber und wird nicht gewandelt
     *
     * @param data
     * @returns {Promise<boolean>} true/ false
     */
    async isInteger(data, tag) {
        if (data === undefined)
            return false;

        let dataInteger = parseInt(data);
        let dataString = dataInteger.toString();

        if (
            isNaN(dataInteger)
            || (data !== dataString)
        ) {
            logger.err(TAG, 'kein Zahlenwert', 'In "' + tag + ' aus ' + data)
            return false;
        }
        return true;
    }

    /**
     * Gibt einen Integer zurück
     *
     * @param data
     * @returns {Promise<number>} Int
     */
    async getInteger(data, tag) {
        if (await this.isInteger(data)) {
            return parseInt(data)
        }

        logger.err(TAG, 'kein Zahlenwert', 'In "' + tag + ' aus ' + data)
    }

    /**
     * Prüft ob der übergebene String in ein Float (Gleitkommazahl) gewandelt werden kann.
     * Prüft auch, ob der ganze String aus einer Zahl bestand.
     * '3x23.12' -> 3 - hier ist die Quelle unsauber und wird nicht gewandelt
     *
     * @param data
     * @returns {Promise<boolean>} true/ false
     */
    async isFloat(data, tag) {
        if (data === undefined)
            return false;

        let dataFloat = parseFloat(data);
        // let dataString = dataFloat.toString();

        if (
            isNaN(dataFloat)
            // || (data !== dataString)
        ) {
            logger.err(TAG, 'kein Dezimalzahlenwert', 'In "' + tag + ' aus ' + data)

            return false;
        }

        return true;
    }
    /**
     * Gibt einen Integer zurück
     *
     * @param data
     * @returns {Promise<number>} Int
     */
    async getFloat(data, tag) {
        if (await this.isFloat(data)) {
            return parseFloat(data)
        }

        logger.err(TAG, 'kein Dezimalzahlenwert', 'In "' + tag + ' aus ' + data)
    }

}

module.exports = index;