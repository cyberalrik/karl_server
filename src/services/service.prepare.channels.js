'use strict';

const channel = require('../repository/repo.channel');

class Index {
    /**
     * Bereitet die zurück gegebenen Daten in ein Assoziative Arrays auf.
     * Gibt alle deaktivierten channel_key's zurück.
     *
     * @return {Promise<Array>}
     * */
    async getPrepareDeactivateChannelsKey() {
        let result = [];
        let channels = await channel.getAllDeactivateChannelKey();

        for (let index in channels) {
            result[channels[index].channel_key] = true;
        }

        return result;
    }
}

module.exports = Index;