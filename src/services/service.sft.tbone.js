'use strict';

const o2x = require('object-to-xml');
const tboneConfig = require('../configs/tbone.config');
const appConfig = require('../configs/app.config').getConfig();
const logger = require('../helper/util/logger.util');
const Mail = require('../services/service.send.mail');
let TAG = 'service.sft.tbone';

const fs = require('fs');

const Client = require('ssh2-sftp-client');

const server = tboneConfig.SERVER;
const user = tboneConfig.USER;
const password = tboneConfig.PASSWORD;
const path = require('path');

/**
 *
 * @param object
 * @param type
 * @param tag
 * @returns {Promise<{}>}
 */
exports.sendToTbOne = async function(object, type, tag) {
	TAG = tag;
	let result = {
		successful: true,
		message: '',
		xmlFile: ''
	}

	if (!appConfig.ACTIVE_TBONE_UPLOAD) {
		result.successful = false;
		result.message = 'Der Upload zu TB.One ist deaktiviert.';

		return result;
	}

	const xml = o2x(object);

	let filename = getFileName(type, appConfig.TEST);
	// let pathFileName = '/Users/Andy/Downloads/' + filename + '.xml';
	let pathFileName = appConfig.FS_WRITE_FILE_PATH + filename + '.xml';
	// let pathFileName = '/' + filename + '.xml';

	await fs.writeFile(pathFileName, xml.toString(), async function(err) {
		if(err) {
			logger.err(TAG, 'fs', 'XML kann nicht geschrieben werden');
			return;
		}
		const sftp = new Client();
		await sftp.connect({
			host: server,
			username: user,
			password: password,
		}).then(async () => {
			await sftp.put(pathFileName, '/in/'+ filename + '.tmp', null, null);
		}).then(async () => {
			await sftp.rename('/in/'+ filename + '.tmp', '/in/'+ filename + '.xml');
		}).then(async () => {
			await fs.unlinkSync(pathFileName);
		}).catch((err) => {
			if (err !== undefined) {
				if (!(err.message === undefined || err.message === null)) {
					logger.err(TAG, 'sendToTbOne-1', err.message);
				} else {
					logger.log(TAG, 'sendToTbOne-2', 'kein "err.message" vorhanden.');
				}

				try {
					let errorJson = JSON.stringify(err) + '\n ' + err.toString();
					let mail = new Mail();
					mail.sendEMail('Übertragungsfehler an TB.One', 'Übertragungsfehler an TB.One', errorJson, TAG)
					logger.err(TAG, 'sendToTbOne-3', `Datei ${filename} konnte nicht übertragen werden.`);
				} catch (e) {
					logger.log(TAG, 'sendToTbOne-4', `Fehler aufgetreten: ${e.message}`);
				}
			} else {
				logger.log(TAG, 'sendToTbOne-5', `err ist undefined`);
			}


			if (appConfig.BACKUP_DATA) {
				/** Dateien von fehlgeschlagenen Übertragungen werden gesichert. */
				let newPathFileName = path.join(appConfig.SERVER_URL, filename + '.xml');
				fs.writeFile(newPathFileName, xml.toString(),
					async function () { //err) {
						logger.err(TAG, 'sendToTbOne-6', `Datei ${newPathFileName} wurde gesichert.`);
					}
				);
			}
		});
	});

	result.xmlFile = pathFileName;

	return result;
};

/***
 *
 * @param type
 * @param isTest
 * @returns {string}
 */
function getFileName(type, isTest){
	let result = '';
	let today = new Date();
	let dateString = `${today.getFullYear()}${
		(today.getMonth() + 1).toString().padStart(2, '0')}${
		today.getDate().toString().padStart(2, '0')}_${
		today.getHours().toString().padStart(2, '0')}${
		today.getMinutes().toString().padStart(2, '0')}${
		today.getSeconds().toString().padStart(2, '0')}_${
		today.getMilliseconds()}`;

	// if (type === tboneConfig.EXPORT_ARTICLE_ID){
	// 	result = tboneConfig.EXPORT_ART;
	// } else if (type === tboneConfig.EXPORT_STOCK_ID){
	// 	result = tboneConfig.EXPORT_STOCK;
	// }

	switch (type) {
		case tboneConfig.EXPORT_PRODUCT_ID:
			result = tboneConfig.EXPORT_PRODUCT;
			break;

		case tboneConfig.EXPORT_STOCK_ID:
			result = tboneConfig.EXPORT_STOCK;
			break;

		case tboneConfig.EXPORT_PRICE_ID:
			result = tboneConfig.EXPORT_PRICE;
			break;

	}

	if (isTest){
		result = result + tboneConfig.EXPORT_TEST;
	}

	result = result + dateString;
	return result;
}