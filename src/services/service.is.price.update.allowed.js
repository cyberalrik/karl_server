'use strict';

const ServicePrepareArticleNoUpdate = require('./service.prepare.article.no.update');
const ServicePrepareArticleChannel = require('./service.prepare.article.channels');
const ServicePrepareChannel = require('./service.prepare.channels');

class Index {
    /**
     * Bedingungen:
     * 1. Die a_nr darf nicht in der Tabelle "NoUpdate" enthalten sein.
     *
     * 2. Die Kombination aus 'a_nr' und 'channel' des Preisupdates,
     * darf keinen deaktivierten (is_active = 0) Datensatz in der Tabelle 'articleChannels' enthalten.
     *
     * 3. Der Kanal darf nicht deaktiviert sein.
     *
     * Die assoziativen Arrays 'articleNoUpdateData', 'articleChannel' und 'deactivateChannelsKey'
     * enthalten alle Datensätze für ein Ausschlusskriterium.
     * Passt eines überein, ist also nicht 'undefined' darf es nicht übernommen werden.
     *
     * @param priceUpdate
     * @return {Promise<Array>}
     * */
    async getAllApproved(priceUpdate) {
        let result = [];

        let servicePrepareArticleNoUpdate = new ServicePrepareArticleNoUpdate();
        let articleNoUpdateData = await servicePrepareArticleNoUpdate.getPrepareArticleNoUpdate();

        /** Wenn ein Artikel für einen Channel deaktiviert wurde, dürfen nur noch 0,00 Preise übergeben werden. */
        let servicePrepareArticleChannel = new ServicePrepareArticleChannel();
        let articleChannel =  await servicePrepareArticleChannel.getPrepareDeactivateArticleChannels();
        for (let i = 0; i < priceUpdate.length;i++){
            if (
                articleChannel['a_nr-' + priceUpdate[i]['a_nr'] + '-c-' + priceUpdate[i].channel] !== undefined
                && priceUpdate[i].getDataValue('price') !== '0'
            ) {
                priceUpdate[i].setDataValue('price', 0);
            }
        }

        let servicePrepareChannel = new ServicePrepareChannel();
        let deactivateChannelsKey = await servicePrepareChannel.getPrepareDeactivateChannelsKey();

        for (let i = 0; i < priceUpdate.length;i++){
            if (
                articleNoUpdateData['a_nr-' + priceUpdate[i]['a_nr']] === undefined
                && deactivateChannelsKey[priceUpdate[i].channel] === undefined
            ){
                result.push(priceUpdate[i]);
            }
        }

        return result
    }
}

module.exports = Index;