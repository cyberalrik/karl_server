'use strict'

const nodemailer = require('nodemailer');
const MailConfig = require('../configs/mail.config');
const appConfig = require('../configs/app.config').getConfig();

class Index {

    async sendEMail(subject,
              short,
              mailBody = 'Es wurden keine Informationen übergeben',
              tag = '-1',
              mailBodyXXL = '',
              addMailTo = null,
              addMailCc = null,
              isHtml= false,
              mailBodyHTML = '',
              attachments = {}
        ){
        let htmlText = '';
        if (appConfig.TEST) {
            subject = 'Aus Testumgebung - ' + subject
        }

        /** Wenn ein längerer BodyText übergeben wurde, soll vorher ein Absatz sein. */
        if (mailBodyXXL === null) {
            mailBodyXXL = '';
        }

        if (mailBodyXXL !== '') {
            mailBodyXXL = `\n\n${mailBodyXXL}`;
        }

        if (isHtml) {
            htmlText = `<h2>${short}</h2><div>${mailBodyHTML}</div>`;
        }
        /**  5 minuten warten. ??? await geht nicht */
        // await new Promise(resolve => setTimeout(resolve, 300000));

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: MailConfig.host,
            port: MailConfig.port,
            secure: MailConfig.secure,
            auth: {
                user: MailConfig.eMailAccount,
                pass: MailConfig.eMailAccountPassword,
            },
        });

        addMailTo += ',' + MailConfig.to;

        /** Um in die richtige Log-Zeit zu springen */
        let date1 = new Date();
        let date2 = new Date();
        date1.setMinutes(date1.getMinutes() - 1);
        date2.setMinutes(date2.getMinutes() + 1);
        let startDate = Math.floor(date1 / 1000)
        let endDate = Math.floor(date2 / 1000)

        // send mail with defined transport object
        let mailMessage = {};
            mailMessage.from = `"KARL" <${MailConfig.from}>'`; // sender address
            mailMessage.to = addMailTo;
            mailMessage.subject = `[KARL] - ${subject}`;
            if (attachments.isAttachments) {
                mailMessage.attachments = attachments.value;
            }

        // language=HTML
        let stylesheet = `<style>
table, th, td, caption {
  /*border: 1px solid royalblue;*/
  border: none;
}

table {
  border-collapse: collapse;
  margin: 0 0 0 0;
  max-width: 100%;
}

th, td {
  font-weight: normal;
  text-align: left;
  margin: 0 10px 0 10px;
}

th, caption {
  background-color: #f1f3f4;
  font-weight: 700;
}

tbody tr:nth-child(even) {
  background-color: #e4ebf2;
  color: #000;;
}

h3 {
    margin-top: 10px;
    margin-bottom: 2px;
}

.numbers {
  text-align: right;
}

.zentriert {
    text-align: center;
}

hr {
      width: 70%;
      margin-left: auto;
      margin-right: auto;
}

</style>`;

        if (isHtml) {
            mailMessage.html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"' +
                '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n' +
                '<html xmlns="http://www.w3.org/1999/xhtml" lang="de">\n' +
                '<head>\n' +
                '  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\n' +
                '  <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n' +
                `${stylesheet}` +
                `  <title>${mailMessage.subject}</title>\n` +
                '</head>\n' +
                '<body style="margin: 0; padding: 0;">\n' +
                '  <table>\n' +
                '    <tr>\n' +
                '      <td>\n';
            mailMessage.html += htmlText;
            mailMessage.html += '<hr /><tr>\n' +
                '  <td style="font-size: 14px; color: #1d1dfe;">\n' +
                // '    <b>Bitte auch immer noch in den Logs nachsehen.</b>\n' +
                `    <a href="${appConfig.DOMAIN}:${appConfig.PORT}/logs/0/25/-1/${tag}/-1/-1/${startDate}/${
                endDate}/">Bitte auch immer noch in den Logs nachsehen.</a>\n` +
                '  </td>' +
                '</tr>';
            mailMessage.html += '</td>' +
                '    </tr>' +
                '  </table>' +
                '</body>';
        } else {
            mailMessage.text = `${short}\n${mailBody}${mailBodyXXL}\n\nBitte auch immer noch in den Logs nachsehen.\n${
                appConfig.DOMAIN}:${appConfig.PORT}/logs/0/25/-1/${tag}/-1/-1/${startDate}/${endDate}/`;
        }

        if (addMailCc !== undefined && addMailCc !== null) {
            mailMessage.cc = addMailCc;
        }

        transporter.sendMail(mailMessage).then();
    }
}

module.exports = Index;