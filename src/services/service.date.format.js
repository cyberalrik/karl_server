'use strict'

class index
{
    /**
     * Gibt das übergebene Datum formatiert als String zurück
     *
     * @param date
     * @returns Promise {<string>}
     */
    async getDateFormatted(date) {
        let result;
        if (Object.prototype.toString.call(date) === "[object Date]") {
            // it is a date
            if (isNaN(date.getTime())) {  // d.valueOf() could also work
                result = 'Das Datum ist nicht gültig';
            } else {
                // date is valid
                result = date.getDate().toString().padStart(2, '0')
                    + '.' + (date.getMonth()+1).toString().padStart(2, '0')
                    + '.' + date.getFullYear().toString().padStart(2, '0')
                    + ' ' + date.getHours().toString().padStart(2, '0')
                    + ':' + date.getMinutes().toString().padStart(2, '0')
                    + ':' + date.getSeconds().toString().padStart(2, '0');
            }
        } else {
            result = 'Es ist kein Datum';
        }

        return result;
    }

    /**
     * Gibt das übergebene Datum formatiert als String zurück
     *
     * @param date
     * @returns Promise {<string>}
     */
    async getUtcDateFormatted(date) {
        let result;
        if (Object.prototype.toString.call(date) === "[object Date]") {
            // it is a date
            if (isNaN(date.getTime())) {  // d.valueOf() could also work
                result = 'Das Datum ist nicht gültig';
            } else {
                // date is valid
                let utcDate = new Date(
                    Date.UTC(
                        date.getFullYear(),
                        date.getMonth(),
                        date.getDate(),
                        date.getHours(),
                        date.getMinutes(),
                        date.getSeconds()
                    )
                );
                result = utcDate.getDate().toString().padStart(2, '0')
                    + '.' + (utcDate.getMonth()+1).toString().padStart(2, '0')
                    + '.' + utcDate.getFullYear().toString().padStart(2, '0')
                    + ' ' + utcDate.getHours().toString().padStart(2, '0')
                    + ':' + utcDate.getMinutes().toString().padStart(2, '0')
                    + ':' + utcDate.getSeconds().toString().padStart(2, '0');
            }
        } else {
            result = 'Es ist kein Datum';
        }

        return result;
    }

    /** gibt das datum von heute ohne Zeit zurück im Format 'yyyy-mm-dd' */
    async getStringDate() {
        let date = new Date();
        let fullYear = date.getFullYear().toString();
        let month = (date.getMonth() + 1 ).toString().length === 1 ? '0' + (date.getMonth() + 1 ).toString() : (date.getMonth() + 1 ).toString();
        let day = date.getDate().toString().length === 1 ? '0' + date.getDate().toString() : date.getDate().toString();

        return fullYear + '-' + month + '-' + day;
    }

    /** Gibt einen String zurück */
    async getStringDateWithTime() {
        let date = new Date();
        let fullYear = date.getFullYear().toString();
        let month = (date.getMonth() + 1).toString().padStart(2, '0');
        let day = date.getDate().toString().padStart(2, '0');
        let hours = date.getHours().toString().padStart(2, '0');
        let minutes = date.getMinutes().toString().padStart(2, '0');
        let seconds = date.getSeconds().toString().padStart(2, '0');

        return fullYear + month + day + '-' + hours + minutes + seconds;
    }

    async getStringDateAddDayAndHours(day = 0, hours = 0) {
        let date = new Date();
        date.setDate(date.getDate() + day);
        date.setHours(date.getHours() + hours); // Unterschied zur DB

        return await this.getStringFromDate(date);
    }

    async getStringFromDate(date) {
        return `${
            date.getFullYear()}-${
            (date.getMonth() + 1).toString().padStart(2, '0')}-${
            date.getDate().toString().padStart(2, '0')} ${
            date.getHours().toString().padStart(2, '0')}:${
            date.getMinutes().toString().padStart(2, '0')}:${
            date.getSeconds().toString().padStart(2, '0')}`;
    }

    async getStringDateAddDayAndHoursGermanFormat(day = 0, hours = 0) {
        let date = new Date();
        date.setDate(date.getDate() + day);
        date.setHours(date.getHours() + hours); // Unterschied zur DB

        return await this.getStringFromDateGermanFormat(date);
    }


    async getStringFromDateGermanFormat(date) {
        return `${
            date.getDate().toString().padStart(2, '0')}.${
            (date.getMonth() + 1).toString().padStart(2, '0')}.${
            date.getFullYear()} ${
            date.getHours().toString().padStart(2, '0')}:${
            date.getMinutes().toString().padStart(2, '0')}:${
            date.getSeconds().toString().padStart(2, '0')}`;
    }

}

module.exports = index;