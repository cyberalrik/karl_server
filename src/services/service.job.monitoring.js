'use strict';

const Stopwatch = require('statman-stopwatch');
const TimeHelper = require('../refac/helper/time');
const TaskActive = require('../services/service.task.active');
const logger = require('../helper/util/logger.util');

/**
 * Hiermit wird eine Zeitspanne gemessen, die eine Aktion gebraucht hat.
 * */
class Index {
    /**
     * Als TAG wird der JobName übergeben.
     * isFull bestimmt, ob hinter dem TAG als JobName noch ein '.delta' oder '.full' gesetzt wird.
     * Als hour die Stunden, die ein laufender Process nicht dauern darf und danach als abgestürzt gilt.
     *
     * @param TAG
     * @param isFull
     * @param hour
     * */
    constructor(TAG, isFull = null, hour) {
        this.isFull = isFull;
        this.tag = TAG;
        this.tagForRun = TAG;
        this.sw = new Stopwatch()
        this.hour = hour;
    }

    /** Zeit wird gemessen ab jetzt */
    async start() {
        let result = {
            successful : true,
            isActive: false
        };

        await this.addJobName();

        this.taskActive = new TaskActive();

        if (await this.taskActive.isRun(this.tagForRun, this.isFull, this.hour)) {

            result.messages = 'Der Job ist schon aktiv und wird nicht ein zweites mal aufgerufen.'
            result.isActive = true;

            return result;
        }

        logger.log(this.tag, 'Job started');
        this.sw.start();

        return result;
    }

    /**
     * Zeit wird gestoppt und der Job wird als beendet gekennzeichnet.
     * */
    async stop(logging = true) {
        this.sw.stop();
        const delta = this.sw.read();
        const time = new TimeHelper();

        if (logging) {
            logger.log(this.tag, 'Job closed');
        }

        switch (this.isFull) {
            case true:
                await this.taskActive.setTheJobToStop(this.tagForRun + '.delta');
                await this.taskActive.setTheJobToStop(this.tagForRun + '.full', time.timeConversionFromMilliSec(delta));
                break;
            case false:
                await this.taskActive.setTheJobToStop(this.tagForRun + '.full');
                await this.taskActive.setTheJobToStop(this.tagForRun + '.delta', time.timeConversionFromMilliSec(delta));
                break;
            default:
                await this.taskActive.setTheJobToStop(this.tagForRun, time.timeConversionFromMilliSec(delta));
                break;
        }

        return  time.timeConversionFromMilliSec(delta);
    }

    /** Der name wird erweitert. */
    async addJobName() {
        switch (this.isFull) {
            case true:
                this.tag += '.full';
                break;

            case false:
                this.tag += '.delta';
                break;
        }
    }
}

module.exports = Index