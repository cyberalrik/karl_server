'use strict';

const articleNoUpdate = require('../repository/article/repo.article.noupdate');

class Index {
    async getPrepareArticleNoUpdate() {
        let result = [];
        let articleNoUpdateData = await articleNoUpdate.getAll();

        for (let index in articleNoUpdateData) {
            result['a_nr-' + articleNoUpdateData[index].a_nr] = true;
        }

        return result;
    }
}

module.exports = Index;