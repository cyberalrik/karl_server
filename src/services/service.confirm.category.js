'use strict'

const CategoryRepo = require('../repository/repo.category');
class index
{
    /**
     * Gibt geprüfte Kategorien als Array zurück
     *
     * @returns Promise {<string>}
     */
    async getConfirmCategoryPrepared() {
        let categoryRepo = new CategoryRepo();
        let confirmedCategory = await categoryRepo.getAllCategory();
        let result = [];
        for (let index in confirmedCategory) {
            let currenCategory = confirmedCategory[index];
            result.push(currenCategory.getDataValue('cat_id'));
        }

        return result;
    }
}

module.exports = index;