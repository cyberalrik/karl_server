'use strict'

const articleRepo = require('../repository/article/repo.article');

const logger = require('../helper/util/logger.util');
const TAG = '';

/**
 * Erstellt ein Template was zur Erzeugung der XML Struktur dient.
 *
 * @return {Promise<Array>}
 * */

class Index {
    async createExportTemplate(list, exportType){
        return {
            '?xml version="1.0" encoding="UTF-8"?': null,
            TBCATALOG: {
                '#': {
                    PRODUCTDATA: {
                        '@': {
                            type: exportType
                        },
                        '#': {
                            PRODUCT: await this.createProductTemplate(list)
                        }
                    }
                },
                '@': {
                    version: '1.2'
                }
            },
        };
    }

    async createProductTemplate(toDo) {
        let data = [];
        for (let i = 0; i < toDo.length; i++) {
            let article = await articleRepo.getArticlePerID(toDo[i]['a_nr']);
            if(article === undefined || article === null || article.p_nr === undefined || article.p_nr === null)
                continue;

            /** Da Amazon nicht über TB verteielt wird, soll hier der Preis auf 0,00€ gesetzt werden. */
            let price = 0
            if (toDo[i]['channel'] !== 'amde') {
                price = await this.createArticlePrice(toDo[i]['price'], toDo[i]['channel'])
            }

            let item = {
                '#': {
                    P_NR: article.p_nr,
                    ARTICLEDATA: {
                        '#': {
                            ARTICLE: {
                                '#': {
                                    A_NR: toDo[i]['a_nr'],
                                    A_PRICEDATA: {
                                        '#': {
                                            A_PRICE: {
                                                '@': {
                                                    channel: toDo[i]['channel'],
                                                    currency: 'EUR',
                                                },
                                                '#': {
                                                    A_VK: price,
                                                    A_MWST: '2'
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                },
            };
            data.push(item);

            // /** Nur temporär */
            // item = {
            //     '#': {
            //         P_NR: article.p_nr,
            //         ARTICLEDATA: {
            //             '#': {
            //                 ARTICLE: {
            //                     '#': {
            //                         A_NR: toDo[i]['a_nr'],
            //                         A_PRICEDATA: {
            //                             '#': {
            //                                 A_PRICE: {
            //                                     '@': {
            //                                         channel: 'amde',
            //                                         currency: 'EUR',
            //                                     },
            //                                     '#': {
            //                                         A_VK: '0',
            //                                         A_MWST: '2'
            //                                     }
            //                                 }
            //                             }
            //                         }
            //                     }
            //                 }
            //             }
            //         },
            //     },
            // };
            // data.push(item);
            // /** ende */

        }
        return data;
    }

    async createArticlePrice(price, channel){
        if (channel === null || channel === undefined || price === undefined || price === null){
            return 0;
        }
        let priceN = parseFloat(price);
        if (priceN < 1){
            if (channel === 'cucybertr1' )
                return 0.01;
            return 0.00;
        }
        return priceN;
    }
}

module.exports = Index
