'use strict';

const articleChannel = require('../repository/article/repo.article.channel');

class Index {
    /**
     * Bereitet die zurück gegebenen Daten in ein Assoziative Arrays auf.
     * Die Daten stammen aus der Tabelle 'articlesChannels'.
     * Es werden nur deaktivierte (is_active = 0) Datensatze benutzt.
     *
     * @return {Promise<Array>}
     * */
    async getPrepareDeactivateArticleChannels() {
        let result = [];
        let activeANrChannel = await articleChannel.getAllDeactivateANrChannel();

        for (let index in activeANrChannel) {
            result['a_nr-' + activeANrChannel[index].a_nr + '-c-' + activeANrChannel[index].channel_key] = true;
        }

        return result;
    }
}

module.exports = Index;