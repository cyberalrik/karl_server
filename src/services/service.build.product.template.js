'use strict';

const articleChannelRepo = require('../repository/article/repo.article.channel');
const channelRepo = require('../repository/repo.channel');
const ComponentsTemplate = require('../refac/use-cases/components/build-export-component-template');
const constants = require('../constants/images.constant');
const logger = require('../helper/util/logger.util');
const MailData = require("../refac/entity/mailData");

// let articleChannels = [];
let channels = [];
let channelMediaType = [];
let thirdParty = false;
let articleComponentData = {
    new_true: 'new',
    new_false: 'new',
    ref_true: 'used_pal',
    ref_false: 'used',
    E2_new_true: 'new_others',
    E2_new_false: 'new_others',
    E2_ref_true: 'second_hand_pal',
    E2_ref_false: 'second_hand',
}
/** Hier wird die articleChannelListe für den hinterlegten Artikel aufgehoben. */
let articleChannelsObj = {
    aNr: '',
    list: []
};

let TAG = '';
class Index {

    /**
     * @param params
     * */
    constructor(params) {
        this.params = params;
        this.mailInfos = [];
    }

    /***
     *
     * @param tag
     * @param list
     * @param exportType
     * @returns abstract xml
     */
    async calcExportTemplate(tag, list, exportType){
        TAG = tag;
        channels = await channelRepo.getChannels();
        channelMediaType = await channelRepo.getAllMediaType();

        return {
            '?xml version="1.0" encoding="UTF-8"?': null,
            TBCATALOG: {
                '#': {
                    PRODUCTDATA: {
                        '@': {
                            type: exportType
                        },
                        '#': {
                            PRODUCT: await this.calcProdTemplate(list)
                        }
                    }
                },
                '@': {
                    version: '1.4'
                }
            },
        };
    }

    /**
     *
     * @param list
     * @returns {Promise<Array>}
     */
    async calcProdTemplate(list) {
        let componentTemplate = new ComponentsTemplate();
        let data = [];
        for (let i = 0; i < list.length; i++) {
            if (await this.isValide(list[i]) !== true) {
                continue;
            }

            thirdParty = list[i].p_3_rd;
            let item = {
                '#': {
                    P_NR: list[i].p_nr,
                    P_NAME: {
                        '#': {
                            VALUE: [
                                {
                                    '@': {
                                        'xml:lang': 'x-default'
                                    },
                                    '#': list[i].p_name
                                }, {
                                    '@': {
                                        'xml:lang': 'en-GB'
                                    },
                                    '#': list[i].p_name
                                }
                            ]
                        },
                    },
                    P_TEXT: {
                        '#': {
                            VALUE: [
                                {
                                    '@': {
                                        'xml:lang': 'x-default'
                                    },
                                    '#': await this.getGermanDescription(list[i])
                                }, {
                                    '@': {
                                        'xml:lang': 'en-GB'
                                    },
                                    '#': list[i].p_text_ee
                                }
                            ]
                        }
                    },
                    P_TEXT_INTERN: {
                        '#': 'Karl v.2'
                    },
                    P_BRAND: {
                        '@': {
                            'identifier': 'key',
                            'key': list[i].p_brand_key,
                            'name': list[i].p_brand
                        },
                    },
                },
            };

            let keywords = await this.calcKeywords(list[i]);
            if (keywords !== null) {
                item['#'].P_KEYWORDS = keywords;
            }

            item['#'].P_COMPONENTDATA= {
                '#': {
                    P_COMPONENT: await componentTemplate.build(list[i].components)
                }
            };

            item['#'].P_CATEGORIES= {
                '#': {
                    P_CATEGORY: {
                        '@': {
                            type: 'internal',
                            identifier: 'key',
                            key: list[i].p_cat_id
                        }
                    }
                }
            };

            item['#'].ARTICLEDATA= {
                '#': {
                    ARTICLE: await this.calcListOfArticle(list[i])
                }
            };

            data.push(item);
        }

        /** Mailauswertung */
        if (this.mailInfos.length > 0) {
            let mailText = this.mailInfos.join('\n');

            logger.err(TAG
                , 'Versandbedingungsschlüssel'
                , `Beim erstellen des Versandbedingungsschlüssel ist ein Fehler aufgetreten.`
                , mailText
            );

            this.mailInfos = [];
        }

        return data;
    }

    async isValide(item) {
        let result = true;

        /** Überprüfen der deutschen Beschreibung */
        // if (item.p_text_de === null
        //     || item.p_text_de === undefined
        //     || item.p_text_de === ''
        // ) {
        //     return false;
        // }

        /** Überprüfen der englischen Beschreibung */
        if (item.p_text_ee === null
            || item.p_text_ee === undefined
            || item.p_text_ee === ''
        ) {
            return false;
        }

        return result;
    }

    /**
     * Gibt die deutsche Beschreibung zurück.
     * Ist diese nicht vorhanden, wird die englische verwendet.
     * So ist die XML-Datei weiterhin valide.
     * */
    async getGermanDescription(product) {
        let result = product.p_text_de;
        let blank = '';
        if (
            !(product.p_text_de !== null
            && product.p_text_de !== undefined
            && product.p_text_de !== '')
        ) {
            result = product.p_text_ee;
            let mailData = new MailData(
                `Deutsche Beschreibung fehlt`,
                `\nBeim erstellen der XML-Datei für TradeByte ist bei dem Produkt mit der \n\nKarl-ProduktId: ${
                    product.p_nr}\nMPN: ${
                    product.p_name}\nHersteller: ${
                    product.p_brand}\n\nein Fehler aufgetreten.\nDie deutsche Beschreibung ist nicht vorhanden.\n${
                    blank}Damit kein Fehler bei der Verarbeitung der XML-Datei entsteht, ${
                    blank}wurde die englische Beschreibung benutzt.`,
                's.wiehe@cybertrading.de',
                'produktmanagement@cybertrading.de'
            );

            await logger.log(TAG,
                `XML-Erstellungsfehler`,
                `Bei dem Produkt ${product} - ${product} ist keine deutsche Beschreibung vorhanden.`,
                mailData);
        }

        return result;
    }

    /***
     * Hier werden die einzelnen Kanäle geprüft, ob der Artikel für sie freigegeben ist.
     * @param article
     * @returns {Promise<[]>}
     */
    async calculateArticleChannelInterface(article){
        let data = [];
        if (article['a_nr'] === undefined) {
            return data;
        }

        // let articleChannels = [];
        if (articleChannelsObj.aNr !== article['a_nr']) {
            articleChannelsObj.aNr = article['a_nr'];
            articleChannelsObj.list = await articleChannelRepo.getArtChannelData(article['a_nr']);
        }
        let articleChannels = articleChannelsObj.list;

        for (let i = 0; i < channels.length; i++){
            let key = channels[i].channel_key;

            if (key === 'default') {
                continue
            }

            let active = true;
            if (!channels[i].channel_is_active){
                active = false;
            } else {
                for (let j = 0; j < articleChannels.length; j++){
                    if (articleChannels[j].channel_key === key){
                        active = articleChannels[j].is_active;
                        break;
                    }
                }
            }

            let item = {
                '@': {
                    channel: key
                },
                '#': active ? '1' : '0'
            };
            data.push(item);
        }
        return data;
    }

    /***
     * Hier wird entschieden, ob der Artikel aktiv oder deaktiviert ist.
     * @param article
     * @returns {Promise<int>}
     */
    async calculateArticleChannelInterfaceComplete(article){
        let result = 0;
        if (article['a_nr'] === undefined) {
            return result;
        }

        // let articleChannels = [];
        if (articleChannelsObj.aNr !== article['a_nr']) {
            articleChannelsObj.aNr = article['a_nr'];
            articleChannelsObj.list = await articleChannelRepo.getArtChannelData(article['a_nr']);
        }
        let articleChannels = articleChannelsObj.list;

        for (let i = 0; i < channels.length; i++){
            let key = channels[i].channel_key;

            if (key === 'default') {
                continue
            }

            result = 1;
            if (!channels[i].channel_is_active){
                result = 0;
            } else {
                for (let j = 0; j < articleChannels.length; j++){
                    if (articleChannels[j].channel_key === key){
                        result = articleChannels[j].is_active ? 1 : 0;
                        break;
                    }
                }
                if (result === 1) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     *
     * @param product
     * @returns {Promise<Array>}
     */
    async calcListOfArticle(product) {
        let articles = product.articles;
        let isPal = await this.isPalette(product);
        let data = [];
        if (articles === undefined)
            return data;

        for (let article of articles) {
            let isVariant = await this.calcVariantsStructure(article, isPal);
            if (isVariant === false) {
                continue;
            }
//            let articleChannelData = await articleChannelRepo.getArtChannelData(articles[i]['a_nr']);
            let item = {
                '#': {
                    A_NR: article['a_nr'],
                    A_ACTIVEDATA: {
                        '#' : {
                            A_ACTIVE: await this.calculateArticleChannelInterface(article)
                        }
                    },
                    A_ACTIVE: await this.calculateArticleChannelInterfaceComplete(article), //1,
                    A_EAN: article['a_ean'],
                    A_PROD_NR: article['a_prod_nr'],
                    A_NR2: article['a_nr2'],
                    A_VARIANTDATA: isVariant,
                    A_COMPONENTDATA:{
                        '#': {
                            A_COMPONENT: await this.calcArticleComponents(article['a_cond'], isPal)
                        }
                    },
                    A_MEDIADATA: {
                        '#': {
                            A_MEDIA: await this.calcMediaDataStructure(article),
                        }
                    },
                    A_DELIVERY_TIME: await this.getDeliveryTime(product, article),
                    A_PARCEL: {
                        '@': {
                            'type' : await this.getDeliveryCategory(product, article), // 'eu_till_25'
                        },
                        '#' : {
                            A_PIECES: 1,
                            A_WIDTH: 60,
                            A_HEIGHT: 60,
                            A_LENGTH: 60,
                            A_WEIGHT: await this.getWeight(product) // 25
                        }
                    }
                }
            };
            data.push(item);
        }
        return data;
    }

    async getWeight(product) {
        return (
                product['p_weight'] !== null
             && product['p_weight'] !== 0
        )
        ? product['p_weight']
        : 25;
    }

    /**
     * Ein Artikel ab 25 kg wird mit Palette versandt.
     * Hier wird zurückgegeben, ob es sich uim Paletten-Ware handelt.
     * */
    async isPalette(product) {
        let gewicht = await this.getWeight(product);

        return gewicht > 25;
    }

    /**
     * Versandart
     * */
    async getDeliveryCategory(product, article) {
        let lizenz = this.params['p_cat_id'];
        let isLizenzArticle = lizenz.includes(product['p_cat_id']);
        let weight = product['p_weight'];
        let isLagerWare = this.isLagerWare(article);
        let deliveryTime = article['a_delivery_time'];

        /** Lizenzen */
        if (isLizenzArticle) {
            let switchValue = `${isLagerWare}_${deliveryTime !== null ? deliveryTime : 5}`;
            return await this.getLizenzDeliveryKey(switchValue, article['a_nr']);
        }

        let switchValue = await this.getSelect(deliveryTime, weight, isLagerWare);

        /** Für E2-Artikel (Lagerware) */
        if (isLagerWare) {
            return await this.getE2ArticleDeliveryKey(switchValue, article['a_nr']);
        }

        /** Der Rest sind Zukauf-Artikel (keine Lagerware) */
        return await this.getLwArticleDeliveryKey(switchValue, article['a_nr']);
    }

    /**
     * Codiert die Suchanfragen.
     * Für Lagerware ist nur das Gewicht wichtig.
     * Hier wird codiert, ob es über 25 kg sind oder nicht.
     * Für alles andere wird die Lieferzeit davor gestellt.
     * */
    async getSelect(deliveryTime = null, weight = 0, isLagerWare = false) {
        let newWeight = weight !== null ? weight : 0
        let isWeightOver25 = `${newWeight > 25}`;

        if (isLagerWare) {
            return isWeightOver25;
        }

        let select = deliveryTime !== null ? deliveryTime : '5';
        select += `_${isWeightOver25}`;

        return select;
    }

    /** gibt den Lizenz-Versandbedingungsschlüssel zurück */
    async getLizenzDeliveryKey(switchValue, a_nr) {
        let result = 'e_delivery_default';
        switch (switchValue) {
            case 'true_2':
            result = 'e_del_2';
                break;
            case 'false_5':
            result = 'e_del_5';
                break;
            case 'false_20':
                result = 'e_del_20';
                break;
            case 'false_40':
                result = 'e_del_40';
                break;
            default:
                console.log(`Aufruf: getLizenzDeliveryKey(${switchValue}, ${a_nr})`);
                this.mailInfos.push(`Aufruf: getLizenzDeliveryKey(${switchValue}, ${a_nr
                    }) - Erlaubte Übergabeparameterinhalte: 'true_2' 'false_5' 'false_20' 'false_40'`
                );
        }

        return result
    }

    /** gibt den Versandbedingungsschlüssel für Lagerware (E2) zurück */
    async getE2ArticleDeliveryKey(switchValue, a_nr) {
        let result = 'lw_delivery_default';
        switch (switchValue) {
            case 'false':
                result = 'LW_2';
                break;
            case 'true':
                result = 'LW_2_pal';
                break;
            default:
                console.log(`Aufruf: getE2ArticleDeliveryKey(${switchValue}, ${a_nr})`);
                this.mailInfos.push(`Aufruf: getE2ArticleDeliveryKey(${switchValue}, ${a_nr
                    }) - Erlaubte Übergabeparameterinhalte: 'false' 'true'`
                );
        }

        return result
    }

    /** gibt den Versandbedingungsschlüssel für Zukaufware (E2) zurück */
    async getLwArticleDeliveryKey(switchValue, a_nr) {
        let result = 'zk_delivery_default';
        switch (switchValue) {
            case '5_false':
                result = 'ZK_5';
                break;
            case '20_false':
                result = 'ZK_20';
                break;
            case '40_false':
                result = 'ZK_40';
                break;
            case '5_true':
                result = 'ZK_5_pal';
                break;
            case '20_true':
                result = 'ZK_20_pal';
                break;
            case '40_true':
                result = 'ZK_40_pal';
                break;
            default:
                console.log(`Aufruf: getLwArticleDeliveryKey(${switchValue}, ${a_nr})`);
                this.mailInfos.push(`Aufruf: getLwArticleDeliveryKey(${switchValue}, ${a_nr
                    }) - Erlaubte Übergabeparameterinhalte: '5_false' '20_false' '40_false' '5_true' '20_true' '40_true'`
                );
        }

        return result
    }

    /**
     * Neue Variante.
     * Die Lieferzeit wird jetzt in Lynn gepflegt und zusammen mit den Artikeldaten auch nach Karl geholt.
     * Wurde das Feld in Lynn gepflegt und es sind in Karl Daten für diesen Artikel enthalten,
     * wird auch dieser Wert übernommen.
     * Ansonsten wird der Standard 5 zurückgegeben.
     *
     * @param product
     * @param article
     * @returns {Promise<Integer>}
     * */
    async getDeliveryTime(product, article) {
        let result = 5;
        let isLagerWare = this.isLagerWare(article);
        let isWeighsUpTo25 = product.p_weight <= 25;
        let lizenz = this.params['p_cat_id'];
        let isLizenzArticle = lizenz.includes(product['p_cat_id']);

        /** Lizenzen */
        if (isLizenzArticle) {
            return 1;
        }

        if (isLagerWare) {
            /**
             * E2-Artikel = Lagerware, soll im eBay+ (Plus) Programm aufgenommen werden
             * und braucht deshalb geänderte DeliveryTime Zeiten (Tage).
             * */

            /** E2 - LW Ware */
            if (isWeighsUpTo25) {
                /** Paket Versand */
                result = 1;
            } else {
                /** Paletten Versand */
                result = 2;
            }
        } else {
            /** Zukaufware */
            if (article.a_delivery_time !== null) {
                /** Ist 'a_delivery_time' gesetzt, dann dies auch benutzen */
                result = article.a_delivery_time;
            } else {
                /**
                 * Wenn 'a_delivery_time' NICHT gesetzt ist,
                 * dann standard Wert benutzen */
                result = 5;
            }
        }

        return result
    }

    /**
     *
     * @param article
     * @returns {Promise<Array>}
     */
    async calcMediaDataStructure(article){
        let data = [];
        let mediaTypeUsed = [];

        for (let mediaType in channelMediaType) {
            mediaTypeUsed[mediaType] = false;
        }

        let mediaCount = [];
        /**
         * Hier werden jetzt alle Bilder zu diesem Artikel einzeln durchlaufen
         * und es wird überprüft, ob das aktuelle Bild zu einem Medientyp passt.
         *
         * Bilder sind immer zu Kanälen freigegeben.
         * Kanäle sind bestimmte Medientypen bei TB zugewiesen.
         * Teilen sich zwei Kanäle, z.B.: eBay.de und eBay.uk, einen Medientyp, z.B.: IMG_EBAY
         * soll dieser Medientyp das Bild nur einmal zugewiesen bekommen.
         * Für eBay dürfen nur 12 Bilder übertragen werden.
         * */
        for (let i = 0; i < article.a_media.length; i++) {
            let currentArticleMediaElement = article.a_media[i];

            if (!currentArticleMediaElement.is_active) {
                continue;
            }
            /** Alle Medientypen durchlaufen  */
            for (let mediaType in channelMediaType) {
                /**
                 * Es gibt Media Typen die nur eine bestimmte Anzahl Bilder erlauben.
                 * z.B. eBay = 12
                 * */
                if (mediaCount[mediaType] === undefined) {
                    mediaCount[mediaType] = [];
                    mediaCount[mediaType]['max_image'] = channelMediaType[mediaType]['max_image'];
                    mediaCount[mediaType]['count_image'] = 0;
                }

                /**
                 * Verschiedene Kanäle benutzen den gleichen Media Typ,
                 * jeder Media Typ sollte aber nur einmal benutzt werden.
                 * */
                let channelName = channelMediaType[mediaType]['channel_key'];
                let channelValue = await constants.getChannelTypeValue(channelName);
                let notForbidden = await constants.isNotForbidden(channelName, thirdParty);

                /**
                 * Ist das Bild für diesen Kanal freigegeben?
                 * Und ist die maximale Anzahl an Bildern für den jeweiligen Channel noch nicht erreicht.
                 * */
                if (
                    (currentArticleMediaElement.channel & channelValue) === channelValue
                    && mediaCount[mediaType]['count_image'] < mediaCount[mediaType]['max_image']
                    && notForbidden
                ) {
                    let item = {
                        '@': {
                            'type': mediaType,
                            'sort': currentArticleMediaElement.image_pos
                        },
                        '#': currentArticleMediaElement.image_url
                    };

                    mediaCount[mediaType]['count_image']++;

                    mediaTypeUsed[mediaType] = true;

                    data.push(item);
                }
            }
        }
        /** Ist für den ImageTyp kein Bild vorhanden gewesen, muss das Standardbild eingesetzt werden. */
        for (let mediaType in mediaTypeUsed) {
            if (!mediaTypeUsed[mediaType]) {
                let item = {
                    '@': {
                        'type': mediaType,
                        'sort': '0'
                    },
                    '#': await constants.getDefaultMediaTypeLink(mediaType)
                };
                data.push(item);
            }
        }

        return data;
    }

    /**
     *
     * @returns {Promise<Array|*>}
     * @param a_cond
     * @param isPal
     */
    async calcArticleComponents(a_cond, isPal){
        let data = [];
        if (a_cond === undefined || a_cond === '')
            return data;

        let item = {
            '@': {
                identifier: 'key',
                key: 'ebay_condition'
            },
            '#': {
                VALUE: {
                    '@': {
                        'xml:lang': 'x-default'
                    },
                    '#': articleComponentData[`${a_cond}_${isPal}`]
                }
            }
        }
        data.push(item);

        return data;
    }

    /**
     *
     * @param a_nr
     * @returns {Promise<Array|*>}
     */
//    async calcArticleComponentsOld(a_nr){
//        let data = [];
//        let components = await articleComponent.getAllComponentPerID(a_nr);
//        if (components === undefined)
//            return data;
//
//        for (let i = 0; i < components.length; i++) {
//            let item = {
//                '@': {
//                    identifier: 'key',
//                    key: components[i]['a_component_key']
//                },
//                '#': {
//                    VALUE: {
//                        '@': {
//                            'xml:lang': 'x-default'
//                        },
//                        '#': components[i]['a_component_value']
//                    }
//                }
//            };
//            data.push(item);
//        }
//        if (data.length === 1){
//            return data[0];
//        }
//        return data;
//    }

    /**
     *
     * @returns {Promise<Array|*>}
     * @param product
     */
    async calcKeywords(product){
        let result = null
        let data = [];

        if (product.p_keyword1 !== '' && product.p_keyword1 !== null)
            data.push(await this.getKeywordInForm(product.p_keyword1));

        if (product.p_keyword2 !== '' && product.p_keyword1 !== null)
            data.push(await this.getKeywordInForm(product.p_keyword2));

        if (product.p_keyword3 !== '' && product.p_keyword1 !== null)
            data.push(await this.getKeywordInForm(product.p_keyword3));

        if (data.length > 1) {
            result = {
                '#': {
                    P_KEYWORD: data
                }
            }
        }

        return result;
    }

    async getKeywordInForm(keyword) {
        return {
            '#': {
                VALUE: {
                    '@': {
                        'xml:lang': 'x-default'
                    },
                    '#': keyword
                }
            }

        }
    }
    /**
     *
     * @param a_cond
     * @param isPal
     * @returns {boolean|{de: string, en: string}}
     */
    async calcConditioning(a_cond, isPal) {
        let result = {};
        switch (a_cond) {
            case 'new':
                result = {
                    de: 'neu',
                    en: 'new'
                }
                break;
            case 'E2_new':
                result = {
                    de: 'neu_sonstige',
                    en: 'new_others'
                }
                break;
            case 'ref':
                if (isPal) {
                    result =  {
                        de: 'generalüberholt_pal',
                        en: 'refurbished_pal'
                    }
                } else {
                    result =  {
                        de: 'generalüberholt',
                        en: 'refurbished'
                    }
                }
                break;
            case 'E2_ref':
                if (isPal) {
                    result =  {
                        de: 'gebraucht_pal',
                        en: 'second_hand_pal'
                    }
                } else {
                    result =  {
                        de: 'gebraucht',
                        en: 'second_hand'
                    }
                }
        }

        return result;
    }

    /**
     *
     * @param art
     * @param isPal
     * @returns {boolean|Array}
     */
    async calcVariantsStructure(art, isPal) {
        let data = [];
        if (art === undefined)
            return data;
        let conditioning = await this.calcConditioning(art.a_cond, isPal);
        if (conditioning === false)
            return false;

        let item = {
            '#': {
                A_VARIANT: {
                    '@': {
                        identifier: 'key',
                        key: 'condition',
                    },
                    '#': {
                        VALUE: [
                            {
                                '@': {
                                    'xml:lang': 'x-default'
                                },
                                '#': conditioning.de
                            }, {
                                '@': {
                                    'xml:lang': 'en-GB'
                                },
                                '#': conditioning.en
                            },
                        ]
                    }
                }
            }
        };
        data.push(item);
        return data;
    }

    /**
     * Hier wird zurückgegeben, ob es sich um Lager- oder Zukaufware handelt.
     * return
     * - true für Lagerware
     * - false für Zukaufware
     *
     * @param article
     * @return {boolean}
     * */
    isLagerWare(article) {
        return article.a_nr.substring(0,3) === 'E2-';
    }
}

module.exports = Index;