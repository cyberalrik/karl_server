'use strikt'

class Index {
    /**
     * Hierüber können Tasks außerhalb der Cron-Verwaltung aufgerufen werden.
     *
     * @Param {string} taskName
     * @Return {boolean}
     * */
    async taskRun(taskName) {
        let result = {successful: true, taskResult: {}};
        let task;
        let Task;

        switch (taskName) {
            case 'task.karl.article.deactivate':
                Task = require('../tasks/task.karl.article.deactivate')
                task = new Task();
                break;
            case 'task.cci.prepare.components':
                Task = require('../tasks/task.cci.prepare.components')
                task = new Task();
                break;
            case 'task.karl.collect.all.delta':
                task = await require('../tasks/task.karl.collect.all.delta');
                break;
            case 'task.karl.collect.all.full':
                task = await require('../tasks/task.karl.collect.all.full');
                break;
            case 'task.karl.cci.ean':
                Task = await require('../tasks/task.karl.cci.ean');
                task = new Task();
                break;
            case 'task.karl.cci.category':
                Task = await require('../tasks/task.karl.cci.category');
                task = new Task();
                break;
            case 'task.karl.collect.components.delta':
                task = await require('../tasks/task.karl.collect.components.delta');
                break;
            case 'task.karl.collect.components.full':
                task = await require('../tasks/task.karl.collect.components.full');
                break;
            case 'task.karl.collect.manufacture':
                task = await require('../tasks/task.karl.collect.manufacture');
                break;
            case 'task.karl.collect.pricelist.delta':
                task = await require('../tasks/task.karl.collect.pricelist.delta');
                break;
            case 'task.karl.collect.pricelist.full':
                task = await require('../tasks/task.karl.collect.pricelist.full');
                break;
            case 'task.karl.image.quality':
                task = await require('../tasks/task.karl.image.quality');
                break;
            case 'task.karl.images':
                task = await require('../tasks/task.karl.images');
                break;
            case 'task.tbone.all.full':
                task = await require('../tasks/task.tbone.all.full');
                break;
            case 'task.tbone.all.delta':
                task = await require('../tasks/task.tbone.all.delta');
                break;
            // case 'loadOrderData':
            // 	task = require('../../../tasks/task.tbone.loadOrderData');
            // 	break;
            case 'task.tbone.price.delta':
                task = await require('../tasks/task.tbone.price.delta');
                break;
            case 'task.tbone.price.full':
                task = await require('../tasks/task.tbone.price.full');
                break;
            case 'task.tbone.stock.delta':
                task = await require('../tasks/task.tbone.stock.delta');
                break;
            case 'task.tbone.stock.full':
                task = await require('../tasks/task.tbone.stock.full');
                break;
            case 'task.lynn.image.link':
                Task = await require('../tasks/task.lynn.image.link');
                task = new Task();
                break;
            case 'task.overall.run':
                Task = await require('../tasks/task.overall.run');
                task = new Task();
                break;
            case 'task.processing.of.new.items':
                Task = await require('../tasks/task.processing.of.new.items');
                task = new Task();
                break;
            case 'task.karl.images.feature.lynn':
                Task = await require('../tasks/task.karl.images.feature.lynn');
                task = new Task();
                break;
            case 'task.check.lynn.products':
                Task = await require('../tasks/task.check.lynn.products');
                task = new Task();
                break;
            case 'task.karl.collect.all.e2':
                Task = await require('../tasks/task.karl.collect.all.e2');
                task = new Task();
                break;
            case 'task.lynn.category':
                Task = await require('../tasks/task.lynn.category');
                task = new Task();
                break;
            case 'task.lynn.ean':
                Task = await require('../tasks/task.lynn.ean');
                task = new Task();
                break;
            /**
             * Diese Klasse wird nach der Strukturänderung der DB (erp.ProductPropertyLink)
             * von ArticleForeignId hinzu ProductForeignId nicht mehr benötigt.
             * */
            // case 'task.lynn.missing.link':
            //     Task = await require('../tasks/task.lynn.missing.link');
            //     task = new Task();
            //     break;
            case 'task.karl.cleanup.logs':
                Task = await require('../tasks/task.karl.cleanup.logs');
                task = new Task();
                break;
            case 'task.karl.article.activate':
                Task = await require('../tasks/task.karl.article.activate');
                task = new Task();
                break;
            default:
                result.successful = false;
                break;
        }

        if (result.successful)
        {
            result.taskResult = await task.cronTask();
        }

        return result;
    }

}

module.exports = Index;