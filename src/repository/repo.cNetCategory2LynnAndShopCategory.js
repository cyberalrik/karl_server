'use strict';

const config = require('../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const logger = require('../helper/util/logger.util');
const {Op} = require("sequelize");
const TAG = 'repo.cNetCategory2LynnAndShopCategory.';

/***
 * Model
 */
class index {
	constructor() {
		const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

		this.categoryRepo = seq.define('cNetCategory2LynnAndShopCategory', {
			p_cat_id: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			},
			shopCategory: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			},
			shopCategoryId: {
				type: Sequelize.INTEGER,
				primaryKey: false
			},
			lynnCategory: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			},
			lynnCategoryId: {
				type: Sequelize.INTEGER,
				primaryKey: false
			},
			default: {
				type: Sequelize.BOOLEAN
			}
		});
	}

	async getDefaultLynnCategoryId(lynnCategoryId) {
		let result;
		try {
			let existingLynnCategory = await this.categoryRepo.findOne({
				where: {
					lynnCategoryId: lynnCategoryId
				}
			});
			if (existingLynnCategory === null || existingLynnCategory === undefined) {
				return null;
			}
			result = await this.categoryRepo.findOne({
				where: {
					lynnCategory: existingLynnCategory.lynnCategory,
					default: 1
				}
			});
		} catch (e) {
			let test = e;
		}

		return result;
	}

	async getLynnCategoryIdFromShopCategoryId(shopCategoryId) {
		let result = null;
		try {
			let existingShopCategory = await this.categoryRepo.findOne({
				where: {
					shopCategoryId: shopCategoryId
				}
			});
			if (existingShopCategory === null || existingShopCategory === undefined) {
				return null;
			}
			result = existingShopCategory.lynnCategoryId;
		} catch (e) {
			let test = e;
		}

		return result;
	}

	async getAllLynnCategoryAndShopCategories() {
		return await this.categoryRepo.findAll();
	}

	async getAllShopCategoriesFromLynnCategory(lynnCategories) {
		let result;
		result = await this.categoryRepo.findAll({
			attributes: ['p_cat_id'],
			where: {
				lynnCategory: {[Op.in]: [lynnCategories]}
			}
		});

		return result;
	}

	/***
	 * sync-stuff
	 * @returns Promise{<void>}
	 */
	async sync() {
		await this.categoryRepo.sync({alter: true});
		logger.log(`${TAG}sync`, `category is sync`,
			`Das Synchronisieren der Tabelle "category" wurde aufgerufen.`);
	}
}

module.exports = index;