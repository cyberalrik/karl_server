'use strict';

const config = require('../configs/app.config').getConfig();

const Sequelize = require('sequelize');
// const { Op } = require("sequelize");
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../helper/util/logger.util');
const TAG = 'repo.product2productImages.';

class index
{
	/***
	 * Model
	 */
	constructor() {

		this.ImagesConnection = seq.define('product2productImages', {
			p_nr: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			},
			p_nr_image: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			}
		});
	}

	/***
	 * Hier werden die Produktverbindungen angelegt.
	 * Verschiedene Produkte können sich Bilder teilen, da sie genauso aussehen.
	 * Bilder werden aber nur unter einer p_nr gespeichert.
	 * Wobei die p_nr alle Produkte aufzählt die sich das Bild von p_nr_images teilen.
	 * Selbst das eine Produkt zeigt auf sich selber.
	 * Beispiel:
	 * die Produkte '12345' und '67890' benutzen das Bild von '12345'
	 * {
	 *     p_nr: '12345',
	 *     p_nr_images: '12345'
	 * }
	 * {
	 *     p_nr: '67890',
	 *     p_nr_images: '12345'
	 * }
	 *
	 * Durch die Darstellung von in Lynn verlinkten Artikel, können p_nr auf verschiedene p_nr_image zeigen
	 *
	 * @param p_nr
	 * @param p_nr_image
	 * @returns {Promise<>}
	 */
	async addImagesConnection(p_nr, p_nr_image) {
		try {
			let existingConnection = await this.ImagesConnection.findOne({
				where: {
					p_nr: p_nr,
					p_nr_image: p_nr_image
				}
			});
			if (existingConnection === null || existingConnection === undefined) {
				await this.ImagesConnection.create({
					p_nr: p_nr,
					p_nr_image: p_nr_image
				})
			}
		} catch (e) {
			logger.err(TAG, 'Fehler in "addImagesConnection"', `Folgender Fehler: ${e.message}`);
		}
	}

	/***
	 * Hier werden die Produktverbindungen zurückgegeben.
	 * Verschiedene Produkte können sich Bilder teilen, da sie genauso aussehen.
	 *
	 * @param p_nr
	 * @returns {Promise<{}>}
	 */
	async getImagesConnection(p_nr) {
		let result = {
			successful: false
		};

		let existingConnection = await this.ImagesConnection.findAll({
			where: {
				p_nr: p_nr,
			}
		});

		if (existingConnection.length > 0) {
			result.successful = true;
			result.p_nr_imagesArray = [];
			for (let index in existingConnection) {
				result.p_nr_imagesArray.push(existingConnection[index].dataValues.p_nr_image);
			}
		}

		return result;
	}

	/***
	 * Gibt alle Verbindungen zurück
	 *
	 * @returns {Promise<{}>}
	 */
	async getAllImagesConnection() {
		return this.ImagesConnection.findAll();
	}

	/***
	 * Löscht eine nicht mehr benötigte Verbindung
	 *
	 * @returns {Promise<{}>}
	 */
	async deleteImagesConnection(p_nr, p_nr_image) {
		try {
			let existingConnection = await this.ImagesConnection.findOne({
				where: {
					p_nr: p_nr,
					p_nr_image: p_nr_image,
				}
			});
			if (existingConnection !== undefined) {
				existingConnection.destroy();
			}
		} catch (e) {
			logger.err(TAG
				, 'Fehler in "deleteImagesConnection"'
				, 'Die Verbindung zwischen Bild und Produkt konnte nicht aufgehoben werden'
				, `Es handelt sich um folgende Verbindungswerte: p_nr= ${p_nr} and p_nr_image= ${
				p_nr_image}\nHFolgender Fehler: ${e.message}`);
		}
	}

	/***
	 * Löscht eine nicht mehr benötigte Verbindung
	 *
	 * @returns {Promise<{}>}
	 */
	async deleteImagesConnectionByPNrAndPNrImage(p_nr, p_nr_image) {
		let existingConnection = await this.ImagesConnection.findOne({
			where: {
				p_nr: p_nr,
				p_nr_image: p_nr_image,
			}
		});
		if (existingConnection !== undefined) {
			existingConnection.destroy();
		}
	}


	/***
	 * sync-stuff
	 * @returns {Promise<void>}
	 */
	async sync() {
		await this.ImagesConnection.sync({alter: true});
		logger.log(`${TAG}sync`, `product2productImages is sync`,
			`Das Synchronisieren der Tabelle "product2productImages" wurde aufgerufen.`);
	}
}

module.exports = index;