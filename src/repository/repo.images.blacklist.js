'use strict';

const config = require('../configs/app.config').getConfig();
const Sequelize = require('sequelize');
// const Op = Sequelize.Op;
const logger = require('../helper/util/logger.util');
const TAG = 'repo.images.blacklist.';

class index {
	constructor() {
		const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

		this.imagesBlacklist = seq.define('imagesBlacklist', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				allowNull: false,
				autoIncrement: true,
			},
			url: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			},
			description: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			}
		});
	}

	/***
	 * Gibt alle geblacklistete URL's zurück.
	 * Hierbei handelt es sich um Firmenlogos
	 *
	 * @returns Promise {<<Model[]>>}
	 */
	async getAll() {
		return this.imagesBlacklist.findAll();
	}

	/***
	 * Gibt alle geblacklistete URL's in einem aufbereitetem Array zurück.
	 * Hierbei handelt es sich um Firmenlogos
	 *
	 * @returns Promise {<<Array[]>>}
	 */
	async getAllPrepared() {
		let result = [];
		let list = await this.imagesBlacklist.findAll();

		for (let index in list) {
			result[list[index].getDataValue('url')] = true;
		}

		return result;
	}

	/***
	 * sync-stuff
	 * @returns {Promise<void>}
	 */
	async sync() {
		await this.imagesBlacklist.sync({alter: true});
		logger.log(`${TAG}sync`, `imagesBlacklist is sync`,
			`Das Synchronisieren der Tabelle "imagesBlacklist" wurde aufgerufen.`);
	};
}

module.exports = index;
