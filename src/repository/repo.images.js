'use strict';

const config = require('../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const { Op } = require("sequelize");
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../helper/util/logger.util');
const TAG = 'repo.images.';
const constants = require('../constants/images.constant');
const RepoProduct2productImages = require('./repo.product2productImages');

/***
 * Model
 */
const Images = seq.define('images', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		allowNull: false,
		autoIncrement: true,
	},
	p_nr: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	image_url: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	image_pos: {
		type: Sequelize.INTEGER,
		primaryKey: false
	},
	is_active: {
		type: Sequelize.BOOLEAN,
		defaultValue: Sequelize.BOOLEAN,
		primaryKey: false
	},
	version: {
		type: Sequelize.INTEGER,
		comment: '0=cNet\n' +
			'1=Canto'
	},
	channel: {
		type: Sequelize.INTEGER,
		comment: 'Bit-Codierung\0' +
			'0000 = 0 = TB\n' +
			'0001 = 1 = Amazon.de\n' +
			'0010 = 2 = it-market\n' +
			'0100 = 4 = ebay.de\n' +
			'1000 = 8 = ebay.uk\n'
	},
	height: {
		type: Sequelize.INTEGER,
	},
	width: {
		type: Sequelize.INTEGER,
	}
});
// exports.test = async function(????) {
// 	let existingImage = await Images.findAll({
// 		where: Sequelize.where(Sequelize.literal('channel & 2+4'), '!=', 0)
// 	});
// }

/***
 *
 * @param p_nr
 * @param image_url
 * @param image_pos
 * @param is_active
 * @param version
 * @param channel
 * @param size
 * @param manually
 * @returns Promise {<>}
 */
exports.addOrUpdateImage = async (
	p_nr,
	image_url,
	image_pos,
	is_active,
	version,
	channel = null,
	size = {height: 0, width: 0},
	manually
) => {
	if (manually === undefined) {
		manually = false;
	}

	if (version === undefined) {
		version = 0;
	}

	if (channel === null) {
		channel = 0;
	}

	let existingImage = await Images.findOne({
		where: {
			p_nr: p_nr,
			image_pos: image_pos
		}
	});
	if (existingImage === null || existingImage === undefined){
		await Images.create({
			p_nr: p_nr,
			image_url : image_url,
			image_pos: image_pos,
			is_active: is_active,
			version: version,
			channel: channel,
			height: size.height,
			width: size.width
		});

		/**
		 * Für neu angelegt Bilder müssen die Produktverbindungen in der Tabelle
		 * 'product2productImages' angelegt werden.
		 * */
		let repoProduct2productImages = new RepoProduct2productImages();
		await repoProduct2productImages.addImagesConnection(p_nr, p_nr);

	} else {
		/**
		 * Für cNet Bilder darf 'is_active' nicht geändert werden,
		 * es sei denn der Auftrag kommt aus dem FrontEnd aus Karl.
		 * Dann ist manually=true.
		 * */
		if (
			version === constants.IMAGES_CNET_TYPE_VALUE
			&& existingImage.version === constants.IMAGES_CNET_TYPE_VALUE
			&& manually === false
		) {
			if (
				existingImage.image_url !== image_url
				|| existingImage.version !== version
				|| existingImage.channel !== channel
				|| existingImage.height !== size.height
				|| existingImage.width !== size.width
			) {
				if (is_active) {
					/** Wurde jedoch explizit "is_active" = true übergeben, soll es auch gesetzt werden. */
					await existingImage.update({
						image_url: image_url,
						version: version,
						channel: channel,
						height: size.height,
						width: size.width,
						is_active: is_active
					});
				} else {
					await existingImage.update({
						image_url: image_url,
						version: version,
						channel: channel,
						height: size.height,
						width: size.width
					});
				}
			}
		} else {
			if (
				existingImage.image_url !== image_url
				|| existingImage.is_active !== is_active
				|| existingImage.version !== version
				|| existingImage.channel !== channel
				|| existingImage.height !== size.height
				|| existingImage.width !== size.width
			) {
				await existingImage.update({
					image_url: image_url,
					is_active: is_active,
					version: version,
					channel: channel,
					height: size.height,
					width: size.width
				})
			}
		}
	}
};

/***
 * @param p_nr
 * @returns Promise {<<Model<T, T2>[]>>}
 */
exports.getImagesPerProductNumber = async function(p_nr){
	return Images.findAll({
		where: {
			p_nr: p_nr
		},
		order: [
			['image_pos', 'ASC']
		]

	});
};

/***
 * @param p_nr
 * @returns Promise {<<Model<T, T2>[]>>}
 */
exports.getActiveImagesPerProductNumber = async function(p_nr){
	return Images.findAll({
		where: {
			p_nr: p_nr,
			is_active: 1
		},
		order: [
			['image_pos', 'ASC']
		]
	});
};

/***
 * Hat das übergebene Produkt eigene Bilder?
 *
 * @param p_nr
 * @returns Promise {<boolean>}
 */
exports.hasProductImage = async function(p_nr){
	let existingImage = await Images.findAll({
		where: {
			p_nr: p_nr
		}
	});

	return  !(existingImage === null || existingImage === undefined)
};

/***
 * @param p_nr
 * @param pos
 * @returns Promise {<<Model<T, T2>[]>>}
 */
exports.getImagesPerProductNumberAndPos = async function(p_nr, pos){
	return Images.findAll({
		where: {
			p_nr: p_nr,
			image_pos: pos
		}
	});
};

/***
 * @param p_nr
 * @returns Promise {<<Model<T, T2>[]>>}
 */
exports.getImageMaxPosForPNr = async function(p_nr){
	// p_nr = 100044;
	let result = await Images.max('image_pos', {
		where: {
			p_nr: p_nr,
		}
	});

	if (isNaN(result)) {
		result = -1;
	}

	return result;
};

/***
 * Hilft beim Austauschen der Position von Bildern.
 *
 * @param id
 * @param pos
 * @returns Promise {<<Model<T, T2>[]>>}
 */
exports.setForIdNewPos = async function(id, pos){
	let existingImage = await Images.findOne({
		where: {
			id: id,
		}
	});
	if (existingImage === null || existingImage === undefined) {
		return false;
	} else {
		await existingImage.update({
			image_pos: pos,
		})
	}

	return true;
};

/***
 * Schafft oder schießt eine Lücke
 * z.B.:
 * Alte Pos = 2
 * neue Pos = 6 (zwischen 5 und 6, also ab 6 nach hinten schieben.)
 *   v  - entnehmen
 * 1 2 3 4 5 6 7 8 9
 * Ergebnis = 1 3 4 5 6 7 8 9 also
 * 1 2 3 4 5 6 7 8
 *
 * an Pos 6 einfügen. Ergebnis:
 * 1 3 4 5 6 n 7 8 9
 *
 *
 * ODER
 * Alte Pos = 8
 * neue Pos = 6 (zwischen 5 und 6, also ab 6 nach hinten schieben.)
 *
 *               v - entnehmen
 * 1 2 3 4 5 6 7 8 9
 * Ergebnis 1 2 3 4 5 6 7 9
 *
 * an Pos 6 einfügen. Ergebnis:
 * 1 2 3 4 5 8 6 7 8 9
 *
 * SQL = 'update images
 * set image_pos = image_pos + 1
 * where p_nr = 117615
 * and image_pos >= 2'
 *
 *
 * @param p_nr
 * @param startPos
 * @param indicator
 * @return Promise {<>}
 * */

exports.setNewPos = async function(p_nr, startPos, indicator){

	let existingImage = await Images.findAll({
		where: {
			p_nr: p_nr,
			image_pos: {
				[Op.gte]: startPos,
			}
		}
	});
	if (existingImage.length > 0) {
		for (let index in existingImage) {
			let currentImage = existingImage[index];
			await currentImage.update({
				image_pos: currentImage.image_pos + indicator,
			})
		}
	}
};


/***
 * Gibt das Image Object mit der übergebenen ID zurück
 *
 * @returns Promise {<<Model<T, T2>[]>>}
 * @param id
 */
exports.getImagesById = async function(id){
	return Images.findOne({
		where: {
			id: id
		}
	});
};

/***
 * Setzt die Kanalfreigabe für ein bestimmtes Images (id)
 *
 * @returns Promise {<<Model<T, T2>[]>>}
 * @param id
 * @param newImageChannelValue
 */
exports.setImageChannel = async function(id, newImageChannelValue) {
	let existingImage = await Images.findOne({
		where: {
			id: id,
		}
	});
	if (!(existingImage === null || existingImage === undefined)) {
		await existingImage.update({
			channel: newImageChannelValue,
		})
	}
};

/***
 * @returns Promise {<<Model<T, T2>[]>>}
 */
exports.getAllActive = async function(){
	return Images.findAll({
		where: {
			is_active: 1
		}
	});
}

/***
 * Gibt alle Bilder zurück
 *
 * @returns Promise {<<Model<T, T2>[]>>}
 */
exports.getAll = async function(){
	return Images.findAll();
}

/***
 * Gibt alle Bilder zurück
 *
 * @returns Promise {<<Model<T, T2>[]>>}
 */
exports.getAllWhitSize = async function(){
	return Images.findAll({
		where: {
			[Op.or]: [
				{
					height: {
						[Op.ne]: null
					}
				},
				{
					width: {
						[Op.ne]: null
					}
				}
			]
		}
	});
}

/**   Setzt die passenden channel zum Bild per id */
exports.setChannelById = async (id, channel) => {
	return Images.update({
		channel: channel
	}, {
		where: {
			id: id
		}
	})
};



/***
 * Speichert die Abmaße des Bildes
 *
 * @param id
 * @param width
 * @param height
 * @returns Promise {<<Model<T, T2>[]>>}
 */
exports.setImageSize = async function(id, height, width) {
	let images = await Images.findOne({
		where: {
			id: id
		}
	});
	if (images === undefined || images === null) {
		return;
	}

	await images.update({
		height: height,
		width: width
	})
}

exports.deactivateImageByUrl = async (url) => {
	return Images.update({is_active: false}, {
		where: {
			image_url: url
		}
	})
};

exports.deactivateImageById = async (id) => {
	return Images.update({is_active: false}, {
		where: {
			id: id
		}
	})
};

exports.deactivateImagePerPNR = async (uri, pnr) => {
	return Images.update({is_active: false}, {
		where: {
			[Op.and]: [
				{
					image_url: uri
				},
				{
					p_nr: pnr
				}
			]
		}
	})
};

/**
 * Gibt es für dieses Produkt wenigstens ein Canto Bild?
 *
 * @param p_nr
 * @return Promise {<boolean>}
 * */
exports.cantoImageIsAvailable = async (p_nr) => {
	let result = await Images.count({
		where: {
			p_nr: p_nr,
			version: constants.IMAGES_CANTO_TYPE_VALUE,
		}
	});

	return result > 0;
}

/***
 * sync-stuff
 * @returns Promise {<void>}
 */
exports.sync = async () => {
	await Images.sync({alter: true});
	logger.log(`${TAG}sync`, `images is sync`,
		`Das Synchronisieren der Tabelle "images" wurde aufgerufen.`);
};
