'use strict';

const config = require('../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../helper/util/logger.util');
const TAG = 'repo.channel.';

/***
 * Model
 */
const Channel = seq.define('channel', {
	channel_key: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: true
	},
	channel_name: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	channel_is_active: {
		type: Sequelize.BOOLEAN,
		defaultValue: Sequelize.BOOLEAN,
		primaryKey: false
	},
	channel_surcharge: {
		type: Sequelize.FLOAT,
		defaultValue: 0,
		primaryKey: false
	},
	image_type: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	stock: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	image_height: {
		type: Sequelize.INTEGER,
		primaryKey: false
	},
	image_width: {
		type: Sequelize.INTEGER,
		primaryKey: false
	},
	max_image: {
		type: Sequelize.INTEGER,
		primaryKey: false
	}
});

/***
 *
 * @returns Promise {<void>}
 */
exports.getChannels = async () => {
	return Channel.findAll({});
};

/**
 * Führt ein Update der übergebenen Werte für den übergebenen Kanal aus.
 * @param channelKey
 * @param isActive
 * @param imageWidth
 * @param imageHeight
 * @returns {Promise<*>}
 */
exports.updateChannel = async (channelKey, isActive, imageWidth, imageHeight) => {
	let channel = await Channel.findOne({
		where: {
			channel_key: channelKey
		}
	});
	if(!(channel === null || channel === undefined)){
		return await channel.update({
			channel_is_active: isActive,
			image_width: imageWidth,
			image_height: imageHeight
		})
	}
};

/**
 * Legt einen neuen Kanal mit den übergebenen Werten an.
 * @param channelName
 * @param channelKey
 * @param isActive
 * @param channelSurcharge
 * @param imageType
 * @param stock
 * @param imageWidth
 * @param imageHeight
 * @returns Promise {<void>}
 */
exports.setChannel = async (channelName, channelKey, isActive, channelSurcharge, imageType, stock, imageWidth, imageHeight) => {
	Channel.create({
		channel_name: channelName,
		channel_key: channelKey,
		channel_is_active: isActive,
		channel_surcharge: channelSurcharge,
		image_type: imageType,
		stock:stock,
		image_width: imageWidth,
		image_height: imageHeight
	})
};

/**
 * Zugehörigkeit Kanal zu Type, soll aber nicht doppelt sein.
 *
 * @return Promise {<Array>}
 * */
exports.getAllMediaType = async () => {
	let result = [];
	let imageType = await Channel.findAll({
		attributes: ['channel_key', 'image_type', 'max_image'],
		where: {
			channel_is_active: true
		},
		// group: ['channel_key', 'image_type'],
	});

	for (let i = 0; i < imageType.length; i++) {
		if (result[imageType[i].image_type] === undefined) {
			result[imageType[i].image_type] = [];
			result[imageType[i].image_type]['max_image'] = imageType[i].max_image;
			result[imageType[i].image_type]['channel_key'] = (imageType[i].channel_key);
		}
	}

	return result;
}

/**
 * Gibt alle channel_key als Array zurück
 *
 * @returns Promise{<Array>}
 * */
exports.getAllChannelKey = async () => {
	let channels = await Channel.findAll();
	let channelKeys = [];

	for (let i = 0; i < channels.length; i++) {
		channelKeys.push(channels[i].dataValues.channel_key);
	}

	return channelKeys;
};

/**
 * Gibt alle deaktivierten channel_key's zurück.
 *
 * @return {Promise<Object>}
 * */
exports.getAllDeactivateChannelKey = async () => {
	return Channel.findAll(
		{
			attributes: ['channel_key'],
			where:
				{
					channel_is_active: false
				}
		}
	);
}

/***
 * sync-stuff
 * @returns Promise{<void>}
 */
exports.sync = async () => {
	await Channel.sync({alter: true});
	logger.log(`${TAG}sync`, `channel is sync`,
		`Das Synchronisieren der Tabelle "channel" wurde aufgerufen.`);
};