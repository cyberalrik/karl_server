'use strict';

const config = require('../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../helper/util/logger.util');
const TAG = 'repo.priceList.';

/***
 * Model
 */
const Pricelist = seq.define('priceList', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
    },
    a_nr: {
        type: Sequelize.STRING,
        defaultValue: Sequelize.STRING,
        primaryKey: false
    },
    channel: {
        type: Sequelize.STRING,
        defaultValue: Sequelize.STRING,
        primaryKey: false
    },
    price: {
        type: Sequelize.STRING,
        defaultValue: Sequelize.STRING,
        primaryKey: false
    },
    quant: {
        type: Sequelize.STRING,
        defaultValue: Sequelize.STRING,
        primaryKey: false
    }
});

exports.addPriceListItem = async function(pli){
    let existingPLI = await Pricelist.findOne({
        where: {
            a_nr: pli.a_nr,
            channel: pli.channel,
        }
    });
    if (existingPLI === null || existingPLI === undefined){
        return Pricelist.create({
            a_nr : pli.a_nr,
            channel : pli.channel,
            price : pli.price,
            quant : pli.quant,

        }).then((resolve) => {
            return resolve;
        });
    }
    if(
        existingPLI.price !== pli.price+'' ||
        existingPLI.quant !== pli.quant
    ){
        existingPLI.update({
            price : pli.price,
            quant : pli.quant
        });
    }
    return existingPLI;
};

exports.getModifiedPriceListItem = async function(date, delta = true){

    await logger.log(`PriceListItem`, `Artikelpreise`,
        `Die Artikelpreisermittlung wurde mit delta = ${delta} aufgerufen.`);

    let options = {};

    if (delta) {
        await logger.log(`PriceListItem`, `Artikelpreise`, `Es werden Bedingungen gesetzt.`);
        let dateOption = `${
            date.getFullYear()}-${
            (date.getMonth() + 1).toString().padStart(2, '0')}-${
            date.getDate().toString().padStart(2, '0')} ${
            date.getHours().toString().padStart(2, '0')}:${
            date.getMinutes().toString().padStart(2, '0')}:00`;
        options = {
            where: {
                updatedAt: {
                    [Op.gte]: dateOption,
                }
            }
        }
    } else {
        await logger.log(`PriceListItem`, `Artikelpreise`, `Es werden keine Bedingungen gesetzt.`);
    }
    try {
        let result = await Pricelist.findAll(options);
        return result;
    } catch (e) {
        return []
    }
}

exports.getPriceListPerANr = async function(a_nr){
    return Pricelist.findAll({
        where: {
            a_nr: a_nr
        }
    });
}

/**
 * Gibt alle Preise und Mengen für den übergebenen Artikel für die übergebenen Channels zurück
 * */
exports.getPriceListPerANrAndChannel = async function(a_nr, channels){
    let channelList = [];
    let result = [];
    try {
        channels.forEach(channel => {
            channelList.push({channel: channel});
        })

        result = await Pricelist.findAll({
            where: {
                a_nr: a_nr,
                [Op.or]: channelList
            }
        });
    } catch (e) {
        logger.err(TAG,
            'Fehler bei Preisermittlung',
            `Von dem Artikel ${a_nr} konnte kein Preise für den Channel ${channelList.join()} ermittelt werden.`
        );
    }

    return result;
}

/**
 * Setze die übergebenen Artikel-Daten, wenn vorhanden aber legt keinen neuen Datensatz an.
 *
 * Gibt TRUE zurück, wenn etwas geändert wurde.
 * Ansonsten FALSE
 *
 * @return {Promise<boolean>}
 * */
exports.editPriceListItem = async function(item) {
    let articleItem = await Pricelist.findOne({
        where: {
            a_nr: item.a_nr,
            channel: item.channel,
        }
    });

    if (articleItem === null || articleItem === undefined) {
        return false;
    }

    if (
        articleItem.price !== item.price + '' ||
        articleItem.quant !== item.quant + ''
    ) {
        articleItem.update({
            price : item.price,
            quant : item.quant
        });

        return true;
    }

    return false;
};

exports.getAllPriceList = async () => {
    return Pricelist.findAll({
        attributes: ['a_nr', 'channel', 'price', 'quant']
    });
}

exports.getAllEBay2PriceList = async () => {
    return Pricelist.findAll({
        where: {
            a_nr: {
                [Op.like]: 'E2-%'
            }
        }
    });
}

/**
 * Gibt alle a_nr zurück die:
 * - mit 'E2-' beginnen
 * - der Preis = 0 ist
 * - die Menge = 0 ist
 * - und das Änderungsdatum kleiner als das übergebene Datum ist
 * */
exports.getAllANrOfPriceQuantIs0 = async function(date) {
    let dateOption = `${
        date.getFullYear()}-${
        (date.getMonth() + 1).toString().padStart(2, '0')}-${
        date.getDate().toString().padStart(2, '0')} ${
        date.getHours().toString().padStart(2, '0')}:${
        date.getMinutes().toString().padStart(2, '0')}:00`;
    let options = {
        attributes: ['a_nr'],
        where: {
            [Op.and]: {
                updatedAt: {
                    [Op.lt]: dateOption,
                },
                price: {
                    [Op.eq]: 0
                },
                quant: {
                    [Op.eq]: 0
                },
                a_nr: {
                    [Op.like]: 'E2-%'
                }
            }
        }
    }

    let result = await  Pricelist.findAll(options)

    return result;
}

/**
 * Löscht nicht mehr benötigte PriceList Einträge
 * vorrangig für E2-Artikel
 *
 * @param a_nr
 * */
exports.delete = function(a_nr) {
    Pricelist.destroy({
        where: {
            // a_nr: ['E2-137592','E2-76999','E2-168564','E2-166262'],
            a_nr: a_nr,
        }
    });
}

/***
 * sync-stuff
 * @returns {Promise<void>}
 */
exports.sync = async () => {
    await Pricelist.sync({alter: true});
    logger.log(`${TAG}sync`, `pricelist is sync`,
        `Das Synchronisieren der Tabelle "pricelist" wurde aufgerufen.`);
};