'use strict';

const config = require('../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;
const logger = require('../helper/util/logger.util');
const TAG = 'repo.product.';

/***
 * Model
 */
const Product = seq.define('product', {
	p_nr: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: true
	},
	p_name: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_text_de: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_text_ee: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_brand: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_brand_key: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_cat_id: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_cat_name: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_3_rd: {
		type: Sequelize.BOOLEAN,
		defaultValue: Sequelize.BOOLEAN,
		primaryKey: false
	},
	p_weight: {
		type: Sequelize.DECIMAL(10, 2),
		primaryKey: false
	},
	p_is_fake:{
		type: Sequelize.BOOLEAN,
		defaultValue: Sequelize.BOOLEAN,
		primaryKey: false
	},
	p_export_block: {
		type: Sequelize.BOOLEAN,
		defaultValue: Sequelize.BOOLEAN,
		primaryKey: false
	},
	p_import_block: {
		type: Sequelize.BOOLEAN,
		defaultValue: Sequelize.BOOLEAN,
		primaryKey: false
	},
	p_keyword1: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false,
		comment: 'Darf nicht länger als 50 Zeichen betragen.'
	},
	p_keyword2: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_keyword3: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}

});

/**
 * Gibt alle Produkte zurück
 * */
exports.getAllProducts = async function() {
	return await Product.findAll();
}

/***
 * add Product
 * @param product
 *
 * @param result
 * @returns {Promise<>}
 */
exports.addOrUpdateProduct = async function(product, result = {create: 0, update: 0, unnecessary: 0}) {
	try {
		let existingProduct = await Product.findOne({
			where: {
				p_nr: product.p_nr
			}
		});
		if (existingProduct === null || existingProduct === undefined) {
			result.create++;
			return Product.create({
				p_nr: product.p_nr,
				p_name: product.p_name,
				p_text_de: product.p_text_de.substring(0, 254),
				p_text_ee: product.p_text_ee.substring(0, 254),
				p_brand: product.p_brand,
				p_brand_key: product.p_brand_key,
				p_cat_id: product.p_cat_id,
				p_cat_name: null,
				p_3_rd: product.is3rd === undefined ? 0 : product.is3rd,
				p_weight:   product.p_weight,
				p_is_fake: product.p_is_fake === undefined ? 1 : product.p_is_fake,
				p_export_block: false,
				p_import_block: false,
				p_keyword1: product.p_keyword1.substring(0, 50),
				p_keyword2: product.p_keyword2.substring(0, 50),
				p_keyword3: product.p_keyword3.substring(0, 50)
			}).then(() => {
				return result;
			}).catch(() => {
				logger.err(TAG + 'addProduct', 'Konnte Produkt nicht anlegen',
					`Das Produkt ${product.p_name} vom Hersteller ${product.p_brand} konnte nicht angelegt werden. - 1`,
					`Es sind bestimmt Sonderzeichen enthalten.`,
					'produktmanagement@cybertrading.de,s.wiehe@cybertrading.de');
			});
		}

		if (
			existingProduct.p_name !== product.p_name ||
			existingProduct.p_nr !== product.p_nr ||
			existingProduct.p_text_de !== product.p_text_de.substring(0, 254) ||
			existingProduct.p_text_ee !== product.p_text_ee.substring(0, 254) ||
			existingProduct.p_brand !== product.p_brand ||
			existingProduct.p_brand_key !== product.p_brand_key ||
			existingProduct.p_3_rd !== product.p_3_rd ||
			existingProduct.p_weight !== product.p_weight ||
			existingProduct.p_keyword1 !== product.p_keyword1.substring(0, 50) ||
			existingProduct.p_keyword2 !== product.p_keyword2.substring(0, 50) ||
			existingProduct.p_keyword3 !== product.p_keyword3.substring(0, 50)
		) {
			result.update++;
			existingProduct.update({
				p_nr: product.p_nr,
				p_name: product.p_name,
				p_text_de: product.p_text_de.substring(0, 254),
				p_text_ee: product.p_text_ee.substring(0, 254),
				p_brand_key: product.p_brand_key,
				p_brand: product.p_brand,
				p_3_rd: product.p_3_rd,
				p_weight: product.p_weight,
				p_keyword1: product.p_keyword1.substring(0, 50),
				p_keyword2: product.p_keyword2.substring(0, 50),
				p_keyword3: product.p_keyword3.substring(0, 50)
			}).catch(() => {
				logger.err(TAG + 'addProduct',
					'Konnte Produkt nicht updaten',
					`Das Produkt ${product.p_name} vom Hersteller ${product.p_brand} mit der Karl-Produkt-Nr. ${product.p_nr
						} konnte nicht korrigiert werden. - 3`,
					'',
					'produktmanagement@cybertrading.de,s.wiehe@cybertrading.de'
				);
			});
		} else {
			result.unnecessary++;
			console.log(`p_nr: ${product.p_nr} unnötig geprüft.`)
		}
	} catch (e) {
		logger.err(TAG + 'addProduct',
			'Konnte Produkt nicht anlegen',
			`Das Produkt ${product.p_name} konnte nicht angelegt werden. - 2`,
			'',
			'produktmanagement@cybertrading.de,s.wiehe@cybertrading.de'
		);
	}

	// return existingProduct;
	return result;
};

/***
 * Get all products
 * @param p_nr as String
 * @param date as String '2023-04-03'
 *
 * @returns Array
 */
exports.getAllProductAfterTime = async function(p_nr = null, date = null){
	/** Wurde eine ProduktId oder ein "ab" Datum übergeben, wird es zur Bedingung */
	let condition = {};
	let whereCondition = {}
	let isCondition = false;
	if (!(p_nr === null || p_nr === undefined))	{
		whereCondition.p_nr = p_nr;
		isCondition = true;
	}

	if (!(date === null || date === undefined)) {
		whereCondition.createdAt = {
			[Op.gt] : `${date}`
		}
		isCondition = true;
	}

	if (isCondition) {
		condition.where = whereCondition;
	}

	/** Als Beispiel */
	// if (!(p_nr === null || p_nr === undefined))	{
	// 	condition = {
	// 		where: {
	// 			p_nr: p_nr
	// 		}
	// 	}
	// }
	// if (!(date === null || date === undefined)) {
	// 	condition2 = {
	// 		where: {
	// 			createdAt: {
	// 				[Op.gt]: `${date}`
	// 			}
	// 		}
	// 	}
	// }
	//

	try {
		return await Product.findAll(condition);
	} catch (e) {
		return [];
	}
};

/***
 * Gibt alle Produkte zurück, bei denen p_cat_id_neu NULL oder Empty ist
 * @returns Array
 */
exports.getAllProductWhereCatIdIsNullOrEmpty = async function(){
	/** Wurde eine ProduktId übergeben, wird sie zur Bedingung */
	let condition = {
		where: {
			[Op.or]: [
				{
					p_cat_id: {
						[Op.eq]: null // Like: p_cat_id_neu IS NULL
					},
				},
				{
					p_cat_id: {
						[Op.eq]: '' // Like: p_cat_id_neu IS NULL
					}
				}
			]
		}
	}

	try {
		return await Product.findAll(condition);
	} catch (e) {
		return [];
	}
};

/**
 * Schreibt in ein Produkt der übergebenen ProductId alle übergebenen Kategorien
 * */
exports.setNewCategory = async function (param) {
	if (param.p_nr !== null && param.p_nr !== undefined) {
		let currentProduct = await Product.findOne({
			where: {
				p_nr: param.p_nr,
			}
		});
		if (currentProduct !== null && currentProduct !== undefined){
			let option = {};
			/** p_cat_id */
			if (param.p_cat_id !== null && param.p_cat_id !== undefined) {
				option.p_cat_id = param.p_cat_id
			}

			/** p_cat_name */
			if (param.p_cat_name !== null && param.p_cat_name !== undefined) {
				option.p_cat_name = param.p_cat_name
			}

			return await currentProduct
				.update(option)
				.then((resolve) => {
				return resolve;
			});
		}
	}
}

/***
 *
 * @param offset
 * @param limit
 * @param search
 */
exports.getProductsPerMPN = async function (offset, limit, search){
	// eslint-disable-next-line no-useless-catch
	try {
		return await Product.findAll({
			where: {
				[Op.or]: [
					{
						p_name: {
							[Op.like]: `%${search}%`
						}
					},
					{
						p_nr: {
							[Op.like]: `${search}`
						}
					}
				]

			},
		})
	} catch (e) {
		throw e;
	}
};

/***
 * get Product-Information per Productnumber
 * @param p_nr
 * @returns {Promise<void>}
 */
exports.getProductInformationPerPnr = async (p_nr) => {
	return await Product.findOne({
		where: {
			p_nr: p_nr
		}
	})
};

/**+
 *
 * @param p_nr
 * @param importLock
 * @param exportLock
 * @returns {Promise<void>}
 */
exports.setProductLock = async (p_nr ,importLock, exportLock) => {
	let product = await Product.findOne({
		where: {
			p_nr: p_nr
		}
	});
	product.update({
		p_export_block: exportLock,
		p_import_block: importLock
	});
	return product;
};

/***
 * sync-stuff
 * @returns {Promise<void>}
 */
exports.sync = async () => {
	await Product.sync({alter: true});
	logger.log(`${TAG}sync`, `products is sync`,
		`Das Synchronisieren der Tabelle "products" wurde aufgerufen.`);
};

/***
 *
 * @param p_nr
 * @returns {Promise<Product.p_import_block|{defaultValue, type, primaryKey}|*>}
 */
exports.isBlocked = async (p_nr) => {
	let product = await Product.findOne({
		where: {
			p_nr: p_nr
		}
	});
	return product=== null? false : product.p_import_block;
};


exports.getAllProductPerValue = async (key, value) => {
	let klausel = {}
	klausel[key] = value;
	return Product.findAll({
		where: klausel
	})
}

exports.updateProd = async (parameter) => {
	let product = await Product.findOne({
		where: {
			p_nr: parameter.p_nr
		}
	});

	if (!(product === null || product === undefined)) {
		product.update({
			p_cat_id: parameter.p_cat_id,
			p_cat_name: parameter.p_cat_name,
		}).catch(() => {
			logger.err(
				TAG + '-updateProd',
				'update cNet-Category',
				`Das Produkt ${parameter.p_nr} konnte nicht korrigiert werden. - 2`
			);
		});
	}

	return product;
};

exports.getProductsPerMpnAndManufacturer = async (mpn, manufacturer) => {
	return Product.findOne({
		where: {
			[Op.and]: [
				{
					p_name: `${mpn}`
				},
				{
					p_brand: `${manufacturer}`
				}
			]

		},
	});
}
