'use strict'

const config = require('../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
// const Op = Sequelize.Op;
const logger = require('../helper/util/logger.util');
const ServiceDateFormat = require("../services/service.date.format");
const TAG = 'repo.sys.';

class index
{
    /**
     * Model
     * Verschiedene Jobs sollen nur einmal laufen.
     */
    constructor() {
        this.sys = seq.define('sys', {
            jobName: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            status: {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            },
            info: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            description: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            timeDescription: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            cronTime: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            initialized: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            grantedRuntime: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            pos: {
                type: Sequelize.INTEGER,
                allowNull: true,
            }
        })
    }

    /**
     * Bei jedem Start wird überprüft, ob die Jobs in der sys-Tabelle angelegt sind.
     * Wenn nicht, werden sie angelegt, ansonsten berichtigt oder so gelassen
     *
     * @param job
     * @return {Promise<void>}
     * */
    async add(job) {
        try {
            let systemValue = await this.sys.findOne({
                where: {
                    jobName: job.name
                }
            })

            if (systemValue === null || systemValue === undefined) {
                await this.sys.create(
                    {
                        jobName: job.name,
                        status: 0,
                        description: job.description,
                        timeDescription: job.timeDescription,
                        cronTime: job.time,
                        initialized: job.active,
                        info: 'wurde noch nicht ausgeführt',
                        pos: job.pos
                    }
                );
            } else if (
                systemValue.description !== job.description ||
                systemValue.timeDescription !== job.timeDescription ||
                systemValue.cronTime !== job.time ||
                systemValue.initialized !== job.active ||
                systemValue.status === 1 ||
                systemValue.pos !== job.pos
            ) {

                await this.sys.update(
                    {
                        status: 0,
                        description : job.description,
                        timeDescription : job.timeDescription,
                        cronTime : job.time,
                        initialized : job.active,
                        pos: job.pos
                    },
                    {
                        where: {
                            jobName: job.name
                        }
                    }
                )
            }
        } catch (e) {
            logger.err(TAG + 'set-1', e.message);
            logger.err(TAG + 'set-2', 'jobName:' + job.name);
        }
    }

    /**
     * Setzt die Werte im Datensatz
     * */
    async set(jobName, status, info = null, hour = null) {
        try {
            let systemValue = await this.sys.findOne({
                where: {
                    jobName: jobName
                }
            })

            if (systemValue === null || systemValue === undefined) {
                await this.sys.create(
                    {
                        jobName: jobName,
                        status: status,
                    }
                );
            } else {
                let setData = {
                    status: status
                };

                if (!(info === undefined || info === null)) {
                    setData.info = `Zuletzt benötigte Zeit: ${info}`;
                }

               if (!(hour === undefined || hour === null)) {
                    setData.grantedRuntime = hour;
                }


                await this.sys.update(
                    setData,
                    {
                        where: {
                            jobName: jobName
                        }
                    }
                )
            }
        } catch (e) {
            logger.err(TAG + 'set-1', e.message);
            logger.err(TAG + 'set-2', 'jobName:' + jobName + ' Status:' + status);
        }
    }

    /**
     * Gibt die Daten im Datensatz zurück
     * */
    async get(jobName) {
        try {
            return await this.sys.findOne(
                {
                    where: {
                        jobName: jobName,
                    }
                }
            );
        } catch (e) {
            logger.err(TAG + 'get', 'Es ist folgender Fehler aufgetreten', e.message);
        }
    }

    /**
     * Gibt alle Datensätze zurück
     * */
    async getAll() {
        try {
            let items = await this.sys.findAll({
                order: [
                    ['pos', 'ASC']
                ]});
            items = await this.formatDate(items);

            return items;
        } catch (e) {
            logger.err(TAG + 'get', e.message);
        }
    }

    async formatDate(items) {
        let serviceDateFormat = new ServiceDateFormat();
        for (let index in items) {
            items[index].displayUpdatedAt = await serviceDateFormat.getStringFromDateGermanFormat(items[index].updatedAt);
        }

        return items;
     }

    /**
     * sync-stuff
     * @return {Promise <void>}
     */
    async sync() {
        await this.sys.sync({ alter: true });
        logger.log(`${TAG}sync`, `sys is sync`,
            `Das Synchronisieren der Tabelle "sys" wurde aufgerufen.`);
    }
}

module.exports = index;