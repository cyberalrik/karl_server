'use strict';

const config = require('../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../helper/util/logger.util');
const TAG = 'repo.param.';

/***
 * Model
 */
const karlParam = seq.define('param', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		allowNull: false,
		autoIncrement: true,
	},
	key: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	value: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});

/***
 *
 * @param key
 * @param value
 * @returns {Promise<any>}
 */
exports.updateParam = async function(key, value){
	let param = await karlParam.findOne({
		where: {
			key: key,
		}
	});
	if (param !== null && param !== undefined){
		return await param.update({
			key: key,
			value: value
		}).then((resolve) => {
			return resolve;
		});
	} else {
		return await karlParam.create({
			key: key,
			value: value
		})
	}
};

/***
 *
 * @param key
 * @returns {Promise<void>}
 */
exports.getParam = async function(key){
	return await karlParam.findOne({
		where: {
			key: key
		}
	});
};

/***
 * sync-stuff
 * @returns {Promise<void>}
 */
exports.sync = async () => {
	await karlParam.sync({alter: true});
	logger.log(`${TAG}sync`, `params is sync`,
		`Das Synchronisieren der Tabelle "params" wurde aufgerufen.`);
};
