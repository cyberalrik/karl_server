'use strict';

const config = require('../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../helper/util/logger.util');
const TAG = 'repo.components.';

/***
 * Model
 */
const Component = seq.define('components', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		allowNull: false,
		autoIncrement: true,
	},
	p_nr: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_component_key: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_component_value: {
		type: Sequelize.STRING(4999),
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_component_language: {
		type: Sequelize.STRING(2),
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},

	p_component_position: {
		type: Sequelize.INTEGER,
		defaultValue: Sequelize.INTEGER,
		primaryKey: false
	}
});

/***
 *
 * @returns {Promise<any>}
 * @param component
 * @param result
 */
exports.addComponent = async function(component, result){
	if (result === undefined) {
		result = {successful: '', create: 0};
	}
	if (
		!(component.p_nr !== undefined
		&& component.p_component_key !== undefined
		&& component.p_component_value !== undefined
		&& component.p_component_language !== undefined
		&& component.p_component_position !== undefined)
	) {
		result.successful = false;
		return result;
	}
	let existingComponent = await Component.findOne({
		where: {
			p_nr: component.p_nr,
			// p_component_key: p_component_key,
			p_component_language: component.p_component_language,
			p_component_position: component.p_component_position
		}
	});
	if (existingComponent === null || existingComponent === undefined){
		result.create++;
		return Component.create({
			p_nr: component.p_nr,
			p_component_key: component.p_component_key,
			p_component_value: component.p_component_value,
			p_component_language: component.p_component_language,
			p_component_position: component.p_component_position
		}).then(() => {
			result.successful = true;
			return result;
		});
	} else if(existingComponent.p_component_key !== component.p_component_key ||
		existingComponent.p_component_value !== component.p_component_value
	) {
		result.update++;
		existingComponent.update({
			p_component_key: component.p_component_key,
			p_component_value: component.p_component_value,
		})
	}

	result.successful = true;
	return result;
};

/***
 *
 * @param p_nr
 * @returns {Promise}
 */
exports.getAllComponentPerID = async function(p_nr){
	return Component.findAll({
		where: {
			p_nr: p_nr
		},
		order: ['p_component_position']
	});
};

/***
 * @returns {Promise}
 */
exports.getAllComponents = async function(){
	return Component.findAll({
		order: ['p_component_position']
	});
};

/**
 * Gibt true zurück, wenn cNet Komponente für das Produkt angelegt wurde
 * */
exports.isCNetComponentFromProductId = async function(p_nr) {
	// p_nr = 755487;
	let result = await Component.count({
		where: {
			p_nr: p_nr,
			p_component_position: {
				[Op.lt]: 100
			},
		}
	});

	return result > 0 //? true : false;
}

/***
 * Um schneller herauszubekommen, ob eine standard Komponente schon angelegt ist,
 * werden hier, z. B.: Alle Komponenten der Position 200 zurückgegeben
 *
 * @return {Promise}
 * @param position
 */
exports.getAllComponentPerPosition = async function(position) {
	return await Component.findAll({
		where: {
			p_component_position: position
		}
	});
};

/**
 * Löscht eine übergebene Komponente
 * @param component
 * @param result
 * @return {Object}
 * */
exports.deleteComponent = async function(component, result) {
	let existingComponent = await Component.findOne({
		where: {
			p_nr: component.p_nr,
			p_component_position: component.p_component_position,
			p_component_language: component.p_component_language,
		}
	});
	if (!(existingComponent === null || existingComponent === undefined)) {
		existingComponent.destroy();
		result.delete++;
	}

	return result;
}

/** Holt alle Komponenten zwischen (include) 100 und 199  */
exports.getAllComponentsFrom100To199 = async function() {
	return await Component.findAll({
		where: {
			p_component_position: {
				[Op.between]: [100, 199]
			}
		}
	})
}

/***
 * sync-stuff
 * @returns {Promise<void>}
 */
exports.sync = async () => {
	await Component.sync({alter: true});
	logger.log(`${TAG}sync`, `components is sync`,
		`Das Synchronisieren der Tabelle "components" wurde aufgerufen.`);
};