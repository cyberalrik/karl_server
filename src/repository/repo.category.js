'use strict';

const config = require('../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const logger = require('../helper/util/logger.util');
const TAG = 'repo.category.';

/***
 * Model
 */
class index {
	constructor() {
		const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

		this.categoryRepo = seq.define('category', {
			cat_id: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			},
			name: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			}
		});
	}

	/**
	 * Legt Kategorien an.
	 * Überprüft vorher, ob es sie schon gibt.
	 * */
	async setCategory(category) {
		let existingCategory = await this.categoryRepo.findAll({
			where: {
				category: category
			}
		});

		if (existingCategory === null
			|| existingCategory === undefined
			|| existingCategory.length === 0
		){
			return await this.categoryRepo.create({
				category: category
			});
		}
	}

	/***
	 * Gibt alle Kategorien zurück die in TB erlaubt sind
	 * @returns Promise {<void>}
	 */
	async getAllCategory() {
		return await this.categoryRepo.findAll({});
	};

	/***
	 * sync-stuff
	 * @returns Promise{<void>}
	 */
	async sync() {
		await this.categoryRepo.sync({alter: true});
		logger.log(`${TAG}sync`, `category is sync`,
			`Das Synchronisieren der Tabelle "category" wurde aufgerufen.`);
	}
}

module.exports = index;