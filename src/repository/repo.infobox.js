'use strict';

const config = require('../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const logger = require('../helper/util/logger.util');
const TAG = 'repo.infobox.';

class index {
	constructor() {
		// const configObject = new Config();
		// this.config = configObject.getConfig().IDE;
		const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

		this.infobox = seq.define('infobox', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				allowNull: false,
				autoIncrement: true,
			},
			type: {
				type: Sequelize.INTEGER,
				comment: '0=Produkt\n' +
					'1=Artikel'
			},
			p_a_nr: {
				type: Sequelize.INTEGER,
				comment: 'Produkt oder Artikel Nummer (Type abhängig)',
			},
			info: {
				type: Sequelize.STRING,
			}
		});
	}

	async saveInfo(nr, type, info) {

		let result = {
			successful: true
		};

		try {
			let queryResult = await this.infobox.findOne({
				where: {
					p_a_nr: nr,
					type: type
				},
			});

			if (queryResult === null || queryResult === undefined){
				await this.infobox.create({
					p_a_nr: nr,
					type: type,
					info: info
				})
			} else {
				queryResult.info = info;
				await queryResult.update({
					info: info
				});
			}

		} catch (e) {
			result.successful = false;
			result.messages = e.message;
			result.messageUser = 'Beim Speichern der Daten ist ein Fehler aufgetreten.';
		}

		return result;
	}

	async getInfo(type, p_a_nr) {

		let result = {
			successful: true
		};

		try {
			let queryResult = await this.infobox.findOne({
				where: {
					type: type,
					p_a_nr: p_a_nr
				},
			});
			result.infoText = queryResult !== null ? queryResult.info : '';
		} catch (e) {
			result.successful = false;
			result.messages = e.message;
			result.messageUser = 'Es ist ein Fehler aufgetreten.';
		}

		return result;
	}

	/**
	 * Löscht nicht mehr benötigte Infobox-Einträge
	 * vorrangig für E2-Artikel
	 *
	 * @param a_nr
	 * @param type
	 * */
	delete(a_nr, type) {
		this.infobox.destroy({
			where: {
				type: type,
				p_a_nr: a_nr
			}
		});
	}

	/***
	 * sync-stuff
	 * @returns {Promise<void>}
	 */
	async sync() {
		await this.infobox.sync({alter: true});
		logger.log(`${TAG}sync`, `infoboxes is sync`,
			`Das Synchronisieren der Tabelle "infoboxes" wurde aufgerufen.`);
	};
}

module.exports = index;
