'use strict';

const config = require('../../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../../helper/util/logger.util');
const {Op} = require("sequelize");
const TAG = 'repo.articleChannel.';

/***
 * Model
 */
const articleChannel = seq.define('articleChannel', {
	channel_key: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: true
	},
	a_nr: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: true
	},
	is_active: {
		type: Sequelize.BOOLEAN,
		defaultValue: Sequelize.BOOLEAN,
		primaryKey: false
	}
});

exports.getArtChannelData = async (a_nr) => {
	return articleChannel.findAll({
		where: {
			a_nr: a_nr
		},
		order: [
			['channel_key', 'DESC']
		]
	});
};

/**
 * Hiermit kann ein Artikel für einen Channel (Key) gesperrt werden (is_active = 0)
 * Ist der Artikel in dieser Tabelle nicht enthalten, ist es dasselbe als wenn er mit is_active 1 angelegt ist.
 * Das bedeutet, ein Datensatz mit is_active 1 muss nicht angelegt werden, sondern kann gelöscht werden.
 *
 * @param a_nr
 * @param key
 * @param value
 * @return Promise {<>}
 * 0 = nichts geändert,
 * 1 = neuanlage,
 * 2 = angepasst,
 * 3 = gelöscht,
 * */
exports.setArtChannelData = async (a_nr, key, value) => {
	let artChannel = await articleChannel.findOne({
		where: {
			a_nr: a_nr,
			channel_key: key
		},
	});
	if (artChannel === null || artChannel === undefined) {
		if (value === 0) {
			articleChannel.create({
				channel_key: key,
				a_nr: a_nr,
				is_active: value
			});
			return 1;
		}
	} else {
		/** Gibt es einen Datensatz und soll er jetzt auf is_active = 1 geändert werden, kann er auch gelöscht werden. */
		if (value === 1) {
			artChannel.destroy();
			return 3;
		} else {
			if (artChannel.is_active === true) {
				await artChannel.update({
					is_active: value
				});
				return 2;
			}
		}
	}
	return 0;
};

/**
 * Gibt TRUE zurück, wenn etwas geändert oder angelegt wurde.
 * Ansonsten FALSE
 *
 * @return {Promise<boolean>}
 * */
exports.setArtChannelDataWithResponse = async (a_nr, key, value) => {
	let artChannel = await articleChannel.findOne({
		where: {
			a_nr: a_nr,
			channel_key: key
		},
	});
	if (artChannel === null ||artChannel === undefined){
		await articleChannel.create({
			channel_key: key,
			a_nr: a_nr,
			is_active: value
		})

		return true;
	}

	if (artChannel.dataValues.is_active !== !!value) {
		await artChannel.update({
			is_active: value
		});

		return true;
	}

	return false;
};

/**
 * Gibt alle deaktivierten a_nr - channel Kombinationen zurück
 *
 * @return {Promise<Object>}
 * */
exports.getAllDeactivateANrChannel = async () => {
	return articleChannel.findAll(
		{
			where:
				{
					is_active: false
				}
		}
	);
}

/**
 * Gibt alle a_nr gruppiert zurück
 *
 * @return {Promise<Object>}
 * */
exports.getAllGroupBy = async (groupBy) => {
	return articleChannel.findAll({
		group: groupBy,
		attributes: [groupBy]
	});
}

exports.getAllDeactivateANrFromChannelKeyAndActivity = async (key, activity = false) => { //, updatedAt) => {
	return articleChannel.findAll(
		{
			where:
			{
				is_active: activity,
				channel_key: key,
				// updatedAt: {
				// 	[Op.gt]: updatedAt,
				// }
			}
		}
	);
}

/**
 * Löscht nicht mehr benötigte articleChannel
 * vorrangig für E2-Artikel.
 * Jetzt auch für normale Artikel die deaktiviert und wieder aktiviert wurden.
 * Deshalb die Umstellung zu "WHERE IN()".
 *
 * @param a_nr
 * */
exports.delete = function(a_nr) {
	articleChannel.destroy({
		where: {
			// a_nr: a_nr,
			a_nr: {[Op.in]: [a_nr]}
		}
	});
}

/**
 * Gibt alle E2-Artikel a_nr - channel Kombinationen zurück
 *
 * @return {Promise<Object>}
 * */
exports.getAllE2ANrChannel = async () => {
	return articleChannel.findAll({
		where: {
			a_nr: {
				[Op.like]: 'E2-%'
			}
		}

	});
}


/***
 * sync-stuff
 * @returns {Promise<void>}
 */
exports.sync = async () => {
	await articleChannel.sync({alter: true});
	logger.log(`${TAG}sync`, `articleChannels is sync`,
		`Das Synchronisieren der Tabelle "articleChannels" wurde aufgerufen.`);
};