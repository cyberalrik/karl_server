'use strict';

'use strict';

const config = require('../../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../../helper/util/logger.util');
const TAG = 'repo.noUpdate.';

/***
 * Model
 */
const noUpdate = seq.define('noUpdate', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		allowNull: false,
		autoIncrement: true,
	},
	a_nr: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
});

/***
 *
 * @returns {Promise<{}>}
 */
exports.getAll = async function(){
	return noUpdate.findAll();
};

/***
 * Gibt TRUE zurück, wenn die übergebene ArtikelNummer enthalten ist.
 * Ansonsten FALSE
 *
 * @param a_nr
 * @returns {Promise<boolean>}
 */
exports.isNoUpdate = async function(a_nr){
	let noupdate = await noUpdate.findOne({
		where: {
			a_nr: a_nr
		}
	});
	return !(noupdate !== undefined);
};

/**
 * Löscht nicht mehr benötigte noUpdate
 * vorrangig für E2-Artikel
 *
 * @param a_nr
 * */
exports.delete = function(a_nr) {
	noUpdate.destroy({
		where: {
			a_nr: a_nr,
		}
	});
}

/***
 * sync-stuff
 * @returns {Promise<void>}
 */
exports.sync = async () => {
	await noUpdate.sync({alter: true});
	logger.log(`${TAG}sync`, `noUpdates is sync`,
		`Das Synchronisieren der Tabelle "noUpdates" wurde aufgerufen.`);
};
