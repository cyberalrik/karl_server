'use strict';

const config = require('../../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../../helper/util/logger.util');
const TAG = 'repo.articleComponents.';

/**
 * MODEL
 */
const ArticleComp = seq.define('articleComponents', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		allowNull: false,
		autoIncrement: true,
	},
	a_nr: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	a_component_key: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	a_component_value: {
		type: Sequelize.STRING(4999),
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});

/**
 * add an Component to an given Article
 * @param a_nr
 * @param a_component_key
 * @param a_component_value
 * @returns {Promise<any>}
 */
exports.addComponent = async function(a_nr, a_component_key, a_component_value){
	let existingComponent = await ArticleComp.findOne({
		where: {
			a_nr: a_nr,
			a_component_key: a_component_key
		}
	});
	if (existingComponent === null || existingComponent === undefined){
		return await ArticleComp.create({
			a_nr : a_nr,
			a_component_key : a_component_key,
			a_component_value: a_component_value,
		}).then((resolve) => {
			return resolve;
		});
	}
	return existingComponent;
};

/**
 * get all Components of an Article
 * @param a_nr
 * @returns Promise{<<>}
 */
exports.getAllComponentPerID = async function(a_nr){
	return ArticleComp.findAll({
		where: {
			a_nr: a_nr
		}
	});
};

/**
 * Löscht nicht mehr benötigte ArticleComp
 * vorrangig für E2-Artikel
 *
 * @param a_nr
 * */
exports.delete = function(a_nr) {
	ArticleComp.destroy({
		where: {
			a_nr: a_nr,
		}
	});
}

/***
 * sync-stuff
 * @returns {Promise<void>}
 */
exports.sync = async () => {
	await ArticleComp.sync({alter: true});
	logger.log(`${TAG}sync`, `articleComponents is sync`,
		`Das Synchronisieren der Tabelle "articleComponents" wurde aufgerufen.`);
};
