'use strict';

const config = require('../../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const logger = require('../../helper/util/logger.util');
const TAG = 'repo.article.';

/***
 * Model
 */
const Article = seq.define('article', {
	a_nr: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: true
	},
	a_nr2: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	p_nr: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	a_ean: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	a_prod_nr: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	a_cond: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	a_is_blocked: {
		type: Sequelize.BOOLEAN,
		defaultValue: Sequelize.BOOLEAN,
		primaryKey: false
	},
	a_delivery_time: {
		type: Sequelize.INTEGER,
		primaryKey: false,
		allowNull: true,
		autoIncrement: false,
	}
});

/**
 * Gibt alle Artikel zurück
 * */
exports.getAllArticle = async function() {
	return await Article.findAll();
}

/***
 *
 * @param art
 * @param result
 * @returns {Promise<any>}
 */
exports.addOrUpdateArticle = async function(art, result){
	let existingArticle = await Article.findOne({
		where: {
			a_nr: art.a_nr
		}
	});
	if (existingArticle === null || existingArticle === undefined){
		result.create++;
		return await Article.create({
			a_nr : art.a_nr,
			a_nr2 : art.a_nr2,
			p_nr: art.p_nr,
			a_ean: art.a_ean,
			a_prod_nr: art.a_prod_nr,
			a_cond: art.a_cond,
			a_is_blocked: false,
			a_delivery_time: art.a_delivery_time
		}).then(() => {
			return result;
		// }).then((resolve) => {
		// 	return resolve;
		}).catch(() => {
			logger.err(TAG + 'addArticle', 'Konnte Artikel nicht updaten', `Der Artikel ${art.a_nr} konnte nicht korrigiert werden. - 1`);
		});
	}
	if(
		existingArticle.a_nr !== art.a_nr ||
		existingArticle.a_nr2 !== art.a_nr2 ||
		existingArticle.p_nr !== art.p_nr ||
		existingArticle.a_prod_nr !== art.a_prod_nr ||
		existingArticle.a_cond !== art.a_cond ||
		existingArticle.a_delivery_time !== art.a_delivery_time
	){
		result.update++;
		existingArticle.update({
			a_nr : art.a_nr,
			a_nr2 : art.a_nr2,
			p_nr: art.p_nr,
			a_prod_nr: art.a_prod_nr,
			a_cond: art.a_cond,
			a_delivery_time: art.a_delivery_time,
		}).catch(() => {
			logger.err(TAG + 'addArticle', 'Konnte Artikel nicht updaten', `Der Artikel ${art.a_nr} konnte nicht korrigiert werden. - 1`);
		});
	} else {
		result.unnecessary++;
		console.log(`a_nr: ${art.a_nr} unnötig geprüft.`)
	}

	return result;
};

/**
 * Legt einen Artikel an, wenn er noch nicht existiert.
 * Als Ergebnis wird zurückgegeben:
 * create.false = schon vorhanden, nichts gemacht
 * data = Artikeldaten
 *
 * create.true = angelegt
 *
 * @param art
 * @return Object
 * */
exports.createIfNotAvailable = async function(art) {
	let result = {
		create: false
	}
	let existingArticle = await Article.findOne({
		where: {
			a_nr: art.a_nr
		}
	});
	if (existingArticle === null || existingArticle === undefined) {
		existingArticle = await Article.create({
			a_nr : art.a_nr,
			a_nr2 : art.a_nr2,
			p_nr: art.p_nr,
			a_ean: art.a_ean,
			a_prod_nr: art.a_prod_nr,
			a_cond: art.a_cond,
			a_is_blocked: false,
			a_delivery_time: art.a_delivery_time
		}).catch(() => {
			logger.err(TAG + 'createIfNotAvailable', 'Konnte Artikel nicht updaten', `Der Artikel ${art.a_nr} konnte nicht korrigiert werden. - 2`);
		});
		result.data = existingArticle;
		result.create = true;

		return result;
	} else {
		if (
			existingArticle.a_nr2 !== art.a_nr2
			|| existingArticle.a_prod_nr !== art.a_prod_nr
			|| existingArticle.a_cond !== art.a_cond
			|| existingArticle.a_delivery_time + '' !== art.a_delivery_time
			|| existingArticle.a_ean + '' !== art.a_ean
		) {
			existingArticle.update({
				a_nr2: art.a_nr2,
				a_prod_nr: art.a_prod_nr,
				a_cond: art.a_cond,
				a_delivery_time: art.a_delivery_time,
				a_ean: art.a_ean,
			}).catch(() => {
				logger.err(TAG + 'createIfNotAvailable', 'Konnte Artikel nicht updaten', `Der Artikel ${art.a_nr} konnte nicht korrigiert werden. - 2`);
			});
		}
		result.data = existingArticle;
	}

	return result;
}

/***
 *
 * @returns {Promise<void>}
 */
exports.getAllEBay2Article = async function() {
	// eslint-disable-next-line no-useless-catch
	try {
		return await Article.findAll({
			where: {
				a_nr: {
					[Op.like]: 'E2-%'
				}
			}
		});
	} catch (e) {
		throw e;
	}
};

/***
 * Gibt alle Artikel mit EAN-Nr. zurück
 * @returns {Promise<void>}
 */
exports.getAllArticleWhitEan = async function() {
	// eslint-disable-next-line no-useless-catch
	try {
		return await Article.findAll({attributes: ['a_nr', 'a_ean']});
	} catch (e) {
		throw e;
	}
};

/**
 * Löscht nicht mehr benötigte Artikel
 * vorrangig für E2-Artikel
 *
 * @param a_nr
 * */
exports.delete = function(a_nr) {
	Article.destroy({
		where: {
			a_nr: a_nr,
		}
	});
}
/***
 *
 * @returns {Promise<void>}
 */
// exports.getAllArticle = async function(){
// 	// eslint-disable-next-line no-useless-catch
// 	try {
// 		return await Article.findAll({
// 			where: {
// 				a_ean: {
// 					[Op.ne]: '0'
// 				}
// 			}
// 		});
// 	} catch (e) {
// 		throw e;
// 	}
// };

/***
 *
 * @param p_nr
 * @returns {Promise<void>}
 */
exports.getAllArticlePerID = async function(p_nr){
	// eslint-disable-next-line no-useless-catch
	try {
		return await Article.findAll({
			where: {
				p_nr: p_nr
			}
		});
	} catch (e) {
		throw e;
	}

};

/***
 *  Ist der Artikel vorhanden?
 *  Gibt TRUE oder FALSE zurück.
 *
 * @param a_nr
 * @returns {Promise<boolean>}
 */
exports.isArticleAvailable = async function(a_nr){
	// eslint-disable-next-line no-useless-catch
	try {
		let article = await Article.findOne({
			where: {
				a_nr: a_nr
			}
		});

		return article !== null;

	} catch (e) {
		throw e;
	}
};

/**l
 *
 * @param date
 * @returns {Promise<Array>}
 */
exports.getUpdatedArticle = async (date) => {
	try {
		return await Article.findAll({
			where: {
				updatedAt: {
					[Op.gte]: `${
						date.getFullYear()}-${
						(date.getMonth() + 1).toString().padStart(2, '0')}-${
						date.getDate().toString().padStart(2, '0')} ${
						date.getHours().toString().padStart(2, '0')}:${
						date.getMinutes().toString().padStart(2, '0')}:00`,
				}
			}
		});
	} catch (e) {
		return []
	}
};

/***
 *
 * @param a_nr
 * @returns Object
 */
exports.getArticlePerID = async function(a_nr){
	return Article.findOne({
		where: {
			a_nr: a_nr
		}
	});
};

/***
 * sync-stuff
 * @returns {Promise<void>}
 */
exports.sync = async () => {
	await Article.sync({alter: true});
	logger.log(`${TAG}sync`, `articles is sync`,
		`Das Synchronisieren der Tabelle "articles" wurde aufgerufen.`);
};


exports.getAllArticlePerValue = async (key, value) => {
	let klausel = {}
	klausel[key] = value;
	return Article.findAll({
		where: klausel
	})
}

exports.updateArt = async (a_nr ,type, value) => {
	let article = await Article.findOne({
		where: {
			a_nr: a_nr
		}
	});
	let klausel = {}
	klausel[type] = value;
	article.update(klausel)
		.catch(() => {
		logger.err(TAG + 'updateArt', 'Konnte Artikel nicht updaten', `Der Artikel ${a_nr} konnte nicht korrigiert werden. 3`);
	});
	return article;
};
