'use strict';

const config = require('../configs/app.config').getConfig();

const Sequelize = require('sequelize');
const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);
const Op = Sequelize.Op;
const logger = require('../helper/util/logger.util');
const TAG = 'repo.log.';

/***
 * Model
 */
const Logs = seq.define('log', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		allowNull: false,
		autoIncrement: true,
	},
	type: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	tag: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	shortDescription: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	description: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});

const FilterLogs = seq.define('log', {
	type: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	tag: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});

/***
 *
 * @param type
 * @param tag
 * @param short
 * @param long
 * @returns Promise {<*>}
 */
exports.log = async function(type, tag, short, long){
	try {
		await seq.authenticate();
		// console.log('Verbindung zur DB ist gut.');
	} catch (error) {
		console.error('Unable to connect to the database:', error);
	}

	return Logs.create({
		type: type,
		tag: tag,
		shortDescription: short.substring(0, 255),
		description: long.substring(0, 255)
	})
};

/***
 *
 * @param offset
 * @param limit
 * @returns Promise {<<Model[]>>}
 */
exports.getLog = async function (offset, limit){
	return Logs.findAll({
		offset: offset,
		limit: limit,
		order: [
			['id', 'DESC']
		]
	});
};

/***
 * sync-stuff
 * @returns {Promise<void>}
 */
exports.sync = async () => {
	await Logs.sync({alter: true});
	logger.log(`${TAG}sync`, `logs is sync`,
		`Das Synchronisieren der Tabelle "logs" wurde aufgerufen.`);
};

/**
 * Gibt die Daten zurück, die in den SELECT Feldern angezeigt werden.
 * @param groupName
 * @param type
 * @param tag
 * @param shortDescription
 * @param description
 * @param unixStartDate
 * @param unixEndDate
 * @returns {Promise<any>}
 */
exports.getGroupOfName = async function (groupName, type, tag, shortDescription, description, unixStartDate, unixEndDate){

	let sql = {
		group: groupName,
		order: [groupName]
	};

	sql = await this.setSqlWhere(sql, type, tag, shortDescription, description, unixStartDate, unixEndDate)

	return FilterLogs.findAll(sql);
};
/**
 * Gibt die Daten zurück, die unter '/logs' angezeigt werden.
 * Sie können gefiltert werden.
 * @param offset
 * @param limit
 * @param type
 * @param tag
 * @param shortDescription
 * @param description
 * @param unixStartDate
 * @param unixEndDate
 * @returns Promise {<<Model[]>>}
 */
exports.getLogFilterData = async function (offset, limit, type, tag, shortDescription, description, unixStartDate, unixEndDate) {
	let sql = {
		offset: offset,
		limit: limit,
		order: [
			['id', 'DESC']
		]
	};

	sql = await this.setSqlWhere(sql, type, tag, shortDescription, description, unixStartDate, unixEndDate);

	return Logs.findAll(sql);
}

/**
 * @param type
 * @param tag
 * @param shortDescription
 * @param description
 * @param unixStartDate
 * @param unixEndDate
 * @returns Promise {<<Model[]>>}
 * */
exports.getNumberOfEntries = async function (type, tag, shortDescription, description, unixStartDate, unixEndDate) {
	let sql = {};
	sql = await this.setSqlWhere(sql, type, tag, shortDescription, description, unixStartDate, unixEndDate);
	let result = await Logs.count(sql);

	return result;
}

/**
 * Setzt die 'WHERE' Klausel im SQL
 * @param sql
 * @param type
 * @param tag
 * @param shortDescription
 * @param description
 * @param unixStartDate
 * @param unixEndDate
 * @returns Promise {<*>}
 */
exports.setSqlWhere = async function (sql, type, tag, shortDescription, description, unixStartDate, unixEndDate) {
	sql.where = {};

	if (!(type === '' || type === undefined)) {
		sql.where.type = type;
	}

	if (!(tag === '' || tag === undefined)) {
		sql.where.tag = tag;
	}

	if (!(shortDescription === '' || shortDescription === undefined)) {
		sql.where.shortdescription = {
			[Op.like]: '%' + shortDescription + '%'
		};
	}

	if (!(description === '' || description === undefined)) {
		sql.where.description = {
			[Op.like]: '%' + description + '%'
		};
	}

	if (!(unixStartDate === undefined || unixStartDate === '-1' || isNaN(parseInt(unixStartDate))
		|| unixEndDate === undefined || unixEndDate === '-1' || isNaN(parseInt(unixEndDate)))
	) {
		let startDate = new Date(unixStartDate * 1000);
		let endDate = new Date(unixEndDate * 1000);
		sql.where.createdAt = {
			[Op.between]: [startDate, endDate]
		};
	}

	return sql;
}

exports.deleteByDate = async function (cleanUpDate) {
	/** EQ("="), GTE(">="), GT(">"), LT("<"), LTE("<="); */
	let destroyCount = await Logs.count({
		where: {
			createdAt: {
				[Op.lte]: `${cleanUpDate}`,
				// [Op.lte]: `2021-05-01 02:14:00`,
			} ,
		}
	});

	Logs.destroy({
		where: {
			createdAt: {
				[Op.lte]: `${cleanUpDate}`,
				// [Op.lte]: `2021-05-01 02:14:00`,
			} ,
		}
	});

	return destroyCount;
}