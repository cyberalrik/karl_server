'use strict';

const config = require('../configs/app.config').getConfig();
const Sequelize = require('sequelize');
const logger = require('../helper/util/logger.util');
const TAG = 'repo.temp.';

/***
 * Model
 */
class index {
	constructor() {
		const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

		this.tempRepo = seq.define('temp', {
			a_nr: {
				type: Sequelize.STRING,
				defaultValue: Sequelize.STRING,
				primaryKey: false
			},
			number: {
				type: Sequelize.INTEGER,
			}
		});
	}

	/**
	 * Legt Kategorien an.
	 * Überprüft vorher, ob es sie schon gibt.
	 * */
	async setTemp(a_nr) {
		let existingCategory = await this.tempRepo.findAll({
			where: {
				a_nr: a_nr
			}
		});

		if (existingCategory === null
			|| existingCategory === undefined
			|| existingCategory.length === 0
		){
			return await this.tempRepo.create({
				a_nr: a_nr
			});
		}
	}


	/***
	 * sync-stuff
	 * @returns Promise{<void>}
	 */
	async sync() {
		await this.tempRepo.sync({alter: true});
		logger.log(`${TAG}sync`, `category is sync`,
			`Das Synchronisieren der Tabelle "category" wurde aufgerufen.`);
	}
}

module.exports = index;