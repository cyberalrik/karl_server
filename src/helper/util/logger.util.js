'use strict';

let winston = require('winston');
let Transport = require('../util/transport.logger.util');
const Mail = require('../../services/service.send.mail');
const MailData = require('../../refac/entity/mailData/check');

/**
 * Creation of the stuff
 * @type {winston.Logger}
 */
const logger = winston.createLogger({
	format: winston.format.json(),
	transports: [
		new winston.transports.Console(),
		new Transport
	]
});

/**
 * standard logging
 * @param tag
 * @param short
 * @param long
 * @param mailData
 */
exports.log = async (tag, short, long = '', mailData = null) => {
	// if (long === null || long === undefined) {
	// 	long = '';
	// }

	logger.log({
		level: 'info',
		shortDescription: short,
		longDescription: long,
		tag: tag,
	});

	if (mailData !== null && await MailData.isOk(mailData)) {

		let subject = await mailData.getSubject();
		let mailBody = await mailData.getMailBody();
		let addMailTo = await mailData.getAddMailTo();
		let addMailCc = await mailData.getAddMailCc();
		let isHtml = await mailData.getIsHtml();
		let mailBodyHTML = await mailData.getMailBodyHTML();
		let attachments = {
			isAttachments: await mailData.isAttachments(),
			value: await mailData.getAttachments()
		};

		let mail = new Mail();
		await mail.sendEMail(subject, short, mailBody, tag, '', addMailTo, addMailCc, isHtml, mailBodyHTML, attachments);
	}

};

/***
 * error-logging :)
 * @param tag
 * @param short
 * @param long
 * @param mailBodyXXL
 * @param addMailTo
 * @param addMailCc
 */
exports.err = async (tag, short, long, mailBodyXXL = '', addMailTo = null, addMailCc = null) => {
	if (long === null || long === undefined){
		long = '';
	}
	let mail = new Mail();

	let mailBody = `In Karl ist ein Fehler aufgetreten.
TAG: ${tag}
Fehlertext: \n${long}`;

	await mail.sendEMail(long, short, mailBody, tag, mailBodyXXL, addMailTo, addMailCc);

	logger.log({
		level: 'error',
		shortDescription: short,
		longDescription: long,
		tag: tag,
	});
};