/* eslint-disable no-console */
'use strict';

const Transport = require('winston-transport');
const logRepo = require('../../repository/repo.log');

/**
 * Implement an DB-Logger as a Custom Transportprotocoll
 * @type {YourCustomTransport}
 */
module.exports = class YourCustomTransport extends Transport {
	constructor(opts) {
		super(opts);
	}

	log(info, callback) {
		logRepo.log(info.level, info.tag, info.shortDescription, info.longDescription).then();
		callback();
	}
};