const mssql = require('mssql');
const selectLineConfig = require('../configs/selectLine.config');
// const logger = require('../helper/util/logger.util');
// const TAG = 'selectLine.database.helper123';

/**
 * Interface to the AzureDB
 * @param cmd
 * @returns {Promise<void>}
 */
exports.sql = async (cmd) => {
    try {
        await mssql.close();
        let pool = await mssql.connect(selectLineConfig);
        return await pool.request().query(cmd);
    } catch (err) {
        await setTimeout(() => {
        }, 10000);
        console.log('Es ist ein Fehler, in zusammenhang mit der SelectLine-Datenbank, aufgetreten.\n' +
            `${err.message}\nDie Abfrage war folgende:\n${cmd}\n`)
        // logger.err(TAG, 'Database Error 1.', 'Es ist ein Fehler, in zusammenhang mit der SelectLine-Datenbank, aufgetreten.',
        //     `${err.message}\nDie Abfrage war folgende:\n${cmd}\n`);
        // logger.log(TAG, 'Database Error 2. ', cmd);
        // logger.log(TAG, 'Database Error 3. ', err.message);
        // throw err;
    }
};