'use strict';

const mariaDb = require('mariadb');
const itmConfig = require('../configs/itm.config');
const logger = require('../helper/util/logger.util');
const TAG = 'itm.database.helper';

/***
 * Interface to the internal MariaDB-Cluster
 * @param cmd
 * @returns {Promise<*>}
 */
exports.sql = async (cmd) => {
	const pool = mariaDb.createPool({
		host: itmConfig.DB_HOST,
		user: itmConfig.USER,
		password: itmConfig.PW,
		database: itmConfig.DB,
		port: itmConfig.PORT,
		connectionLimit: 5
	});
	let conn;
	try {
		conn = await pool.getConnection();
		return await conn.query(cmd);
	} catch (err) {
		logger.err(TAG, 'Database Error', 'unable to request');
		return [];
	} finally {
		if (conn)
			conn.end();
		await pool.end();
	}
};