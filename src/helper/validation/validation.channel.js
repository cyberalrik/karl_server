'use strict';

/**
 * Prüft, ob imageWidth und imageHeight keine Nummern sind.
 * @param imageWidth
 * @param imageHeight
 * @returns {boolean}
 */
exports.checkChannelImageDimensions = (imageWidth, imageHeight) => {
    return (isNaN(imageWidth) || isNaN(imageHeight));
};

/**
 * Prüft, ob channelSurcharge keine Nummer ist.
 * @param channelSurcharge
 * @returns {boolean}
 */
exports.checkChannelSurcharge = (channelSurcharge) => {
    return isNaN(channelSurcharge);
}