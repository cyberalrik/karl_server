'use strict';

/***
 * Check if the new URL is valid
 * @param url
 * @returns {boolean}
 */
exports.checkImageURL = (url) => {
	if (url === null ||url === undefined){
		return false;
	}
	// if (!url.includes('jpg') && !url.includes('jpeg')){
	// 	return false;
	// }
	return !(!url.includes('http') && !url.includes('https'));
};