'use strict';

const cron = require('node-cron');
const logger = require('../../helper/util/logger.util');
const TAG = 'installer.cronInstaller.toInstall';

class Initialize {
    /***
     * Installinterface for specific Crons
     * @param time
     * @param location
     * @param scheduled
     * @param cronName
     * @param cronClass
     * @return {Promise <void>}
     */
    async toInstall(time, location, scheduled, cronName, cronClass = false){
        await cron.schedule(time, async () => {
            logger.log(TAG, 'Start ' + cronName,'Der Cronjob ' + cronName + ' wird aufgerufen.');
            try {
                let result = {};
                if (cronClass) {
                    let Task = require(location);
                    let task = new Task();
                    result = await task.cronTask();
                } else {
                    result = await require(location).cronTask();
                }

                if (result.successful) {
                    logger.log(TAG, 'End ' + cronName, 'Der Cronjob '
                        + cronName + ' wurde erfolgreich abgeschlossen.');

                } else {
                    logger.err(TAG, 'Error-Stop ' + cronName, 'Der Cronjob '
                        + cronName + ' wurde fehlerhaft beendet.');

                    logger.err(TAG, 'Fehlerbeschreibung 1', result.messages);
                }
            } catch (e) {
                logger.err(TAG
                    , 'Fehlerbeschreibung 2'
                    , `Beim Aufruf des CronJobs ${cronName} ist ein Fehler aufgetreten.`
                    , e.message);
            }
        }, {
            scheduled: scheduled,
        }).start();
    }

}

module.exports = Initialize