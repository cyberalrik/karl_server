'use strict';

let channelRepo = require('../../repository/repo.channel');

module.exports = async function() {
	let channels = await channelRepo.getChannels();
	if (channels.length === 0){
		await channelRepo.setChannel('Amazon.de', 'amde', false, 0.0, 'IMG_AMZ', 'AMZQTY', 1000, 1200);
		await channelRepo.setChannel('it-market', 'cucybertr1', true, 0.0, 'IMG_ITM', 'ITMQTY', 0, 0);
		await channelRepo.setChannel('ebay.de', 'ebde', true, 0.0, 'IMG_EBAY', 'EBAYQTY', 500, 500);
		await channelRepo.setChannel('ebay.uk', 'ebuk', true, 0.0, 'IMG_EBAY', 'EBAYUKQTY', 500, 500);
		await channelRepo.setChannel('TB', 'default', true, 0.0, 'IMAGE', '', 0, 0);
	}
	return true;
};