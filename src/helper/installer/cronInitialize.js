'use strict';

const CronInstaller = require('./cronInstaller');
const CronParameter = require('../../configs/cronParameter');
const DataAccessSys = require('../../repository/repo.sys');
const config = require('../../configs/app.config').getConfig();
const logger = require('../../helper/util/logger.util');
const TAG = 'cronInstaller.cronInitialize';

class CronInitialize {
    async initializeCron() {
        let done = true;
        let cronParameter = new CronParameter();
        let parameter = await cronParameter.getCronParameter();
        let dataAccessSys = new DataAccessSys();
        /**
         * muss unbedingt gemacht werden, falls die Struktur von sys geändert wurde
         * oder bei neuanlage die Tabelle noch nicht existiert.
         * */
        await dataAccessSys.sync();
        try {
            if (config.ACTIVE_CRON) {
                for (let i = 0; i < parameter.length; i++){
                    if (parameter[i].active) {
                        let cronInstaller = new CronInstaller();
                        await cronInstaller.toInstall(
                            parameter[i].time,
                            parameter[i].loc,
                            false,
                            parameter[i].name,
                            parameter[i].cronClass
                        );

                        logger.log(TAG,
                            parameter[i].name,
                            'Der Cronjob "' + parameter[i].name + '" wurde initialisiert und läuft '
                            + parameter[i].timeDescription);

                    } else {
                        logger.log(TAG,
                            parameter[i].name,
                            'Der Cronjob "' + parameter[i].name
                            + '" ist ausgestellt und wurde nicht initialisiert.');
                    }

                    /**
                     * Bei jedem Start wird überprüft, ob die Jobs in der sys-Tabelle angelegt sind.
                     * Wenn nicht, werden sie angelegt, ansonsten berichtigen oder so gelassen
                     */
                    await dataAccessSys.add(parameter[i]);
                }
            } else {
                logger.log(TAG, 'Cronjob\'s',
                    'Alle Cronjob\'s sind deaktiviert. ');
                for (let i = 0; i < parameter.length; i++){
                    parameter[i].active = false;
                    await dataAccessSys.add(parameter[i]);
                }
            }
        } catch (err){
            done = false;
        }
        return done;

    }
}

module.exports = CronInitialize