'use strict';

const constants = require('../../configs/app.config').getConfig();

/****
 * Sync the repos to the database.
 * @returns {Promise<void>}
 */
module.exports = async function() {

	if( constants.SYNC === false)
		return;

	await require('../../repository/repo.log').sync();
	await require('../../repository/article/repo.article.components').sync();

	let param = await require('../../repository/repo.karl.param');
	await param.sync();

	await param.updateParam('COLLECTION_JOB_RUNNER', '0');

	let InfoBox = require('../../repository/repo.infobox');
	let infoBox = new InfoBox();
	await infoBox.sync();

	await require('../../repository/article/repo.article').sync();
	await require('../../repository/repo.components').sync();
	await require('../../repository/article/repo.article.noupdate').sync();
	await require('../../repository/repo.product').sync();
	await require('../../repository/repo.images').sync();
	await require('../../repository/repo.channel').sync();
	await require('../../repository/article/repo.article.channel').sync();
	await require('../../repository/repo.pricelist').sync();
	await require('../../repository/repo.sys').sync();
};
