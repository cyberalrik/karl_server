'use strict';

const mssql = require('mssql');
const horstConfig = require('../configs/horst.config');
const logger = require('../helper/util/logger.util');
const TAG = 'horst.azure.database.helper';

/**
 * Interface to the AzureDB
 * @param cmd
 * @returns {Promise<void>}
 */
exports.sql = async (cmd) => {
	try {
		await mssql.close();
		let pool = await mssql.connect(horstConfig);
		return await pool.request().query(cmd);
	} catch (err) {
		await setTimeout(() => {
		}, 10000);
		logger.err(TAG, 'Database Error', 'unable to set up connection');
		throw err;
	}
};