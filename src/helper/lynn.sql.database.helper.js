const mssql = require('mssql');
const lynnConfig = require('../configs/lynn.config');
const logger = require('../helper/util/logger.util');
const Stopwatch = require("statman-stopwatch");
const TAG = 'lynn.database.helper';

/**
 * Interface to the AzureDB
 * @param cmd
 * @param QueryTypes
 * @param newTimeout
 * @returns {Promise<{}>}
 */
exports.sql = async (cmd, QueryTypes = null, newTimeout = null, TAG2 = '-') => {
    try {
        await mssql.close();
        let sw = {};
        if (newTimeout !== null) {
            lynnConfig.requestTimeout = newTimeout;
            logger.log(TAG, 'TimeOut geändert', `Es wurde das TimeOut zur Lynn-DB für diese Abfrage auf ${newTimeout} ms geändert.`)
            sw = new Stopwatch();
            sw.start();
        }
        // await logger.log(TAG2, 'vor', 'mssql.connect(lynnConfig)');
        let pool = await mssql.connect(lynnConfig);
        // await logger.log(TAG2, 'nach', 'mssql.connect(lynnConfig)');
        let result = await pool.request().query(cmd);

        if (newTimeout !== null) {
            sw.stop();
            const delta = sw.read();
            logger.log(TAG, 'TimeOut geändert', `Vom neu gesetzten TimeOut ${newTimeout} wurden ${delta.toFixed(0)} ms benötigt.`)
        }

        return result;

    } catch (err) {
        await setTimeout(() => {
        }, 10000);

        logger.err(TAG, 'Database Error 1.', 'Es ist ein Fehler, in zusammenhang mit der LYNN-Datenbank, aufgetreten.',
            `${err.message}\nDie Abfrage war folgende:\n${cmd}\n`);
        logger.log(TAG, 'Database Error 2. ', cmd);
        logger.log(TAG, 'Database Error 3. ', err.message);
        if (QueryTypes !== null) {
            logger.log(TAG, 'Database Error 4. ', QueryTypes);
        }
        // throw err;
        return null;
    } finally {
        await mssql.close();
    }
};