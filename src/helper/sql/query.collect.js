'use strict';

/***
 * Get Components for an given MPN
 * @param mpn
 * @returns {string}
 */
exports.getComponentsFromCCISQL = (mpn) => {
	return `SELECT
 v2.text AS Header,
 v3.text AS Body,
  e.DisplayOrder
FROM cds_Especde e
JOIN cds_Evocde v ON e.sectid = v.id
JOIN cds_Evocde v2 ON e.hdrid = v2.id
JOIN cds_Evocde v3 ON e.bodyid = v3.id
join cds_Prod p ON p.ProdID = e.ProdID
WHERE  p.MfPN = '${mpn}'`
};

/***
 *
 * @param offset
 * @param next
 * @returns {string}
 */
exports.getMasterSQL = (offset, next) => {
	return `Select
    pp.ProductForeignId as 'ProductId',
    p.Number as 'ManufacturerProductNumber',
    p.Description as 'ManufacturerDescriptionShort',
    p.ThirdParty as 'IsThirdParty',
    m.Name as 'p_brand',
    SUBSTRING(m.name,0,3) +'_CYBER_' + CONVERT(varchar(10), m.Id) as 'p_brand_key',
    p.Description as 'p_text_de'
from erp.ProductProperty as pp
    join erp.Product as p with (nolock) 
    	on pp.ProductId = p.Id
    join erp.Manufacturer as m with (nolock) 
    	on p.ManufacturerId = m.Id
where 1=1
    and ISNULL(pp.ITMMenge,0) + ISNULL(pp.LagerMenge,0) > 0
    and pp.State = 1056
    and pp.SelOnly = 0 
order by pp.ProductForeignId desc
OFFSET ${offset} Rows FETCH NEXT ${next} ROWS ONLY;`;
};

/***
 *
 * @param mpn
 * @returns {string}
 */
exports.getEanFromCciSQL = (mpn) => {
	return `SELECT m.distisku AS UPCEAN_Code
FROM   cds_Metamap m
  join cds_Prod as p 
  	on p.ProdID = m.ProdID

WHERE  p.MfPN = '${mpn}'
  and DistiID like 'C01%'
limit 1;`
};

/***
 *
 * @param mpns
 * @returns {string}
 */
exports.getCciSQL = (mpns) => {
	return `SELECT
  p.MfPN as mpn,
  de.p_text_de as p_text_de,
  ee.p_text_ee as p_text_ee,
  media.url as url,
  d.Name as p_brand,
  d.ID as p_brand_key,
  cats.cat_id as cat_id,
  cats.CategoryName as p_cat_name
from cds_Prod as p
left join (SELECT
  m.ProdID as id,
  v2.text AS p_text_de
FROM cds_Mspecde m
JOIN cds_Mvocde v 
	ON m.hdrid = v.id
JOIN cds_Mvocde v2 
	ON m.bodyid = v2.id
WHERE 1=1
  and v.Text = 'Produktbeschreibung') as de 
  	on p.ProdID = de.id
left join (
  SELECT
  m.ProdID as id,
  v2.text AS p_text_ee
FROM cds_Mspecee m
JOIN cds_Mvocee v 
	ON m.hdrid = v.id
JOIN cds_Mvocee v2 
	ON m.bodyid = v2.id
WHERE 1=1
  and v.Text = 'Product Description') as ee 
									  on p.ProdID = ee.id
left join (
    SELECT dcl.prodid as prodId,
      dc.url as url
    FROM cds_DigContent dc 
    JOIN cds_DigContent_Links dcl
      ON dc.contentguid = dcl.contentguid
    JOIN cds_DigContent_Meta dcm
      ON dcm.contentguid = dc.contentguid AND dcm.metaatrid = 6
    JOIN cds_DigContent_Meta_ValVoc dcmv
      ON dcmv.metavalueid = dcm.metavalueid
    WHERE dc.mediatypeid = 15
      AND dcmv.metavaluename = '640 x 480'
  ) as media on media.prodId = p.ProdID
join cds_Distivoc as d on p.MfID = d.ID
join (
  select cca.CatID as cat_id, cca.CategoryName, pr.ProdID as pid from cds_Prod as pr
join cds_Cct_Products as ca 
	on pr.ProdID = ca.ProdID
join cds_Cct_Categories as cca 
	on cca.CatID = ca.CatID
where 1=1
  and isPrimary = true
  and CategoryLevel = 4
  ) as cats 
    on cats.pid = p.ProdID
 where 1=1 
 	and lower(p.MfPN) = '${mpns}';`;
};

/***
 *
 * @param date
 * @returns {string}
 */
exports.getPriceAndStockUpdates = (date) => {
	date.setHours(date.getHours() - 2);

	return `select a.Id,
    ISNULL(ap_price.MarketplacePrice, 0) as MarketplacePrice,
    ISNULL(ap_quant.MarketplaceQuantity, 0)  as MarketplaceQuantity
from erp.Article as a with (nolock)
left join (
    select ArticleId as Id, ValueM  * 1.16 as 'MarketplacePrice'
        from erp.ArticleProperty with (nolock)
    where PropertyDefinitionId = 39
    ) as ap_price 
	  on a.Id = ap_price.Id
left join (
    select ArticleId as Id, ValueN as 'MarketplaceQuantity'
        from erp.ArticleProperty with (nolock)
    where PropertyDefinitionId = 56
    ) as ap_quant 
      on a.Id = ap_quant.Id
where 1=1 
	and (ap_quant.MarketplaceQuantity > 0
            or ap_price.MarketplacePrice > 0
            or a.ModifiedDate >= '${date.getFullYear()}-${
		(date.getMonth() + 1).toString().padStart(2, '0')}-${
		date.getDate().toString().padStart(2, '0')} ${
		date.getHours().toString().padStart(2, '0')}:${
		date.getMinutes().toString().padStart(2, '0')}:00')`;
};

/***
 *
 * @param products
 * @returns {string}
 */
exports.getLimitedArticles = (products) => {
	let inList = '(\'' + products.recordset[0]['ProductId'] +'\'';
	for (let i = 1; i < products.recordset.length; i++ ){
		inList = inList + ',\'' + products.recordset[i]['ProductId'] + '\''
	}
	inList = inList + ')';
	return `Select
    pp.ProductForeignId as 'p_nr',
    pp.ArticleForeignId as 'a_nr',
    isNull(pp.ReferenceCode,'NoRefCode') as 'a_nr2',
    p.Number as 'a_prod_nr',
    d.ValueC as 'a_cond'
from erp.ProductProperty as pp
    join erp.Product as p with (nolock) 
    	on pp.ProductId = p.Id
    join erp.Manufacturer as m with (nolock) 
    	on p.ManufacturerId = m.Id
    join core.Domainvalue as d with (nolock) 
    	on d.Id = pp.Condition
where 1=1 
    and pp.State = 1056
    and pp.SelOnly = 0 
    and pp.ProductForeignId in ${inList}`
};

/***
 *
 * @param product
 * @returns {string}
 */
exports.getImagesPerList = (product) => {
	return `SELECT p.MfPN,
       		dc.url
			FROM cds_DigContent dc 
			JOIN cds_DigContent_Links dcl 
				ON dc.contentguid = dcl.contentguid
			JOIN cds_Prod as p 
				on p.ProdID = dcl.ProdID
			WHERE dc.mediatypeid=15
				AND p.MfPN like '${product['a_prod_nr']}'`
};

exports.getPriceList = (price, quant, channel, delta = true, p_nr = null) => {
	let prae = '';
	let condition_p_nr = '';
	let date = new Date();
	date.setHours(date.getHours() - 10);

	if(delta)
		prae = `and erpProductPropertyModifiedDate >= '${
		date.getFullYear()}-${
		(date.getMonth() + 1).toString().padStart(2, '0')}-${
		date.getDate().toString().padStart(2, '0')} ${
		date.getHours().toString().padStart(2, '0')}:${
		date.getMinutes().toString().padStart(2, '0')}:00'`;

	if(p_nr !== null)
		condition_p_nr = `and erpProductPropertyProductForeignId = '${p_nr}'`;

	return `Select
       '${channel}' as channel,
       erpProductPropertyArticleForeignId as 'a_nr',
       ROUND(ISNULL(${price},0),2) as 'price',
       ISNULL(${quant},0) as 'quant'
		from bde.AllProperties with (nolock)
		where 1 = 1
			and erpProductPropertyState = 1056
			and erpProductPropertySelOnly = 0 
			and coreDomainvalueErpProductPropertyConditionId in (8, 9)
    ${prae}
    ${condition_p_nr}`
}

exports.getProductsFromLynn = (p_nr, date) => {
	let prae = '';
	if(!(p_nr === null || p_nr === undefined )) {
		prae = `and p.erpProductPropertyProductForeignId = ${p_nr}`;
	}

	if(!(date === null || date === undefined )) {
		// prae += ` and p.erpProductCreationDateProduct > '${date}'`;
		prae += ` and (
	        p.erpProductCreationDateProduct > '${date}'
	        or
	        p.erpProductModifiedDate > '${date}'
	        or
	        p.erpProductPropertyCreationDateArticle > '${date}'
	        or
	        p.erpProductPropertyModifiedDate > '${date}'
	     )`;
	}

	return `SELECT distinct
    p.erpProductPropertyProductForeignId as 'p_nr',
    p.erpProductNumber as 'p_name',
    p.erpProductDescriptionGerman as 'p_text_de',
    p.erpProductDescription as 'p_text_ee',
    p.erpManufacturerName as 'p_brand',
    CONVERT(varchar(10), p.erpProductManufacturerId) + '_CYBER' as 'p_brand_key',
    'CY0001' as 'p_cat_id',
    p.erpProductThirdParty as 'p_3_rd',
	p.erpProductSuchbegriffe1 as 'p_keyword1',
	p.erpProductSuchbegriffe2 as 'p_keyword2',
	p.erpProductSuchbegriffe3 as 'p_keyword3',
    p.erpProductProductinfoExtern as 'p_infoExternGerman',
    p.erpProductProductinfoExternEnglish as 'p_infoExternEnglish',
    isnull(p.erpProductGewicht, 0) as 'p_weight'
from bde.AllProperties as p with (nolock)
where 1=1
    and p.erpProductPropertyState = 1056
    and p.erpProductPropertySelOnly = 0
    and (
        p.erpProductPropertyITMMenge > 0
        or p.erpProductPropertyeBayMenge > 0
        or p.erpProductPropertyLagerMenge > 0
    )
    and p.coreDomainvalueErpProductPropertyConditionId in (8,9)
	${prae}`;
}

exports.getArticleFromLynn = (p_nr, date) => {
	let prae = '';
	if(!(p_nr === null || p_nr === undefined )) {
		prae = `and p.erpProductPropertyProductForeignId = ${p_nr}`;
	}

	if(!(date === null || date === undefined )) {
		// prae += ` and p.erpProductPropertyCreationDateArticle > '${date}'`;
		prae += ` and (
	        p.erpProductCreationDateProduct > '${date}'
	        or
	        p.erpProductModifiedDate > '${date}'
	        or
	        p.erpProductPropertyCreationDateArticle > '${date}'
	        or
	        p.erpProductPropertyModifiedDate > '${date}'
	     )`;
	}

	return `SELECT 
    p.erpProductPropertyArticleForeignId as 'a_nr',
    p.erpProductPropertyReferenceCode as 'a_nr2',
    p.erpProductPropertyProductForeignId as 'p_nr',
    0 as 'a_ean',
    p.erpProductnumber as 'a_prod_nr',
    p.coreDomainvalueErpProductPropertyConditionValueC as 'a_cond',
    p.coreSelectionErpProductPropertyEbayLieferzeit + 0 as 'a_delivery_time'
from bde.AllProperties as p with (nolock)
where 1=1
    and p.erpProductPropertyState = 1056
    and p.erpProductPropertySelOnly = 0
    and (
        p.erpProductPropertyITMMenge > 0
        or p.erpProductPropertyeBayMenge > 0
        or p.erpProductPropertyLagerMenge > 0
    )
	and p.coreDomainvalueErpProductPropertyConditionId in (8,9)
	${prae}`;
}

exports.getE2ArticleFromLynn = (p_nr) => {
	let prae = '';
	if(!(p_nr === null || p_nr === undefined )) {
		prae = `and p.erpProductPropertyProductForeignId = ${p_nr}`;
	}

	return `SELECT
    p.erpProductPropertyArticleForeignId as 'a_nr_original',
    concat('E2-', p.erpProductPropertyArticleForeignId) as 'a_nr',
    concat('E2-', p.erpProductPropertyReferenceCode) as 'a_nr2',
    p.erpProductPropertyProductForeignId as 'p_nr',
    p.erpProductNumber as 'a_prod_nr',
    CASE
        WHEN p.coreDomainvalueErpProductPropertyConditionValueC = 'new' THEN 'E2_new'
        WHEN p.coreDomainvalueErpProductPropertyConditionValueC = 'ref' THEN 'E2_ref'
    END as 'a_cond',
    'false' as a_is_blocked,
    2 as 'a_delivery_time',
    0 as a_ean,
    'ebde' as channel,
    ROUND(ISNULL(ISNULL(erpProductPropertyeBay2PreisBruttoEur, erpProductPropertyeBay2PreisBruttoEurCalculated), 0),2) as 'price',
    p.erpProductPropertyeBay2Menge as 'quant'
from bde.AllProperties as p with (nolock)
where 1=1
    and p.erpProductPropertyState = 1056
    and p.erpProductPropertySelOnly = 0
    and (
            erpProductPropertyeBay2PreisBruttoEur is not null
        or
            erpProductPropertyeBay2PreisBruttoEurCalculated is not null
        )
    and isnull(p.erpProductPropertyeBay2Menge, 0) > 0
    and p.coreDomainvalueErpProductPropertyConditionValueC in ('ref', 'new')
    ${prae}`;
}

exports.getAllArticleFromProduct = (p_nr) => {
	return `SELECT 
    p.erpProductPropertyArticleForeignId as 'a_nr'
from bde.AllProperties as p with (nolock)
where 1=1
    and p.erpProductPropertyState = 1056
    and p.erpProductPropertySelOnly = 0
	and p.erpProductPropertyProductForeignId = ${p_nr}`;
}

exports.getDeletedAndInaktivArticleFromLynn = (days) => {
	return `SELECT erpProductPropertyArticleForeignId as a_nr
FROM bde.AllProperties with (nolock)
WHERE erpProductPropertyState in (1057, 1058, 1065)
AND erpProductPropertyModifiedDate > CAST(DATEADD(DAY, -${days}, GETDATE()) AS DATE)`;
}

exports.getSelOnlyArticleFromLynn = (days) => {
	return `SELECT erpProductPropertyArticleForeignId as a_nr
FROM bde.AllProperties with (nolock)
WHERE erpProductPropertySelOnly = 1
AND erpProductPropertyModifiedDate > CAST(DATEADD(DAY, -${days}, GETDATE()) AS DATE)`;
}

exports.getAllLanguageComponentsFromMpn = (mpn) => {
	return `select mpn as MfPN, identifier as Header, value as Body, DisplayOrder, language as Lang
from ct_components
where mpn = '${mpn}'`
};

/**
 * Gibt alle Verlinkungen zurück, die zu dem übergebenen Typ gehören.
 * Es können auch mehrere Typen übergeben werden.
 * Dazu sind die Werte als String und mit Komma getrennt zu übergeben.
 *
 * Beispiele:
 * als Parameter übergeben:
 * '1'   --> gibt alle vom Typ Foto zurück
 * '1,2' --> gibt alle vom Typ Foto und Komplett zurück
 * */
exports.getLinksForType = (type) => {
	return `SELECT ap1.erpProductPropertyProductForeignId as parent, ap2.erpProductPropertyProductForeignId as child
FROM erp.ProductPropertyLink AS ppl with (nolock)
JOIN bde.AllProperties AS ap1 with (nolock)
	ON ap1.erpProductId = ppl.ParentProduct
JOIN bde.AllProperties AS ap2 with (nolock)
	ON ap2.erpProductId = ppl.ChildProduct
WHERE Typ in (${type})
GROUP BY ap1.erpProductPropertyProductForeignId, ap2.erpProductPropertyProductForeignId
ORDER BY ap1.erpProductPropertyProductForeignId, ap2.erpProductPropertyProductForeignId;`
};

/** Es sollen alle Produkte geholt werden, egal welcher Status. */
exports.getAllCNetFotoFeature = () => {
	return `select p.erpProductPropertyProductForeignId as productId
from bde.AllProperties as p with (nolock)
where p.erpProductCNetFoto = 1
group by p.erpProductPropertyProductForeignId`
};

/** Es sollen alle Produkte geholt werden, egal welcher Status. */
exports.getAllCTFotoFeature = () => {
	return `select p.erpProductPropertyProductForeignId as productId
from bde.AllProperties as p with (nolock)
where p.erpProductCTFoto = 1
group by p.erpProductPropertyProductForeignId`
};

/** Sucht alle Artikel heraus, die nur einen Artikel haben.
 * Berücksichtigt ist 3rd, Hersteller */
exports.getAllSingleArticles = () => {
	return `select *
from
(select new.ProductId as productId, new.mpn, new.ManufacturerId as mfg, new.ThirdParty as thirdParty, 'new' as condition, new.ProductForeignId as productForeignId
from (
    select p.id as ProductId, p.Number as mpn, p.ManufacturerId, p.ThirdParty, pp.ProductForeignId
from erp.Product as p with (nolock)
join erp.ProductProperty as pp with (nolock) 
	on p.Id = pp.ProductId
where pp.Condition = 8
    and pp.State = 1056
    and pp.SelOnly = 0 
group by p.id, p.Number, p.ManufacturerId, p.ThirdParty, pp.ProductForeignId
    ) as new

left join (
select p.id as ProductId, p.Number as mpn, p.ManufacturerId, p.ThirdParty, pp.ProductForeignId
from erp.Product as p with (nolock)
join erp.ProductProperty as pp with (nolock) 
	on p.Id = pp.ProductId
where pp.Condition = 9
    and pp.State = 1056
    and pp.SelOnly = 0 
group by p.id, p.Number, p.ManufacturerId, p.ThirdParty, pp.ProductForeignId
) as ref 
  on new.ProductId = ref.ProductId
where ref.ProductId is null

union

select ref.ProductId as productId, ref.mpn, ref.ManufacturerId as mfg, ref.ThirdParty as thirdParty, 'ref' as condition, ref.ProductForeignId as productForeignId
from (
    select p.id as ProductId, p.Number as mpn, p.ManufacturerId, p.ThirdParty, pp.ProductForeignId
from erp.Product as p
join erp.ProductProperty as pp with (nolock) 
	on p.Id = pp.ProductId
where pp.Condition = 8
    and pp.State = 1056
    and pp.SelOnly = 0 
group by p.id, p.Number, p.ManufacturerId, p.ThirdParty, pp.ProductForeignId
    ) as new

right join (
select p.id as ProductId, p.Number as mpn, p.ManufacturerId, p.ThirdParty, pp.ProductForeignId
from erp.Product as p with (nolock)
join erp.ProductProperty as pp with (nolock) 
	on p.Id = pp.ProductId
where pp.Condition = 9
    and pp.State = 1056
    and pp.SelOnly = 0 
group by p.id, p.Number, p.ManufacturerId, p.ThirdParty, pp.ProductForeignId
) as ref 
  on ref.ProductId = new.ProductId
where new.ProductId is null) as data
group by data.productId, data.mpn, data.mfg, data.thirdParty, data.condition, data.productForeignId
order by data.mpn, data.condition`
};

exports.updateSingleArticle = (oldProductId, newProductId) => {
	return `update erp.ProductProperty
set erp.ProductProperty.ProductId = ${newProductId}
where erp.ProductProperty.ProductId = ${oldProductId}`
};

exports.getProductArticleFromLynn = (a_nrs) => {
	return `select pp.ArticleForeignId as a_nr, p.Number as mpn, m.Name as mfg, 
case when p.ThirdParty = 1 then 'ja' else 'nein' end as ThirdParty,
case when pp.Condition = 8 then 'new' when pp.Condition = 9 then 'ref' else 'unbekannt' end as Condition,
p.Description
from erp.Product as p
join erp.Manufacturer as m with (nolock) 
	on m.Id = p.ManufacturerId
join erp.ProductProperty as pp with (nolock) 
	on p.Id = pp.ProductId
where pp.ArticleForeignId in (${a_nrs})`
};

exports.getAllNewAndRefArticlesOfAProduct = () => {
	return `select new.ProductId as productId, new.mpn as mpn, new.ThirdParty as thirdParty, new.manufacturer as manufacturer,
       new.ArticleForeignId as newArticleForeignId,
       ref.ArticleForeignId as refArticleForeignId
from
(select pp.ProductId, pp.ArticleForeignId, p.Number as mpn,
        case when p.ThirdParty = 0 then 'nein' when p.ThirdParty = 1 then 'ja' end as ThirdParty, m.Name as manufacturer
from erp.ProductProperty as pp with (nolock)
join erp.Product as p with (nolock) 
	on p.id = pp.ProductId
join erp.Manufacturer as m with (nolock) 
	on m.id = p.ManufacturerId
where pp.ProductId in
      (
        /** Alle Produkte mit new und ref Artikel */
        select p.id
        from erp.Product as p
        left join
             (select p.Id as productId, pp.ArticleForeignId as ppArticleForeignId, pp2.ArticleForeignId as pp2ArticleForeignId
              from erp.Product as p with (nolock)
                       join erp.ProductProperty as pp with (nolock) 
                       		on p.Id = pp.ProductId
                       left join erp.ProductProperty as pp2 with (nolock) 
                       		on p.id = pp2.ProductId
              where 1 = 1
                and pp.State = 1056
                and pp2.State = 1056
                and pp.SelOnly = 0
                and pp2.SelOnly = 0
                and (
                  pp.Condition = 8 and pp2.Condition = 9
                  )
             ) as data 
               on p.Id = data.productId
        where data.productId is not null

    )
and pp.Condition = 8
    and pp.State = 1056 
    and pp.SelOnly = 0 ) as new

join

(select pp.ProductId, pp.ArticleForeignId, p.Number as mpn,
        case when p.ThirdParty = 0 then 'nein' when p.ThirdParty = 1 then 'ja' end as ThirdParty, m.Name as manufacturer
from erp.ProductProperty as pp with (nolock)
join erp.Product as p with (nolock) 
	on p.id = pp.ProductId
join erp.Manufacturer as m with (nolock) 
	on m.id = p.ManufacturerId
where pp.ProductId in
      (
        /** Alle Produkte mit new und ref Artikel */
        select p.id
        from erp.Product as p
        left join
             (select p.Id as productId, pp.ArticleForeignId as ppArticleForeignId, pp2.ArticleForeignId as pp2ArticleForeignId
              from erp.Product as p with (nolock)
                       join erp.ProductProperty as pp 
                       		on with (nolock) p.Id = pp.ProductId
                       left join erp.ProductProperty as pp2 
                       		on with (nolock) p.id = pp2.ProductId
              where 1 = 1
                and pp.State = 1056
                and pp2.State = 1056
                and pp.SelOnly = 0
                and pp2.SelOnly = 0
                and (
                  pp.Condition = 8 and pp2.Condition = 9
                  )
             ) as data 
               on p.Id = data.productId
        where data.productId is not null

    )
and pp.Condition = 9
    and pp.State = 1056
    and pp.SelOnly = 0 ) as ref 
on ref.ProductId = new.ProductId;`
};

exports.setCTFoto = (p_nrs, value) => {
	return `update erp.Product
set CTFoto = ${value},
	ModifiedDate = GETUTCDATE()
where id in (
    select pp.ProductId
    from erp.ProductProperty as pp
    where pp.ProductForeignId in (${p_nrs}))`
};

exports.setCNetFoto = (p_nrs, value) => {
	return `update erp.Product
set CNetFoto = ${value},
	ModifiedDate = GETUTCDATE()
where id in (
    select pp.ProductId
    from erp.ProductProperty as pp
    where pp.ProductForeignId in (${p_nrs}))`
};

exports.getDoubleLynnProduct = () => {
	return `select p.erpProductnumber as Number -- , p.ManufacturerId, p.ThirdParty, count(8) as Anzahl
from bde.AllProperties as p with (nolock)
where p.erpProductPropertyState = 1056
and p.erpProductPropertySelOnly = 0
and p.coreDomainvalueErpProductPropertyConditionId in (8,9)
group by p.erpProductnumber, p.erpProductManufacturerId, p.erpProductThirdParty
having count(8) > 2
order by p.erpProductnumber`
};


exports.getQueryAllCategoryFromLynn = () => {
	return `select erpProductPropertyProductForeignId as p_nr
, MAX(erpProductPropertyArticleForeignId) as a_nr
, erpProductLynnKategorie as lynnCategoryId
, erpProductShopKategorie as shopCategoryId
, erpProductId as productId
from bde.AllProperties with (nolock)
where 1=1
and erpProductPropertyState = 1056
and erpProductPropertySelOnly = 0
group by erpProductPropertyProductForeignId, erpProductLynnKategorie, erpProductShopKategorie, erpProductId`;
}

/** Gibt alle aktiven Artikelnummern zurück */
exports.getQueryAllActiveArticleNumber = () => {
	return `select erpProductPropertyArticleForeignId
from bde.AllProperties
where erpProductPropertyState = 1056`; // 1056 = Aktiv
}

/** Gibt alle Artikelnummern, EAN und die Produkt-Id zurück */
exports.getQueryAllArticleWithEan = () => {
	return `select erpProductPropertyArticleForeignId as a_nr
, erpProductPropertyEAN as a_ean
, erpProductPropertyId
from bde.AllProperties`;
}

/** Gibt das Query zurück, mit dem die Lynn-Kategorie gesetzt werden kann. */
exports.getQuerySetLynnCategory = (lynnCategoryId, p_nr) => {
	return `UPDATE erp.Product
SET LynnKategorie = ${lynnCategoryId}
WHERE id in (
select pp.ProductId
from erp.ProductProperty as pp
where ProductForeignId = ${p_nr});`
}

/** Gibt das Query zurück, mit dem die Shop-Kategorie gesetzt werden kann. */
exports.getQuerySetShopCategory = (shopCategoryId, p_nr) => {
	return `UPDATE erp.Product
SET ShopKategorie = ${shopCategoryId}
WHERE id in (
select pp.ProductId
from erp.ProductProperty as pp
where ProductForeignId = ${p_nr});`
}