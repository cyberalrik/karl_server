'use strict'

class Index {
    /**
     * Setzt eine passende cNet p_cat_id und den shopCategory Namen
     * aus der übergebenen shopKategorie bei dem übergebenen Produkt
     * */
    async setShopCategoryIdInKarl(p_nr, shopCategoryId) {
    let result = `update products
set p_cat_id = (
select p_cat_id
from cNetCategory2LynnAndShopCategories
where shopCategoryId = ${shopCategoryId}
limit 1),
p_cat_name = (
select shopCategory
from cNetCategory2LynnAndShopCategories
where shopCategoryId = ${shopCategoryId}
limit 1),
updatedAt = now()
where p_nr = ${p_nr};`

        return result
    }
}

module.exports = Index;