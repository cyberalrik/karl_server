'use strict';

/***
 * Die cci DB kann nicht über "Sequelize" angesprochen werden.
 * Deshalb harte SQL-Anweisungen.
 */

class Index {
    /**
     * 0.
     * NUR IM NOTFALL BENUTZEN
     * Erstellt die benötigte Tabelle
     * */
    async createTable() {
        return `
            create table ct_components
                (
                    mpn          varchar(100)  null,
                    identifier   varchar(2000) null,
                    value        varchar(2000) null,
                    language        varchar(2) null,
                    displayOrder int           null
                );`
    }

    /**
     * 1.
     * Leert die Tabelle
     * */
    async truncate() {
        return `TRUNCATE TABLE ct_components;`
    }

    /**
     * 2.
     * Fügt alle cNet Categorien (deutsch) in die Tabelle
     * */
    async insertDe() {
        return `
            INSERT INTO ct_components (mpn, identifier, value, displayOrder, language)
                SELECT p.MfPN, v2.text AS Header, v3.text AS Body, e.DisplayOrder, 'de'
                FROM cds_Especde e
                JOIN cds_Evocde v ON e.sectid = v.id
                JOIN cds_Evocde v2 ON e.hdrid = v2.id
                JOIN cds_Evocde v3 ON e.bodyid = v3.id
                JOIN cds_Prod p ON p.ProdID = e.ProdID;`
    }

    /**
     * 3.
     * Fügt alle cNet Categorien (englisch) in die Tabelle
     * */
    async insertEe() {
        return `
            INSERT INTO ct_components (mpn, identifier, value, displayOrder, language)
                SELECT p.MfPN, v2.text AS Header, v3.text AS Body, e.DisplayOrder, 'ee'
                FROM cds_Especee e
                JOIN cds_Evocee v ON e.sectid = v.id
                JOIN cds_Evocee v2 ON e.hdrid = v2.id
                JOIN cds_Evocee v3 ON e.bodyid = v3.id
                JOIN cds_Prod p ON p.ProdID = e.ProdID;`
    }

    /**
     * 4.
     * Indiziert die Tabelle
     * Muss nur benutzt werden, wenn die Tabelle neu angelegt wurde. Siehe 0.
     * */
    async indexTable() {
        return `
        create index ct_components_mpn_index
            on ct_components (mpn);`
    }
}

module.exports = Index