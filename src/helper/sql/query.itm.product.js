'use strict';

/***
 * Die ITM DB kann nicht über "Sequelize" angesprochen werden.
 * Deshalb harte SQL-Anweisungen.
 */

exports.getProductsFromMediaId = (mediaData) => {
    return `SELECT 
a.name as mpn, s.name as mfg
FROM s_articles AS a
JOIN s_articles_img AS ai ON a.id = ai.articleID
JOIN s_media AS m ON m.id = ai.media_id
JOIN s_articles_supplier AS s ON a.supplierID = s.id
JOIN s_articles_details AS ad ON a.id = ad.articleID
WHERE ai.media_id IN ('${mediaData}')
OR m.name = '${mediaData}'
group BY  a.name , s.name;`
}
