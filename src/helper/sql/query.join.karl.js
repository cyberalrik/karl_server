'use strict'

// const config = require('../../configs/app.config').getConfig();
// const Sequelize = require('sequelize');
// const seq = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

class Index {

    /**
     * Gibt alle ProduktId's zurück, die Bilder von Cybertrading/ Canto besitzen.
     * Da auch fremde Bilder zu Canto hochgeladen wurden und wir immer 4 oder 24 Bilder von einem Produkt haben,
     * wird die 4 als kriterium herangezogen.
     *
     * Canto = Version 1
     *  */
    async getAllProductIdsFromCtImages() {
        let result = `select p2pI.p_nr as productId
from product2productImages as p2pI
join
    (
select p_nr
from images as i
where i.version = 1
and i.is_active = 1
group by p_nr
having count(*) >= 4
    ) as images on images.p_nr = p2pI.p_nr_image
group by p2pI.p_nr`;

//             `select p2pI.p_nr as productId
// from images as i
// join product2productImages p2pI on i.p_nr = p2pI.p_nr_image
// where i.version = 1
// group by p2pI.p_nr
// having count(*) = 24;`;

        return result;
    }


    /**
     * Gibt alle ProduktId's von Produkten zurück, deren Bilder auf Canto hochgeladen wurden aber nicht von uns sind.
     * Hier ist das Kriterium, dass es weniger als 4 Bilder sind.
     *
     * Canto = Version 1
     *  */
    async getAllProductIdsFromCantoForeignImages() {
        let result = `select p2pI.p_nr as productId
from product2productImages as p2pI
join
    (
select p_nr
from images as i
where i.version = 1
and i.is_active = 1
group by p_nr
having count(*) < 4
    ) as images on images.p_nr = p2pI.p_nr_image
group by p2pI.p_nr;`;



//             `select p2pI.p_nr as productId
// from images as i
// join product2productImages p2pI on i.p_nr = p2pI.p_nr_image
// where i.version = 1
// group by p2pI.p_nr
// having count(*) < 24;`;

        return result;
    }

    /**
     * Gibt alle ProduktId's von Produkten zurück, wo wir CNet Bilder haben.
     *
     * CNet = Version 0
     *
     * */
    async getAllProductIdsFromCNetImages() {
        let result = `select p2pI.p_nr as productId
from product2productImages p2pI
where p2pI.p_nr_image in (
    select i.p_nr
from images as i
where i.version = 0
and i.is_active = 1
    )
group by p2pI.p_nr;`

//             `select p2pI.p_nr as productId
// from product2productImages p2pI
// where p2pI.p_nr_image in
// (select i.p_nr
// from images as i
// where i.version = 0
// and i.is_active = 1);`;

        return result;
    }

    /**
     * Gibt alle Produkt zurück die Artikel besitzen deren Herstellungsdatum jünger, gleich ist
     * als das übergebene Datum ist.
     *
     * @Param date as String '2023-04-03'
     *
     * @Return SQL
     * */
    async getAllProductOfTheNewArticle(date) {
        let result = `select p.*
from products as p
    join articles a on p.p_nr = a.p_nr
where a.createdAt > '${date}';`
        return result;
    }

    /**
     * Gibt alle Produkt der übergebenen Artikelliste zurück.
     * Alle geänderten E2-Artikel sollen an TB gesendet werden.
     *
     * @Param articles as String 'E2-137984','E2-950'
     *
     * @Return SQL
     * */
    async getAllProductOfArticleList(articles) {
        let result = `select p.*
from products as p
    join articles a on p.p_nr = a.p_nr
where a.a_nr in (${articles});`
        return result;
    }

    /**
     * Gibt das Query für ein JOIN alle Produkte und Kategorien zurück.
     * */
    async getQueryAllProductCategoryFromKarlForLynn() {
        return `select p.p_nr, c.lynnCategoryId, c.shopCategoryId
from products p
join cNetCategory2LynnAndShopCategories c on p.p_cat_id = c.p_cat_id;`;
    }

    /**
     * löscht alle Artikel-Einträge in der Tabelle articleChannels die nicht mehr in der Tabelle article enthalten sind.
     * */
    async getQueryDeleteAllEntriesTheIsNotInArticle() {
        return `delete ac
from articleChannels as ac
left join articles a on ac.a_nr = a.a_nr
where a.a_nr is null`;
    }
}

module.exports = Index;