'use strict';
const config = require('../configs/tbone.config');

/***
 * Simple TBONE-Namespace-Helper
 * @param exportType
 * @returns {string}
 */
exports.calcExportType = (exportType) => {
	if (exportType === config.EXPORT_FULL_ID){
		return 'full';
	} else if (exportType === config.EXPORT_DELTA_ID){
		return 'delta';
	}
};
