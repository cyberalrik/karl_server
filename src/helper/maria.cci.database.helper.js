'use strict';

const mariaDb = require('mariadb');
const cciConfig = require('../configs/cci.config');
const logger = require('../helper/util/logger.util');
const TAG = 'cci.database.helper';

/***
 * Interface to the internal MariaDB-Cluster
 * @param cmd
 * @returns {Promise<*>}
 */
exports.sql = async (cmd) => {
	const pool = mariaDb.createPool({
		host: cciConfig.DB_HOST,
		user: cciConfig.USER,
		password: cciConfig.PW,
		database: cciConfig.DB,
		connectionLimit: 5
	});
	let conn;
	try {
		conn = await pool.getConnection();
		return await conn.query(cmd);
	} catch (err) {
		logger.err(TAG, 'Database Error', 'unable to request');
		return [];
	} finally {
		if (conn)
			conn.end();
		await pool.end();
	}
};