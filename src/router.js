'use strict';

const log = require('./helper/util/logger.util');
const TAG = 'router';

module.exports = async function(app) {

	await require('./helper/installer/sequelize.installer')();

	/** Cron */
	const CronJobs = require('./helper/installer/cronInitialize');
	let cronJobs = new CronJobs();
	await cronJobs.initializeCron();

	if (await require('./helper/installer/channel.installer')()) {
		log.log(TAG, 'channel.installer', 'Alle Kanäle wurden installiert.');
	}

	require('./api/api.router')(app);
};