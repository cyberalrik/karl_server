'use strict';

/** Es gibt unterschiedliche Bilder, um sie in der DB zu unterscheiden: cNet = 0, Canto = 1 */
exports.IMAGES_UNKNOWN_TYPE_VALUE = -1;
exports.IMAGES_CNET_TYPE_VALUE = 0;
exports.IMAGES_CANTO_TYPE_VALUE = 1;
/** Von Canto gibt es 24 Bilder, nicht alle sollen hoch geladen werden, nur vorn, hinten, rechts und links */
exports.IMAGES_CANTO_ACTIVE_POS = [0,6,12,18];
/**
 * In welchem Kanal soll das Bild zugeordnet werden, die Daten werden Binär bewertet.
 * In der DB gibt es nur das Feld channel, hier wird Binär festgehalten für welchen Kanal das Bild genutzt werden soll.
 * 0001 = amazon
 * 0010 = it-market
 * 0100 = eBay.de
 * 1000 = ebay.uk
 *
 * 1010 = bedeutet dann für it-market und eBay.uk (Dezimal = 10)
 * */
exports.CHANNEL_DEFAULT_TYPE_VALUE = 0;
exports.CHANNEL_AMAZONDE_TYPE_VALUE = 1;
exports.CHANNEL_ITMARKET_TYPE_VALUE = 2;
exports.CHANNEL_EBAYDE_TYPE_VALUE = 4;
exports.CHANNEL_EBAYUK_TYPE_VALUE = 8;

/**
 * Gibt den Channel Type Value zurück.
 *
 * @param channelName
 * @return Promise {<int>}
 * */
exports.getChannelTypeValue = async (channelName) => {
    let result = 0;
    switch(channelName) {
        case 'amde' :
            result = this.CHANNEL_AMAZONDE_TYPE_VALUE;
            break
        case 'cucybertr1' :
            result = this.CHANNEL_ITMARKET_TYPE_VALUE;
            break
        case 'ebde' :
            result = this.CHANNEL_EBAYDE_TYPE_VALUE;
            break
        case 'ebuk' :
            result = this.CHANNEL_EBAYUK_TYPE_VALUE;
            break
        case 'default' :
            result = this.CHANNEL_DEFAULT_TYPE_VALUE;
            break
    }

    return result;
}

/**
 * Gibt den Standardbilderlink zurück.
 *
 * @return Promise {<string>}
 * @param mediaType
 * */
exports.getDefaultMediaTypeLink = async (mediaType) => {
    let result = 0;
    mediaType = mediaType.toUpperCase()
    switch(mediaType) {
        case 'IMAGE' :
        case 'IMG_EBAY' :
            result = 'https://cybertrading.de/eBay-4.jpg';
            break
        case 'IMG_ITM' :
            result = 'https://cybertrading.de/no-picture.jpg';
            break
    }

    return result;
}

/**
 * Gibt zurück, ob hier Bilder zurückgegeben werden dürfen.
 * Es dürfen keine Bilder bei ThirdParty Produkte an eBay zurückgegeben werden.
 * Ist ThirdParty jedoch FALSE, können immer Bilder zurückgegeben werden. Also TRUE;
 *
 * @param channelName
 * @param thirdParty
 * @return Promise {<int>}
 * */
exports.isNotForbidden = async (channelName, thirdParty) => {
    if (!thirdParty) {
        return true;
    }
    let result = false;
    switch(channelName) {
        case 'amde' :
            result = true;
            break
        case 'cucybertr1' :
            result = true;
            break
        case 'ebde' :
            result = false;
            break
        case 'ebuk' :
            result = false;
            break
        case 'default' :
            result = true;
            break
    }

    return result;
}

