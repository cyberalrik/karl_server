'use strict';

const ManufactureFac = require('../../src/refac/entity/manufacture')

describe('Manufacture Entity', () => {
    it("simple Manufacture", function() {
        let cnetid = 'Z12345';
        let name = 'Bauer';
        let id = 1;
        let fac = new ManufactureFac(cnetid, name, id);
        let manufacture = fac.getManufacture();
        expect(cnetid).toEqual(manufacture.cnetID);
        expect(name).toEqual(manufacture.name);
        expect(manufacture.id).toBeGreaterThan(0);
    });
})
