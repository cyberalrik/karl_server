'use strict';

const DataSet = require('../../src/refac/data-access/in-memory/manufactureData')

describe('Manufacture Data-Access', () => {
    it("simple Manufacture DataAccess", function() {
        let db = new DataSet();
        db.addManufacturer('11', 'test');
        db.addManufacturer('12', 'test2');
        expect(2).toEqual(db.getAllManufacturers().length);
    });
    it("if invalid", function() {
        let db = new DataSet();
        db.addManufacturer(undefined, undefined);
        db.addManufacturer(null, null);
        expect(0).toEqual(db.getAllManufacturers().length);
    });
    it("no double", function() {
        let db = new DataSet();
        db.addManufacturer('11', 'test');
        db.addManufacturer('11', 'test');
        expect(1).toEqual(db.getAllManufacturers().length);
    });
})
