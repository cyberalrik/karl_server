'use strict';

const TimeHelper = require('../../src/refac/helper/time');

describe('simple time conversion test', () => {
    it("ms -> s", function() {
        let timeHelper = new TimeHelper();

        expect(timeHelper.timeConversionFromMilliSec(100.00245263)).toBe('0.1 s')
        expect(timeHelper.timeConversionFromMilliSec(0.1)).toBe('0.0 s')
        expect(timeHelper.timeConversionFromMilliSec(59001)).toBe('59.0 s')
    });
    it("ms -> min", function() {
        let timeHelper = new TimeHelper();

        expect(timeHelper.timeConversionFromMilliSec(60001)).toBe('1.0 m')
        expect(timeHelper.timeConversionFromMilliSec(2700000)).toBe('45.0 m')
    });
    it("ms -> h", function() {
        let timeHelper = new TimeHelper();

        expect(timeHelper.timeConversionFromMilliSec(3600000)).toBe('1.0 h')
        expect(timeHelper.timeConversionFromMilliSec(72000000)).toBe('20.0 h')
    });
})
